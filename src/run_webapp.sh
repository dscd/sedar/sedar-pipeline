#!/bin/bash
# cd cfg

echo "[LOADING] ENV variables"
WEBAPP_PORT=`python -c "from config.env import webapp_environ; print(webapp_environ().WEBAPP_PORT);"`
WEBAPP_DEBUG_MODE=`python -c "from config.env import webapp_environ; print(webapp_environ().WEBAPP_DEBUG_MODE);"`

echo "[RUNNING] migrations"
python manage.py makemigrations
python manage.py migrate --no-input

echo "[COPYING] Collecting Static Files"
mkdir -p webapp/static_root && chmod 777 webapp/static_root
python manage.py collectstatic --noinput

echo "[RUNNING] app on port $WEBAPP_PORT"

if [ $WEBAPP_DEBUG_MODE == "True" ]
then
  python manage.py runserver $WEBAPP_PORT
  #gunicorn webapp.webapp.wsgi -b 0.0.0.0:$WEBAPP_PORT --log-level debug
else
  #python manage.py runserver $WEBAPP_PORT
  gunicorn webapp.webapp.wsgi -b 0.0.0.0:$WEBAPP_PORT --workers 4 --threads 8 --log-level debug  # --access-logfile access.txt --error-logfile error.txt --log-level debug
fi
