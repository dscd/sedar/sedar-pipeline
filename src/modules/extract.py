from typing import List

import pandas as pd

import modules.page_extraction as pe
from config.env import sedar
from config.models import DetectedPage, ExtractedTable


class TableExtractor:
    def __init__(self, detected_pages: DetectedPage, pdf_path: str):
        self.table_data = detected_pages
        self.pdf_path = pdf_path

    def extract_statements(self) -> List:
        sheets = sedar().page_types
        sheet_models = []

        for sheet_type in sheets:
            if sheet_type in self.table_data.dict().keys():
                page_no = self.table_data.dict()[sheet_type]
                statement_model = self.extract_sheet(
                    self.table_data, page_no, sheet_type
                )
                sheet_models.append(statement_model)

        return sheet_models

    def extract_sheet(
        self, row_data: DetectedPage, page_no: int, page_type: str
    ) -> ExtractedTable:
        sheet_dict = pe.extract_table(
            row_data.FILE_ID, page_no, page_type, self.pdf_path, draw_img=False
        )
        sheet_model = ExtractedTable()
        sheet_model.ORIGINAL_TABLE = sheet_dict["EXTRACTED_VARIABLES"]
        sheet_model.UNITS = sheet_dict["UNITS"]
        sheet_model.CURRENCY = sheet_dict["CURRENCY"]
        sheet_model.PAGE_NO = page_no
        sheet_model.PAGE_TYPE = page_type
        sheet_model.EDITED_TABLE_1 = None
        sheet_model.EDITED_TABLE_2 = None
        sheet_model.CREATED_ON = pd.to_datetime("today").strftime("%Y-%m-%d %H:%M:%S")
        return sheet_model
