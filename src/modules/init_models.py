"""
SEDAR Ingress pipeline

Created on: 
@authors:  Sayema Mashhadi, Andres Solis Montero
"""


import os
from datetime import datetime
from os.path import basename, dirname, join

import pytz

from config.api import S3, S3Location
from modules.s3client import S3Client


def check_models():

    client = S3Client.get_client()
    local_fldr = S3.CACHE.prefix
    if not os.path.exists(local_fldr):
        os.makedirs(local_fldr)
        print("\tDownloaded:")
        for obj in client.ls_objs(
            bucket=S3.MODELS.bucket,
            prefix=S3.MODELS.prefix,
            recursive=True,
            default_folder_file=False,
        ):
            localname = join(local_fldr, obj.object_name)
            tmp_loc = S3Location(
                S3.MODELS.bucket, dirname(obj.object_name), basename(obj.object_name)
            )
            client.fget_loc(tmp_loc, localname)
            print(f"\t\t{localname}")
    else:
        utc = pytz.UTC
        for obj in client.ls_objs(
            bucket=S3.MODELS.bucket,
            prefix=S3.MODELS.prefix,
            recursive=True,
            default_folder_file=False,
        ):
            localname = join(local_fldr, obj.object_name)
            tmp_loc = S3Location(
                S3.MODELS.bucket, dirname(obj.object_name), basename(obj.object_name)
            )
            if os.path.isfile(localname):
                minio_mtime = client.stat_loc(tmp_loc).last_modified
                local_mtime = datetime.fromtimestamp(os.path.getmtime(localname))
                local_mtime = local_mtime.replace(tzinfo=utc)
                if minio_mtime > local_mtime:
                    client.fget_loc(tmp_loc, localname)
                else:
                    print(
                        "Model {} in local is up-to-date".format(
                            basename(obj.object_name)
                        )
                    )
            else:
                client.fget_loc(tmp_loc, localname)
