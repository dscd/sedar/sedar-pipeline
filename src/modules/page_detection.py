"""
Developer: Chatana Mandava
"""

import re
from functools import lru_cache

import joblib
import nltk
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from scipy.signal import find_peaks

from config.api import S3

nltk.data.path.append(S3.get_cached_location(S3.NLTK_DATA))

# required_words = {"income", "expense"}
# share_required_words = {"revenue", "sale", "loss"}
# not_required_words = {""}


@lru_cache
def load_models():

    bow_vect = joblib.load(S3.get_cached_location(S3.IS_BS_BOW))
    seg_bow_vect = joblib.load(S3.get_cached_location(S3.SEG_BOW))
    notes_bow_vect = joblib.load(S3.get_cached_location(S3.NOTES_BOW))
    classify_rf = joblib.load(S3.get_cached_location(S3.IS_BS_CLASSIFIER))
    final_model_rf = joblib.load(S3.get_cached_location(S3.RF_CLASSIFIER))
    seg_classify_rf = joblib.load(S3.get_cached_location(S3.SEG_CLASSIFIER))
    notes_classify_rf = joblib.load(S3.get_cached_location(S3.NOTES_CLASSIFIER))
    return (
        bow_vect,
        seg_bow_vect,
        notes_bow_vect,
        classify_rf,
        seg_classify_rf,
        notes_classify_rf,
        final_model_rf,
    )


@lru_cache
def stop_words():
    return set(stopwords.words("english"))


@lru_cache
def lematizer():
    wnl = WordNetLemmatizer()
    return lambda y: wnl.lemmatize(y)


def custom_tokenize(text):
    result = word_tokenize(text)
    result = list(set(map(lematizer(), result)) - stop_words())
    return result


def clean_text(text):
    text = text.lower()
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "can not ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r"\'scuse", " excuse ", text)
    text = re.sub("\W", " ", text)
    text = re.sub("\s+", " ", text)
    text = text.strip(" ")
    return text


def word_check(tokens, check_type):
    if check_type == "word_check_income":
        required_words = {"income", "expense"}
        share_required_words = {"revenue", "sale", "loss"}
        not_required_words = {""}
        if (
            (len(set(tokens) & required_words) >= 1)
            & (len(set(tokens) & not_required_words) == 0)
            & (len(set(tokens) & share_required_words) > 0)
        ):
            # if (len(set(tokens) & share_required_words) >= 1):
            return 1
        else:
            return 0
    elif check_type == "word_check_balance":
        required_words = {"asset", "liability"}
        share_required_words = {"equity", "deficiency", "deficit", "share"}
        not_required_words = {""}
        if (
            (len(set(tokens) & required_words) == 2)
            & (len(set(tokens) & not_required_words) == 0)
            & (len(set(tokens) & share_required_words) > 0)
        ):
            return 1
        else:
            return 0

    elif check_type == "word_check_flow":
        required_words = {
            "operating",
            "investment",
            "financing",
            "asset",
            "activity",
            "cash",
        }
        share_required_words = {""}
        not_required_words = {""}
        if (
            (len(set(tokens) & required_words) >= 1)
            & (len(set(tokens) & not_required_words) == 0)
            # & (len(set(tokens) & share_required_words) > 0)
        ):
            return 1
        else:
            return 0
    elif check_type == "word_check_other":
        required_words = {"asset", "liability"}
        if len(set(tokens) & required_words) == 1:
            return 1
        else:
            return 0


def neg(i, min_i=7, max_i=2):
    ranges_list = np.array(range(i - min_i, i + max_i))
    #     print(ranges_list)
    if ranges_list.min() < 0:
        ranges_list = ranges_list + abs(ranges_list.min())
    return ranges_list


def post_process_page_df(df):
    df["tokens"] = df.text.apply(lambda x: custom_tokenize(" ".join(x)))
    df["word_check_balance"] = df.tokens.apply(
        lambda x: word_check(x, "word_check_balance")
    )
    df["word_check_income"] = df.tokens.apply(
        lambda x: word_check(x, "word_check_income")
    )
    df["word_check_flow"] = df.tokens.apply(lambda x: word_check(x, "word_check_flow"))
    df["word_check_other"] = df.tokens.apply(
        lambda x: word_check(x, "word_check_flow")
    )  # modified
    df["rolling_mean"] = df.selected_total_ratio.rolling(window=8, center=True).mean()
    df["rolling_word_check_income"] = df.word_check_income.rolling(
        window=8, center=True
    ).max()
    df["rolling_word_check_balance"] = df.word_check_balance.rolling(
        window=8, center=True
    ).max()
    df["rolling_word_check_flow"] = df.word_check_flow.rolling(  # MODIFIED
        window=8, center=True
    ).max()
    df["rolling_mean_notes"] = df.selected_total_ratio.diff()
    df["text"] = df["tokens"].apply(lambda x: " ".join(x))
    return df


def detect_df(table):

    (
        bow_vect,
        seg_bow_vect,
        notes_bow_vect,
        classify_rf,
        seg_classify_rf,
        notes_classify_rf,
        final_model_rf,
    ) = load_models()

    try:
        pg_df = table
        if pg_df.shape[0] > 1:
            mid_page_df = df = post_process_page_df(pg_df).copy(deep=True)
            wanted_index = np.unique(
                np.array(
                    [
                        neg(i, 2, 2)
                        for i in mid_page_df[
                            (
                                (mid_page_df.word_check_income == 1)
                                | (mid_page_df.word_check_balance == 1)
                                | (mid_page_df.word_check_flow == 1)  # MODIFIED
                            )
                        ].index
                    ]
                ).flatten()
            )
            mid_page_df.loc[
                ~mid_page_df.index.isin(wanted_index), "selected_total_ratio"
            ] = 0

            mid_page = df[
                (df.rolling_word_check_income == 1.0)
                & (df.rolling_word_check_balance == 1.0)
                & (df.rolling_word_check_flow == 1.0)  # MODIFIED
            ].sort_values("rolling_mean", ascending=False)
            values = np.nan_to_num(
                mid_page_df.selected_total_ratio.rolling(window=5, center=True)
                .mean()
                .values,
                nan=0,
            )
            peaks, prominences = find_peaks(
                values, prominence=np.median(values[np.where(values > 0)])
            )
            peaks = peaks[values[peaks] > 0.25]

            if len(peaks) > 0:
                big_peak_max = peaks[values[peaks].argmax()]
                big_peak_first = peaks[0]
                ranges_list = np.unique(
                    np.array([neg(i) for i in [big_peak_max, big_peak_first]]).flatten()
                )

            elif mid_page_df.PAGE_NO.max() == np.nan:
                ranges_list = [0, mid_page_df.PAGE_NO.max()]
            else:
                ranges_list = [0, 0]
            notes_df = (
                df[["PAGE_NO", "text", "selected_total_ratio", "rolling_mean_notes"]]
                .dropna()
                .reset_index()
                .drop("index", axis=1)
            )
            seg_df = (
                df[df.apply(lambda x: "geographic" in x["tokens"], axis=1)]
                .dropna()
                .reset_index()
                .drop("index", axis=1)
            )

            df = df[df.PAGE_NO.isin(ranges_list)].reset_index(drop=True)
            df["text"] = df["text"].map(lambda com: clean_text(com))

            x = bow_vect.transform(df["text"].values)
            notes_x = notes_bow_vect.transform(notes_df["text"].values)

            new_df = pd.DataFrame(x.toarray(), columns=bow_vect.get_feature_names())
            X = pd.concat(
                [
                    new_df,
                    df[
                        [
                            "selected_total_ratio",
                            "word_check_income",
                            "word_check_balance",
                            "word_check_flow",  # MODIFIED
                            "word_check_other",
                        ]
                    ],
                ],
                axis=1,
            )
            if len(X) != 0:
                predicted = final_model_rf.predict(df["text"])
                df["predicted"] = predicted
                df[
                    [
                        "balance_sheet",
                        "cash_flow_statement",
                        "income_statment",
                        "other",
                    ]  # MODIFIED
                ] = final_model_rf.predict_proba(df["text"])
            else:
                df["balance_sheet"] = 1
                df["income_statment"] = 1
                df["cash_flow_statement"] = 1  # MODIFIED
                df["other"] = 1
            if len(seg_df) > 0:
                seg_x = seg_bow_vect.transform(seg_df["text"].values)
                X = pd.DataFrame(
                    seg_x.toarray(), columns=seg_bow_vect.get_feature_names()
                )
                predicted = seg_classify_rf.predict(X)
                seg_df["predicted"] = predicted
                seg_df[["other", "seg_page"]] = seg_classify_rf.predict_proba(X)
                seg_page = seg_df.loc[seg_df["seg_page"].idxmax()].PAGE_NO
            else:
                seg_page = 1

            X = pd.DataFrame(
                notes_x.toarray(), columns=notes_bow_vect.get_feature_names()
            )
            X = pd.concat(
                [X, notes_df[["selected_total_ratio", "rolling_mean_notes"]]], axis=1
            )
            predicted = notes_classify_rf.predict(X)
            notes_df["predicted"] = predicted
            notes_df[["other", "notes_page"]] = notes_classify_rf.predict_proba(X)

            detection_op = {
                "BALANCE_SHEET": str(
                    df.loc[df["balance_sheet"].idxmax(), "PAGE_NO"] + 1
                ),
                "INCOME_STATEMENT": str(
                    df.loc[df["income_statment"].idxmax(), "PAGE_NO"] + 1
                ),
                "CASHFLOW_STATEMENT": str(
                    df.loc[df["cash_flow_statement"].idxmax(), "PAGE_NO"] + 1
                ),  # MODIFIED
                "SEGMENTATION_PAGE": str(seg_page + 1),
                "NOTES_PAGE": str(
                    notes_df.loc[notes_df["notes_page"].idxmax(), "PAGE_NO"] + 1
                ),
                "OTHER": "1",
            }
            return detection_op  # , mid_page_df # , plt
        else:
            return {
                "BALANCE_SHEET": "0",
                "INCOME_STATEMENT": "0",
                "SEGMENTATION_PAGE": "0",
                "CASHFLOW_STATEMENT": "0",  # modified
                "NOTES_PAGE": "0",
                "OTHER": "0",
            }
    except ValueError as err:
        print(err)


def detect_page(page_detection_df):
    try:
        if len(page_detection_df) > 0:
            page_detection_df = page_detection_df.groupby(
                [
                    "CTL_INFO",
                    "ACCESSION_NO",
                    "FILE_ID",
                    "ISSUERNO",
                    "PAGE_COUNT",
                    "PDF_PATH",
                    "SEDAR_FILE_PATH",
                    "RECORD_DATE",
                    "TYPE",
                    "DOCUMENT_DESC",
                ]
            ).apply(lambda x: detect_df(x))
            page_detection_df = page_detection_df.reset_index()
            page_detection_df = page_detection_df[page_detection_df[0] != ""]
            page_detection_df = pd.concat(
                [page_detection_df, page_detection_df[0].apply(pd.Series)], axis=1
            ).drop([0], axis=1)
            page_detection_df["RECORD_STATUS"] = 0
            page_detection_df["CREATED_ON"] = pd.to_datetime("today").strftime(
                "%Y-%m-%d %H:%M:%S"
            )
            return page_detection_df
        else:
            return pd.DataFrame()
    except:
        print("valueerror")


def parse_text(lines):
    """Function to recursively parse the layout tree."""
    lines = list(
        filter(
            None,
            map(
                str.strip,
                list(map(lambda x: re.sub(r"\s+", " ", x), lines.lower().splitlines())),
            ),
        )
    )
    r1 = re.compile("[0-9\]\)\-]$")
    selected_line = len(list(filter(r1.search, lines)))
    if len(lines) > 0:
        selected_total_ratio = selected_line / len(lines)
    else:
        selected_total_ratio = 0
    return [lines, selected_total_ratio]
