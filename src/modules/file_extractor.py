import codecs
import warnings
from datetime import datetime
from enum import auto
from functools import lru_cache
from pathlib import Path

import numpy as np
import pandas as pd
import xmltodict
from pydantic import Field
from typing import Optional
import modules.page_detection as spd
from config import models
from config.api import S3, AutoEnum
from modules.extract import TableExtractor

warnings.filterwarnings("ignore")


class XML(AutoEnum):
    SUBMISSION = auto()
    THIRD_PARTY_FILER_INFO = auto()
    THIRD_PARTY_FILER_INFO_NAME_E = auto()
    NAME_E = auto()
    PREVIOUS_PROFILE_NAME_E = auto()
    PREVIOUS_PROFILE = auto()
    LASTUPDATE = auto()
    TIMEUPDATE = auto()
    JURISDICTION = auto()
    REGION = auto()
    DOCUMENT_INFO = auto()
    PAGE_COUNT = auto()
    DOCUMENT_DESC = auto()
    PAGE_NO = auto()
    CTL_INFO = auto()
    ACCESSION_NO = auto()
    FILE_ID = auto()
    ISSUERNO = auto()
    CREATED_ON = auto()
    RECORD_DATE = auto()
    RECORD_STATUS = auto()
    FINANCIAL_END_DATE = auto()
    FPEDATE = auto()
    TYPE = auto()
    PAGE_TEXT = auto()
    TEXT = auto()


class Index(AutoEnum):
    FILE_INFO = auto()
    OTHER_ISSUER = auto()
    OTHER_FILER = auto()
    MUTUAL_FUND_ISSUER = auto()
    MUTUAL_FUND_GROUP = auto()
    COMPANY_DATA = auto()


class Excel(AutoEnum):
    Variable_Type = auto()
    mapping = auto()
    CODE = auto()
    Description = auto()
    Representation = auto()
    representation = auto()
    decimal_binary = auto()
    binary = auto()
    region_lookup = auto()
    lookup = auto()
    date = auto()
    integer = auto()


@lru_cache
def get_sedar_lookup_table():
    lookup_df = pd.read_excel(
        S3.get_cached_location(S3.LOOKUP_TABLES), sheet_name="lookup_table"
    )
    lookup_dict = {}
    for a, b in lookup_df.groupby(Excel.Variable_Type.value):
        lookup_dict[a] = {}
        lookup_dict[a][Excel.mapping.value] = (
            b[[Excel.CODE.value, Excel.Description.value]]
            .set_index(Excel.CODE.value)
            .to_dict()[Excel.Description.value]
        )
        lookup_dict[a][Excel.representation.value] = b[
            Excel.Representation.value
        ].unique()[0]
    return lookup_dict


def validate(date_str:Optional[str] , date_format: str):
    try:
        return datetime.strptime(date_str, date_format)
    except:
        pass
    return None

def process_lookup(a_dict):
    # Code replaces keys with '|' in it
    processed_dict = {}
    lookup_dict = get_sedar_lookup_table()

    processed_dict = { k.replace("|", "_"): v for k, v in  a_dict.items() }

    for name, _ in processed_dict.items():
        if name in lookup_dict.keys():
            if (
                lookup_dict[name][Excel.representation.value]
                == Excel.decimal_binary.value
            ):
                processed_dict[name] = [
                    lookup_dict[name][Excel.mapping.value][str(int(v) + 1)]
                    for v in np.where(np.array(list(processed_dict[name])) == "1")[0]
                ]
            elif lookup_dict[name][Excel.representation.value] == Excel.binary.value:
                processed_dict[name] = [
                    lookup_dict[name][Excel.mapping.value][str(int(v) + 1)]
                    for v in np.where(np.array(list(processed_dict[name])) == "1")[0]
                ]
            elif lookup_dict[name][Excel.representation.value] == Excel.lookup.value:
                processed_dict[name] = lookup_dict[name][Excel.mapping.value].get(
                                                processed_dict[name],
                                                processed_dict[name])
            elif (
                lookup_dict[name][Excel.representation.value]
                == Excel.region_lookup.value
            ):
                processed_dict[name] = [
                    lookup_dict[name][Excel.mapping.value].get(v, v)
                    for v in processed_dict[name].split(",")
                ]
            elif lookup_dict[name][Excel.representation.value] == Excel.date.value:

                default_format =  "%Y%m%d"
                processed      = None
                #check if there is a format for this Name
                if processed_dict.get(name) and lookup_dict[name].get(Excel.mapping.value):
                    date_format = lookup_dict[name][Excel.mapping.value].get(Excel.date.value, default_format)
                    processed = validate(processed_dict.get(name), date_format)
                #if the value doesn't have a correct format try the default format 
                processed =  processed if processed else validate(processed_dict.get(name), default_format)
                #if it's not possible to parse, then choose the default value for a date
                processed_dict[name] =  processed if processed else pd.to_datetime("1900-01-01") 

            elif lookup_dict[name][Excel.representation.value] == Excel.integer.value:
                processed_dict[name] = 0 if processed_dict[name] is None else int(processed_dict[name])

    return processed_dict


class FileInfoExtractor(models.FileID):
    pdf_path: str
    xml_path: str
    FILE_INFO: models.FileInfo = Field(default=models.FileInfo())

    def sync(self):
        self.FILE_INFO.__sync__(self, self.FILE_INFO)
        self.FILE_INFO.sync()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sync()

    def compute_metadata(self) -> None:
        """
        Populates
              OtherIssuer, or
              MutualFundGroup,
              MutualFundIssuer,
              OtherFile
        """
        output = codecs.open(
            self.xml_path.encode("ISO-8859-1", "surrogateescape"), "r", "ISO-8859-1"
        )
        xml_object = xmltodict.parse(output.read())[XML.SUBMISSION.value]

        if XML.THIRD_PARTY_FILER_INFO.value in xml_object.keys():
            xml_object[XML.THIRD_PARTY_FILER_INFO_NAME_E.value] = xml_object[
                XML.THIRD_PARTY_FILER_INFO.value
            ][XML.NAME_E.value]
            xml_object.pop(XML.THIRD_PARTY_FILER_INFO.value, None)

        #         if XML.PREVIOUS_PROFILE_NAME_E.value in xml_object.keys():
        #             xml_object[XML.PREVIOUS_PROFILE.value] = xml_object[XML.PREVIOUS_PROFILE_NAME_E.value]
        #             xml_object.pop(XML.PREVIOUS_PROFILE_NAME_E.value, None)

        xml_object_type = list(xml_object[Index.COMPANY_DATA.value].keys())[0]

        if (
            XML.LASTUPDATE.value
            in xml_object[Index.COMPANY_DATA.value][xml_object_type].keys()
        ):
            xml_object[Index.COMPANY_DATA.value][xml_object_type][
                XML.LASTUPDATE.value
            ] = (
                xml_object[Index.COMPANY_DATA.value][xml_object_type][
                    XML.LASTUPDATE.value
                ]
                + " "
                + xml_object[Index.COMPANY_DATA.value][xml_object_type][
                    XML.TIMEUPDATE.value
                ]
            )
            xml_object[Index.COMPANY_DATA.value][xml_object_type].pop(
                XML.TIMEUPDATE.value, None
            )

        if isinstance(xml_object[XML.JURISDICTION.value][XML.REGION.value], list):
            xml_object[XML.JURISDICTION.value][XML.REGION.value] = ",".join(
                xml_object[XML.JURISDICTION.value][XML.REGION.value]
            )

        xml_object[Index.COMPANY_DATA.value][xml_object_type][
            XML.REGION.value
        ] = xml_object[XML.JURISDICTION.value][XML.REGION.value]
        # xml_object[XML.CREATED_ON.value] = xml_object[Index.COMPANY_DATA.value][xml_object_type][XML.CREATED_ON.value] = pd.to_datetime("today")
        xml_object[XML.CREATED_ON.value] = xml_object[Index.COMPANY_DATA.value][
            xml_object_type
        ][XML.CREATED_ON.value] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        xml_object[XML.RECORD_STATUS.value] = xml_object[Index.COMPANY_DATA.value][
            xml_object_type
        ][XML.RECORD_STATUS.value] = 0
        if XML.FPEDATE.value in xml_object[Index.COMPANY_DATA.value][xml_object_type]:
            xml_object[XML.FINANCIAL_END_DATE.value] = xml_object[
                Index.COMPANY_DATA.value
            ][xml_object_type][XML.FPEDATE.value]

        def flatten_dict(dd, separator="_", prefix=""):
            return (
                {
                    prefix + separator + k if prefix else k: v
                    for kk, vv in dd.items()
                    for k, v in flatten_dict(vv, separator, kk).items()
                }
                if isinstance(dd, dict)
                else {prefix: dd}
            )

        if xml_object_type == Index.OTHER_ISSUER.value:
            new_dict = flatten_dict(
                xml_object[Index.COMPANY_DATA.value][xml_object_type]
            )
            # Map all codes to equivalent string using lookup tables
            self.ISSUERNO = Path(self.pdf_path).name[18:26]
            new_dict = process_lookup(new_dict)
            OTHER_ISSUER = models.OtherIssuer(**new_dict)
            # Set CompanyNames attributes
            COMPANY_NAMES = models.CompanyNames(**new_dict)
            COMPANY_NAMES.ID = self.ISSUERNO
            COMPANY_NAMES.TYPE = Index.OTHER_ISSUER.value

        elif xml_object_type == Index.MUTUAL_FUND_GROUP.value:
            mfg_dict = xml_object[Index.COMPANY_DATA.value][xml_object_type]
            mfi_dict = mfg_dict.pop(Index.MUTUAL_FUND_ISSUER.value, None)

            # Only GROUPNO is used from filename for Mutual Fund Group
            # ISSUERNO and GROUPNO are saved in each individual Mutual Fund Issuer.
            self.GROUPNO = Path(self.pdf_path).name[18:26]
            MUTUAL_FUND_ISSUERS = []
            if isinstance(mfi_dict, list):
                for x_dict in mfi_dict:
                    new_dict = flatten_dict(x_dict)
                    new_dict = process_lookup(new_dict)
                    MUTUAL_FUND_ISSUERS.append(models.MutualFundIssuer(**new_dict))
            else:
                new_dict = flatten_dict(mfi_dict)
                new_dict = process_lookup(new_dict)
                MUTUAL_FUND_ISSUERS.append(models.MutualFundIssuer(**new_dict))

            mfg_dict = process_lookup(mfg_dict)
            MUTUAL_FUND_GROUP = models.MutualFundGroup(**mfg_dict)
            # Set CompanyNames attributes
            COMPANY_NAMES = models.CompanyNames(**mfg_dict)
            COMPANY_NAMES.ID = self.GROUPNO
            COMPANY_NAMES.TYPE = "MUTUAL_FUND_GROUP"

        elif xml_object_type == Index.OTHER_FILER.value:
            self.ISSUERNO = Path(self.pdf_path).name[18:26]
            new_dict = flatten_dict(
                xml_object[Index.COMPANY_DATA.value][xml_object_type]
            )
            # Map all codes to equivalent string using lookup tables
            new_dict = process_lookup(new_dict)
            OTHER_FILER = models.OtherFiler(**new_dict)
            # Set CompanyNames attributes
            COMPANY_NAMES = models.CompanyNames(**new_dict)
            COMPANY_NAMES.ID = self.ISSUERNO
            COMPANY_NAMES.TYPE = "OTHER_FILER"

        xml_object.pop(XML.JURISDICTION.value, None)
        xml_object.pop(Index.COMPANY_DATA.value, None)

        # Get page count by splitting DOCUMENT_INFO on <PAGE> tag
        if xml_object[XML.DOCUMENT_INFO.value]["TEXT"] is not None:
            filetext = xml_object[XML.DOCUMENT_INFO.value]["TEXT"].split("<PAGE>")
            xml_object[XML.PAGE_COUNT.value] = len(filetext)
        else:
            xml_object[XML.PAGE_COUNT.value] = -1

        ## Generate RECORD DATE for the cleanup process
        xml_object[XML.RECORD_DATE.value] = "".join(
            [self.FOLDER_YEAR, self.FOLDER_MONTH, self.FOLDER_DAY]
        )

        # Map all codes to equivalent string using lookup tables
        xml_object = process_lookup(xml_object)

        # Create FileInfo instance
        self.FILE_INFO = models.FileInfo(**xml_object)

        # Use Company Detail Models setters
        if xml_object_type == Index.OTHER_ISSUER.value:
            self.FILE_INFO.OTHER_ISSUER = OTHER_ISSUER

        elif xml_object_type == Index.MUTUAL_FUND_GROUP.value:
            self.FILE_INFO.MUTUAL_FUND_GROUP = MUTUAL_FUND_GROUP
            self.FILE_INFO.MUTUAL_FUND_ISSUER = MUTUAL_FUND_ISSUERS

        elif xml_object_type == Index.OTHER_FILER.value:
            self.FILE_INFO.OTHER_FILER = OTHER_FILER

        self.FILE_INFO.COMPANY_NAMES = COMPANY_NAMES

        self.sync()
        return None

    def compute_text(self) -> None:
        """
        Populates
              FileText
        """
        output = codecs.open(
            self.xml_path.encode("ISO-8859-1", "surrogateescape"), "r", "ISO-8859-1"
        )
        xml_object = xmltodict.parse(output.read())[XML.SUBMISSION.value]
        xml_object[XML.DOCUMENT_INFO.value] = xml_object[XML.DOCUMENT_INFO.value][
            XML.TEXT.value
        ]
        filetext = xml_object[XML.DOCUMENT_INFO.value].split("<PAGE>")
        text = {"PAGES": filetext, "DOCUMENT_DESC": xml_object[XML.DOCUMENT_DESC.value]}
        self.FILE_INFO.FILE_TEXT = models.FileText(**text)
        self.sync()
        return None

    def compute_page(self) -> None:
        """
        Populates
             PageDescription
        """
        page_detection_df = pd.DataFrame(
            columns=["PAGE_NO", "text", "selected_total_ratio"]
        )
        for page_no, page_text in enumerate(self.FILE_INFO.FILE_TEXT.PAGES):
            page_detection_df.loc[len(page_detection_df)] = [page_no] + spd.parse_text(
                page_text
            )

        detected_page_dict = spd.detect_df(page_detection_df)
        detected_page_dict = process_lookup(detected_page_dict)

        page_info = models.PageInfo()
        page_info.PAGE_COUNT = self.FILE_INFO.PAGE_COUNT
        page_info.DETECTED_PAGES = models.DetectedPage(**detected_page_dict)

        page_description = models.PageDescription()
        page_description.CREATED_ON = (
            pd.to_datetime("today").strftime("%Y-%m-%d %H:%M:%S"),
        )
        page_description.PAGE_INFO = page_info

        self.FILE_INFO.PAGE_DESCRIPTION = page_description

        self.sync()

        return None

    def compute_tables(self) -> None:
        """
        Populates
              extracted_tables
        """
        try:
            detected_pages = self.FILE_INFO.PAGE_DESCRIPTION.PAGE_INFO.DETECTED_PAGES
            Tables = TableExtractor(detected_pages, self.pdf_path)
            self.FILE_INFO.EXTRACTED_TABLES = Tables.extract_statements()
            self.sync()
        except:
            pass
        return None
