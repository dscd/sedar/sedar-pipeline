#!/usr/bin/env bash
# -*- coding: utf-8 -*-
"""
Control pipeline

Created on: Tuesday February 8th 2022
@authors:  Chatana Mandava & Ayush Pratap Singh

"""

import codecs
import datetime as dt
import hashlib
import os
from pathlib import Path
from typing import List

import glob2
import pandas as pd

import modules.file_extractor as fe
from config import models
from config.ctrl_info import *
from config.model_serializers import EsSerializer, FileInfoSerializer, Indices
from config.models import ControlInformation


### ingest_ctrl_data() is used to Ingest the new control information to the sedar_control_information table
def ingest_ctrl_data(ctl_to_process_list: List[ControlInformation]) -> None:
    for i in ctl_to_process_list:
        if not i.ISSUERNO == "":
            ctrl_files_process = fe.process_lookup(i.dict())
            EsSerializer().serialize(
                Indices.sedar_prod_control_information.value, ctrl_files_process
            )
    return None


### delete_prev_records() is used to delete all the previous records of Ctl documents from FILE_INFO,FILE_INFO_TEXT and PAGE_DETECTION tables
# New Method
def delete_prev_records(list_of_ctl: List[ControlInformation]) -> None:
    for ctrl in list_of_ctl:
        FileInfoSerializer().deserialize_by_fileid(
            query={"query": {"match": {"FILE_ID": ctrl.FILE_ID}}}
        )
    return None


### get_files_from_path() is used to get all the control files with .ctl extension in a particular folder
def get_files_from_path(
    ctrl_folder_path: str, document_type: str = ".ctl"
) -> List[str]:
    return glob2.glob(
        os.path.join(ctrl_folder_path, "**", f"*{document_type}"), recursive=True
    )


### encode_id() is used to generate FILE_ID using Accession Number
def encode_id(Accession_no: str) -> str:
    return hashlib.sha256(Accession_no.encode("utf-8", "surrogateescape")).hexdigest()


### extract_ctl_info() is used to extract all the control information from the path and folder in which the .ctl files are present.
def extract_ctrl_info(ctrl_folder_path: str) -> List[ControlInformation]:

    paths = get_files_from_path(ctrl_folder_path, document_type=".ctl")
    CTL_List = []

    for path in paths:
        CTL = models.ControlInformation()
        CTL.CTL_PATH = path
        CTL.ZIP_PATH = ctrl_folder_path
        CTL.ZIP_NAME = Path(ctrl_folder_path).name
        CTL.DATE_FOLDER = Path(str(CTL.CTL_PATH)).parts[-2]
        CTL.CTL_INFO = codecs.open(CTL.CTL_PATH, "r", encoding="ISO-8859-1").read()
        CTL.ACCESSION_NO = CTL.CTL_INFO[:ACCESSION_NO_E].strip()
        CTL.PROJECT_NO = CTL.CTL_INFO[:PROJECT_NO_E]
        CTL.SUBMISSION_NO = CTL.CTL_INFO[SUBMISSION_NO_S:SUBMISSION_NO_E]
        CTL.ISSUERNO = CTL.CTL_INFO[ISSUERNO_S:ISSUERNO_E]
        CTL.CLIENT_FN = CTL.CTL_INFO[CLIENT_FN_S:CLIENT_FN_E]
        CTL.FILER_TYPE = CTL.CTL_INFO[FILER_TYPE_S:FILER_TYPE_E]
        CTL.FILING_TYPE = CTL.CTL_INFO[FILING_TYPE_S:FILING_TYPE_E]
        CTL.DOCUMENT_TYPE = CTL.CTL_INFO[DOCUMENT_TYPE_S:DOCUMENT_TYPE_E]
        CTL.NAME_E = CTL.CTL_INFO[NAME_E_S:NAME_E_E]
        CTL.FILING_DATE = CTL.CTL_INFO[FILING_DATE_S:FILING_DATE_E]
        CTL.PUBLIC_DATE = CTL.CTL_INFO[PUBLIC_DATE_S:PUBLIC_DATE_E]
        CTL.PUBLIC_FLAG = CTL.CTL_INFO[PUBLIC_FLAG_S:PUBLIC_FLAG_E]
        CTL.FILE_ID = encode_id(CTL.ACCESSION_NO)
        CTL.CREATED_ON = dt.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
        CTL.UPDATED_ON = pd.to_datetime("today")
        CTL.RECORD_STATUS = 0
        CTL_List.append(CTL)

    return CTL_List


### ctl_file_check() is used to identify the new control files and update the control_information table.
### This function also identifies already exisiting ctl files and delete the records from the other tables.
def ctl_file_check(
    ctl_list: List[ControlInformation], ctl_ids_esdb: List[str]
) -> List[str]:
    ctl_ids = [i.FILE_ID for i in ctl_list]
    new_ids = list(set(ctl_ids).difference(ctl_ids_esdb))
    ctl_list_process = [i for i in ctl_list if i.FILE_ID in new_ids]
    ### Ingesting new records into Control Information table
    ingest_ctrl_data(ctl_list_process)
    ### Deleting Prev records from file_info , File_info_text and page detection table
    delete_prev_records(ctl_list_process)
    return new_ids + ctl_ids_esdb


### Executing the entire control pipeline
def run_control_pipeline(ctrl_folder_path: str, ctl_ids_esdb: List[str]) -> List[str]:
    return ctl_file_check(extract_ctrl_info(ctrl_folder_path), ctl_ids_esdb)
