# -*- coding: utf-8 -*-
"""
SEDAR App pipeline util functions

Created on: May 06, 2022
@authors:  Andres Solis Montero
"""
import os
import sys
import traceback
from os.path import basename

import glob2

from config.api import S3
from modules.s3client import S3Client


def rm_files_prefix(prefix_filename: str) -> None:
    """
    Locally removes all filenames with  the path_filename
    """
    for filename in glob2.glob(
        f"{prefix_filename}*".translate({ord("["): "[[]", ord("]"): "[]]"})
    ):
        os.remove(filename)


def log_exception_minio(
    client: S3Client, prefix_filename: str, prefix_minio: str
) -> None:
    """
    Logs an Exception details and all submission files (i.e., with prefix filename)
    to the error MinIO location using the prefix_minio.

    client: S3Client instance used to upload files to minio
    prefix_filename: prefix_path of all files to upload
    prefix_minio: prefix path to append to the error location
    """
    # Get current system exception
    ex_type, ex_value, ex_traceback = sys.exc_info()

    # Extract unformatter stack traces as tuples
    trace_back = traceback.extract_tb(ex_traceback)

    # Format stacktrace
    stack_trace = list()

    for trace in trace_back:
        stack_trace.append(
            f"File : {trace[0]} , Line : {trace[1]}, Func.Name : {trace[2]}, Message : {trace[3]}"
        )

    # Create file for logging traceback
    with open(f"{prefix_filename}.message", "w") as f:
        f.write(f"Exception type    : {ex_type.__name__} \n")
        f.write(f"Exception message : \n{ex_value}\n")
        f.write(f"Stack trace       : \n{stack_trace}\n")

    for error_file_name in glob2.glob(
        f"{prefix_filename}*".translate({ord("["): "[[]", ord("]"): "[]]"})
    ):
        file_minio_location = S3.get_error_loc(prefix_minio, basename(error_file_name))
        client.fput_loc(file_minio_location, error_file_name)
        os.remove(error_file_name)
