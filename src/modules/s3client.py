# -*- coding: utf-8 -*-
"""
Minio wrapper class that extends functionalities to the original API.



Created on Thursday Nov 19 12:13:20 2020
@authors:  Andres Solis Montero
"""
import functools
import io
import os
import re
import tempfile
import time
from os.path import basename, dirname, join
from sys import prefix
from typing import Callable, Generator, List, Optional, Union

import minio
import pandas as pd
from minio import Minio
from minio.commonconfig import CopySource
from pandas import DataFrame

from config.api import S3, S3Location
from config.env import environ, webapp_environ

# assert minio.__version__ == "6.0.0"


def s3_stream(function: Callable):
    """
    **Description:**
    Used as a decorator that read multiple SelectObjectReader or HTTPResponse stream objects into a single CSV file.
    SelectObjectReader and HTTPResponse have different api to access content, this decorator unifies them.

    **Examples:**

    ```python
    [description]
    ```

    Args:
        function: Callable
            [description]. Valid Values are [values]

    Returns:
        [type]:
            [description]. Valid Values are [values]
    """

    @functools.wraps(function)
    def _response(*args, **kwargs):
        data = None
        try:

            mode = "w" if isinstance(args[0], minio.select.SelectObjectReader) else "wb"
            # minio.select.SelectObjectReader is text based (needs "a")
            # urllib3.response.HTTPResponse is binary based (needs "ab")
            files = []
            for response in args:
                with tempfile.NamedTemporaryFile(mode, delete=False) as temp:
                    for buffer in response.stream(1024 * 1024):
                        temp.write(buffer)
                        temp.flush()
                files.append(temp.name)

            data = function(*files, **kwargs)

            for f in files:
                os.remove(f)

        except Exception as e:
            print(f"Failed to read stream: {str(e)}. Check query or compression type.")
        return data

    return _response


@s3_stream
def pandas_from_csv(*args, **kwargs) -> Optional[pd.DataFrame]:
    """
    **Description:**

    Converts a list of S3 streams into a list of temporary files that will be deleted
    after consumed into a single dataframe. The pandas kargs parameters will be passed to every read_csv call

    **Examples:**

    ```python
    df = pandas_from_csv([f for f in csv_filenames])
    ```

    Args:
        args: str
            csv filenames
        panda_args: dict
            read_csv parameters

    Returns:
        pd.DataFrame:
            A pandas DataFrame containing the contents of the CSV files
    """
    # kwargs["header"] = None
    kwargs["skip_blank_lines"] = True
    return (
        pd.concat((pd.read_csv(f, **kwargs) for f in args), ignore_index=True)
        if args
        else None
    )


class S3Client(Minio):
    """
    **Author(s)**

    - Andres Solis Montero - `andres.solismontero@canada.ca`

    - Collin Brown - `collin.brown@canada.ca`

    **Created On:** Thursday Nov 19 12:13:20 2020

    **Description:**

    Minio wrapper class that extends functionalities to the original API.

    Attributes:
        config_json_path str:
            Path to a json file having the MINIO_URL|ACCESS_KEY|SECRETE_KEY
        empty_folder_name str:
            Default name while creating an "empty folder", default = '.empty'
    """

    @staticmethod
    def get_client(env="default"):
        """Get a connection to the minimal Minio tenant"""
        return S3Client(env=env)

    def __init__(self, empty_folder_filename: str = ".empty", env="default") -> None:
        """
        @params
            config_json_path: (str) path to a json file having the MINIO_URL|ACCESS_KEY|SECRETE_KEY
            empty_folder_name:  (str) default name while creating an "empty folder", default = '.empty'
        """
        self._default_folder_file_ = empty_folder_filename
        # Get rid of http:// in minio URL
        http = lambda url: re.sub("^https?://", "", url)
        secure = lambda url: re.match("^https://", url)
        # Get MinIO URL + creds from environment variables - if config_json_path is passed then get
        # MinIO url/creds from json. Otherwise get them from environment variables.
        if env == "default":
            MINIO_URL = environ().MINIO_URL
            MINIO_ACCESS_KEY = environ().MINIO_ACCESS_KEY
            MINIO_SECRET_KEY = environ().MINIO_SECRET_KEY
        else:
            MINIO_URL = webapp_environ().MINIO_URL
            MINIO_ACCESS_KEY = webapp_environ().MINIO_ACCESS_KEY
            MINIO_SECRET_KEY = webapp_environ().MINIO_SECRET_KEY

        # Create the minio client.
        Minio.__init__(
            self,
            http(MINIO_URL),
            access_key=MINIO_ACCESS_KEY,
            secret_key=MINIO_SECRET_KEY,
            secure=secure(MINIO_URL),
            region="us-west-1",
        )

    def get_dir(self, location: S3Location):
        local_dir = tempfile.mkdtemp()

        print(f"\tDownloaded:")
        for obj in self.ls_objs(
            location.bucket,
            recursive=True,
            prefix=location.prefix,
            only_files=True,
            default_folder_file=False,
        ):
            localname = join(local_dir, basename(obj.object_name))
            tmp_loc = S3Location(
                location.bucket, location.prefix, basename(obj.object_name)
            )
            self.fget_loc(tmp_loc, localname)
            print(f"\t\t{localname}")
        return local_dir

    def ls_objs(
        self,
        bucket: str,
        prefix: str,
        recursive: bool = False,
        only_files: bool = False,
        default_folder_file: bool = False,
    ) -> Generator:
        """
        **Description:**

        Same API call as minio.list_objects with additional functionalities. It will not list the
        `.empty` files inside folders if default_folder_file is set to False.

        **Example:**

        ```python
        from modules.s3client  import S3Client

        client = S3Client.get_client()

        for obj in client.ls_objs(bucket='aaw-sedar', prefix='folder/path/', recursive=True,
                                  default_folder_file=False):
            print(obj.object_name)
        ```

        Args:
            bucket: str
                The name of the s3 storage bucket where we are listing objects from.
            prefix: str
                The prefix to the location where we are listing objects from.
            recursive: bool=False
                If `True`, recursively list objects in folders contained at the given prefix.
            only_files: bool=False
                If `True`, only list files. Folder can be displayed when recursive is set to False.
            default_folder_file: bool=False
                If `True`, displays the `self._default_folder_file_` filename if found.

        Returns:
            Yields objects at a given s3 bucket and prefix.
        """
        for obj in self.list_objects(bucket, prefix=prefix, recursive=recursive):
            if only_files and obj.is_dir:
                continue
            if (
                not default_folder_file
                and basename(obj.object_name) == self._default_folder_file_
            ):
                continue
            yield obj

    def mv_obj(
        self, src_bucket: str, src_obj: str, dst_obj: str, dst_bucket: str = None
    ) -> "minio.helpers.ObjectWriteResult":
        """
        **Description:**

        Moves an object to a different "path". It does it by using `copy_object`and `remove_object`
        from `minio.Minio`. If no dst_bucket is defined, the `src_bucket` is used. The source
        object will be deleted.

        !!! note
            The copying and removing of objects happens entirely on the server side.

        **Examples:**

        ```python
        from modules.s3client  import S3Client

        client = S3Client.get_client()
        client.mv_obj(bucket='aaw-sedar', src_obj='inbox/a.dat', dst_bucket='error/a.dat')
        ```

        Args:
            src_bucket: str
                The source S3 bucket.
            src_obj: str
                The source S3 object prefix.
            dst_obj: str
                The destination S3 bucket.
            dst_bucket: str=None
                The destination S3 object prefix.

        Returns:
            minio.helpers.ObjectWriteResult:
                The new location of the moved object.
        """
        new_object = self.cp_obj(src_bucket, src_obj, dst_obj, dst_bucket)
        self.remove_object(src_bucket, src_obj)
        return new_object

    def cp_obj(
        self, src_bucket: str, src_obj: str, dst_obj: str, dst_bucket: str = None
    ) -> "minio.helpers.ObjectWriteResult":
        """
        **Description:**

        Copies a object from a bucket/path to another bucket path in the server side. If no
        dst_bucket is defined, the src_bucket is used. This is a thin wrapper around
        `Minio.copy_object`.

        **Examples:**

        ```python
        from modules.s3client  import S3Client

        client = S3Client.get_client()
        client.cp_obj(bucket='aaw-sedar', src_obj='inbox/a.dat', dst_bucket='error/a.dat')
        ```

        Args:
            src_bucket: str
                The source S3 bucket.
            src_obj: str
                The source S3 object prefix.
            dst_obj: str
                The destination S3 bucket.
            dst_bucket: str=None
                The destination S3 object prefix.

        Returns:
            minio.helpers.ObjectWriteResult:
                The new location of the copied object.
        """
        dst_bucket = dst_bucket if dst_bucket else src_bucket

        return self.copy_object(dst_bucket, dst_obj, CopySource(src_bucket, src_obj))

    def mkdir(self, bucket: str, folder: str) -> None:
        """
        **Description:**

        Creates an empty "folder" by creating an empty file `self._default_folder_file` in the
        specified folder path. This is used because S3 storage is not technically a file system.
        It only preserves a prefix name if there is at least one file (object) under that prefix.
        The convention to deal with this is to include a dummy hidden file called ".empty" at the
        empty prefix location we want to preserve.

        **Examples:**

        ```python
        from modules.s3client import S3Client

        # This will add the obj folder/path/.empty to the bucket aaw-sedar
        client = S3Client.get_client()
        client.mkdir(bucket='aaw-sedar', folder='folder/path/')
        ```

        Args:
            bucket: str
                The name of the S3 storage bucket that the folder is in.
            folder: str
                The folder path that we want to make in S3 storage.
        """
        _object_ = join(folder, self._default_folder_file_)
        content = ""
        content_as_bytes = content.encode("utf-8")
        content_as_stream = io.BytesIO(content_as_bytes)
        self.put_object(
            bucket, _object_, content_as_stream, len(content_as_bytes), "text/plain"
        )

    def rm(self, bucket: str, folder: str, remove_dir: bool = False) -> None:
        """
        **Description:**

        Removes all the content of a "folder", all objects with the same prefix will be deleted.

        **Examples:**

        ```python
        from modules.s3client  import S3Client

        client = S3Client.get_client()
        client.rm(bucket='aaw-sedar', folder='inbox/')
        ```

        Args:
            bucket: str
                The bucket containing the content to be removed.
            folder: str
                The folder (prefix) containing the content to be removed.
            remove_dir: bool=False
                If `True`, also remove the `.empty` default file, otherwise delete the `.empty`
                file as well and remove the prefix location from S3 storage.
        """
        for obj in self.ls_objs(
            bucket,
            prefix=folder,
            recursive=True,
            only_files=True,
            default_folder_file=remove_dir,
        ):
            self.remove_object(bucket, obj.object_name)

    def mv_loc(self, src: S3Location, dst: S3Location):
        """
        **Description:**
        Moves an object from a source S3Location to another.
        This operation is done in the server. No downloads.

        **Examples:**

        ```python
        [description]
        ```

        Args:
            src: S3Location
                [description]. Valid Values are [values]
            dst: S3Location
                [description]. Valid Values are [values]

        Returns:
            [type]:
                [description]. Valid Values are [values]
        """
        return self.mv_obj(
            src.bucket, S3.loc_to_obj(src), S3.loc_to_obj(dst), dst.bucket
        )

    def cp_loc(self, src: S3Location, dst: S3Location):
        """
        **Description:**

        [description]
        Copies an object from a S3Location to a S3Location

        ```python
        [description]
        ```

        Args:
            src: S3Location
                [description]. Valid Values are [values]
            dst: S3Location
                [description]. Valid Values are [values]

        Returns:
            [type]:
                [description]. Valid Values are [values]
        """
        return self.cp_obj(
            src.bucket, S3.loc_to_obj(src), S3.loc_to_obj(dst), dst.bucket
        )

    def stat_loc(self, location: S3Location) -> Union[object, None]:
        """
        **Description:**

        Retrieve objects stats from a S3Location.
        see minio.stat_object for details.
        Throws minio.error.NoSuchKey exception if object is not found

        **Examples:**

        ```python
        [description]
        ```

        Args:
            location: S3Location
                [description]. Valid Values are [values]

        Returns:
            Union[object, None]:
                [description]. Valid Values are [values]
        """
        try:
            return self.stat_object(location.bucket, S3.loc_to_obj(location))
        except Exception:
            return None

    def _get_last_updated(self, loc: S3Location) -> Optional[time.struct_time]:
        """
        **Description:**

        Gets the time that an object was last updated.

        Args:
            loc: S3Location
                [description]. Valid Values are [values]

        Returns:
            Optional[time.struct_time]:
                [description]. Valid Values are [values]
        """
        stats = self.stat_loc(loc)
        return stats.last_modified if stats else None

    def dependency_change(
        self, deps: List[S3Location], outputs: List[S3Location]
    ) -> bool:
        """
        **Description:**

        Checks if a dependency has changed more recently than its output.

        Args:
            deps: List[S3Location]
                [description]. Valid Values are [values]
            output: S3Location
                [description]. Valid Values are [values]

        Returns:
            bool:
                [description]. Valid Values are [values]

        Gotchas:
            1. Need to treat last_modified_dep and last_modified_output differently depending on
               whether there are 1 or many dependencies/outputs. The reason for this is that the
               min/max functions will behave differently in each case. If there is a single element
               in the list of outputs, for example, then max(*[list of 1 time.struc_time instance])
               returns the maximum value of the properties in the time.timestruct instance.

               E.g.

               max(*[time.struct_time(tm_year=2021, tm_mon=7, tm_mday=8, tm_hour=19, tm_min=21,
               tm_sec=10, tm_wday=3, tm_yday=189, tm_isdst=0])

               returns 2021 (int)

               Conversely, if you call max(*[list of many time.struc_time instances]), then the
               most recent time.struc_time instance is returned.

               This is why the code below distinguishes between these two cases.

        """
        if not deps or not outputs:
            return True
        deps_list = [self._get_last_updated(dep) for dep in deps]
        outputs_list = [self._get_last_updated(out) for out in outputs]
        # Get the date of the most recently modified dependency
        if len(deps_list) == 1:
            last_modified_dep = deps_list.pop()
        else:
            last_modified_dep = max(*deps_list)
        # Get the last modified date of the output
        if len(outputs_list) == 1:
            last_modified_output = outputs_list.pop()
        else:
            last_modified_output = min(*outputs_list)
        # If the output was modified more recently than any of the inputs, then there has not been
        # a dependency change since it was last computed. Otherwise there has and it needs to be
        # recomputed.
        return last_modified_dep > last_modified_output

    def locs_exist(self, locs: List[S3Location]) -> bool:
        """
        **Description:**

        Given a list of S3Locations, returns true if every one exists and returns false otherwise.

        Args:
            locs: List[S3Location]
                [description]. Valid Values are [values]

        Returns:
            bool:
                [description]. Valid Values are [values]

        Raises:
            NotImplemented:
                [description]
        """
        return None not in [self.stat_loc(loc) for loc in locs]

    def put_loc(self, loc: S3Location, data, length, **kawrgs):
        """
        **Description:**

        [description]

        **Examples:**

        ```python
        [description]
        ```

        Args:
            loc: S3Location
                [description]. Valid Values are [values]
            data: [type]
                [description]. Valid Values are [values]
            length: [type]
                [description]. Valid Values are [values]

        Returns:
            [type]:
                [description]. Valid Values are [values]
        """
        return self.put_object(loc.bucket, S3.loc_to_obj(loc), data, length, **kawrgs)

    def fput_loc(self, loc: S3Location, file_path: str, **kwargs):
        """
        **Description:**

        Wrapper function of minio API fput_object but using an S3Location

        **Examples:**

        ```python
        [description]
        ```

        Args:
            loc: S3Location
                A S3Location namedtuple
            file_path: str
                the local filename to copy to the S3 Location

        Returns:
            [type]:
                See minio API for the output of minio.fput_object
        """
        return self.fput_object(loc.bucket, S3.loc_to_obj(loc), file_path, **kwargs)

    def fget_loc(self, loc: S3Location, localpath: str, **karwgs):
        """
        **Description:**

        Wrapper function of minio API fget_object but using an S3Location

        **Examples:**

        ```python
        [description]
        ```

        Args:
            loc: S3Location
                A S3Location namedtuple
            localpath: str
                Local path to download the S3 Location file to

        Returns:
            [type]:
                See minio API for the output of minio.fget_object
        """
        return self.fget_object(loc.bucket, S3.loc_to_obj(loc), localpath, **karwgs)

    def get_CSV(self, loc: S3Location, **kwargs) -> DataFrame:
        """
        **Description:**

        Retrieves a CSV file from the defined S3Location. No SQL is performed all data downloaded

        **Examples:**

        ```python
        [description]
        ```

        Args:
            loc: S3Location
                [description]. Valid Values are [values]

        Returns:
            DataFrame:
                [description]. Valid Values are [values]
        """
        response = self.get_object(
            bucket_name=loc.bucket, object_name=S3.loc_to_obj(loc)
        )
        return pandas_from_csv(response, **kwargs)

    def get_CSVs(self, loc: S3Location, **kwargs) -> DataFrame:
        """
        **Description:**

        Retrieves multiple CSV files from the S3Location.prefix. No SQL is performed all data
        downloaded and concatenated. CSV must have the same format (schema).

        **Examples:**

        ```python
        [description]
        ```

        Args:
            loc: S3Location
                [description]. Valid Values are [values]

        Returns:
            DataFrame:
                [description]. Valid Values are [values]
        """
        objs = self.ls_objs(
            loc.bucket,
            loc.prefix,
            recursive=True,
            only_files=True,
            default_folder_file=False,
        )
        return pandas_from_csv(
            *[
                self.get_object(bucket_name=o.bucket_name, object_name=o.object_name)
                for o in objs
            ],
            **kwargs,
        )

    def ls_locs(
        self,
        loc: S3Location,
        recursive: bool = False,
        only_files: bool = False,
        default_folder_file: bool = False,
    ) -> Generator:
        """
        Extended version of ls_objs to use S3Locations objects.
        """
        for obj in self.ls_objs(
            loc.bucket,
            loc.prefix,
            recursive=recursive,
            only_files=only_files,
            default_folder_file=default_folder_file,
        ):
            yield S3Location(
                obj.bucket_name,
                prefix=join(dirname(obj.object_name), ""),
                basename=basename(obj.object_name),
            )

    def ls_objs(
        self,
        bucket: str,
        prefix: str,
        recursive: bool = False,
        only_files: bool = False,
        default_folder_file: bool = False,
    ) -> Generator:
        """
        **Description:**

        Same API call as minio.list_objects with additional functionalities. It will not list the
        `.empty` files inside folders if default_folder_file is set to False.

        **Example:**

        ```python
        from modules.s3client  import S3Client

        client = S3Client.get_client()

        for obj in client.ls_objs(bucket='aaw-sedar', prefix='folder/path/', recursive=True,
                                  default_folder_file=False):
            print(obj.object_name)
        ```

        Args:
            bucket: str
                The name of the s3 storage bucket where we are listing objects from.
            prefix: str
                The prefix to the location where we are listing objects from.
            recursive: bool=False
                If `True`, recursively list objects in folders contained at the given prefix.
            only_files: bool=False
                If `True`, only list files. Folder can be displayed when recursive is set to False.
            default_folder_file: bool=False
                If `True`, displays the `self._default_folder_file_` filename if found.

        Returns:
            Yields objects at a given s3 bucket and prefix.
        """
        for obj in self.list_objects(bucket, prefix=prefix, recursive=recursive):
            if only_files and obj.is_dir:
                continue
            if (
                not default_folder_file
                and basename(obj.object_name) == self._default_folder_file_
            ):
                continue
            yield obj
