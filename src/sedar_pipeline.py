#!/usr/bin/env python
"""
SEDAR Ingress pipeline

Created on: Tuesday October 5th 2021
@authors:  Oladayo Ogunnoiki
"""


import argparse
import datetime
import os
import re
import shutil
import tarfile
from os.path import basename, dirname, join
from pathlib import Path

########################################
# Import Required Libraries
########################################
import glob2
from tqdm.auto import tqdm

import modules.control_pipeline as ctrl
import modules.init_models as imod
from config.api import S3, DataSource, S3Location
from config.env import sedar
from config.model_serializers import (FileInfoSerializer, Indices,
                                      get_es_client, ls_ctrl_information_objs)
from config.models import FileInfo
from modules.file_extractor import FileInfoExtractor
from modules.s3client import S3Client
from modules.utils import log_exception_minio, rm_files_prefix
from modules.progress import Progress

def pipeline(parser: argparse.Namespace):
    client = S3Client.get_client()
    ########################################
    # Initialize model in cache/models folder
    ########################################
    ## Step 0: Initialize model in cache
    imod.check_models()
    ctrl_file_list = ls_ctrl_information_objs()

    ## Step 1: Iterate through zip files inside MinIO <inbox> folder
    for zip_path in tqdm(
        client.ls_objs(
            bucket=S3.INBOX.bucket,
            prefix=S3.INBOX.prefix,
            recursive=True,
            default_folder_file=False,
        ),
        desc="Zip file extraction",
        initial=1,
    ):
        try:
            zip_path = zip_path.object_name
            zip_file = Path(zip_path).stem
            zip_file_name = Path(zip_file).stem
            date_match = re.match(sedar().name_pattern, zip_file)

            loc = S3Location(S3.INBOX.bucket, dirname(zip_path), basename(zip_path))
            err = S3.get_error_loc(zip_file_name, basename(zip_path))
            ## Step 1.a
            ## If file does not match format then continue to the next file
            if date_match is None:
                raise Exception("Filename not allowed")
            ## Step 2
            ## Capture year and month of file
            year, month, _ = date_match.groups()

            ## Throws a Value Error Exception when date is invalid
            ## Captured by outer try/except
            ## Gracefully finish execution and log the error
            datetime.date(year=int(year), month=int(month), day=int(_))

            ## Step 3
            ## Checks if current tar.gz file exists in MinIO historical
            historical_location = S3.get_historical_data_loc(
                DataSource.sedar, int(year), int(month), basename(zip_path)
            )

            historical_available = client.stat_loc(historical_location) is not None

            ## Step 3
            ## If Zip file exists in Historical on Minio then
            ## remove file from Inbox directory and continue to the next zip file
            ## If parser.force == True, go ahead as usual
            ## cache_zip_location = S3.get_cache_location(historical_location) ->
            ## 'cache/historical/sedar/year/month/archive_yyyy-mm-dd.tar.gz or archive-yyyy-mm.tar.gz'
            cache_zip_location = S3.get_cached_location(historical_location)

            ## Step 3.a.b
            ## Remove element if historical is available and not being forced
            if historical_available and not parser.force:
                if not parser.keep_inbox:
                    client.remove_object(S3.INBOX.bucket, zip_path)
                continue  # to next inbox file

            ## Step 3.b
            ## Download historical data to cache directory
            if not os.path.exists(cache_zip_location):
                os.makedirs(os.path.dirname(cache_zip_location), exist_ok=True)
                zip_minio_location = S3Location(
                    S3.INBOX.bucket, dirname(zip_path), basename(zip_path)
                )
                client.fget_loc(zip_minio_location, cache_zip_location)

            ## Step 4
            ## Check if the cache zip file has been uncompressed
            cache_zip_directory = join(
                os.path.dirname(cache_zip_location), zip_file_name
            )
            if not os.path.exists(cache_zip_directory):
                ## uncompress if not existing
                os.mkdir(cache_zip_directory)
                tar = tarfile.open(cache_zip_location, "r:gz")
                tar.extractall(cache_zip_directory)
                tar.close()
            ## Step 5
            ## list_of_ctl is the Control File IDs that are already processed.
            ## The run_control_pipeline() will process all the Control files and update the Control_Information table.
            ctrl_file_list = ctrl.run_control_pipeline(
                cache_zip_directory, ctrl_file_list
            )
        except Exception as log_exception:
            log_exception_minio(
                client=client, prefix_filename=zip_file_name, prefix_minio=zip_file_name
            )
            move_compressed_file(client, loc, err, cache_zip_location, parser)
            continue

        ## Step 6
        error_processing_file = False
        for xml_file in tqdm(
            glob2.glob(join(cache_zip_directory, "**", "*.xml"), recursive=True),
            desc="Pdf extraction",
            initial=1,
        ):

            try:
                ## Step 6.1
                ## Generate file_id
                pdf_file = f"{xml_file[:-8]}.pdf"
                file_id = ctrl.encode_id(Path(pdf_file).stem)
                ## Check if file_id is in the list of ctrl files.
                ## If remove the pdf and xml files and move on to the next pdf and xml files
                if file_id in ctrl_file_list:
                    rm_files_prefix(xml_file[:-8])
                    continue

                file_info_ex = FileInfoExtractor(pdf_path=pdf_file, xml_path=xml_file)
                file_info_ex.FILE_ID = file_id
                file_info_ex.FOLDER_MONTH = month
                file_info_ex.FOLDER_YEAR = year
                file_info_ex.FOLDER_DAY = xml_file.split("/")[-2].split("-")[-1]
                file_info_ex.sync()

                # Only process xml files that have a pdf file associated with them
                if os.path.exists(file_info_ex.pdf_path):
                    ## Step 6.2
                    ## Compute metadata

                    file_info_ex.compute_metadata()

                    ## Step 6.3

                    if (
                        file_info_ex.FILE_INFO.DOCUMENT_DESC
                        in sedar().extraction_documents
                    ):
                        if file_info_ex.FILE_INFO.PAGE_COUNT != -1:
                            file_info_ex.compute_text()
                    else:
                        rm_files_prefix(xml_file[:-8])

                    ## Step 6.4

                    if file_info_ex.FILE_INFO.DOCUMENT_DESC in sedar().sedar_documents:
                        ## 6.4.1 Compute page detection
                        ## Parse File_Info into new page_detection function
                        if file_info_ex.FILE_INFO.PAGE_COUNT != -1:
                            file_info_ex.compute_page()

                            ## Step 6.4.2 Run page extraction
                            ## Parse File_Info into new page_extraction function
                            file_info_ex.compute_tables()

                    ## Step 6.5 Update ES records: Does document exist on Elastic Search
                    # Consolidate File_INFO
                    query = {"query": {"match": {"FILE_ID": f"{file_info_ex.FILE_ID}"}}}
                    old_file_query = get_es_client().search(
                        index=Indices.sedar_prod_file_info.value, body=query
                    )

                    ## Step 6.5.a  If Yes remove previous record
                    if old_file_query["hits"]["total"]["value"] > 0:
                        # delete_prev_records(esp, old_file_name_query, indexes.keys())
                        FileInfoSerializer().deserialize_by_fileid(query=query)

                    ## Step 6.5.b Add new record
                    # update_records for FILE_INFO, FILE_TEXT, PAGE_DESCRIPTION, EXTRACTED_TABLE
                    FileInfoSerializer().serialize_by_fileid(file_info_ex)
                    ## Step 6.5.1 Update / Add new record for submission type
                    # update records for OTHER_ISSUER, OTHER_FILER, MUTUAL_FUND_GROUP, MUTUAL_FUND_ISSUER
                    FileInfoSerializer().serialize_by_uniqueno_lastupdate(file_info_ex)

                    ## Step 6.6 Update Minio. Overwrite pdf if it exists on Minio

                    # uniqueno can be ISSUERNO if submission type is OTHER_FILER, OTHER_ISSUER
                    # else it will be GROUPNO if submission type is MUTUAL FUND GROUP
                    if (
                        file_info_ex.FILE_INFO.DOCUMENT_DESC
                        in sedar().upload_minio_documents
                    ):
                        uniqueno = (
                            file_info_ex.ISSUERNO
                            if file_info_ex.ISSUERNO is not None
                            else file_info_ex.GROUPNO
                        )
                        pdf_minio_location = S3.get_data_loc(
                            DataSource.sedar,
                            int(year),
                            int(month),
                            uniqueno,
                            f"{file_info_ex.FILE_ID}.pdf",
                        )
                        client.fput_loc(pdf_minio_location, file_info_ex.pdf_path)

                    ## Step 6.7 Remove pdf and xml
                    rm_files_prefix(xml_file[:-8])

            except Exception as log_exception:
                log_exception_minio(
                    client=client,
                    prefix_filename=xml_file[:-8],
                    prefix_minio=zip_file_name,
                )
                error_processing_file = True
                continue

        ## Step 7 Upload archive_XXXX-XX-XX.tar.gz to historical data
        ## Upload only if the save_file flag is set to true.

        src = loc
        dst = err if error_processing_file else historical_location
        move_compressed_file(client, src, dst,cache_zip_location, parser)

        # if (not historical_available) or parser.save_file:
        #     client.fput_loc(historical_location, cache_zip_location)

        ## Step 8
        ## Remove cache
        os.remove(cache_zip_location)
        shutil.rmtree(cache_zip_directory)
        # ## Clear Inbox folder if keep_inbox flag is set
        # if parser.keep_inbox:
        #     # Remove inbox/archive_yyyy-mm-dd.tar.gz or inbox/archive-yyyy-mm.tar.gz
        #     client.remove_object(S3.INBOX.bucket, zip_path)

# While moving or copying the compressed files, the file size should be < 5Gb.
# Minio wrapper client doesn't allow to copy or move the files greater than 5GB.
# In case the file size > 5Gb then upload the file from the cache folder
# Once uploaded remove the file from the inbox folder (if necessary)
def move_compressed_file(client: S3Client, 
                         src: S3Location, 
                         dst: S3Location, 
                         cache_zip_Location:str , 
                         parser: argparse.Namespace):
    MAX_GB_SIZE = 5 *1024 *1024 *1024
    if client.stat_loc(src).size <= MAX_GB_SIZE:
        client.cp_loc(src, dst)
    else:  
        progress=Progress()
        progress.set_meta(os.path.getsize(cache_zip_Location),dst.basename)
        client.fput_loc(dst,cache_zip_Location,progress=progress)
    
    if not parser.keep_inbox:
        client.remove_object(src.bucket,S3.loc_to_obj(src))


def retention_policy(parser: argparse.Namespace):

    ## Instantiate S3 Client
    client = S3Client.get_client()

    ## Initialize model in cache/models folder
    imod.check_models()

    ## Number of years to filter by
    no_of_years = int(parser.years)

    ## Derive threshold date
    current_date = datetime.datetime.now()
    threshold_date = current_date - datetime.timedelta(no_of_years * 365)

    ## Define query using threshold date to get all entries earlier
    filter_query = {
        "size" : 9500,
        "query": {
            "range": {
                "RECORD_DATE": {
                    "lt": threshold_date,
                }
            }
        }
    }
    filtered_documents = get_es_client().search(
        index=Indices.sedar_prod_file_info.value, body=filter_query
    )
    if filtered_documents:
        ## Loop through derived dictionaries
        for document in tqdm(
            filtered_documents["hits"]["hits"],
            desc="Cleaning minio and elastic search",
            initial=1,
        ):
            ## Use filed id to delete indeices entries with the same filed id
            row = FileInfo(**document["_source"])
            file_id = row.FILE_ID
            issuer_no = row.ISSUERNO

            file_id_query = {"query": {"match": {"FILE_ID": file_id}}}

            FileInfoSerializer().deserialize(file_id_query=file_id_query)

            ## Use file id, year, month, and issuerno to delete file from minio
            year = int(row.FOLDER_YEAR)
            month = int(row.FOLDER_MONTH)

            ## Delete file from minio
            minio_file_object = S3.get_data_loc(
                DataSource.sedar, year, month, issuer_no, f"{file_id}.pdf"
            )
            file_path = S3.loc_to_obj(minio_file_object)
            client.remove_object(S3.DATA.bucket, file_path)

            ## Check and delete .empty file
            minio_empty_object = S3.get_data_loc(
                DataSource.sedar, year, month, issuer_no, ".empty"
            )
            if client.stat_loc(minio_empty_object) is not None:
                empty_file_path = S3.loc_to_obj(minio_empty_object)
                client.remove_object(S3.DATA.bucket, empty_file_path)

        ## Clean company name index
        FileInfoSerializer().deserialize(company_clean=True)
        print("Retention Policy Implemented")
    else:
        return "No document entry prior to current date"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Sedar Extraction Pipeline")
    parser.add_argument(
        "-f",
        "--force",
        dest="force",
        default=False,
        action="store_true",
        help="A boolean to force the pipeline to process old zip files",
    )

    parser.add_argument(
        "-k",
        "--keep_inbox",
        dest="keep_inbox",
        default=False,
        action="store_true",
        help="A boolean to keep inbox file after processed",
    )
    parser.add_argument(
        "-r",
        "--clean_data",
        default=False,
        action="store_true",
        help="If true the retention_policy function will be executed to remove files that don't meet a time threshold",
    )
    parser.add_argument(
        "-y",
        "--years",
        default=3,
        help="If clean_data is true, this the time threshold to clean old files",
    )
    parser = parser.parse_args()
    ## If true run retention_policy function else run the extraction pipeline
    if parser.clean_data:
        retention_policy(parser)
    else:
        pipeline(parser)
