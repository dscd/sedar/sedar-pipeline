"""
**Author(s)**

- Andres Solis Montero - `andres.solismontero@canada.ca`

- Collin Brown - `collin.brown@canada.ca`

**Created On:** Thursday Nov 19 12:13:20 2020

**Description:**

The `config` module contains a variety of configuration utilities and enumerators. All Enum classes
that are used throughout the project are here. The `S3` proxy class for generating S3Locations
is defined here as well.
"""
import json
from collections import namedtuple
from enum import Enum, auto
from functools import lru_cache
from os.path import basename, dirname, join
from typing import Optional

"""
S3 Locations relevant to our application.
"""
S3Location = namedtuple(
    "S3Location", ["bucket", "prefix", "basename"], defaults=["", "", ""]
)


class ConfigFile(Enum):
    """
    Enumerator containig all Configuration files
    """

    s3locations = "locations.json"
    aaw_minio = "/vault/secrets/minio-standard-tenant-1.json"
    environment = ".env"
    webapp_environment = "webapp/.env"
    sedar = "config/sedar.json"


class AutoEnum(Enum):
    """
    Base Enumerator Class to create Enumerators
    that have the same value as their name.
    """

    def _generate_next_value_(name, start, count, last_values):
        return name


class DataSource(AutoEnum):
    """
    Enumerator Class
    """

    sedar = auto()


class Factory:
    """
    Factory class to read configurations files once
    """

    @staticmethod
    @lru_cache
    def s3locs() -> "Factory":
        """
        Static Factory method to load ConfigFile.s3Locations into python objs
        """
        with open(join(dirname(__file__), ConfigFile.s3locations.value)) as f:
            locs = json.load(f)

        l = [(k, S3Location(**v)) for k, v in locs.items()]
        keys, values = zip(*l)

        return namedtuple("S3Locs", keys, defaults=values)()


class S3:
    """
    Util functions to generate unique files following our folder structures and organization. The
    set of static methods here uses our S3Locations to generate the correct folder structure.

    !!! check
        **Developers should always use the `S3` proxy class instead of hard-coding S3 buckets,
        prefixes, or basenames.** This will significantly reduce the overhead associated with
        remembering naming conventions and also substantially reduce the probability of making an
        error when hard-coding path names.

    """

    INBOX = Factory.s3locs().INBOX
    ERROR = Factory.s3locs().ERROR
    HISTORICAL_DATA = Factory.s3locs().HISTORICAL_DATA
    DATA = Factory.s3locs().DATA
    TMP = Factory.s3locs().TMP
    MODELS = Factory.s3locs().MODELS
    LOOKUP_TABLES = Factory.s3locs().LOOKUP_TABLES
    CACHE = Factory.s3locs().CACHE
    # Model Locations
    NLTK_DATA = Factory.s3locs().NLTK_DATA
    IS_BS_BOW = Factory.s3locs().IS_BS_BOW
    IS_BS_CLASSIFIER = Factory.s3locs().IS_BS_CLASSIFIER
    RF_CLASSIFIER = Factory.s3locs().RF_CLASSIFIER
    NOTES_BOW = Factory.s3locs().NOTES_BOW
    NOTES_CLASSIFIER = Factory.s3locs().NOTES_CLASSIFIER
    SEG_BOW = Factory.s3locs().SEG_BOW
    SEG_CLASSIFIER = Factory.s3locs().SEG_CLASSIFIER

    @staticmethod
    def get_historical_data_loc(
        datasource: DataSource,
        year: Optional[int] = None,
        month: Optional[int] = None,
        basename: Optional[str] = None,
    ) -> S3Location:
        prefix = join(
            S3.HISTORICAL_DATA.prefix,
            datasource.value,
            "" if (year is None) else f"{year:4d}",
            "" if (year is None) or (month is None) else f"{month:02d}",
            "",
        )
        basename = (
            "" if (year is None) or (month is None) or (basename is None) else basename
        )
        return S3Location(S3.HISTORICAL_DATA.bucket, prefix, basename)

    @staticmethod
    def get_cached_location(loc: S3Location) -> str:
        return join(S3.CACHE.prefix, loc.prefix, loc.basename)

    @staticmethod
    def get_data_loc(
        datasource: DataSource,
        year: Optional[int] = None,
        month: Optional[int] = None,
        issue: Optional[str] = None,
        uuid: Optional[str] = None,
    ) -> S3Location:
        prefix = join(
            S3.DATA.prefix,
            datasource.value,
            "" if (year is None) else f"{year:4d}",
            "" if (year is None) or (month is None) else f"{month:02d}",
            "" if (year is None) or (month is None) or (issue is None) else issue,
            "",
        )
        uuid = (
            ""
            if (year is None) or (month is None) or (uuid is None) or (issue is None)
            else uuid
        )
        return S3Location(S3.DATA.bucket, prefix, uuid)

    @staticmethod
    def get_error_loc(
        zip_file_name: Optional[str] = None, basename: Optional[str] = None
    ) -> S3Location:
        prefix = join(S3.ERROR.prefix, zip_file_name)
        return S3Location(S3.ERROR.bucket, prefix, basename)

    @staticmethod
    def obj_to_loc(bucket: str, obj: str) -> S3Location:
        """
        **Description:**

        A helper method that is used to translate between the S3 `obj` type and our
        project-specific `S3Location` type. This is the inverse method to `S3.loc_to_obj`.

        **Example:**
            ```
            # List all the objects from the loc S3location
            objs = client.ls_objs(
                loc.bucket,
                recursive=True,
                prefix=loc.prefix,
                only_files=True,
                default_folder_file=False,
            )

            # Recode all objs to S3Location instances
            s3locs = [ S3.obj_to_loc(loc.bucket, obj.object_name) for obj in objs ]
            ```

        Args:
            bucket: str
                The name of the S3 bucket where the object is located.
            obj: str
                The `obj` string returned by an S3 client.

        Returns:
            S3Location:
                The instance of `S3Location` that corresponds to the given `bucket` and `obj`
                strings passed.
        """
        return S3Location(bucket, join(dirname(obj), ""), basename(obj))

    @staticmethod
    def loc_to_obj(location: S3Location) -> str:
        """
        **Description:**

        A helper method that is used to translate between  our project-specific `S3Location` type
        and the S3 `obj` type and. This is the inverse method to `S3.obj_to_loc`. While translating
        from a S3Location to obj, the bucket name is lost. The returned string is the concatenation of
        S3Location.prefix + S3Location.basename.

        **Example:**
            ```
            path =  S3.loc_to_obj(s3loc)
            print(s3loc.bucket, path)
            ```

        Args:
            location: S3Location
                An instance of our project-specific `S3Location` where we want to get the
                corresponding path as a string.

        Returns:
            str
                An `obj` string that can be used in an S3 client or other use case that requires
                a path as a string.
        """
        return join(location.prefix, location.basename)
