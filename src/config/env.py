import json
import os
from functools import lru_cache
from os.path import exists

from pydantic import BaseSettings, Field

from config.api import ConfigFile
from config.models import SedarModel


class GlobalConfig(BaseSettings):
    """s
    Necesary enviroment variables needed for authentication.
    If these variables are not set, any file including this
    module will stop executing.
    """

    MINIO_URL: str
    MINIO_ACCESS_KEY: str
    MINIO_SECRET_KEY: str
    ES_HOST: str
    ES_USER: str
    ES_PASS: str

    """
    Provides the path to a ".env" containing a list of environments
    variables to be set in case that they don't exist yet.
    """

    class Config:
        env_file: str = ConfigFile.environment.value


class WebappConfig(BaseSettings):
    """
    Necesary enviroment variables needed for authentication.
    If these variables are not set, any file including this
    module will stop executing.
    """

    WEBAPP_DEBUG_MODE: bool
    WEBAPP_PORT: int
    WEBAPP_SECRET_KEY: str
    WEBAPP_BASE_DOMAIN: str
    NB_PREFIX: str = Field(env="NB_PREFIX", default="")

    MINIO_URL: str
    MINIO_ACCESS_KEY: str
    MINIO_SECRET_KEY: str

    ES_HOST: str
    ES_USER: str
    ES_PASS: str

    DJANGO_SETTINGS_MODULE: str = "webapp.webapp.settings"

    """
    Provides the path to the webapp ".env" containing environments
    variables to be set in case that they don't exist yet.
    """

    def root_path(self):
        return f"{self.NB_PREFIX}/proxy/{self.WEBAPP_PORT}/" if self.NB_PREFIX else ""
        # return ""

    class Config:
        env_file: str = ConfigFile.webapp_environment.value


def aaw_minio_credentials():
    if exists(ConfigFile.aaw_minio.value):
        with open(ConfigFile.aaw_minio.value) as f:
            data = json.load(f)
            for var, val in data.items():
                os.environ[var] = str(val)


@lru_cache
def environ():
    aaw_minio_credentials()
    return GlobalConfig()


@lru_cache
def webapp_environ():
    return WebappConfig()


@lru_cache
def sedar():
    if exists(ConfigFile.sedar.value):
        with open(ConfigFile.sedar.value) as f:
            data = json.load(f)
            return SedarModel(**data)
