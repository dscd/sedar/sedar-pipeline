"""
 Control Info Substring positions
 Related to the SEDAR CTRL file name convention.
 Please refer to SEDAR Manual (https://www.sedar.com/)
 for naming convention.

 Naming convention for variables defined here:
 <NAME>_S(TART)
 <NAME>_E(END)
 Variables without a suffix "_S" are assumed 0
"""
ACCESSION_NO_E = 173
PROJECT_NO_E = 8
SUBMISSION_NO_S = 9
SUBMISSION_NO_E = 17
ISSUERNO_S = 18
ISSUERNO_E = 26
CLIENT_FN_S = 27
CLIENT_FN_E = 173
FILER_TYPE_S = 173
FILER_TYPE_E = 176
FILING_TYPE_S = 176
FILING_TYPE_E = 179
DOCUMENT_TYPE_S = 179
DOCUMENT_TYPE_E = 185
NAME_E_S = 185
NAME_E_E = 225
FILING_DATE_S = 225
FILING_DATE_E = 233
PUBLIC_DATE_S = 233
PUBLIC_DATE_E = 241
PUBLIC_FLAG_S = 247
PUBLIC_FLAG_E = 248
