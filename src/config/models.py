import inspect
from datetime import datetime
from typing import List, Optional, no_type_check, Union

from pydantic import BaseModel, Field
from pydantic.fields import PrivateAttr


###################### END #########################
class PatchedModel(BaseModel):
    @no_type_check
    def __setattr__(self, name, value):
        """
        To be able to use properties with setters in pydantic models
        """
        try:
            # Trying to identify public pydantic attributes
            super().__setattr__(name, value)
        except ValueError as e:
            # If the attribute is private and defined as a property, search all the
            # properties declared for the class
            setters = inspect.getmembers(
                self.__class__,
                predicate=lambda x: isinstance(x, property) and x.fset is not None,
            )
            # Iterate over all the propoerties, find the correct property name and call
            # its setter
            for setter_name, func in setters:
                if setter_name == name:
                    object.__setattr__(self, name, value)
                    break
            # If the property name wasn't found in the pydantic BaseModel, neither the
            # PatchedModel properties, then raise the initial exception because it's not
            # a valid property/attribute name.
            # *the else is part of the for loop
            else:
                raise e


########## Company Name Related Models #############


class MutualFundGroup(PatchedModel):
    APPSEXEMPTION: Optional[list]
    APPSREGISTRATION: Optional[list]
    CONTACT: Optional[str]
    CREATED_ON: Optional[datetime]
    EXT: Optional[str]
    FAXAREA: Optional[str]
    FAXNO: Optional[str]
    FPEDATE: Optional[datetime]
    FPERELATES: Optional[str]
    GROUPNO: Optional[str]
    LASTUPDATE: Optional[datetime]
    MANAGER_E: Optional[str]
    MANAGER_F: Optional[str]
    NAME_E: Optional[str]
    NAME_F: Optional[str]
    NPELECTIONS: Optional[list]
    PHONEAREA: Optional[str]
    PHONENO: Optional[str]
    PREFILING: Optional[list]
    # RECORD_DATE: Optional[datetime]
    RECORD_STATUS: Optional[int]
    REGION: Optional[list]
    # TYPE: Optional[str]


class MutualFundIssuer(PatchedModel):
    AUDITOR_E: Optional[str]
    AUDITOR_F: Optional[str]
    BUSINESS_ADDRESS_CITY: Optional[str]
    BUSINESS_ADDRESS_CODE: Optional[str]
    BUSINESS_ADDRESS_COUNTRY: Optional[str]
    BUSINESS_ADDRESS_EXT: Optional[str]
    BUSINESS_ADDRESS_FAXAREA: Optional[str]
    BUSINESS_ADDRESS_FAXNO: Optional[str]
    BUSINESS_ADDRESS_PHONEAREA: Optional[str]
    BUSINESS_ADDRESS_PHONENO: Optional[str]
    BUSINESS_ADDRESS_PROV: Optional[str]
    BUSINESS_ADDRESS_STREET1: Optional[str]
    BUSINESS_ADDRESS_STREET2: Optional[str]
    CREATED_ON: Optional[datetime]
    CUSTODIAN_E: Optional[str]
    CUSTODIAN_F: Optional[str]
    DISTRIBUTOR_E: Optional[str]
    DISTRIBUTOR_F: Optional[str]
    FORMDATE: Optional[datetime]
    FORMTYPE: Optional[str]
    FUND_TYPE: Optional[str]
    GROUPNO: Optional[str]
    ISSUERNO: Optional[str]
    LASTUPDATE: Optional[datetime]
    LAW: Optional[str]
    MAILING_ADDRESS_CITY: Optional[str]
    MAILING_ADDRESS_CODE: Optional[str]
    MAILING_ADDRESS_COUNTRY: Optional[str]
    MAILING_ADDRESS_EXT: Optional[str]
    MAILING_ADDRESS_FAXAREA: Optional[str]
    MAILING_ADDRESS_FAXNO: Optional[str]
    MAILING_ADDRESS_PHONEAREA: Optional[str]
    MAILING_ADDRESS_PHONENO: Optional[str]
    MAILING_ADDRESS_PROV: Optional[str]
    MAILING_ADDRESS_STREET1: Optional[str]
    MAILING_ADDRESS_STREET2: Optional[str]
    MANAGER_E: Optional[str]
    MANAGER_F: Optional[str]
    NAME_E: Optional[str]
    NAME_F: Optional[str]
    PREVIOUS_PROFILE_NAME_E: Optional[Union[str, List]]
    # RECORD_DATE: Optional[datetime]
    RECORD_STATUS: Optional[int]
    REPORT: Optional[list]
    RRSP: Optional[str]
    SALESCOMP: Optional[str]
    TRUSTEE_E: Optional[str]
    TRUSTEE_F: Optional[str]
    # TYPE: Optional[str]
    YEDATE: Optional[str]


class OtherFiler(PatchedModel):
    ISSUERNO: Optional[str]
    NAME_E: Optional[str]
    NAME_F: Optional[str]
    CONTACT: Optional[str]
    PHONEAREA: Optional[str]
    PHONENO: Optional[str]
    EXT: Optional[str]
    FAXAREA: Optional[str]
    FAXNO: Optional[str]
    LASTUPDATE: Optional[datetime]
    PREVIOUS_PROFILE_NAME_E: Optional[Union[str, List]]
    # RECORD_DATE: Optional[datetime]
    RECORD_STATUS: Optional[int]
    CREATED_ON: Optional[datetime]
    # JURISDICTION: Optional[str]
    REGION: Optional[list]


class OtherIssuer(PatchedModel):
    ISSUERNO: Optional[str]
    PHONEAREA: Optional[str]
    PHONENO: Optional[str]
    NAME_E: Optional[str]
    NAME_F: Optional[str]
    CONTACT: Optional[str]
    PHONEAREA: Optional[str]
    EXT: Optional[str]
    FAXAREA: Optional[str]
    FAXNO: Optional[str]
    LAW: Optional[str]
    CUSIPCDSNO: Optional[str]
    EXCHANGES: Optional[list]
    FORMTYPE: Optional[str]
    FORMDATE: Optional[datetime]
    YEDATE: Optional[str]
    INDUSTRY: Optional[str]
    REPORT: Optional[list]
    OTCISSUERDETAIL: Optional[list]
    PARTNER_E: Optional[str]
    PARTNER_F: Optional[str]
    AUDITOR_E: Optional[str]
    AUDITOR_F: Optional[str]
    TRANAGENT_E: Optional[str]
    TRANAGENT_F: Optional[str]
    SIZE: Optional[str]
    LASTUPDATE: Optional[datetime]
    SHORT_FORM_PROSPECTUS_ISSUER: Optional[str]
    STOCK_SYMBOL: Optional[str]
    APPSREGISTRATION: Optional[list]
    APPSEXEMPTION: Optional[list]
    NPELECTIONS: Optional[list]
    OFFERINGS: Optional[list]
    SECTYPE: Optional[list]
    PROCEEDS: Optional[str]
    UNDERWRITER_E: Optional[str]
    UNDERWRITER_F: Optional[str]
    UWCOUNSEL: Optional[str]
    UWCONTACT: Optional[str]
    PROMOTER: Optional[str]
    PREFILING: Optional[list]
    LPSELECTIONS: Optional[list]
    FPEDATE: Optional[datetime]
    FPERELATES: Optional[str]
    BUSINESS_ADDRESS_CITY: Optional[str]
    BUSINESS_ADDRESS_CODE: Optional[str]
    BUSINESS_ADDRESS_COUNTRY: Optional[str]
    BUSINESS_ADDRESS_EXT: Optional[str]
    BUSINESS_ADDRESS_FAXAREA: Optional[str]
    BUSINESS_ADDRESS_FAXNO: Optional[str]
    BUSINESS_ADDRESS_PHONEAREA: Optional[str]
    BUSINESS_ADDRESS_PHONENO: Optional[str]
    BUSINESS_ADDRESS_PROV: Optional[str]
    BUSINESS_ADDRESS_STREET1: Optional[str]
    BUSINESS_ADDRESS_STREET2: Optional[str]
    MAILING_ADDRESS_CITY: Optional[str]
    MAILING_ADDRESS_CODE: Optional[str]
    MAILING_ADDRESS_COUNTRY: Optional[str]
    MAILING_ADDRESS_EXT: Optional[str]
    MAILING_ADDRESS_FAXAREA: Optional[str]
    MAILING_ADDRESS_FAXNO: Optional[str]
    MAILING_ADDRESS_PHONEAREA: Optional[str]
    MAILING_ADDRESS_PHONENO: Optional[str]
    MAILING_ADDRESS_PROV: Optional[str]
    MAILING_ADDRESS_STREET1: Optional[str]
    MAILING_ADDRESS_STREET2: Optional[str]
    CREATED_ON: Optional[datetime]
    REGION: Optional[List]
    # JURISDICTION: Optional[str]
    # RECORD_DATE: Optional[datetime]


class CompanyNames(PatchedModel):
    ISSUERNO: Optional[str]
    GROUPNO: Optional[str]
    ID: Optional[str]
    NAME_E: Optional[str]
    NAME_F: Optional[str]
    LASTUPDATE: Optional[datetime]
    CREATED_ON: Optional[datetime]
    TYPE: Optional[str]


###################### END #########################

###################### CONTROL FILE RELATED MODELS ############


class ControlInformation(PatchedModel):
    CTL_PATH: Optional[str]
    ZIP_PATH: Optional[str]
    ZIP_NAME: Optional[str]
    DATE_FOLDER: Optional[str]
    CTL_INFO: Optional[str]
    ACCESSION_NO: Optional[str]
    PROJECT_NO: Optional[str]
    SUBMISSION_NO: Optional[str]
    ISSUERNO: Optional[str]
    CLIENT_FN: Optional[str]
    FILER_TYPE: Optional[str]
    FILING_TYPE: Optional[str]
    DOCUMENT_TYPE: Optional[str]
    NAME_E: Optional[str]
    FILING_DATE: Optional[str]
    PUBLIC_DATE: Optional[str]
    PUBLIC_FLAG: Optional[str]
    FILE_ID: Optional[str]
    CREATED_ON: Optional[datetime]
    UPDATED_ON: Optional[datetime]
    RECORD_STATUS: Optional[str]


############## File Related Models #################


class FileID(PatchedModel):
    FILE_ID: Optional[str]
    ISSUERNO: Optional[str]
    GROUPNO: Optional[str]
    FOLDER_YEAR: Optional[str]
    FOLDER_MONTH: Optional[str]
    FOLDER_DAY: Optional[str]


class FileText(FileID):
    DOCUMENT_DESC: Optional[str]
    PAGES: Optional[List[str]]


class ExtractedTable(FileID):
    ORIGINAL_TABLE: Optional[List[dict]]
    EDITED_TABLE_1: Optional[List[dict]]
    EDITED_TABLE_2: Optional[List[dict]]
    UNITS: Optional[str]
    CURRENCY: Optional[str]
    PAGE_NO: Optional[int]
    PAGE_TYPE: Optional[str]
    CREATED_ON: Optional[datetime]


class DetectedPage(FileID):
    BALANCE_SHEET: Optional[int]
    CASHFLOW_STATEMENT: Optional[int]
    INCOME_STATEMENT: Optional[int]
    SEGMENTATION_PAGE: Optional[int]
    NOTES_PAGE: Optional[int]
    OTHER: Optional[int]


class PageInfo(PatchedModel):
    PAGE_COUNT: Optional[int]
    DETECTED_PAGES: Optional[DetectedPage]


class PageDescription(FileID):
    CREATED_ON: Optional[datetime]
    PAGE_INFO: PageInfo = Field(default=PageInfo())


class FileInfo(FileID):
    # Elastic search data
    CREATED_ON: Optional[datetime]
    RECORD_DATE: Optional[datetime]
    FINANCIAL_END_DATE: Optional[datetime]
    PAGE_COUNT: Optional[int]
    CTL_INFO: Optional[str]
    ACCESSION_NO: Optional[str]
    DOCUMENT_DESC: Optional[str]
    DOCUMENT_SUBTYPE: Optional[str]
    DOCUMENT_TYPE: Optional[str]
    FILER_DESC: Optional[str]
    FILER_TYPE: Optional[str]
    FILING_DATE: Optional[datetime]
    FILING_DESC: Optional[str]
    FILING_TYPE: Optional[str]
    PROJECT_NO: Optional[str]
    PUBLIC_DATE: Optional[datetime]
    SUBMISSION_NO: Optional[str]
    THIRD_PARTY_FILER_INFO_NAME_E: Optional[str]

    # Private Members holding meta info
    _file_text: Optional[FileText] = PrivateAttr(default=None)
    _page_description: Optional[PageDescription] = PrivateAttr(default=None)
    _extracted_tables: List[ExtractedTable] = PrivateAttr(default_factory=list)

    _other_issuer: Optional[OtherIssuer] = PrivateAttr(default=None)
    _mutual_fund_group: Optional[MutualFundGroup] = PrivateAttr(default=None)
    _mutual_fund_issuer: Optional[List[MutualFundIssuer]] = PrivateAttr(
        default_factory=list
    )
    _other_filer: Optional[OtherFiler] = PrivateAttr(default=None)
    _control_information: Optional[ControlInformation] = PrivateAttr(default=None)
    _company_names: Optional[CompanyNames] = PrivateAttr(default=None)

    # Copy all FileID attributes from variable _from to variable _to
    def __sync__(self, _from: FileID, _to: FileID):
        for attr in FileID.__fields__:
            setattr(_to, attr, getattr(_from, attr))
        # These are the updated attributes
        # _to.FILE_ID      = _from.FILE_ID
        # _to.ISSUERNO    = _from.ISSUERNO
        # _to.FOLDER_YEAR  = _from.FOLDER_YEAR
        # _to.FOLDER_MONTH = _from.FOLDER_MONTH
        # _to.FOLDER_DAY   = _from.FOLDER_DAY

    def sync(self):
        if self._file_text:
            self.__sync__(self, self._file_text)
        for idx, _ in enumerate(self._extracted_tables):
            self.__sync__(self, self._extracted_tables[idx])
        if self._page_description:
            self.__sync__(self, self._page_description)
            self.__sync__(self, self._page_description.PAGE_INFO.DETECTED_PAGES)

    # Public Interface for private members. They are not part of the final schema
    # Extracted Text
    @property
    def FILE_TEXT(self) -> Optional[FileText]:
        self.sync()
        return self._file_text

    @FILE_TEXT.setter
    def FILE_TEXT(self, value: Optional[FileText]):
        self._file_text = value
        self.sync()

    # File Description Relation
    @property
    def PAGE_DESCRIPTION(self) -> Optional[PageDescription]:
        self.sync()
        return self._page_description

    @PAGE_DESCRIPTION.setter
    def PAGE_DESCRIPTION(self, value: Optional[PageDescription]):
        self._page_description = value
        self.sync()

    # # Extracted Tables
    @property
    def EXTRACTED_TABLES(self) -> List[ExtractedTable]:
        self.sync()
        return self._extracted_tables

    @EXTRACTED_TABLES.setter
    def EXTRACTED_TABLES(self, value: List[ExtractedTable]):
        self._extracted_tables = value
        self.sync()

    # ### Company Detail Models ###
    # # Other Issuer
    @property
    def OTHER_ISSUER(self) -> Optional[OtherIssuer]:
        return self._other_issuer

    @OTHER_ISSUER.setter
    def OTHER_ISSUER(self, value: Optional[OtherIssuer]):
        self._other_issuer = value

    # #Mutual Fund Group
    @property
    def MUTUAL_FUND_GROUP(self) -> Optional[MutualFundGroup]:
        return self._mutual_fund_group

    @MUTUAL_FUND_GROUP.setter
    def MUTUAL_FUND_GROUP(self, value: Optional[MutualFundGroup]):
        self._mutual_fund_group = value

    # #Mutual Fund Issuer
    @property
    def MUTUAL_FUND_ISSUER(self) -> Optional[List[MutualFundIssuer]]:
        return self._mutual_fund_issuer

    @MUTUAL_FUND_ISSUER.setter
    def MUTUAL_FUND_ISSUER(self, value: Optional[List[MutualFundIssuer]]):
        self._mutual_fund_issuer = value

    # # Other Filer
    @property
    def OTHER_FILER(self) -> Optional[OtherFiler]:
        return self._other_filer

    @OTHER_FILER.setter
    def OTHER_FILER(self, value: Optional[OtherFiler]):
        self._other_filer = value

    # # Control File
    @property
    def CONTROL_INFORMATION(self) -> Optional[ControlInformation]:
        return self._control_information

    @CONTROL_INFORMATION.setter
    def CONTROL_INFORMATION(self, value: Optional[ControlInformation]):
        self._control_information = value

    # # Company Names
    @property
    def COMPANY_NAMES(self) -> Optional[CompanyNames]:
        return self._company_names

    @COMPANY_NAMES.setter
    def COMPANY_NAMES(self, value: Optional[CompanyNames]):
        self._company_names = value

    # Enabling the use of private attributes
    class Config:
        underscore_attrs_are_private = True


class FileList(PatchedModel):
    """
    Saves multiple objects of FileInfo in a list if required
    """

    each_file: List[FileInfo] = []


###################### END #########################


###### Config Files Models ######


class SedarModel(PatchedModel):
    extraction_documents: List
    sedar_documents: List
    months: List
    currencies: List
    units: List
    page_types: List
    attribute_list: List
    name_pattern: str
    upload_minio_documents: List
