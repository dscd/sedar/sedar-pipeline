import json
from datetime import datetime
from enum import auto
from functools import lru_cache
from typing import List

import elasticsearch
import requests
from elasticsearch import Elasticsearch
import elasticsearch.helpers

from config.api import AutoEnum
from config.env import environ, sedar
from config.models import CompanyNames, ControlInformation


@lru_cache
def get_es_client():
    return Elasticsearch(
        hosts=environ().ES_HOST,
        http_auth=(environ().ES_USER, environ().ES_PASS),
        timeout=1200,
        max_retries=10,
        retry_on_timeout=True,
    )


class Indices(AutoEnum):
    sedar_prod_file_info = auto()
    sedar_prod_file_text = auto()
    sedar_prod_page_description = auto()
    sedar_prod_extracted_tables = auto()
    sedar_prod_other_issuer = auto()
    sedar_prod_other_filer = auto()
    sedar_prod_mutual_fund_group = auto()
    sedar_prod_mutual_fund_issuer = auto()
    sedar_prod_control_information = auto()
    sedar_prod_company_names = auto()
    sedar_lookup_table = auto()


def ls_ctrl_information_objs() -> List[ControlInformation]:
    """
    Return a list of ControlInformation objs from the ctrl pipeline index
    """
    control_table = elasticsearch.helpers.scan(
        get_es_client(),
        query={"query": {"match_all": {}}},
        index=Indices.sedar_prod_control_information.value,
    )

    return [ControlInformation(**j.get("_source")).FILE_ID for j in control_table]


def create_kibana_index(index_pattern: str, ES_USR: str, ES_PSW: str, ES_HOST: str) -> str:
    """
    params:
        ES_USR: ElasticSearch User
        ES_PSW: ElasticSEarch Password
        url: sedar config variable: elastic_search_url
    """
    headers = {"Content-Type": "application/json", "kbn-xsrf": "true"}
    data = {
        "override": True,
        "refresh_fields": False,
        "index_pattern": {"title": index_pattern},
    }

    return requests.post(
        ES_HOST,
        headers=headers,
        data=json.dumps(data),  # could add more fields, perform mappings.
        auth=(ES_USR, ES_PSW),
    ).json()


def create_es_indice(index_name: str) -> None:

    print(index_name)
    if index_name == Indices.sedar_prod_extracted_tables.value:
        print("Extracted tables")
        mappings = {
            "properties": {
                "ORIGINAL_TABLE": {"type": "nested"},
                "EDITED_TABLE_1": {"type": "nested"},
                "EDITED_TABLE_2": {"type": "nested"},
            }
        }
    elif index_name == Indices.sedar_prod_page_description.value:
        mappings = {"properties": {"PAGE_INFO": {"type": "nested"}}}
    else:
        mappings = None
    response = get_es_client().indices.create(index=index_name, mappings=mappings)
    print(f"Created new ElasticSearch index {index_name}")
    if response["acknowledged"]:
        create_kibana_index(
            index_name,
            ES_USR=environ().ES_USER,
            ES_PSW=environ().ES_PASS,
            ES_HOST=environ().ES_HOST,
        )
    return None


def generate_search_query(index_name: str, record: dict) -> dict:
    """
    Based on the index a search query is generated to identify if
    record exist
    """
    query = {}

    if index_name in [
        Indices.sedar_prod_other_issuer.value,
        Indices.sedar_prod_other_filer.value,
        Indices.sedar_prod_mutual_fund_issuer.value,
    ]:
        query = {"query": {"match": {"ISSUERNO": record["ISSUERNO"]}}}
    elif index_name == Indices.sedar_prod_mutual_fund_group.value:
        query = {"query": {"match": {"GROUPNO": record["GROUPNO"]}}}
    else:
        if record["ISSUERNO"] != None:
            query = {"query": {"match": {"ISSUERNO": record["ISSUERNO"]}}}
        else:
            query = {"query": {"match": {"GROUPNO": record["GROUPNO"]}}}

    return query


def update_submission_record(index_name: str, query: dict, record: dict) -> bool:
    """
    Check if submission details exists.
    If it does exists, check if it is the latest based on lastupdate field
    If it does not exist create a record
    """

    results = elasticsearch.helpers.scan(
        client=get_es_client(), query=query, index=index_name
    )
    hits = [x["_source"] for x in results]
    if hits != []:
        db_lastupdate = datetime.strptime(hits[0]["LASTUPDATE"], "%Y-%m-%dT%H:%M:%S")
        if record["LASTUPDATE"] > db_lastupdate:
            return True
        elif record["LASTUPDATE"] < db_lastupdate:
            return False
        else:
            return True
    else:
        return True


class EsSerializer:

    """
    Save any dict to ES index.
    Usage:

    serial
    """

    def serialize(self, index_name, data_dict):
        if not get_es_client().indices.exists(index=index_name):
            create_es_indice(index_name)
        get_es_client().index(index=index_name, body=data_dict)


class EsDeSerializer:
    """
    Delete entry from ES index.
    """

    def deserialize(self, index_name, query):
        if not get_es_client().indices.exists(index=index_name):
            create_es_indice(index_name)
        get_es_client().delete_by_query(index=index_name, body=query)


def create_company_name_index():
    ## 1. Identify unique issuernos and groupnos from page description
    query = {"query": {"match_all": {}}}
    results = elasticsearch.helpers.scan(
        client=get_es_client(),
        index=Indices.sedar_prod_page_description.value,
        query=query,
    )

    issuernos = []
    groupnos = []

    for item in results:

        record = item["_source"]
        if record["ISSUERNO"] != None and record["ISSUERNO"] not in issuernos:
            issuernos.append(record["ISSUERNO"])

        if record["GROUPNO"] != None and record["GROUPNO"] not in groupnos:
            groupnos.append(record["GROUPNO"])

    ## 2. Serialize the  other filer and other issuer entries
    indices = [
        Indices.sedar_prod_other_issuer.value,
        Indices.sedar_prod_other_filer.value,
    ]
    query = {"query": {"match_all": {}}}
    other_results = elasticsearch.helpers.scan(
        client=get_es_client(), index=indices, query=query
    )

    for item in other_results:

        record = item["_source"]
        if "ISSUERNO" in record and record["ISSUERNO"] in issuernos:
            company_name = CompanyNames(**record)
            company_name.CREATED_ON = datetime.today()
            company_name.ID = company_name.ISSUERNO
            company_name.TYPE = (
                "OTHER_ISSUER"
                if item["_index"] == Indices.sedar_prod_other_issuer.value
                else "OTHER_FILER"
            )

            EsSerializer().serialize(
                Indices.sedar_prod_company_names.value, company_name.dict()
            )

    ## 3. Serialize the mutual fund group entries
    indices = [Indices.sedar_prod_mutual_fund_group.value]
    query = {"query": {"match_all": {}}}
    group_results = elasticsearch.helpers.scan(
        client=get_es_client(), index=indices, query=query
    )

    for item in group_results:

        record = item["_source"]
        if "GROUPNO" in record and record["GROUPNO"] in groupnos:
            company_name = CompanyNames(**record)
            company_name.CREATED_ON = datetime.today()
            company_name.ID = company_name.GROUPNO
            company_name.TYPE = "MUTUAL_FUND_GROUP"

            EsSerializer().serialize(
                Indices.sedar_prod_company_names.value, company_name.dict()
            )

    return None


class FileInfoSerializer:
    def __init__(self):
        self.es_serializer = EsSerializer()
        self.es_deserializer = EsDeSerializer()

    def serialize_by_fileid(self, file_info_ex) -> bool:
        """
        Save File Info data to ES
        """

        self.es_serializer.serialize(
            index_name=Indices.sedar_prod_file_info.value,
            data_dict=file_info_ex.FILE_INFO.dict(),
        )

        if file_info_ex.FILE_INFO.FILE_TEXT:
            self.es_serializer.serialize(
                index_name=Indices.sedar_prod_file_text.value,
                data_dict=file_info_ex.FILE_INFO.FILE_TEXT.dict(),
            )

        if file_info_ex.FILE_INFO.PAGE_DESCRIPTION:
            self.es_serializer.serialize(
                index_name=Indices.sedar_prod_page_description.value,
                data_dict=file_info_ex.FILE_INFO.PAGE_DESCRIPTION.dict(),
            )

        if file_info_ex.FILE_INFO.EXTRACTED_TABLES:
            for table in file_info_ex.FILE_INFO.EXTRACTED_TABLES:
                self.es_serializer.serialize(
                    index_name=Indices.sedar_prod_extracted_tables.value,
                    data_dict=table.dict(),
                )

        return True

    def deserialize_by_fileid(self, query) -> bool:
        """
        Remove entries from ES
        """

        # Delete File Info data
        self.es_deserializer.deserialize(
            index_name=Indices.sedar_prod_file_info.value, query=query
        )

        # Delete File text data
        self.es_deserializer.deserialize(
            index_name=Indices.sedar_prod_file_text.value, query=query
        )

        # Delete Page Description data
        self.es_deserializer.deserialize(
            index_name=Indices.sedar_prod_page_description.value, query=query
        )

        # Delete Extracted Tables data
        self.es_deserializer.deserialize(
            index_name=Indices.sedar_prod_extracted_tables.value, query=query
        )

        return True

    def serialize_by_uniqueno_lastupdate(self, file_info_ex) -> bool:
        """
        Add issuer / group details based on lastupdate field.
        If new issuer / group appears add the details in respective index
        If existing issuer / group appears only update records if lastupdate is latest date
        """

        # If submission type == OTHER_ISSUER
        # insert new record if user does not exist or update existing record based on lastupdate date
        if file_info_ex.FILE_INFO.OTHER_ISSUER:
            query = generate_search_query(
                index_name=Indices.sedar_prod_other_issuer.value,
                record=file_info_ex.FILE_INFO.OTHER_ISSUER.dict(),
            )

            if update_submission_record(
                index_name=Indices.sedar_prod_other_issuer.value,
                query=query,
                record=file_info_ex.FILE_INFO.OTHER_ISSUER.dict(),
            ):

                get_es_client().delete_by_query(
                    index=Indices.sedar_prod_other_issuer.value,
                    body=query,
                    refresh=True,
                )

                self.es_serializer.serialize(
                    index_name=Indices.sedar_prod_other_issuer.value,
                    data_dict=file_info_ex.FILE_INFO.OTHER_ISSUER.dict(),
                )

        # If submission type == OTHER_FILER
        # insert new record if user does not exist or update existing record based on lastupdate date
        if file_info_ex.FILE_INFO.OTHER_FILER:
            query = generate_search_query(
                index_name=Indices.sedar_prod_other_filer.value,
                record=file_info_ex.FILE_INFO.OTHER_FILER.dict(),
            )

            if update_submission_record(
                index_name=Indices.sedar_prod_other_filer.value,
                query=query,
                record=file_info_ex.FILE_INFO.OTHER_FILER.dict(),
            ):

                get_es_client().delete_by_query(
                    index=Indices.sedar_prod_other_filer.value, body=query, refresh=True
                )

                self.es_serializer.serialize(
                    index_name=Indices.sedar_prod_other_filer.value,
                    data_dict=file_info_ex.FILE_INFO.OTHER_FILER.dict(),
                )

        # If submission type == MUTUAL_FUND_GROUP
        # insert new record if user does not exist or update existing record based on lastupdate date
        if file_info_ex.FILE_INFO.MUTUAL_FUND_GROUP:
            query = generate_search_query(
                index_name=Indices.sedar_prod_mutual_fund_group.value,
                record=file_info_ex.FILE_INFO.MUTUAL_FUND_GROUP.dict(),
            )

            if update_submission_record(
                index_name=Indices.sedar_prod_mutual_fund_group.value,
                query=query,
                record=file_info_ex.FILE_INFO.MUTUAL_FUND_GROUP.dict(),
            ):

                get_es_client().delete_by_query(
                    index=Indices.sedar_prod_mutual_fund_group.value,
                    body=query,
                    refresh=True,
                )

                self.es_serializer.serialize(
                    index_name=Indices.sedar_prod_mutual_fund_group.value,
                    data_dict=file_info_ex.FILE_INFO.MUTUAL_FUND_GROUP.dict(),
                )

        # If submission type == MUTUAL_FUND_ISSUER
        # insert new record if user does not exist or update existing record based on lastupdate date
        if file_info_ex.FILE_INFO.MUTUAL_FUND_ISSUER:
            for mfi in file_info_ex.FILE_INFO.MUTUAL_FUND_ISSUER:
                query = generate_search_query(
                    index_name=Indices.sedar_prod_mutual_fund_issuer.value,
                    record=mfi.dict(),
                )

                if update_submission_record(
                    index_name=Indices.sedar_prod_mutual_fund_issuer.value,
                    query=query,
                    record=mfi.dict(),
                ):

                    get_es_client().delete_by_query(
                        index=Indices.sedar_prod_mutual_fund_issuer.value,
                        body=query,
                        refresh=True,
                    )

                    self.es_serializer.serialize(
                        index_name=Indices.sedar_prod_mutual_fund_issuer.value,
                        data_dict=mfi.dict(),
                    )

        # If tables are extracted from the user document update the COMPANY_NAMES index
        # insert new record if user does not exist or update existing record based on lastupdate date
        if (
            file_info_ex.FILE_INFO.COMPANY_NAMES
            and file_info_ex.FILE_INFO.PAGE_DESCRIPTION != None
        ):
            query = generate_search_query(
                index_name=Indices.sedar_prod_company_names.value,
                record=file_info_ex.FILE_INFO.COMPANY_NAMES.dict(),
            )

            if update_submission_record(
                index_name=Indices.sedar_prod_company_names.value,
                query=query,
                record=file_info_ex.FILE_INFO.COMPANY_NAMES.dict(),
            ):

                get_es_client().delete_by_query(
                    index=Indices.sedar_prod_company_names.value,
                    body=query,
                    refresh=True,
                )

                self.es_serializer.serialize(
                    index_name=Indices.sedar_prod_company_names.value,
                    data_dict=file_info_ex.FILE_INFO.COMPANY_NAMES.dict(),
                )

        return True

    def deserialize(self, file_id_query=None, company_clean=False) -> bool:
        """
        Remove entries from ES for the Retention policy
        file_id_query =   {"query" : {"match" : {"FILE_ID" : FILE_ID}}}

        """
        if file_id_query is not None:
            # Delete File Info data
            self.es_deserializer.deserialize(
                index_name=Indices.sedar_prod_file_info.value, query=file_id_query
            )

            # Delete File text data
            self.es_deserializer.deserialize(
                index_name=Indices.sedar_prod_file_text.value, query=file_id_query
            )

            # Delete Page Description data
            self.es_deserializer.deserialize(
                index_name=Indices.sedar_prod_page_description.value,
                query=file_id_query,
            )

            # Delete Extracted Tables data
            self.es_deserializer.deserialize(
                index_name=Indices.sedar_prod_extracted_tables.value,
                query=file_id_query,
            )

        if company_clean:
            ## Delete sedar_company_name index
            company_query = {"query": {"match_all": {}}}
            self.es_deserializer.deserialize(
                index_name=Indices.sedar_prod_company_names.value, query=company_query
            )
            ## Recreate sedar_company_name index
            create_company_name_index()

        return True
