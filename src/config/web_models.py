import hashlib
import json
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel


class FileID(BaseModel):
    FILE_ID: Optional[str]
    ISSUERNO: Optional[str]
    GROUPNO: Optional[str]
    FOLDER_YEAR: Optional[str]
    FOLDER_MONTH: Optional[str]
    FOLDER_DAY: Optional[str]


class ExtractedTable(FileID):
    ORIGINAL_TABLE: Optional[List[dict]]
    EDITED_TABLE_1: Optional[List[dict]]
    EDITED_TABLE_2: Optional[List[dict]]
    UNITS: Optional[str]
    CURRENCY: Optional[str]
    PAGE_NO: Optional[int]
    PAGE_TYPE: Optional[str]

class CompanyDetails(BaseModel):
    NAME_E: Optional[str]
    ISSUERNO: Optional[str]
    LAW: Optional[str]
    CUSIPCDSNO: Optional[str]
    EXCHANGES: Optional[list]
    FORMTYPE: Optional[str]
    FORMDATE: Optional[datetime]
    YEDATE: Optional[str]
    INDUSTRY: Optional[str]
    REPORT: Optional[list]
    OTCISSUERDETAIL: Optional[list]
    SIZE: Optional[str]
    STOCK_SYMBOL: Optional[str]
    TYPE: Optional[list]


class CompanyDetailsKeys(BaseModel):
    NAME_E = "Company Name"
    ISSUERNO = "Issuer No."
    LAW = "Form Dates"
    CUSIPCDSNO = "CUSIP"
    EXCHANGES = "Exchanges"
    FORMTYPE = "Form Type"
    FORMDATE = "Form Dates"
    YEDATE = "Year End Date"
    INDUSTRY = "Industry"
    REPORT = "Report"
    OTCISSUERDETAIL = "OTC Issuer Detail"
    SIZE = "Size of Issuer"
    STOCK_SYMBOL = "Stock Symbol"
    TYPE = "Type"


class CompanyDetails_FR(BaseModel):
    NAME_F: Optional[str]
    ISSUERNO: Optional[str]
    LAW: Optional[str]
    CUSIPCDSNO: Optional[str]
    EXCHANGES: Optional[list]
    FORMTYPE: Optional[str]
    FORMDATE: Optional[datetime]
    YEDATE: Optional[str]
    INDUSTRY: Optional[str]
    REPORT: Optional[list]
    OTCISSUERDETAIL: Optional[list]
    SIZE: Optional[str]
    STOCK_SYMBOL: Optional[str]
    TYPE: Optional[list]


class CompanyDetailsKeys_FR(BaseModel):
    NAME_F = "Nom de la société"
    ISSUERNO = "Numéro d' émetteur"
    LAW = "Dates du formulaire"
    CUSIPCDSNO = "CUSIP"
    EXCHANGES = "Bourses"
    FORMTYPE = "Type de formulaire"
    FORMDATE = "Dates du formulaire"
    YEDATE = "Date de fin d'année"
    INDUSTRY = "Industrie"
    REPORT = "Rapport"
    OTCISSUERDETAIL = "Détail de l'émetteur de gré à gré"
    SIZE = "Taille de l'émetteur"
    STOCK_SYMBOL = "Symbole de l'action"
    TYPE = "Type"
    FPEDATE = "Date de fin de la période financière"
    FPERELATES = "Date relative à la période financière"


class ContactInformation(BaseModel):
    CONTACT: Optional[str]
    PHONEAREA: Optional[str]
    PHONENO: Optional[str]
    EXT: Optional[str]
    FAXAREA: Optional[str]
    FAXNO: Optional[str]


class ContactInformationKeys(BaseModel):
    CONTACT = "Contact Name"
    PHONEAREA = "Phone Area Code"
    PHONENO = "Phone Number"
    EXT = "Extension"
    FAXAREA = "Fax Area Code"
    FAXNO = "Fax Number"


class ContactInformationKeys_FR(BaseModel):
    CONTACT = "Nom du contact"
    PHONEAREA = "Indicatif régional du téléphone"
    PHONENO = "Numéro de téléphone"
    EXT = "Poste"
    FAXAREA = "Indicatif régional de télécopie"
    FAXNO = "Numéro de fax"


class RecordDetails(BaseModel):
    LASTUPDATE: Optional[datetime]
    SHORT_FORM_PROSPECTUS_ISSUER: Optional[str]
    CREATED_ON: Optional[datetime]
    RECORD_DATE: Optional[datetime]


class RecordDetailsKeys(BaseModel):
    LASTUPDATE = "Last Update"
    SHORT_FORM_PROSPECTUS_ISSUER = "Short Form Propectus Issuer"
    CREATED_ON = "Created On"
    RECORD_DATE = "Record Date"


class RecordDetailsKeys_FR(BaseModel):
    LASTUPDATE = "Dernière mise à jour"
    SHORT_FORM_PROSPECTUS_ISSUER = "Émetteur du prospectus simplifié"
    CREATED_ON = "Créé le"
    RECORD_DATE = "Date d'enregistrement"


class OtherInformation(BaseModel):
    PARTNER_E: Optional[str]
    AUDITOR_E: Optional[str]
    TRANAGENT_E: Optional[str]
    APPSREGISTRATION: Optional[list]
    APPSEXEMPTION: Optional[list]
    NPELECTIONS: Optional[list]
    OFFERINGS: Optional[list]
    SECTYPE: Optional[list]
    PROCEEDS: Optional[str]
    UNDERWRITER_E: Optional[str]
    UWCOUNSEL: Optional[str]
    UWCONTACT: Optional[str]
    PROMOTER: Optional[str]
    PREFILING: Optional[list]
    LPSELECTIONS: Optional[list]


class OtherInformationKeys(BaseModel):
    PARTNER_E = "Partner"
    AUDITOR_E = "Auditor"
    TRANAGENT_E = "Transfer Agent"
    APPSREGISTRATION = "Registration Application"
    APPSEXEMPTION = "Application for Exemption"
    NPELECTIONS = "National Policy Elections"
    OFFERINGS = "Offerings"
    SECTYPE = "Security Type"
    PROCEEDS = "Proceeds"
    UNDERWRITER_E = "Underwriter"
    UWCOUNSEL = "Underwriter Counsel"
    UWCONTACT = "Underwriter Contact"
    PROMOTER = "Promoter"
    PREFILING = "Prefiling"
    LPSELECTIONS = "Local Policy Statement"


class OtherInformation_FR(BaseModel):
    PARTNER_F: Optional[str]
    AUDITOR_F: Optional[str]
    TRANAGENT_F: Optional[str]
    APPSREGISTRATION: Optional[list]
    APPSEXEMPTION: Optional[list]
    NPELECTIONS: Optional[list]
    OFFERINGS: Optional[list]
    SECTYPE: Optional[list]
    PROCEEDS: Optional[str]
    UNDERWRITER_F: Optional[str]
    UWCOUNSEL: Optional[str]
    UWCONTACT: Optional[str]
    PROMOTER: Optional[str]
    PREFILING: Optional[list]
    LPSELECTIONS: Optional[list]


class OtherInformationKeys_FR(BaseModel):
    PARTNER_F = "Partenaire français"
    AUDITOR_F = "Auditeur français"
    TRANAGENT_F = "Agent de transfert français"
    APPSREGISTRATION = "Demande d'enregistrement"
    APPSEXEMPTION = "Demande d'exemption"
    NPELECTIONS = "Élections politiques nationales"
    OFFERINGS = "Offerings"
    SECTYPE = "Type de titre"
    PROCEEDS = "Produits"
    UNDERWRITER_F = "Souscripteur français"
    UWCOUNSEL = "Underwriter Counsel"
    UWCONTACT = "Contact du souscripteur"
    PROMOTER = "Promoteur"
    PREFILING = "Préfiling"
    LPSELECTIONS = "Déclaration de politique locale"


class BusinessAddress(BaseModel):
    BUSINESS_ADDRESS_CITY: Optional[str]
    BUSINESS_ADDRESS_CODE: Optional[str]
    BUSINESS_ADDRESS_COUNTRY: Optional[str]
    BUSINESS_ADDRESS_EXT: Optional[str]
    BUSINESS_ADDRESS_FAXAREA: Optional[str]
    BUSINESS_ADDRESS_FAXNO: Optional[str]
    BUSINESS_ADDRESS_PHONEAREA: Optional[str]
    BUSINESS_ADDRESS_PHONENO: Optional[str]
    BUSINESS_ADDRESS_PROV: Optional[str]
    BUSINESS_ADDRESS_STREET1: Optional[str]
    BUSINESS_ADDRESS_STREET2: Optional[str]


class BusinessAddressKeys(BaseModel):
    BUSINESS_ADDRESS_CITY = "City"
    BUSINESS_ADDRESS_CODE = "Postal Code"
    BUSINESS_ADDRESS_COUNTRY = "Country"
    BUSINESS_ADDRESS_EXT = "Phone Extension Number"
    BUSINESS_ADDRESS_FAXAREA = "Fax Area Code"
    BUSINESS_ADDRESS_FAXNO = "Fax Number"
    BUSINESS_ADDRESS_PHONEAREA = "Phone Area Code"
    BUSINESS_ADDRESS_PHONENO = "Phone Number"
    BUSINESS_ADDRESS_PROV = "Province"
    BUSINESS_ADDRESS_STREET1 = "Street 1"
    BUSINESS_ADDRESS_STREET2 = "Street 2"


class BusinessAddressKeys_FR(BaseModel):
    BUSINESS_ADDRESS_CITY = "Ville"
    BUSINESS_ADDRESS_CODE = "Code postal"
    BUSINESS_ADDRESS_COUNTRY = "Pays"
    BUSINESS_ADDRESS_EXT = "Numéro de poste téléphonique"
    BUSINESS_ADDRESS_FAXAREA = "Indicatif régional de télécopie"
    BUSINESS_ADDRESS_FAXNO = "Numéro de fax"
    BUSINESS_ADDRESS_PHONEAREA = "Code régional du téléphone"
    BUSINESS_ADDRESS_PHONENO = "Numéro de téléphone"
    BUSINESS_ADDRESS_PROV = "Province"
    BUSINESS_ADDRESS_STREET1 = "Rue 1"
    BUSINESS_ADDRESS_STREET2 = "Rue 2"


class MailingAddress(BaseModel):
    MAILING_ADDRESS_CITY: Optional[str]
    MAILING_ADDRESS_CODE: Optional[str]
    MAILING_ADDRESS_COUNTRY: Optional[str]
    MAILING_ADDRESS_EXT: Optional[str]
    MAILING_ADDRESS_FAXAREA: Optional[str]
    MAILING_ADDRESS_FAXNO: Optional[str]
    MAILING_ADDRESS_PHONEAREA: Optional[str]
    MAILING_ADDRESS_PHONENO: Optional[str]
    MAILING_ADDRESS_PROV: Optional[str]
    MAILING_ADDRESS_STREET1: Optional[str]
    MAILING_ADDRESS_STREET2: Optional[str]


class MailingAddressKeys(BaseModel):
    MAILING_ADDRESS_CITY = "City"
    MAILING_ADDRESS_CODE = "Postal Code"
    MAILING_ADDRESS_COUNTRY = "Country"
    MAILING_ADDRESS_EXT = "Phone Extension Number"
    MAILING_ADDRESS_FAXAREA = "Fax Area Code"
    MAILING_ADDRESS_FAXNO = "Fax Number"
    MAILING_ADDRESS_PHONEAREA = "Phone Area Code"
    MAILING_ADDRESS_PHONENO = "Phone Number"
    MAILING_ADDRESS_PROV = "Province"
    MAILING_ADDRESS_STREET1 = "Street 1"
    MAILING_ADDRESS_STREET2 = "Street 2"


class MailingAddressKeys_FR(BaseModel):
    MAILING_ADDRESS_CITY = "Ville"
    MAILING_ADDRESS_CODE = "Code postal"
    MAILING_ADDRESS_COUNTRY = "Pays"
    MAILING_ADDRESS_EXT = "Numéro de poste téléphonique"
    MAILING_ADDRESS_FAXAREA = "Indicatif régional de télécopie"
    MAILING_ADDRESS_FAXNO = "Numéro de fax"
    MAILING_ADDRESS_PHONEAREA = "Code régional du téléphone"
    MAILING_ADDRESS_PHONENO = "Numéro de téléphone"
    MAILING_ADDRESS_PROV = "Province"
    MAILING_ADDRESS_STREET1 = "Rue 1"
    MAILING_ADDRESS_STREET2 = "Rue 2"


class ExtractedAPITableContent(BaseModel):
    ORIGINAL_TABLE: Optional[List[dict]]
    ORIGINAL_TABLE_MD5: Optional[str]
    EDITED_TABLE_1: Optional[List[dict]]
    EDITED_TABLE_1_MD5: Optional[str]
    EDITED_TABLE_2: Optional[List[dict]]
    EDITED_TABLE_2_MD5: Optional[str]

    def MD5_hash(self, data: list):
        a = json.dumps(data, sort_keys=True)
        return hashlib.md5(a.encode("utf-8")).hexdigest()

    def __init__(self, **data) -> None:
        super().__init__(**data)
        self.ORIGINAL_TABLE_MD5 = self.MD5_hash(self.ORIGINAL_TABLE)
        self.EDITED_TABLE_1_MD5 = self.MD5_hash(self.EDITED_TABLE_1)
        self.EDITED_TABLE_2_MD5 = self.MD5_hash(self.EDITED_TABLE_2)


class PageDetails(BaseModel):
    BALANCE_SHEET: Optional[int]
    CASHFLOW_STATEMENT: Optional[int]
    INCOME_STATEMENT: Optional[int]


class ExtractedAPITable(BaseModel):
    FILE_ID: Optional[str]
    ISSUERNO: Optional[str]
    GROUPNO: Optional[str]
    UNITS: Optional[str]
    CURRENCY: Optional[str]
    BALANCE_SHEET: Optional[List[dict]]
    INCOME_STATEMENT: Optional[List[dict]]
    CASHFLOW_STATEMENT: Optional[List[dict]]
