/**
 * @param  {} jsonData - json to parse from the API response 
 */
function setCompanyDetails(jsonData, translateDict) {
	if (jsonData != null && translateDict!= null) {
		const companyDetails = jsonData[translateDict['Company Details']];
		const contactInformation = jsonData[translateDict['Contact Information']];
		const recordDetails = jsonData[translateDict['Record Details']];
		const otherInfo = jsonData[translateDict['Other Information']];

		// Set Company Details
		if (companyDetails != null && companyDetails != undefined) {
			setDetailsData(document.getElementById('company-details'), companyDetails);
		}

		// Set Contact Details
		if (recordDetails != null && recordDetails != undefined) {
			setDetailsData(document.getElementById('record-details'), recordDetails);
		}

		// Set Other Information
		if (contactInformation != null && contactInformation != undefined) {
			setDetailsData(document.getElementById('contact-information'), contactInformation);
		}

		// Set Record Detais
		if (otherInfo != null && otherInfo != undefined) {
			setDetailsData(document.getElementById('other-information'), otherInfo);
		}
	}
}
/**
 * @param  {} table - HTML table to add data to
 * @param  {} resp - Json response to parse and add to the table
 */
function setDetailsData(table, resp) {
	table.innerHTML = '';
	for (let obj in resp) {
		var row = table.insertRow();
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		cell1.innerHTML = obj;
		cell1.style.fontWeight = '500';
		cell1.style.width = '35%';
		cell1.style.padding = '5px';

		cell2.innerHTML = resp[obj] == null ?  '-': resp[obj];
		cell2.style.width = '65%';
		cell2.style.padding = '5px';
	}
}
