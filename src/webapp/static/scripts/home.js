//Global Vars
var timer;

//Main Function
function loadData(details_and_location = true, statements = true, only_table = false) {
	//    Load Details and location	
	if (details_and_location) {
		getCompanyDetails(getDropdownSelectedValues('company'));
	}
	if (statements && getDropdownSelectedValues('date')){
		getCompanyPDFDetails(getDropdownSelectedValues('date'));
	    getExtractedTables(getDropdownSelectedValues('date'))
	}
}

// //Open Issue
// function openIssueWithData(message=null){
//     var company = getDropdownSelectedValues('company')
//     var file_id = getDropdownSelectedValues('date')
//     var date = $('#select2Date').select2('data')[0].text;

//     url=`https://gitlab.com/dsd4/sedar/sedar_pipeline/-/issues/new?issue[title]=Issue from Dashboard - [Issue name here]&issue[description]`+
//     `=**Click PREVIEW THEN Please type your issue description here**`+
//     `%0D%0D------------------Data For Developer belolw this lines -----------------`+
//     `%0D%0DCompany=${company}%0D%0DDate=${date}%0D%0Dfile_id=${file_id}%0D%0Derror_message=${message}`
//     window.open(url, '_blank').focus();
// }

// EVENT LISTNERS

// FOR MAIN TAB CHANGE
$('#main-tabs li').click(function() {
	$('#main-tabs li').removeClass('active');
	$(this).addClass('active');
});

$( "#select2Companies" ).change(function() {
    const loadAll = new dropdownLoader
    loadAll.loadLastDropdown()
});

$('#select2Date').change(function() {
	loadData((details_and_location = false));
});


var target = $( "#select2Date" )[0];
// Create an observer instance
var observer = new MutationObserver(function( mutations ) {
   mutations.forEach(function( mutation ) {
       var newNodes = mutation.addedNodes; // DOM NodeList
       if( newNodes !== null ) { // If there are new nodes added

		if (timer) clearTimeout(timer);
		timer = setTimeout(
			function() {
				loadData();
			},
			50
		);
      }
   });    
});

// Configuration of the observer:
var configuration = { 
    attributes: true, 
    childList: true, 
    characterData: true 
};

// Pass in the target node, as well as the observer options
observer.observe(target, configuration);
// Later, you can stop observing

$("#logout-button").click(function(){
    window.focus();
    $.ajax({
        url: window.location.pathname+"oauth2/sign_out",
        complete: function () {
            location.reload();
        }
        })
});

//Document Ready Function (Runs code when webpage is loaded)
$(document).ready(function() {
	// Load Dropdown Values
	getTranslateDict(site_lang)
	const loadAll = new dropdownLoader();
	loadAll.loadAllDropdown();
});
