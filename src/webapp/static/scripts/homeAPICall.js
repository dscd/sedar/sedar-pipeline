var translateDict = null

function getCompanyDetails(issuerno) {
	const link =  'api/company-details/?issuerno=' + issuerno +'&lang='+site_lang;
	$.ajax({
		url: link,
		type: 'GET',
		dataType: 'json',
	})
		.done(function(resp) {
			jsonData = JSON.parse(JSON.stringify(resp))
			setCompanyDetails(jsonData, translateDict);
			setCompanyLocation(jsonData, translateDict);
		})
		.fail(function() {
			console.log('error');
			setCompanyDetails(null, translateDict);
			setCompanyLocation(null, translateDict);
		});
}


// Company Statements

// Pdf File
function getCompanyPDFDetails(date) {
	const link =  'api/pdf-file/?data_format=select2&file_id='+date;
	$.ajax({
		url: link,
		type: 'GET',
		dataType: 'json',
	})
		.done(function(resp) {
			jsonData = JSON.parse(JSON.stringify(resp))
			setCompanyStatementDetails(jsonData);
		})
		.fail(function() {
			setCompanyStatementDetails(null);
			console.log('error');
		});
}

//Extracted tables Balance Sheet and Income Statements
function getExtractedTables(file_id){
	const link =  'api/extracted-tables/?file_id='+file_id;
    $.ajax({
		url: link,
		type: 'GET',
		dataType: 'json',
	})
		.done(function(resp) {
		    setExtractedVariables(resp)
		})
		.fail(function() {
			setExtractedVariables(null)
			console.log('error');
		});
		
}


function getTranslateDict(siteLang){
	const link = 'api/translate-dict/?lang='+siteLang;
    $.ajax({
		url: link,
		type: 'GET',
		dataType: 'json',
	})
		.done(function(resp) {
			jsonData = JSON.parse(JSON.stringify(resp))
			translateDict = jsonData
		})
		.fail(function() {
			console.log('error');
		});
}