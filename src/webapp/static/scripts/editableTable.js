// Clone a html row: https://jsfiddle.net/7A6MQ/

// AWAYS 0 = BS Table and 1 = IS table id
var array_table_toedit = ['balanceSheetTable', 'incomeStatementTable', 'cashflowStatementTable']
const table_editor_button = document.querySelector('#table-editor-button');

function makeTableEditable(value, index, array) {
    div_id = '#'+value+' td';
    $(div_id).each(function(index, element) {
        var $el = $(this);
        $el.attr('contenteditable', true);
      });
}
function lockTable(value, index, array) {
    div_id = '#'+value+' td';
    $(div_id).each(function(index, element) {
        var $el = $(this);
        $el.attr('contenteditable', false);
      });
}

function highlightSelectedRow(value, index, array) {
    div_id = '#'+value+' tr';
    $(div_id).click(function(){
        $(this).addClass('tr-selected').siblings().removeClass('tr-selected');    
        // var value=$(this).find('td:first').html();
      
     });
}

function unHighlightSelectedRow(value, index, array) {
    div_id = '#'+value+' tr';
    $(div_id).unbind('click');
    // Remove existing selected
    $('tr').removeClass("tr-selected");
}

// Move, Add Rows
function addRow(div_id){
    var mynewrow = '<tr><td></td><td></td><td></td><td></i><i class="fas fa-arrow-down pl-1 move down"></i><i class="fas fa-minus-circle pl-1 move delete"></i> </td></tr>';
	
    $('#'+div_id).find('tbody').append(mynewrow);
}

function parseTableToJSON(tableid) {
    tableEl = document.getElementById(tableid)
    const columns = Array.from(tableEl.querySelectorAll('th')).map(it => it.textContent)
    const rows = tableEl.querySelectorAll('tbody > tr')
    return Array.from(rows).map(row => {
        const cells = Array.from(row.querySelectorAll('td'))
        return columns.reduce((obj, col, idx) => {
            obj[col] = cells[idx].textContent
            return obj
        }, {})
    })
}
function mergeAcceptChanges(what, table_to_edit){
    text = what == 'database' ? translateDict['database'] :translateDict['your'] 
    toastme.yesNoDialog({
        title: translateDict["Accept value changes?"].replace('value', text ),
        text: translateDict["This action will remove all rows highlighted other than the value changes."].replace('value', text ),
        textConfirm: translateDict["Confirm"],
        textCancel: translateDict["Cancel"],
        showCancel: true, // true or false
        type: "question", // 'success', 'danger', 'warning', 'info' or 'question'
        dark: false, // Show dark theme? 'true' or 'false'
    }).then(function(value) {
        if (value[0]) {
            if(what == 'database'){
                var query = `#${table_to_edit} tr.tr-conflict`
            }
            else if (what == 'your'){
                var query = `#${table_to_edit} tr.tr-no-conflict`
            }
            document.querySelectorAll(query).forEach((tr) => {
                tr.remove();
            });
            document.getElementById('mergeAcceptChanges-buttons').remove()
            mytoast.success(translateDict["Removed rows with value changes"].replace('value', text ));
        }
    });
}

async function conflictWhileSave(resp, tabToEdit){
    var resp_r = false
    // Show Dialogue conflict
    const value = await toastme.yesNoDialog({
        title: translateDict["Cannot Save Table"],
        text: resp.message,
        textConfirm: translateDict["Manually Resolve Conflicts"],
        textCancel: translateDict["Reload new data"],
//        showCancel: true, // true or false
        type: "warning", // 'success', 'danger', 'warning', 'info' or 'question'
        dark: true, // Show dark theme? 'true' or 'false'
    })
    if (value[0]) { // Manually Resolve conflicts
        table_editor_button.className = "btn btn-sm btn-outline-success"
        table_editor_button.innerHTML = getEditButtonImageText("Save Table");
        table_editor_button.disabled = false;
        // Fill table
        table_div = document.getElementById(tabToEdit);
        table_div.innerHTML = '';
        if (resp.table_json) {
            let cols = Object.keys(resp.table_json[0].data);
            //Map over columns, make headers,join into string
            let headerRow = cols
                .map(
                    function(col, i) {
                        return `<th>${col}</th>`;
                    }.bind(this)
                )
                .join('');
            let rows = resp.table_json
                .map(row => {
                    let tds = cols
                        .map(
                            function(col, i) {
                                if (i === 0) {
                                    return `<td width="60%">${row.data[col]}</td>`;
                                } else {
                                    return `<td class='text-right'>${row.data[col]}</td>`;
                                }
                            }.bind(this)
                        )
                        .join('');
                    if(row.conflict === 'red'){
                        return `<tr class="tr-conflict">${tds}</tr>`;
                    }
                    else if (row.conflict === 'green'){
                        return `<tr class="tr-no-conflict">${tds}</tr>`;
                    }
                    else{
                        return `<tr>${tds}</tr>`;
                    }
                })
                .join('');
            //build the table
            const table = `
              <table id="conflict_table" class="table text-secondary table-striped table-sm mt-1">
                  <thead>
                      <tr class='d-none'>${headerRow}</tr>
                  <thead>
                  <tbody>
                      ${rows}
                  <tbody>
              <table>`;
            table_div.innerHTML = `<div class="alert alert-danger mt-3 mb-0 text-center" role="alert">`+ translateDict["Resolve conflicts by selecting a version or manually editing the table."] +`<div class="text-center pt-1">
            <span><span class="badge badge-success p-2"> </span> `+ translateDict["Changes from database"] +`</span><span class="pl-3"><span class="badge badge-danger p-2"> </span> `+ translateDict["Your changes"] +`</span><span class="pl-3"><span class="badge badge-light p-2 border"> </span> `+ translateDict["No Conflict"] +`</span>
            </div></div>
            <div id="mergeAcceptChanges-buttons" class="text-center"><div class="btn-group mt-1">
              <button type="button" class="btn btn-success" onclick="mergeAcceptChanges('database', '${tabToEdit}')">`+ translateDict["Click to accept changes from database"] +`</button>
              <button type="button" class="btn btn-danger ml-1" onclick="mergeAcceptChanges('your', '${tabToEdit}')">`+ translateDict["Click to accept your changes"] +`</button>
            </div></div>${table}`;
            $(`#${tabToEdit} tbody tr:first`).addClass('font-weight-bold');
            // Make Table editable again
            makeAllTableEditable()
        } else {
            table_div.innerHTML = `<div class="alert alert-secondary mt-5 text-center" role="alert">`
                +translateDict["No Extracted Table found for the selected Company and date."]+
            `</div>`;
        }
    }
    else { // Reload Table
        resp_r = true
        mytoast.info(translateDict["Fetching New tables from the database"]);
    }
    return new Promise(resolve => {
            resolve(resp_r);
        });
}

async function tableSave(revert_table){
    var success_r = false

    var activeTab = $("#extractedTableTabs").find(".active");

    try {
        if (activeTab.attr('id') === 'nav-tab-BS'){
            var PAGE_TYPE = 'BALANCE_SHEET'
            var id_table_to_edit = array_table_toedit[0]
            var table_md5 = BALANCE_SHEET_MD5
        }
        else if (activeTab.attr('id') === 'nav-tab-IS'){
            var PAGE_TYPE = 'INCOME_STATEMENT'
            var id_table_to_edit = array_table_toedit[1]
            var table_md5 = INCOME_STATEMENT_MD5
        }
        else if (activeTab.attr('id') === 'nav-tab-CS'){
            var PAGE_TYPE = 'CASHFLOW_STATEMENT'
            var id_table_to_edit = array_table_toedit[1]
            var table_md5 = CASHFLOW_STATEMENT_MD5
        }
        var post_data = {
            "file_id": getDropdownSelectedValues('date'),
            "page_type": PAGE_TYPE,
            "table_json": parseTableToJSON(id_table_to_edit),
            "table_md5": table_md5,
            "lang":site_lang,
            "revert":revert_table
            }

        let url = "api/extracted-tables/";
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(post_data) // body data type must match "Content-Type" header
          });
          const data = await response.json()
          document.getElementById(id_table_to_edit).innerHTML =`<div class="d-flex justify-content-center">
        <div class="spinner-grow text-primary mt-5" role="status">
        <span class="sr-only">`+translateDict["Loading"]+`...</span></div></div>`
            if (data.type == 'success'){
                mytoast.success(data.message);
                table_editor_button.innerHTML = getEditButtonImageText("Table Saved, Reload Page!");
                success_r = true
            }
            else if (data.type == 'error'){
                dialogueError(data.message);
                table_editor_button.className = "btn btn-sm btn-outline-success"
                table_editor_button.innerHTML = getEditButtonImageText("Save Table");
                table_editor_button.disabled = false;
            }
            else if (data.type == 'conflict'){
                // set new MD5
                if (PAGE_TYPE == 'BALANCE_SHEET'){
                    BALANCE_SHEET_MD5 = data.new_MD5
                }
                else if (PAGE_TYPE = 'INCOME_STATEMENT'){
                    INCOME_STATEMENT_MD5 = data.new_MD5
                }
                // Resolve Conflicts
                success_r = await conflictWhileSave(data, id_table_to_edit)
            }
            // set new MD5
            if (PAGE_TYPE == 'BALANCE_SHEET'){
                BALANCE_SHEET_MD5 = data.new_MD5
            }
            else if (PAGE_TYPE = 'INCOME_STATEMENT'){
                INCOME_STATEMENT_MD5 = data.new_MD5
            }            
            else if (PAGE_TYPE = 'CASHFLOW_STATEMENT'){
                CASHFLOW_STATEMENT_MD5 = data.new_MD5
            }
    }
    catch(err){
        dialogueError(translateDict["An error occurred please try again, error = "] + err);
        table_editor_button.className = "btn btn-sm btn-outline-success"
        table_editor_button.innerHTML = getEditButtonImageText("Save Table");
        document.getElementById("table-editor-button").disabled = false;
    }
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(success_r);
        }, 1000);
        });
}
async function asyncLoadThenRemoveEditBtns(just_remove=false, revert_table=false) {
    if (just_remove){
        array_table_toedit.forEach(lockTable);
        table_editor_button.innerHTML = getEditButtonImageText("Edit Table")
        table_editor_button.className = "btn btn-sm btn-outline-secondary"
    
        document.getElementById("edit_table_buttons").innerHTML = ""
        array_table_toedit.forEach(unHighlightSelectedRow);
    }
    else{
        // Wait to POST DATA
        table_editor_button.disabled = true; //disable the button
        table_editor_button.innerHTML +=`<span class="spinner-border spinner-border-sm ml-1" role="status" aria-hidden="true"></span><span class="sr-only">`+ translateDict["Loading"] + `...</span>`
        // document.getElementById('table-editor-button').appendChild(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        // <span class="sr-only">Loading...</span>`);//add spinner
        const result = await tableSave(revert_table);
        // Load NEW DATA if true
        if (result){
            loadData(details_and_location = false, statements = true)
            removeButtonLockTable()
        }
    }

  }
  

//  EVENT HANDLERS
$(document.body).on('click', '#edit_table_buttons button.move', function(){
    var row = $('tr.tr-selected');
    if ($(this).hasClass('add')){
        var tr_html_array = '';
        for (let i = 0; i < $(row).find('td').length; i++) {
            if (i===0){
                tr_html_array += `<td class="text-black" contenteditable="true"></td>`;
            }
            else {tr_html_array += `<td contenteditable="true"></td>`};
        }
        $('<tr class="move-add">'+tr_html_array+'</tr>').insertAfter($(row));
    }
    else if ($(this).hasClass('up')) {
        row.prev().before(row);
    }
    else if ($(this).hasClass('down')) {
        row.next().after(row);
    }
    else if ($(this).hasClass('delete')) {
        $(row).removeClass('tr-selected');
        $(row).addClass('move-delete');
        setTimeout(() => {
            row.remove();
        }, "100")
    }
    else if ($(this).hasClass('cancel')) {
        removeButtonLockTable();
        loadData(details_and_location = false, statements = true);
    }else if ($(this).hasClass('revert')) {
        removeButtonLockTable();
        loadData(details_and_location = false, statements = true);
        asyncLoadThenRemoveEditBtns(just_remove =false,revert_table=true)
    }
    array_table_toedit.forEach(highlightSelectedRow);

});

function makeAllTableEditable(e=null){
        array_table_toedit.forEach(makeTableEditable);
        // highlighter for rows
        array_table_toedit.forEach(highlightSelectedRow);
        // Change button text
        if(e){
            table_editor_button.className = "btn btn-sm btn-outline-success"
            table_editor_button.innerHTML = getEditButtonImageText("Save Table")
      }

        // Add editable buttons
    document.getElementById("edit_table_buttons").innerHTML = `
        <button type="button" class="btn btn-outline-danger  btn-sm ml-2  move cancel"><i class="fa fa-times move cancel pr-2"></i><span>`+ translateDict["Cancel"] + `</span></button>
        <button type="button" class="btn btn-outline-secondary  btn-sm ml-2  move revert"><i class="fa fa-undo move revert pr-2"></i><span>`+ translateDict["Revert to Original Extraction"] + `</span></button>
        <div class="mt-2">
        <button type="button" class="btn btn-outline-secondary mr-1 btn-sm  move add"><i class="fas fa-plus move add pr-2"></i><span>`+ translateDict["Add"] + `</span></button>
        <button type="button" class="btn btn-outline-secondary mr-1 btn-sm  move up"><i class="fas fa-arrow-up move up pr-2"></i><span>`+ translateDict["Up"] + `</span></button>
        <button type="button" class="btn btn-outline-secondary mr-1 btn-sm  move down"><i class="fas fa-arrow-down move down pr-2"></i><span>`+ translateDict["Down"] + `</span></button>
        <button type="button" class="btn btn-outline-secondary btn-sm  move delete"><i class="fas fa-minus-circle move delete pr-2"></i><span>`+ translateDict["Delete"] + `</span></button>
        </div>`
}


$("#table-editor-button").click(function(e, only_clear) {
    // alert(e.innerText)
    if (table_editor_button.innerText.trim()  === translateDict["Edit Table"]){
        makeAllTableEditable(e)
    }
    else{
        if (only_clear){
            asyncLoadThenRemoveEditBtns(just_remove =true)
        }
        else{
            // Table Save and clear
            asyncLoadThenRemoveEditBtns()
        }
    }
});

function removeEditTableButtons(){
    if($("span#edit_table_buttons button").length !== 0){
        $('#table-editor-button').trigger('click', [true]);
    }
}

function getEditButtonImageText(imageText) {
    table_editor_button.innerHTML = ""
    switch (imageText) {
        case "Save Table":
            return `<i class="fa fa-save pr-1 " style="color:green"></i>` + `<span>` + translateDict[imageText] + `</span>`
        case "Table Saved, Reload Page!":
            return `<i class="fa fa-refresh text-primary pr-1"></i>` + `<span>` + translateDict[imageText] + `</span>`
        case "Edit Table":
            return `<i class="fas fa-file-signature text-primary pr-1"></i>` + `<span>` + translateDict[imageText] + `</span>`
        default:
            return `<i class="fas fa-file-signature text-primary pr-1"></i>` + `<span>` + translateDict[imageText] + `</span>`
    }
}

function removeButtonLockTable() {
    array_table_toedit.forEach(lockTable);
    table_editor_button.innerHTML = getEditButtonImageText("Edit Table")
    table_editor_button.className = "btn btn-sm btn-outline-secondary"
    table_editor_button.disabled = false;
    document.getElementById("edit_table_buttons").innerHTML = ""
    array_table_toedit.forEach(unHighlightSelectedRow);
}

$('body').on('blur', '[contenteditable]', function() {
    var val= $(this).html().replace(/,/g,'');
    this.innerHTML = val;
});

$(document).ready(function(){
    // ON TAB CHANGE
    //     - remove highlighted rowS
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //e.target -> activated tab
        //e.relatedTarget -> previous tab
        $('tr').removeClass("tr-selected");
    });

  });
