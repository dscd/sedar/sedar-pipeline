class dropdownLoader{
    constructor() {
    }
    async companies_dropdown(){
        $("#select2Companies").empty();
//        const province_exists = await fetchDropdowns("#select2Companies", "api/companies/")
        const province_exists = await fetchCompanyNameDropdown("#select2Companies", "api/companies/")
    }
    async dates_dropdown(){
        var issuerno = $('#select2Companies').select2('data')[0].id;
        $("#select2Date").empty();
        const company_exists = await fetchDropdowns("#select2Date", "api/filing-dates/?data_format=select2&issuerno="+issuerno)
    }
    async loadAllDropdown () {
        const one = await this.companies_dropdown()
        const two = await this.dates_dropdown()
    }
    async loadLastDropdown () {
        const two = await this.dates_dropdown()
    }

}


const fetchDropdowns = async (select_id, url, data=false) => {
    if(data===false){
        await $.ajax({
            type: "GET",
            url:  url,
            success: function(data){
            //    console.log(data)
                $(select_id).select2({
                data: data,
                theme: 'bootstrap4',
//                dropdownAutoWidth: true,
//                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                closeOnSelect: !$(this).attr('multiple'),
                })

            }
        });
    }
    else {
        $(select_id).empty();
        $(select_id).select2({
            data: data,
            theme: 'bootstrap4',
//            dropdownAutoWidth: true,
//            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
            closeOnSelect: !$(this).attr('multiple'),
        })
    }
    return true
}


function getDropdownSelectedValues(what){
    if (what==='company'){
        company = $('#select2Companies').select2('data')[0].id;
        return company
    }
    else if (what==='date'){
        date = $('#select2Date').select2('data')[0];
        return date == null ?null :date.id
    }
}

var scroll_id = null
const fetchCompanyNameDropdown = async (select_id, url) => {
    var company_select = $(select_id);
    company_select.select2({
      ajax: {
        url:  url,
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            company: params.term,
            type: params._type,
            data_format: 'select2',
            scroll_id: scroll_id,
          };
        },
        processResults: function (data, params) {
            scroll_id = data.scroll_id
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;

          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: true
      },
      placeholder: 'Select a company',
      theme: 'bootstrap4',
//      dropdownAutoWidth: true,
//      width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
      placeholder: $(this).data('placeholder'),
      allowClear: Boolean($(this).data('allow-clear')),
      closeOnSelect: !$(this).attr('multiple'),
    //  minimumInputLength: 1,
    });


    //     Fetch the preselected item, and add to the control
    const setPreselected = await fetch( url + "?data_format=select2&type=query")
      .then(response => response.json())
      .then(data =>
      {
        // create the option and append to Select2
        let company = data.items[0];
        var option = new Option(company.text, company.id, true, true);
        company_select.append(option).trigger('change');

        // manually trigger the `select2:select` event
        company_select.trigger({
            type: 'select2:select',
            params: {
                data: company
            }
        });
      });

    return true
}