var pdfJsonData = null;
var companyPDF = null;
var BALANCE_SHEET_MD5;
var INCOME_STATEMENT_MD5;
var CASHFLOW_STATEMENT_MD5;

function setCompanyStatementDetails(jsonData) {
    companyPDF = jsonData != null ? jsonData['PDF_LINK'] : jsonData;
    pdfJsonData = jsonData != null ? jsonData['DETECTED_PAGES'] : jsonData;
    setCompanyStatementSelector(pdfJsonData);
}

// JSON TO TABLE
function json2Table(json, div_id, table_id) {
	table_div = document.getElementById(div_id);
	table_div.innerHTML = '';
	if (json && Object.keys(json).length>0) {
		let cols = Object.keys(json[0]);

		//Map over columns, make headers,join into string
		let headerRow = cols
			.map(
				function(col, i) {
					let tmp = $(`<div>${col}</div>`).text()
					return `<th>${tmp}</th>`;
					
				}.bind(this)
			)
			.join('');
		//map over array of json objs, for each row(obj) map over column values,
		//and return a td with the value of that object for its column
		//take that array of tds and join them
		//then return a row of the tds
		//finally join all the rows together

		let rows = json
			.map(row => {
				let tds = cols
					.map(
						function(col, i) {
                            let tmp = $(`<div>${row[col]}</div>`).text()
							if (i === 0) {
								return `<td width="60%">${tmp}</td>`;
							} else {
								return `<td class='text-right'>${tmp}</td>`;
							}
						}.bind(this)
					)
					.join('');
				return `<tr>${tds}</tr>`;
			})
			.join('');
		//build the table
		const table = `
          <table id=${table_id} class="table text-secondary mt-3 table-striped table-sm">
              <thead>
                  <tr class='d-none'>${headerRow}</tr>
              <thead>
              <tbody>
                  ${rows}
              <tbody>
          <table>`;
		table_div.innerHTML = table;
        $(`#${div_id} tbody tr:first`).addClass('font-weight-bold');
	} else {
		table_div.innerHTML = `<div class="alert alert-secondary mt-5 text-center" role="alert">`
		+ translateDict["No Extracted Table found for the selected Company and date."] +
        `</div>`;
	}
}

function setExtractedVariables(jsonData) {
    let BS_JSON =null;
    let IS_JSON=null;
    let CS_JSON=null;
    
    if (jsonData) {
        // Balance_Sheet ASSINGMENT
        if (jsonData.BALANCE_SHEET === null) {
            BS_JSON = null
            BALANCE_SHEET_MD5 = null
        }
        else if (jsonData.BALANCE_SHEET.EDITED_TABLE_1) {
            BS_JSON = jsonData.BALANCE_SHEET.EDITED_TABLE_1
            BALANCE_SHEET_MD5 = jsonData.BALANCE_SHEET.EDITED_TABLE_1_MD5
        }
        else if (jsonData.BALANCE_SHEET.EDITED_TABLE_2) {
            BS_JSON = jsonData.BALANCE_SHEET.EDITED_TABLE_2
            BALANCE_SHEET_MD5 = jsonData.BALANCE_SHEET.EDITED_TABLE_2_MD5
        }
        else if (jsonData.BALANCE_SHEET.ORIGINAL_TABLE) {
            BS_JSON = jsonData.BALANCE_SHEET.ORIGINAL_TABLE
            BALANCE_SHEET_MD5 = jsonData.BALANCE_SHEET.ORIGINAL_TABLE_MD5
        }
        // Income Statement Assignment
        if (jsonData.INCOME_STATEMENT === null) {
            IS_JSON = null
            INCOME_STATEMENT_MD5 = null
        }
        else if (jsonData.INCOME_STATEMENT.EDITED_TABLE_1) {
            IS_JSON = jsonData.INCOME_STATEMENT.EDITED_TABLE_1
            INCOME_STATEMENT_MD5 = jsonData.INCOME_STATEMENT.EDITED_TABLE_1_MD5
        }
        else if (jsonData.INCOME_STATEMENT.EDITED_TABLE_2) {
            IS_JSON = jsonData.INCOME_STATEMENT.EDITED_TABLE_2
            INCOME_STATEMENT_MD5 = jsonData.INCOME_STATEMENT.EDITED_TABLE_2_MD5
        }
        else if (jsonData.INCOME_STATEMENT.ORIGINAL_TABLE) {
            IS_JSON = jsonData.INCOME_STATEMENT.ORIGINAL_TABLE
            INCOME_STATEMENT_MD5 = jsonData.INCOME_STATEMENT.ORIGINAL_TABLE_MD5
        }
        // Cashflow Statement Assignment
        if (jsonData.CASHFLOW_STATEMENT === null) {
            CS_JSON = null
            CASHFLOW_STATEMENT_MD5 = null
        }
        else if (jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_1) {
            CS_JSON = jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_1
            CASHFLOW_STATEMENT_MD5 = jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_1_MD5
        }
        else if (jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_2) {
            CS_JSON = jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_2
            CASHFLOW_STATEMENT_MD5 = jsonData.CASHFLOW_STATEMENT.EDITED_TABLE_2_MD5
        }
        else if (jsonData.CASHFLOW_STATEMENT.ORIGINAL_TABLE) {
            CS_JSON = jsonData.CASHFLOW_STATEMENT.ORIGINAL_TABLE
            CASHFLOW_STATEMENT_MD5 = jsonData.CASHFLOW_STATEMENT.ORIGINAL_TABLE_MD5
        }
    
        setUnitCurrency(jsonData.UNITS, jsonData.CURRENCY)
    }
    
	// DRAW TABLE BS
	json2Table(BS_JSON, 'balanceSheetTable', 'bsTable');
	$('#balanceSheetTable tr td:nth-child(1)').addClass('text-black');

	// DRAW TABLE IS
	json2Table(IS_JSON, 'incomeStatementTable', 'isTable');
	$('#incomeStatementTable tr td:nth-child(1)').addClass('text-black');

    // DRAW TABLE CS
	json2Table(CS_JSON, 'cashflowStatementTable', 'csTable');
	$('#cashflowStatementTable tr td:nth-child(1)').addClass('text-red');

	removeButtonLockTable()
}

function setUnitCurrency(unit,currency){
    let unit_currency_text = ""
    if (unit && unit != ""){
        unit_currency_text += `Unit is ${unit}`
        unit_currency_text = unit.length >4 ? unit_currency_text += `, ` : unit_currency_text
    } 
    if (currency && currency!= "" ){
        unit_currency_text += `Currency in ${currency}` 
    } 

    document.getElementById('unit_currency').innerHTML =unit_currency_text
}

function setCompanyStatementSelector(pdfJsonData) {
	$('#select2PageType').empty();
    if (pdfJsonData != null){
        $('#select2PageType').select2({
            data: pdfJsonData,
            theme: 'bootstrap4',
            width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
            placeholder: $(this).data('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
            closeOnSelect: !$(this).attr('multiple'),
        });
        loadPdfPage($('#select2PageType').select2('data')[0].page_no)
    }else{
        loadPdfPage(null)
    }
}

//Download CSV
function getCSVFile(div_id, type=null){
    var filename = `${$('#select2Companies').select2('data')[0].text}_${type}_${$('#select2Date').select2('data')[0].text}.csv`.replace(/\s+/g, '_');
    var csvFile=$(div_id).table2CSV({delivery:'value'});
    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

function getCSVTable(){
    // Get Active Tab ID to get the table for csv
    var activeTab = $("#extractedTableTabs").find(".active");
    switch (activeTab.attr('id')){
        case 'nav-tab-BS':
            getCSVFile('#balanceSheetTable', type='Balance_Sheet');
            break;
        case 'nav-tab-IS':
            getCSVFile('#incomeStatementTable', type='Income_Statement');
            break;
        case 'nav-tab-CS':
            getCSVFile('#cashflowStatementTable', type='Cashflow_Statement');
            break;
    }
  }


$('#select2PageType').change(function() {
	loadPdfPage($('#select2PageType').select2('data')[0].page_no);
});

$('#nav-tab-BS').click(function() {
        if (document.getElementById('PDF_Page_change_checkbox').checked) {
			$('#select2PageType').val(1).trigger('change');
		}
});

$('#nav-tab-IS').click(function() {
	if (document.getElementById('PDF_Page_change_checkbox').checked) {
		$('#select2PageType').val(3).trigger('change');
	}
});

$('#nav-tab-CS').click(function() {
	if (document.getElementById('PDF_Page_change_checkbox').checked) {
		$('#select2PageType').val(2).trigger('change');
	}
});

function loadPdfPage(pageNo) {

	var options = {
		width:"100%",
		height:"1000px",

        pdfOpenParams: {
            view: 'FitH',
			scrollbar: '1', 
			toolbar: '1', 
			statusbar: '1',
			navpanes: '1',
			messages: '1',
			pagemode: 'thumbs',
			page: pageNo
        }
    }

	if (companyPDF != null && pageNo != null) {
        $("#pdf-page").show(); 
		PDFObject.embed(companyPDF, '#pdf-page', options);
        
    }else{
        $('#select2PageType').empty();
        $("#pdf-page").html( `<div class="alert alert-secondary mt-5 text-center" role="alert">`
        +translateDict["No PDF found for the selected Company and date"]+
    `</div>`);
    }
	$('#nav-tab-BS').tab('show');
}


$('a[data-toggle="tab"]').click(function(){
	$($(this).attr('href')).show().addClass('show active').siblings().hide();
});