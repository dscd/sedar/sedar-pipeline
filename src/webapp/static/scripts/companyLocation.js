var businessAddressResponse = undefined;
var mailingAddressResponse = undefined;
var markerAddress = undefined;
var map;
var layerGroup;
var group;
/**
 * @param  {} businessLatLong - Lat Lon vales for the business locations
 * @param  {} mailingLatLong - Lat Lon for the mailing location
 */
function locationTabLoaded(businessLatLong, mailingLatLong) {
	const openStreetMapURL = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	const openStreetMapAttr = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';

	const businessAddressDisplayName = markerAddress[0];
	const mailingAddressDisplayName = markerAddress[1];

	const businessAddressLatLon = [ businessLatLong[0]['lat'], businessLatLong[0]['lon'] ];
	const mailingAddressLatLon = [ mailingLatLong[0]['lat'], mailingLatLong[0]['lon'] ];

	const sameAddress = businessAddressDisplayName == mailingAddressDisplayName;

	var businessIcon = L.AwesomeMarkers.icon({
		icon: sameAddress ? 'bookmark' : 'building',
		prefix: 'fa',
		markerColor: 'darkblue',
	});

	var mailIcon = L.AwesomeMarkers.icon({
		icon: sameAddress ? 'bookmark' : 'envelope',
		prefix: 'fa',
		markerColor: 'darkblue',
	});

	var businessAddressMarker = L.marker(businessAddressLatLon, { icon: businessIcon }).bindPopup(
		'<b>'+translateDict['Business Location'] +'<br> ' + businessAddressDisplayName
	);
	var mailingAddressMarker = L.marker(mailingAddressLatLon, { icon: mailIcon }).bindPopup(
		'<b>'+translateDict['Mailing Location'] +'<br> ' + mailingAddressDisplayName
	);

	if (businessAddressResponse != null && businessAddressResponse != undefined) {
		setLocationData(document.getElementById('business-location-details'), businessAddressResponse);
	}

	if (mailingAddressResponse != null && mailingAddressResponse != undefined) {
		setLocationData(document.getElementById('mailing-location-details'), mailingAddressResponse);
	}

	if (map == undefined && map == null) {
		map = L.map('map', {
			zoom: 20,
			animate: true,
		});

		L.easyButton('<span class="star">&olarr;</span>', function(btn, map) {
			map.fitBounds(group.getBounds().pad(0.5));
			map.closePopup();
		}).addTo(map);
	}

	layerGroup = L.layerGroup([ businessAddressMarker, mailingAddressMarker ]);
	layerGroup.addTo(map);

	group = new L.featureGroup([ businessAddressMarker, mailingAddressMarker ]);
	map.fitBounds(group.getBounds().pad(0.5));

	// Initialize Open Street Maps ( Map URL, Attr Value)
	L.tileLayer(openStreetMapURL, {
		attribution: openStreetMapAttr,
	}).addTo(map);

	//on Click Listeners
	$('#btn_businessLocation').click(function() {
		map.flyTo(businessAddressLatLon, 17, {
			animate: true,
			duration: 2, // in seconds
		});
		businessAddressMarker.openPopup();
	});

	$('#btn_mailingLocation').click(function() {
		map.flyTo(mailingAddressLatLon, 17, {
			animate: true,
			duration: 2, // in seconds
		});
		mailingAddressMarker.openPopup();
	});

	$('body').removeClass('busy');
	L.Util.requestAnimFrame(map.invalidateSize, map, !1, map._container);
}
/**
 * @param  {} address - fetch the lat and lon for the address
 */
async function fetchMap(address) {
	const request = await fetch('https://nominatim.openstreetmap.org/search?format=json&limit=1&q=' + address);
	return await request.json();
}
/**
 * @param  {} businessAddress - Business Location Address
 * @param  {} mailingAddress - Mailing Location Address
 * @param  {} callback - Callback to the function when the lat & lon are receieved
 */
async function getMapData(businessAddress, mailingAddress, callback) {
	const businessLatLong = fetchMap(businessAddress);
	const mailingLatLong = fetchMap(mailingAddress);
	const resolvedResponses = await Promise.all([ businessLatLong, mailingLatLong ]);
	callback(...resolvedResponses);
}
/**
 * @param  {} '#profile-tab' - find the tabe by its name
 * @param  {} .bind('click' - when the tavb is binded to the view
 * @param  {} function( - set the response when the tab is binded
 */

/**
 * @param  {} businessAddress - business address
 * @param  {} mailingAddress- mailing address
 */
function getMarkerAddress(businessAddress, mailingAddress,translateDict) {
	add1 = businessAddress[translateDict['Street 1']] +
		', ' +
		mailingAddress[translateDict['Street 2']] +
		', ' +
		businessAddress[translateDict['City']] +
		', ' +
		businessAddress['Province'] +
		', ' +
		businessAddress[translateDict['Postal Code']] +
		', ' +
		businessAddress[translateDict['Country']];

	add2 = mailingAddress[translateDict['Street 1']] +
		', ' +
		mailingAddress[translateDict['Street 2']] +
		', ' +
		mailingAddress[translateDict['City']] +
		', ' +
		mailingAddress['Province'] +
		', ' +
		mailingAddress[translateDict['Postal Code']] +
		', ' +
		mailingAddress[translateDict['Country']];

	return [ add1.replace('null,', ''), add2.replace('null,', '') ];
}

function getAddress(businessAddress, mailingAddress) {
	add1 = businessAddress['Street 1'] + ' ' + businessAddress['Province'] ;
	add2 = mailingAddress['Street 1'] + ' ' + mailingAddress['Province'];

	return [add1,add2];
}


$('#location-tab').click(function (event) {       
	if (map != null && map != undefined) {	
		L.Util.requestAnimFrame(map.invalidateSize, map, !1, map._container);
	} 
});


function setCompanyLocation(jsonData, translateDict) {
	const businessAddress = jsonData[translateDict['Business Location']];
	const mailingAddress = jsonData[translateDict['Mailing Location']];

	if (businessAddress != null && mailingAddress != null) {
	
		businessAddressResponse = businessAddress;
		mailingAddressResponse = mailingAddress;

		setLocationData(document.getElementById('business-location-details'), businessAddressResponse);
		setLocationData(document.getElementById('mailing-location-details'), mailingAddressResponse);
		
		resetMap();
		
		if (businessAddress['Province'] !== null || mailingAddress['Province'] !== null) {
			markerAddress = getMarkerAddress(businessAddress, mailingAddress,translateDict);
			$('body').addClass('busy');
			getMapData(businessAddress[translateDict['Street 1']] + ' ' + businessAddress['Province'],
			mailingAddress[translateDict['Street 1']] + ' ' + mailingAddress['Province'], checkLocationData);
		}
	}
}


function checkLocationData(businessLatLong, mailingLatLong) {

	 if ( businessLatLong[0] == undefined || mailingLatLong[0] == undefined ){
		getMapData(businessAddressResponse[ translateDict['City'] ] , businessAddressResponse[ translateDict['City'] ] ,locationTabLoaded);
	 }else{
		locationTabLoaded(businessLatLong, mailingLatLong)
	 }
}

function resetMap() {
	if (map != null && map != undefined) {
		map.eachLayer(function(layer) {
			map.removeLayer(layer);
		});
		layerGroup.clearLayers();
		L.Util.requestAnimFrame(map.invalidateSize, map, !1, map._container);
	}
}

/**
 * @param  {} table - HTML table to add data to
 * @param  {} resp - Json response to parse and add to the table
 */
function setLocationData(table, resp) {
	table.innerHTML = '';

	for (let obj in resp) {
		var row = table.insertRow();
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		cell1.innerHTML = obj;
		cell1.style.fontWeight = '500';
		cell1.style.width = '50%';
		cell1.style.padding = '5px';

		cell2.innerHTML = resp[obj] == null ?  '-': resp[obj];
		cell2.style.width = '50%';
		cell2.style.padding = '5px';
	}
}

