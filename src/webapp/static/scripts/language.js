let lang_selector_div = document.getElementById("lang_selector")

function languageSelector(change_lang=false){
    
    if (site_lang === 'en'){
        lang_selector_div.innerHTML ='EN'
        lang_selector_div.title ='English'

    }
    else if (site_lang === 'fr'){
        lang_selector_div.innerHTML='FR'
        lang_selector_div.title ='French'

    }
    // If window is changed Reload Page
    if (change_lang===true){
        var new_lang = site_lang === 'en' ? 'fr' : 'en'
        fetch(`lang/${new_lang}`).
        then(res => window.location.reload())
        .catch(err => alert('Failed'));
        
    }

}

lang_selector_div.addEventListener('click', event => {
    languageSelector(change_lang=true);
});

window.addEventListener('DOMContentLoaded', (event) => {
    languageSelector()
});
