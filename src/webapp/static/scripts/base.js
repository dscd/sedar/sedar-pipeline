//Customize your Notification  
const config = {
    timeout: 5000,
    positionY: "top", // top or bottom
    positionX: "right", // right left, center
    distanceY: 20, // Integer value
    distanceX: 20, // Integer value
    zIndex: 100, // Integer value
    theme: "default", // default, ligh or  dark (leave empty for "default" theme)
    duplicates: false // true or false - by default it's false
};

const config_errr = {
    timeout: 50000,
    positionY: "top", // top or bottom
    positionX: "right", // right left, center
    distanceY: 20, // Integer value
    distanceX: 20, // Integer value
    zIndex: 100, // Integer value
    theme: "default", // default, ligh or  dark (leave empty for "default" theme)
    duplicates: true // true or false - by default it's false
};

//Create a new Toastmejs class instance
const mytoast = new Toastme(config);
const mytoast_error = new Toastme(config_errr);



function dialogueError(message){
    toastme.yesNoDialog({
        title: message,
        text: "Do you want to report the issue?",
        textConfirm: "Confirm",
        textCancel: "Cancel", 
        showCancel: true, // true or false
        type: "danger", // 'success', 'danger', 'warning', 'info' or 'question'
        dark: true, // Show dark theme? 'true' or 'false'
    }).then(function(value) {
        if (value[0]) {
            // alert(value[1])
            openIssueWithData(value[1])
        } 
        // else {
        //     console.log('You clicked Cancel')
        // }
    });
}

