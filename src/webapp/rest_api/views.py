import re
import subprocess
from datetime import datetime
from os.path import join

from django.core.files.storage import default_storage
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from config.api import S3, DataSource
from config.env import sedar
from config.model_serializers import Indices
from config.web_models import (BusinessAddress, BusinessAddressKeys,
                               BusinessAddressKeys_FR, CompanyDetails,
                               CompanyDetails_FR, CompanyDetailsKeys,
                               CompanyDetailsKeys_FR, ContactInformation,
                               ContactInformationKeys,
                               ContactInformationKeys_FR,
                               ExtractedAPITableContent, ExtractedTable,
                               MailingAddress, MailingAddressKeys,
                               MailingAddressKeys_FR, OtherInformation,
                               OtherInformation_FR, OtherInformationKeys,
                               OtherInformationKeys_FR, PageDetails,
                               RecordDetails, RecordDetailsKeys,
                               RecordDetailsKeys_FR)
from webapp.rest_api.patterns import *
from webapp.rest_api.utils import (MD5_hash, Serializer, clean_media_folder,
                                   client, company_model_processing, es_client,
                                   exact_name, format_table, sedar_desc,
                                   strip_table, strip_tags,
                                   table_merge_conflicts)
from webapp.webapp.settings import BASE_DIR, MEDIA_ROOT
from webapp.webapp.translate_dict import TRANSLATE_DICT


def validate(value:str, pattern:str) -> str:
    """
    Function to validate API string parameters that can be
    described with a regex.

    Args:
        @value: (str) the value to check
        @pattern: (str) a regex expression
    Returns:
        The value if correctly matches the pattern or if it's None.
    Raise:
        ValueError Exception if a missmatch is found .
    """
    if value:
        if not re.match(pattern, str(value)):
            raise ValueError("Not valid request argument")
    return value

class companyView(APIView):
    """
    **Description:**

        This API(GET) is to recover company names. It could be all company names or a specific company name.
        If no company name is given it returns all the company names in the elasticsearhc database.
        The presence or absence of the data_format argument determines the format of what is returned.

    **Args:**

        data_format (str): The format of the data to be returned. 

        name (str): The name of the company to be searched or the default name of None.

    **Returns:**
            return [{"id": ISSUERNO, "text": COMPANY NAME (ISSUERNO), "selected": True}]


    **Example url:**

        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8992/api/companies/?company=B
        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8992/api/companies/?issuer_no=00034434
        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8992/api/companies/?scroll_id=FGluY2x1ZGVfY29udGV4dF91dWlkDXF1ZXJ5QW5kRmV0Y2gBFmNhMzVVeXVQUjhHd2dabU5wNndLZ2cAAAAAACdzzRZOTTdBbTJWSVJwcXZuZEhaNnAxamFn

    """

    def get(self, request, format=None):
        try:
            name        = strip_tags(request.GET.get("company"))
            data_format = validate(request.GET.get("data_format"), DATA_FORMAT_PATTERN)
            scroll_id   = validate(request.GET.get("scroll_id"), SCROLL_ID_PATTERN)
            query_type  = validate(request.GET.get("type"), QUERY_TYPE_PATTERN)
            issuerno    = validate(request.GET.get("issuer_no"), ISSUENO_PATTERN)
        except:
            return Response([{"items": None, "total_count": None}])

        ## Step 1a: If a str for the name variable is provided
        if name:
            ## Step 1a.1: Elasticsearch query for approximates matches to the
            ## string, which could be French or English
            query = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match_phrase_prefix": {
                                    "NAME_E": {
                                        "query": name,
                                    }
                                }
                            }
                        ]
                    }
                }
            }
            ## Step 1a.2: Query sedar_prod_company_names
            documents = es_client.search(
                index=Indices.sedar_prod_company_names.value, body=query, scroll="2m"
            )
        elif issuerno:
            query = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match_phrase_prefix": {
                                    "ID": {
                                        "query": issuerno,
                                    }
                                }
                            }
                        ]
                    }
                }
            }
            ## Step 1a.3: Query sedar_prod_company_names
            documents = es_client.search(
                index=Indices.sedar_prod_company_names.value, body=query, scroll="2m"
            )

        ## Step 1.b: If no string is provided
        else:
            ## Step 1.b.1: Query to return everything in an elasticsearch index
            query = {"query": {"match_all": {}}}

            ## Step 1.b.2 Query sedar_prod_company_names index
            if query_type == "query:append":
                documents = es_client.scroll(scroll_id=scroll_id, scroll="2m")

            else:
                documents = es_client.search(
                    index=Indices.sedar_prod_company_names.value,
                    body=query,
                    sort="LASTUPDATE:desc",
                    scroll="2m",
                    size=30,
                )
        ## Step 2.a: If data format is provided
        if data_format == "select2":
            ## Step 2.a.1: If data is recovered from Elasticsearch
            ## Transform data and assign "True" to the "selected"
            ## key for the first entry to "True"
            if documents:
                company_name_list = [
                    {
                        "id": strip_tags(row["_source"]["ID"]),
                        "text": f"{strip_tags(row['_source']['NAME_E'])} ({strip_tags(row['_source']['ID'])})",
                    }
                    for row in documents["hits"]["hits"]
                ]

                company_list = {
                    "items": company_name_list,
                    "total_count": documents["hits"]["total"]["value"],
                    "scroll_id": documents["_scroll_id"],
                }
                return Response(company_list)

        return Response([{"items": None, "total_count": None}])


class filingdatesView(APIView):
    """
    **Description:**

        This API(GET) is to recover the filing dates for a company.
        It could either take the issuerno and data_format or name and data_format.

    **Args:**

        data_format (str): The format of the data to be returned. It could be select2 or select1.

        name (str): The name of the company to be searched or the default name of None.

        issuerno (str): A unique identifier for a company.

    **Returns:**

        List of company dates in the selected format

    **Example url:**

        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8991/api/filing-dates/?data_format=select2&issuerno=00005820
    """

    def get(self, request, format=None):
        try:
            name        = strip_tags(request.GET.get("company"))
            data_format = validate(request.GET.get("data_format"), DATA_FORMAT_PATTERN)
            issuerno    = validate(request.GET.get("issuerno"), ISSUENO_PATTERN)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

        dates_list = []
        desc = sedar_desc()

        ## Step 1: If exact name is provided, query the issuerno/groupno.
        ## Else issuerno/groupno is the provided issuerno/groupno
        if name:
            company_info = exact_name(name)
            issuerno = company_info["ISSUERNO"]
        ## Step 2: Query for elasticsearh, givern the issuerno or groupno
        q = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "multi_match": {
                                "query": issuerno,
                                "fields": ["ISSUERNO", "GROUPNO"],
                            }
                        },
                        {"bool": {"should": desc}},
                    ],
                }
            }
        }
        ## Step 3.a: If "select2" data format is provided
        ## query a sorted list from sedar_prod_info index
        ## and format the data
        if data_format == "select2" and issuerno:
            FS_dates = es_client.search(
                index=Indices.sedar_prod_file_info.value,
                filter_path=["hits.hits._source"],
                _source=sedar().attribute_list,
                sort="FINANCIAL_END_DATE:desc",
                body=q,
            )

            if FS_dates:
                document_list = [doc["_source"] for doc in FS_dates["hits"]["hits"]]
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)


            for idx, entry in enumerate(document_list):
                if entry["FINANCIAL_END_DATE"] == None:
                    folder_date = f'{entry["FOLDER_YEAR"]}-{entry["FOLDER_MONTH"]}-{entry["FOLDER_DAY"]}'
                    dt = datetime.strptime(folder_date, "%Y-%m-%d").strftime(
                        "%B %d, %Y"
                    )
                else:
                    dt = datetime.strptime(
                        entry["FINANCIAL_END_DATE"], "%Y-%m-%dT%H:%M:%S"
                    ).strftime("%B %d, %Y")
                if idx == 0:
                    select = True
                else:
                    select = False

                dates_list.append(
                    {
                        "id": entry["FILE_ID"],
                        "text": dt
                        + " | "
                        + entry["DOCUMENT_DESC"].split("-", 1)[0].strip(),
                        "selected": select,
                    }
                )

        ## Step 4: If dates list is not empty, return dates list
        ## else return empty dictionary with a default message
        if not dates_list:
            dates_list.append(
                {"id": issuerno, "text": "No files found", "selected": data_format}
            )

        return Response(dates_list)


class companyDetailsView(APIView):
    """
    **Description:**

        This API(GET) is to recover company details. It uses either issuerno or company name as arguments.

    **Args:**

        issuerno (str): A unique identifier for a company.

        name (str): The name of the company to be searched or the default name of None.

    **Returns:**

        List containing company details

    **Example url:**

        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8980/api/company-details/?issuerno=00005820

    """

    def get(self, request, format=None):

        try:
            name        = strip_tags(request.GET.get("company"))
            lang        = validate(request.GET.get("lang"), LANGUAGE_PATTERN)
            issuerno    = validate(request.GET.get("issuerno"), ISSUENO_PATTERN)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)


        ## Step 1: If exact company name is provided, get issuerno
        ## from with get_company, else issuerno is provided issuerno
        if name:
            company = exact_name(name)
            issuerno = company["ISSUERNO"]

        ## Step 2: Query to recover company details
        ## using issuerno or groupno

        q = {
            "query": {
                "multi_match": {
                    "query": issuerno,
                    "fields": ["ISSUERNO", "GROUPNO"],
                }
            }
        }

        ## Step 3: Query necessary elasticsearch indexes
        index_list = [
            Indices.sedar_prod_other_issuer.value,
            Indices.sedar_prod_other_filer.value,
            Indices.sedar_prod_mutual_fund_group.value,
        ]

        documents = es_client.search(
            index=index_list,
            body=q,
            sort={"LASTUPDATE": {"order": "desc"}},
            scroll="2m",
            size=10000,
        )

        models = [
            CompanyDetails if lang == "en" else CompanyDetails_FR,
            ContactInformation,
            RecordDetails,
            OtherInformation if lang == "en" else OtherInformation_FR,
            BusinessAddress,
            MailingAddress,
        ]

        en_model_keys = [
            CompanyDetailsKeys,
            ContactInformationKeys,
            RecordDetailsKeys,
            OtherInformationKeys,
            BusinessAddressKeys,
            MailingAddressKeys,
        ]

        fr_model_keys = [
            CompanyDetailsKeys_FR,
            ContactInformationKeys_FR,
            RecordDetailsKeys_FR,
            OtherInformationKeys_FR,
            BusinessAddressKeys_FR,
            MailingAddressKeys_FR,
        ]

        en_keys = [
            "Company Details",
            "Contact Information",
            "Record Details",
            "Other Information",
            "Business Location",
            "Mailing Location",
        ]

        fr_keys = [
            "Détails de l'entreprise",
            "Informations sur le contact",
            "Détails de l'enregistrement",
            "Autres informations",
            "Emplacement de l'entreprise",
            "Adresse postale",
        ]

        model_keys = en_model_keys if lang == "en" else fr_model_keys
        keys = en_keys if lang == "en" else fr_keys

        company_info_dict = {}
        ## Step 4: If data is recovered from elasticsearch
        ## format return company details
        if documents:
            documents_list = [
                results["_source"] for results in documents["hits"]["hits"]
            ]
            if len(documents_list) >0 :
                for model, model_key, key in zip(models, model_keys, keys):
                    company_detail = company_model_processing(
                        model(**documents_list[0]), model_key()
                    )
                    for k, v in company_detail.items():
                        company_detail[k] = strip_tags(v)        
                    company_info_dict[key] = company_detail
                return Response(company_info_dict)
        return Response(status=status.HTTP_204_NO_CONTENT)


class pdffileView(APIView):
    """
    **Description:**

        This a GET API to recover the MinIO path to the pdf file stored in the MinIO database.
        For the GET request, it accepts a combination of name and date or just file id
        to recover the link.

    **Args:**

        data_format (str): The format of the data to be returned. It could be select2 or select1.

        name (str): The name of the company to be searched or the default name of None.

        issuerno (str): A unique identifier for a company.

    **Returns:**

        List of company dates in the selected format

    **Example url:**

        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8991/api/pdf-file/?file_id=a2dfe3c376ad8c8f5396001d4d673408f81ff637b78532cbbe0fa59bc443debb
    """

    def get(self, request, format=None):
        try:
            name        = strip_tags(request.GET.get("company"))
            date        = strip_tags(request.GET.get("date") )
            file_id     = validate(request.GET.get("file_id"), FILE_ID_PATTERN)
            data_format = validate(request.GET.get("data_format"), DATA_FORMAT_PATTERN)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

        if clean_media_folder():
            subprocess.Popen(
                ["bash", f"{BASE_DIR}/clean_media.sh"],
                cwd=BASE_DIR,
                stdin=subprocess.DEVNULL,
                stdout=open("clean_media.logs", "w"),
                stderr=subprocess.STDOUT,
                start_new_session=True,
            )

        local_path_name = join(MEDIA_ROOT, "".join(["pdf/", file_id, ".pdf"]))

        company_pdf_dict = {}

        table_query = {
            "query": {
                "bool": {
                    "must": [
                        {"match": {"FILE_ID": file_id}},
                    ]
                }
            }
        }

        pages = es_client.search(
            index=Indices.sedar_prod_page_description.value,
            filter_path=["hits.hits._source"],
            body=table_query,
        )

        pageDetails = PageDetails(
            **pages["hits"]["hits"][0]["_source"]["PAGE_INFO"]["DETECTED_PAGES"]
        ).dict()
        if data_format == "select2":
            list_of_pages = []
            for index, (page_name, page_number) in enumerate(
                pageDetails.items(), start=1
            ):
                if index > 3:
                    break
                page_name = page_name.replace("_", " ")
                page_name = page_name.title()
                list_of_pages.append(
                    {
                        "id": str(index),
                        "text": f"{page_name} (Page-{page_number})",
                        "page_no": str(page_number),
                    }
                )
            company_pdf_dict["DETECTED_PAGES"] = list_of_pages

        if default_storage.exists(local_path_name):
            company_pdf_dict["PDF_LINK"] = f"webapp/media/pdf/{file_id}.pdf"
            return Response(company_pdf_dict)

        ## Step 1: If File Id is not provided, query issuerno
        if not file_id:
            company_info = exact_name(name)
            issuerno = company_info["ISSUERNO"]
        ## Step 2: Query for elasticsearch either using file_id or issuerno and date
        if file_id:
            q = {
                "query": {
                    "bool": {
                        "must": [{"match": {"FILE_ID": file_id}}],
                    }
                }
            }

        else:
            q = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "query": issuerno,
                                    "fields": ["ISSUERNO", "GROUPNO"],
                                }
                            },
                            {"match": {"FINANCIAL_END_DATE": date}},
                        ]
                    }
                }
            }

        ## Step 3: Query sorted documents from elasticsearch
        FS_dates = es_client.search(
            index=Indices.sedar_prod_file_info.value,
            filter_path=["hits.hits._source"],
            size="10000",
            sort={"RECORD_DATE": {"order": "desc"}},
            body=q,
        )

        if FS_dates:
            document_list = [doc["_source"] for doc in FS_dates["hits"]["hits"]]

        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
            # return Response("No info of this company and date or file id on elasticsearch")

        ## Step 4: Query MinIO to recover pdf path on MinIO
        if document_list:
            year = document_list[0]["FOLDER_YEAR"]
            month = document_list[0]["FOLDER_MONTH"]
            file_id = document_list[0]["FILE_ID"]

            issuerno = (
                document_list[0]["ISSUERNO"]
                if document_list[0]["ISSUERNO"] is not None
                else document_list[0]["GROUPNO"]
            )

            minio_file_object = S3.get_data_loc(
                DataSource.sedar,
                int(year),
                int(month),
                issuerno,
                "".join([file_id, ".pdf"]),
            )
            stats = client.stat_loc(minio_file_object)
            if stats is not None:
                client.fget_loc(minio_file_object, local_path_name)
            else:
                company_pdf_dict["PDF_LINK"] = None
                return Response(company_pdf_dict)
        else:
            company_pdf_dict["PDF_LINK"] = None
            return Response(company_pdf_dict)
        company_pdf_dict["PDF_LINK"] = f"webapp/media/pdf/{file_id}.pdf"
        return Response(company_pdf_dict)


class extractedtableView(APIView):
    """
    **Description:**

        This both a GET and POST API to recover and update financial statement tables with edited tables.
        For the GET request, it accepts a combination of name and date or just file id
        to recover the link.
        For the POST request, it accepts file id, page type, and table json.

    **Args:**

        name (str): name of the company
        date (datetime): Financial end date of the company
        file_id (str): unique id of the document entry in elasticsearch for that company

    **Returns:**

        A path string of the MInIO address of the pdf file

    **Example url:**

        For GET:
        https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8991/api/extracted-tables/?file_id=a2dfe3c376ad8c8f5396001d4d673408f81ff637b78532cbbe0fa59bc443debb
    """

    def get(self, request, format=None):
        try:
            name        = strip_tags(request.GET.get("company"))
            date        = strip_tags(request.GET.get("date") )
            file_id     = validate(request.GET.get("file_id"), FILE_ID_PATTERN)
            lang        = validate(request.GET.get("lang"), LANGUAGE_PATTERN)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)



        if not file_id:
            company_info = exact_name(name)
            issuerno = company_info["ISSUERNO"]

        desc = sedar_desc()
        if file_id:
            q = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"FILE_ID": file_id}},
                            {"bool": {"should": desc}},
                        ],
                    }
                }
            }
        else:
            q = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"ISSUERNO": issuerno}},
                            {"match": {"FINANCIAL_END_DATE": date}},
                            {"bool": {"should": desc}},
                        ]
                    }
                }
            }

        file_info = es_client.search(
            index=Indices.sedar_prod_file_info.value,
            filter_path=["hits.hits._source"],
            size="10000",
            sort={"RECORD_DATE": {"order": "desc"}},
            body=q,
        )
        if file_info:
            document_list = [doc["_source"] for doc in file_info["hits"]["hits"]]
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
            # return Response(
            #     "No info of this company and date or file id on elasticsearch"
            # )

        if document_list:
            file_id = document_list[0]["FILE_ID"]

            table_query = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"FILE_ID": file_id}},
                        ]
                    }
                }
            }
            tables = es_client.search(
                index=Indices.sedar_prod_extracted_tables.value,
                filter_path=["hits.hits._source"],
                size="10000",
                body=table_query,
            )
            if len(tables) == 0:
                return Response(
                    "No tables for this company and date"
                    if lang == "en"
                    else "Aucun tableau pour cette entreprise et cette date"
                )
            financial_tables = [table["_source"] for table in tables["hits"]["hits"]]
            formatted_tables = format_table(financial_tables)
            return Response(formatted_tables)
        else:
            return Response(
                "No tables for this company and date"
                if lang == "en"
                else "Aucun tableau pour cette entreprise et cette date"
            )

    def post(self, request, format=None):
        try:
            file_id   = validate(request.data.get("file_id"), FILE_ID_PATTERN)
            table_md5 = validate(request.data.get("table_md5"), MD5_PATTERN)
            lang      = validate(request.data.get("lang"), LANGUAGE_PATTERN)
            revert    = validate(request.data.get("revert"), REVERT_PATTERN)

            page_type  = validate(request.data.get("page_type"), PAGE_TYPE_PATTERN)

            edited_table = request.data.get("table_json")
            ExtractedAPITableContent(EDITED_TABLE_1=edited_table)
            edited_table = strip_table(edited_table)
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

        try:
            es_serializer = Serializer()
            query = {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"FILE_ID": file_id}},
                            {"match": {"PAGE_TYPE": page_type}},
                        ]
                    }
                }
            }

            tables = es_client.search(
                index=Indices.sedar_prod_extracted_tables.value,
                filter_path=["hits.hits._source"],
                body=query,
            )
            document = ExtractedTable(**tables["hits"]["hits"][0]["_source"])

            if revert:
                document.EDITED_TABLE_2 = document.EDITED_TABLE_1
                document.EDITED_TABLE_1 = document.ORIGINAL_TABLE
                es_serializer.deserialize(
                    index_name=Indices.sedar_prod_extracted_tables.value, query=query
                )
                es_serializer.serialize(
                    index_name=Indices.sedar_prod_extracted_tables.value,
                    data_dict=document.dict(),
                )
                return Response(
                    {
                        "type": "success",
                        "message": "Table restored successfully"
                        if lang == "en"
                        else "Table restaurée avec succès",
                    }
                )

            elif not document.EDITED_TABLE_1 or table_md5 == MD5_hash(
                document.EDITED_TABLE_1
            ):
                document.EDITED_TABLE_2 = document.EDITED_TABLE_1
                document.EDITED_TABLE_1 = edited_table
                es_serializer.deserialize(
                    index_name=Indices.sedar_prod_extracted_tables.value, query=query
                )
                es_serializer.serialize(
                    index_name=Indices.sedar_prod_extracted_tables.value,
                    data_dict=document.dict(),
                )
                return Response(
                    {
                        "type": "success",
                        "message": "Table saved successfully"
                        if lang == "en"
                        else "Table sauvegardée avec succès",
                    }
                )
            else:
                resp_en = "Table cannot be saved because of conflicts. This happens when someone has recently edited the table after it was loaded on your screen. Please select one of the options to resolve the conflicts."
                resp_fr = "Le tableau ne peut être enregistré en raison de conflits. Cela se produit lorsque quelqu'un a récemment modifié le tableau après qu'il ait été chargé sur votre écran. Veuillez sélectionner l'une des options pour résoudre les conflits."
                resps = resp_en if lang == "en" else resp_fr

                note_en = (
                    "Note: Loading new changes from database will discard your edits. "
                )
                note_fr = "Note : Le chargement de nouvelles modifications à partir de la base de données annulera vos modifications. "
                notes = note_en if lang == "en" else note_fr

                table_jsn = table_merge_conflicts(document.EDITED_TABLE_1, edited_table)
                return Response(
                    {
                        "type": "conflict",
                        "message": resps
                        + '<div class="border border-light">'
                        + notes
                        + "</div>",
                        "table_json": table_jsn,
                        "new_MD5": MD5_hash(document.EDITED_TABLE_1),
                    }
                )

        except Exception as e:
            return Response({"type": "error", "message": str(e)})


class translateDictView(APIView):
    def get(self, request, format=None):
        lang = request.GET.get("lang")
        translateDict = dict()
        for key, value in TRANSLATE_DICT.items():
            translateDict[key] = value[lang]
        return Response(translateDict)


class HealthzView(APIView):
    def get(self, request):
        return Response({"status": "OK"}, status=status.HTTP_200_OK)


class ReadyzView(APIView):
    def get(self, request):
        return Response({"status": "OK"}, status=status.HTTP_200_OK)

class LivexView(APIView):
    def get(self, request):
        return Response({"status": "OK"}, status=status.HTTP_200_OK)
