from django.urls import path
from webapp.rest_api.views import companyView, filingdatesView, pdffileView, extractedtableView, companyDetailsView, translateDictView, HealthzView, ReadyzView, LivexView

urlpatterns = [
    path("companies/", companyView.as_view(), name="company-rest_api"),
    path("filing-dates/", filingdatesView.as_view(), name="filing-dates-rest_api"),
    path("pdf-file/", pdffileView.as_view(), name="pdf-file-rest_api"),
    path("company-details/", companyDetailsView.as_view(), name="company-details-rest_api"),
    path(
        "extracted-tables/", extractedtableView.as_view(), name="extracted-tables-rest_api"
    ),
    path(
        "translate-dict/", translateDictView.as_view(), name="translate-dict-rest_api"
    ),
    path("healthz/", HealthzView.as_view(), name="healthz-check"),
    path("readyz/", ReadyzView.as_view() ,name="readyz-check"),
    path("livez/", LivexView.as_view() ,name="livez-check")
]
