import hashlib
import itertools
import json
from datetime import date, datetime, timedelta
from html.parser import HTMLParser
from io import StringIO

from dateutil.parser import parse
from elasticsearch import Elasticsearch

from config.env import sedar, webapp_environ
from config.model_serializers import Indices
from config.web_models import ExtractedAPITable, ExtractedAPITableContent
from modules.s3client import S3Client


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()

def strip_tags(html):
    if html:
        s = MLStripper()
        s.feed(html)
        return s.get_data()
    return html

def strip_table(table):
    if table:
        for row in table:
            for k in row:
                row[k] = strip_tags(row[k])
    return table

def strip(obj:ExtractedAPITableContent):
    obj.ORIGINAL_TABLE = strip_table(obj.ORIGINAL_TABLE)
    obj.EDITED_TABLE_1 = strip_table(obj.EDITED_TABLE_1)
    obj.EDITED_TABLE_2 = strip_table(obj.EDITED_TABLE_2)
    obj.ORIGINAL_TABLE_MD5 = strip_tags(obj.ORIGINAL_TABLE_MD5)
    obj.EDITED_TABLE_1_MD5 = strip_tags(obj.EDITED_TABLE_1_MD5)
    obj.EDITED_TABLE_2_MD5 = strip_tags(obj.EDITED_TABLE_2_MD5)
    return obj

es_client = Elasticsearch(
    hosts=webapp_environ().ES_HOST,
    http_auth=(webapp_environ().ES_USER, webapp_environ().ES_PASS),
    timeout=1200,
    max_retries=10,
    retry_on_timeout=True,
)

client = S3Client.get_client('webapp_environ')


class Serializer:
    """
    Save any dict to ES index.
    Usage:

    serial
    """

    def serialize(self, index_name, data_dict):
        es_client.index(index=index_name, body=data_dict)

    def deserialize(self, index_name, query):
        es_client.delete_by_query(index=index_name, body=query)


def sedar_desc():
    """This functions aids in returning a list of queries for elasticsearch.
        The query is a list of "match_phrase" queries to sedar document description.

    Returns:
        [{'match_phrase': {'DOCUMENT_DESC': 'Audited annual financial statements - English'}},
        {'match_phrase': {'DOCUMENT_DESC': 'Interim financial statements/report - English'}},
        {'match_phrase': {'DOCUMENT_DESC': 'Interim financial statements/report (amended) - English'}},
        {'match_phrase': {'DOCUMENT_DESC': 'Audited annual financial statements (amended) - English'}}]
    """
    document_description = []
    for description in sedar().sedar_documents:
        document_description.append({"match_phrase": {"DOCUMENT_DESC": description}})
    return document_description


def company_model_processing(company_model, company_key):
    """This functions works with the company details rest_api.
        It accepts a pydanctic model containing all the available necessary company details called company_model.
        It also accepts a pydantic model containing the keys that we want assigned to the values in company_model.

    Args:
        company_model (Pydantic Model): The pydantic model with the company details.

        company_key (Pydantic Model): The pydantic model with the keys to be used in the output dictionary.

    Returns:
        A dictionary with the company details available from the company_model

    """
    comp_det = company_model.dict()
    comp_keys = company_key.dict()
    detail = {}
    for key, value in comp_det.items():
        if type(value) == datetime:
            detail[comp_keys[key]] = value.strftime("%B %d, %Y")
        elif type(value) == list:
            detail[comp_keys[key]] = ", ".join(value)
        else:
            detail[comp_keys[key]] = value

    return detail


def exact_name(name: str = None):
    query = {
        "query": {
            "bool": {
                "must": [
                    {
                        "multi_match": {
                            "query": name,
                            "fields": ["NAME_E", "NAME_F"],
                            "operator": "and",
                        }
                    }
                ]
            }
        }
    }
    ## Step 1a.2: Query sedar_prod_company_names
    documents = es_client.search(
        index=Indices.sedar_prod_company_names.value,
        body=query,
        filter_path=["hits.hits._source"],
    )
    if documents:

        company_name_list = [
            {
                "ISSUERNO": row["_source"]["ID"],
                "NAME": f"{row['_source']['NAME_E']}",
            }
            if "NAME_E" in row["_source"].keys()
            else {
                "ISSUERNO": row["_source"]["ID"],
                "NAME": f"{row['_source']['NAME_F']}",
            }
            for row in documents["hits"]["hits"]
        ]

        return company_name_list[0]

    else:
        return {"ISSUERNO": None, "NAME": None}


def format_table(tables: list) -> dict:
    formatted_table = ExtractedAPITable(**tables[0])

    for table in tables:
        if table["PAGE_TYPE"] == "BALANCE_SHEET":
            balance_table = strip(ExtractedAPITableContent(**table))
            formatted_table.BALANCE_SHEET = balance_table.dict()

        elif table["PAGE_TYPE"] == "INCOME_STATEMENT":
            income_statement_table = strip(ExtractedAPITableContent(**table))
            formatted_table.INCOME_STATEMENT = income_statement_table.dict()

        elif table["PAGE_TYPE"] == "CASHFLOW_STATEMENT":
            cash_flow_table = strip(ExtractedAPITableContent(**table))
            formatted_table.CASHFLOW_STATEMENT = cash_flow_table.dict()

    return formatted_table.dict()


def clean_media_folder():
    try:
        with open('clean_media.logs', 'r') as f:
            lines = f.read().splitlines()
            last_line = lines[-1]
            date_last = parse(last_line.split('|')[-1])
            # keel last 20 logs
            # with open("clean_media.logs", "w") as outfile:
            #     outfile.write("".join(f'{item}\n' for item in lines[-20:]))
            # Return boolean
            return True if date_last.date() <= date.today() - timedelta(days=7) else False
    except:
        return True


def MD5_hash(data: list):
    a = json.dumps(data, sort_keys=True)
    return hashlib.md5(a.encode("utf-8")).hexdigest()


def table_merge_conflicts(a: list, b: list):
    result = []

    def check_if_in_result_values(value):
        """Check if given value exists in list of dictionaries """
        for elem in result:
            if value == elem.get('data'):
                return True
        return False

    for combination in itertools.zip_longest(a, b):
        result.append(
            {
                "data": combination[0],
                "conflict": 'white'
            }
        ) if combination[0] is not None else False
        if not check_if_in_result_values(combination[1]):
            result[-1]['conflict'] = 'green'
            result.append(
                {
                    "data": combination[1],
                    "conflict": 'red'
                }
            )
    return result

