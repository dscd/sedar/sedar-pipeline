from django import template
from webapp.webapp.settings import TRANSLATE_DICT

register = template.Library()


@register.filter(name="get_label_lang")
def get_label_lang(value, arg):
    if TRANSLATE_DICT.get(value):
        return TRANSLATE_DICT.get(value).get(arg)
    else:
        return value
