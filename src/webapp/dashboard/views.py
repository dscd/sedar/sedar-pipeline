from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from webapp.webapp.settings import LANGUAGE_CODE
from django.utils import translation
from django.http import JsonResponse


def HomeView(request):
    if request.session.get("LANGUAGE_SESSION_KEY") is None:
        request.session["LANGUAGE_SESSION_KEY"] = LANGUAGE_CODE


    return render(request, "index.html")
    

def setLangView(request, language):
    if language:
        request.session["LANGUAGE_SESSION_KEY"] = language

    return JsonResponse({"lang": request.session["LANGUAGE_SESSION_KEY"]})
