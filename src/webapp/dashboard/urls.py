from django.urls import path
from webapp.dashboard.views import HomeView, setLangView

urlpatterns = [
    path("", HomeView, name="home"),
    path("lang/<str:language>/", setLangView, name="set-language"),
]
