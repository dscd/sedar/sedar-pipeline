"""webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from webapp.webapp.settings import MEDIA_URL
from django.urls import re_path
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webapp.dashboard.urls')),
    path('api/', include('webapp.rest_api.urls')),
]

urlpatterns += static('/webapp/static/', document_root=settings.STATIC_ROOT)
urlpatterns += [re_path(r'^webapp/media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT})]
