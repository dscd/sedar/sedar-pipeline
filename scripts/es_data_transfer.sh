#!/bin/bash

# Usage:
#  ./es_data_transfer.sh [<env_from>] [<env_to>] [<start_idx> <offset>]
# 
# Arguments:
#     <env_from>: Default value is k8s-cancentral-01-development
#     <env_to>: Default value is k8s-cancentral-01-production
#     <start_idx>: Only necesarry if execution is interrupted and you would 
#                  like to resume operation from the name of the index you left off. 
#                  Default value is sedar_prod_file_info (i.e, the first index in the list)
#                  If the index name provided is not correct, it won't resume operations.
#     <offset>:    Integer number to continue execution of data transfer from. It's always combined with 
#                  <start_idx> 
#

echo "[KILLING THE PROCESS ON LOCALHOST PORTS] 8081, 8082"
ES_FROM_PORT=8081
ES_TO_PORT=8082

KIBANA_FROM_PORT=9091
KIBANA_TO_PORT=9092

npx kill-port $ES_FROM_PORT
npx kill-port $ES_TO_PORT
npx kill-port $KIBANA_FROM_PORT
npx kill-port $KIBANA_TO_PORT

echo "[ALL PORTS KILLED] Start Port Forwarding Now!"

ENV_FROM=${1-k8s-cancentral-01-production}
ENV_TO=${2-k8s-cancentral-01-development}

START_IDX=${3-sedar_prod_company_names}
OFFSET=${4-0}

ES_INDICES=("sedar_prod_file_info" \
"sedar_prod_file_text" \
"sedar_prod_page_description" \
"sedar_prod_extracted_tables" \
"sedar_prod_other_issuer" \
"sedar_prod_other_filer" \
"sedar_prod_mutual_fund_group" \
"sedar_prod_mutual_fund_issuer" \
"sedar_prod_control_information" \
"sedar_prod_company_names")

find_index_of_value()
{
    NEEDLE=$1
    shift 1
    local HAYSTACK=("$@")

    for i in "${!HAYSTACK[@]}";
    do
        if [[ "${HAYSTACK[$i]}" = "${NEEDLE}" ]];
        then
           return $i;
        fi
    done
    return $(($i + 1))
}

expose_es_ports() {
    TARGET_ENV=$1
    EXPOSE_PORT=$2
    KIBANA_PORT=$3
    local -n ENV_PWD=$4
    kubectl config use-context $TARGET_ENV
    kubectl config set-context --current --namespace sedar
    kubectl port-forward svc/elasticsearch-es-http $EXPOSE_PORT:9200 &
    kubectl port-forward svc/kibana-kb-http  $KIBANA_PORT:5601 &
    ENV_PWD=$(kubectl get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo)
    echo "[$TARGET_ENV Port Forwarded] running on port http://localhost:$EXPOSE_PORT"
    return 0
}

es_migration(){
    FROM_ENV=$1
    FROM_PORT=$2
    FROM_PWD=$3

    TO_ENV=$4
    TO_PORT=$5
    TO_PWD=$6

    IDX=$7
    OFF=$8
    shift 8
    local INDICES_ARRAY=("$@")
    echo "[STARTING DATA TRANSFER]  $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";

    find_index_of_value $IDX "${INDICES_ARRAY[@]}"
    SUB_IDX="${INDICES_ARRAY[@]:$?}"
   
    for idx in ${SUB_IDX[@]}
    do
        if [[ $OFF -eq  0 ]]; then

            echo "DELETING TARGET INDEX: $idx"
            curl -X DELETE http://elastic:$TO_PWD@localhost:$TO_PORT/$idx
            echo "INDEX DELETED"

            echo "[MAPPING TRANSFER STARTED FOR $idx] $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";
            elasticdump \
            --input=http://elastic:$FROM_PWD@localhost:$FROM_PORT/$idx \
            --output=http://elastic:$TO_PWD@localhost:$TO_PORT/$idx \
            --type=mapping;
            echo "[MAPPING TRANSFER COMPLETE FOR $idx] $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";
        fi
        echo "[DATA TRANSFER STARTED FOR $idx] $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";
        elasticdump \
        --input=http://elastic:$FROM_PWD@localhost:$FROM_PORT/$idx \
        --output=http://elastic:$TO_PWD@localhost:$TO_PORT/$idx \
        --type=data \
        --offset=$OFF || return 1;

        echo "[DATA TRANSFER COMPLETE FOR $idx] $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";

        if [[ $OFF -ne 0 ]]; then
           OFF=0
        fi
    done
    echo "[ALL DATA TRANSFER COMPLETE]  $FROM_ENV($FROM_PORT) ===> $TO_ENV($TO_PORT)";
    return 0
}

ENV_FROM_PASSWORD=""
expose_es_ports $ENV_FROM $ES_FROM_PORT $KIBANA_FROM_PORT ENV_FROM_PASSWORD
ENV_TO_PASSWORD=""
expose_es_ports $ENV_TO $ES_TO_PORT $KIBANA_TO_PORT ENV_TO_PASSWORD

es_migration $ENV_FROM $ES_FROM_PORT $ENV_FROM_PASSWORD \
             $ENV_TO $ES_TO_PORT $ENV_TO_PASSWORD \
             $START_IDX $OFFSET "${ES_INDICES[@]}" 
