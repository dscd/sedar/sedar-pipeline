#!/bin/bash

alias k='kubectl'
alias kgp='k get pods'
alias kns='k config set-context --current --namespace'
alias create='k create -f'
alias kdp='k describe pods'
export dry='--dry-run=client -o yaml'
export now='--force --grace-period=0'
#alias drawio='docker run -it --rm --name="draw" -p 8080:8080 -p 7777:7777 jgraph/drawio'
alias dev-cluster='k config use-context k8s-cancentral-01-development'
alias prod-cluster='k config use-context k8s-cancentral-01-production'