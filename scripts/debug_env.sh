#!/bin/bash

# Usage:
#  ./debug_env.sh [<environment>] [<namespace>][<es_port>] [<minio_port>]
#
# Arguments:
#     <environment>: Name of the environment to forward services to debug.
#                    Default value is : k8s-cancentral-01-development
#     <namespace>:   K8s namespace name to use. Default value is "sedar"
#     <es_port>: Numeric value port indicating the localhost port forwarded to the ES instance.
#                Default value is 9200.
#     <minio_port>: Numeric value port indicating the localhost port forwarded to
#                   the minio instnace. Default value is 8080

ENVIRONMENT=${1-k8s-cancentral-01-development}
NAMESPACE=${2-sedar}
ES_PORT=${3-8081}
MINIO_PORT=${4-8080}
KIBANA_PORT=${5-9091}

npx kill-port $ES_PORT
npx kill-port $MINIO_PORT
npx kill-port $KIBANA_PORT
kubectl config use-context $ENVIRONMENT
kubectl config set-context --current --namespace $NAMESPACE
kubectl port-forward svc/elasticsearch-es-http $ES_PORT:9200 &
kubectl port-forward svc/s3proxy $MINIO_PORT:8080 &
kubectl port-forward svc/kibana-kb-http  $KIBANA_PORT:5601 &
ES_PWD=$(kubectl get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo)
echo Forwards
echo ES http://elastic:$ES_PWD@localhost:$ES_PORT  
echo Minio http://localhost:$MINIO_PORT
echo Kibana http://localhost:$KIBANA_PORT
