# v0.25.0 is the latest version of the micromamba image at the time of writing
# Original Dockerfile for micromamba v0.25.0
# https://github.com/mamba-org/micromamba-docker/blob/e2d1bf88a7ac01502ad1df6d3075ad4f199817f9/Dockerfile
ARG VERSION=0.25.0
FROM mambaorg/micromamba:${VERSION}

# Update zlib1g to fix CVE-2022-37434 (this stanza can be removed once CVE-2022-37434 is fixed in upstream image)
# sudo isn't part of micromamba image, so need to run the apt-get install as root user then immediately switch
# to $MAMBA_USER.
USER root
RUN apt-get update && \
    apt-get -y install zlib1g=1:1.2.11.dfsg-2+deb11u2 --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
USER $MAMBA_USER

# Copy the environment.yml file and build the conda virtual environment with mamba
COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/environment.yml
RUN micromamba create -n conda-sedar && \
    micromamba install --yes --file /tmp/environment.yml && \
    micromamba clean --all --yes

COPY --chown=$MAMBA_USER:$MAMBA_USER . /home/$MAMBA_USER

WORKDIR /home/$MAMBA_USER/src

# Need to activate mamba environment (otherwise python will not be found)
ARG MAMBA_DOCKERFILE_ACTIVATE=1  # (otherwise python will not be found)
