## Django MVT

The MVT (Model View Template) is a software design pattern. It is a collection of three important components Model View and Template. The Model helps to handle database. It is a data access layer which handles the data.

The Template is a presentation layer which handles User Interface part completely. The View is used to execute the business logic and interact with a model to carry data and renders a template.

Although Django follows MVC pattern but maintains it?s own conventions. So, control is handled by the framework itself.

There is no separate controller and complete application is based on Model View and Template. That?s why it is called MVT application.

See the following graph that shows the MVT based control flow.

![django-mvt-based-control-flow](uploads/cbc3b6d1314b1ada38dcd6cb5b338a0a/django-mvt-based-control-flow.png)

Here, a user requests for a resource to the Django, Django works as a controller and check to the available resource in URL.

If URL maps, a view is called that interact with model and template, it renders a template.

Django responds back to the user and sends a template as a response.![rest-api-model-1]

## Django Rest Framework

When a user interacts with the webpage. The JS file makes an API call for to fetch the data from Elastic Search(ES). Elsatic Search queries and return the Json which is displayed at the Front End.

![rest-api-model-1](uploads/ec3340a7f353c66479ceb8662220c154/rest-api-model-1.png)