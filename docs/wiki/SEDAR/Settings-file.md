...

<span dir="">#</span>This is the detailed documentation of Settings.yaml file used for the entire extraction process in Sedar.

```plaintext
# This is used as a prefix to save new tables or query already exsisting tables in the elastic search database.
database_project: "SEDAR"
```

* The database_project will always be set to the Project name.
* For instance this is used to load_data_to_es function in esdb_helper.py file in sedar_utilities folder. If the table name is set to "page_detection" and load_data_to_es function is used to load the data into database then the table is saved as "sedar_page_detection" as the prefix is set to sedar.
* This was developed to have a consistent naming convention for all the data tables in Elastic search for the Sedar project.

```plaintext
# Database Connections
ES_HOST: 'https://elastic.covid.cloud.statcan.ca'
ES_USR: '<Type your username>'
ES_PSW: '<Type your password>'
```

* The ES_HOST (url) will allow us to access the statcan elastic search database.
* ES_USR is the elastic search username and ES_PSW is the password. One should use their own credentials to access the elastic search.
* These credentials are needed to extract the new data and update the database after performing page detection, extraction and also for the dashboard. These credentials in setting file are used in almost every part of the project as we have to query the data and load the new data into the database on regular basis.

```plaintext
tar_bucket_name: "aaw-sedar"
tar_path: "sedar/"
minio_tar_files: "../minio/standard-tenant-1/private/sedar/"
minio_output_files: "../minio/standard-tenant-1/shared/aaw-sedar/sedar/"
```

* The tar_bucket_name is the Minio storage bucket where the Sedar files before processing(tar files) and also the processed files are saved. The Minio bucket for sedar project is "aaw-sedar".
* There are multiple folders in the "aaw_sedar" bucket. Tar_path is the actual folder inside the bucket where the files are stored.
* Minio storage bucket for Sedar can be accessed using the following URL <https://minio-standard-tenant-1.covid.cloud.statcan.ca/minio/aaw-sedar/>
* minio_tar_files : This is the path to local data storage folder. The tar files are saved in the Local advanced analytics workspace in the Sedar_prod namespace.
* minio_output_files: This is the path to access the extracted pdf documents. Once we receive the tar files from the CGI bucket we unzip these files and extract all the information from these files and save it in data tables like sedar_file_info, sedar_file_info_text. These unzipped pdf files are saved in the minio_output_files folder.

```plaintext
prod_bucket_name: "shared"
prod_path: "aaw-sedar/sedar/"
```

* prod_bucket_name : This is the bucket in Minio that we are using to store the files for production phase. The two buckets are private and shared. For the production phase we will be using shared
* prod_path : Inside the shared bucket `"aaw-sedar/sedar/"` is the path where we save the input tar files and the unzipped output pdf documents.

```plaintext
extraction_documents: [
# SEDAR financial stements are used
'Audited annual financial statements - English',
'Audited annual financial statements - French',
'Interim financial statements/report - English',
'Interim financial statements/report - French',

'Interim financial statements/report (amended) - French',
'Interim financial statements/report (amended) - English',
'Audited annual financial statements (amended) - French',
'Audited annual financial statements (amended) - English',
]
```

* The Extracted document list is used to run metadata extraction pipeline in Sedar_meta_data_extraction.ipynb. This notebook is used to used to extract information from unzipped tar files.
* We only need to identify and extract variables from the financial statements. Inorder to filter the financial statements we have created this dictionary with the types of financial statements included.
* To execute the metadata extraction part in the above mentioned notebook we developed a python script which is in sedar_utilities/metadata_extractor.py file.
* Once we filter the documents with just financial statements, we update the file_info_text table with the filtered info of all the pages in a pdf document.

```plaintext
# The below snippet is from the metadata_extractor.py python script where the document types are filtered. 
file_page_df = file_page_df[file_page_df["DOCUMENT_DESC"].isin(settings["extraction_documents"])]\
                    .rename({"page_no": "PAGE_NO"}, axis=1)\
                    [['CTL_INFO', 'ACCESSION_NO', "FILE_ID", "ISSUERNO", \
                      "PAGE_COUNT","SEDAR_FILE_PATH",\
                        "RECORD_DATE",  "TYPE",  "DOCUMENT_DESC", "PAGE_NO", 'DOCUMENT_INFO']]

    output["FILE"]["FILE_INFO_TEXT"] = file_page_df
## Output of this function will be the filtered data frame with financial documents. 
```

```plaintext
sedar_images_path: "../data-vol-1/output_images/"
sedar_document_path: "../data-vol-1/sedar/"
sedar_temp_directory: "../data-vol-2/sedar_temp/"
sedar_temp_files_to_transfer: "../data-vol-2/sedar_transfer_files/"
sedar_minio_temp_directory: "../data-vol-1/sedar_minio_temp/"
sedar_bbox_directory: "../data-vol-1/sedar_coordinates/bbox/"
sedar_xml_directory: "../data-vol-1/sedar_coordinates/xml/"
```

* **sedar_images_path** : Used in plot_coordinates_on_image function page_extraction.py python script. This path is used to save the images of financial documents once the variables are extracted.
* **sedar_document_path** : All the extracted pdf documents for which the document description is a financial statement as mentioned in the above list are transferred to the sedar_document_path folder. This folder has two other sub folders. One is French which has the French articles and the other one is English which has the English pdf documents. The transfer of these files to this folder is done through sedar_information_extraction.ipynb notebook.
* **sedar_temp_directory**: Not found.Used in sedar_utilities/file_utilities.py script. This path is used to unpack all the tar files and save it in the temporary folder.

  ```plaintext
  ### Snippet from file_utilities.py script
  
  def unpack_tar_file(
      zip_path, zip_name="", user="admin", destination=settings["sedar_temp_directory"]
  ):
      clean_temp_dir(user, zip_name)
      unzip_command = "tar -I {0} -xvf {1} -C {2}".format(
          settings["pigz"], zip_path, settings["sedar_temp_directory"]
      )
      a = delegator.run(unzip_command)
      return a
  ## Output is the unzipped tar files.
  ```
* **sedar_temp_files_to_transfer** : Not found in server. This folder is used the store the newly transferred files.
* **sedar_minio_temp_directory** : Not found
* **sedar_bbox_directory** : Not found
* **sedar_xml_directory**: Not found

```plaintext
pdf_info_path: "/opt/conda/envs/conda-sedar/bin/pdfinfo"
pdf_text_path: "/opt/conda/envs/conda-sedar/bin/pdftotext"
pdf_html_path: "/opt/conda/envs/conda-sedar/bin/pdftohtml"
```

* **pdf_info_path** : Not found
* **pdf_text_path** : pdftotext is a simple python package used to read all the text from the pdf document. This is used in create_coordinate_table function in page_extraction.py script.
* **pdf_html_path** : This package is used to convert pdf to html. This is used in get_coordinates_from_html function in page_extraction.py script.

```plaintext
pigz: "/opt/conda/envs/conda-sedar/bin/pigz"

nltk_path: "sedar_utilities/nltk_data/"

data_dictionary_path: 'sedar_data_dictionary.xlsx'
sedar_lookup_table_path: "sedar_lookup_table.xlsx"
```

* **Pigz (parallel implementation of gzip)**<span dir=""> is a free, open-source multi-threaded compression software for Linux that compresses and uncompresses files. Pigz is pronounced as “pig-zee”, it compresses data using the zlib and pthread libraries and takes full advantage of many processors and cores.</span> This is used in unpack_tar_files function in file_utilities.
* **nltk_path** : Path with the natural language tool kit.

```plaintext
seg_classify_model: '../data-vol-2/sedar_models/page_detection/SEG/segmentation_detection.sav'
seg_bag_of_word_model: '../data-vol-2/sedar_models/page_detection/SEG/segmentation_detection_bow.sav'
notes_bag_of_word_model: '../data-vol-2/sedar_models/page_detection/Notes/notes_detection_bow.sav'
notes_classify_model: '../data-vol-2/sedar_models/page_detection/Notes/notes_detection.sav'
bag_of_word_model: '../data-vol-2/sedar_models/page_detection/IS_BS/is_bs_detection_bow.sav'
classify_model: '../data-vol-2/sedar_models/page_detection/IS_BS/is_bs_detection.sav'
```
| Model | Usage |
|-------|-------|
| seg_classify_model | Built to identify the segmentation page |
| seg_bag_of_word_model | Bag of words features to identify segmentation page |
| notes_bag_of_word_model | Bag of words model to identify notes page |
| notes_classify_model | Classification Model to identify notes page |
| bag_of_word_model | Bag of words model for identifying Balance sheet and Income statement. |
| classify_model | Random forest classifier to classify Balance sheet and Income statement. |

These models are trained on sedar training data and saved in the above mentioned paths.

```plaintext
months: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'] 
currencies: ['cad','cdn','canadian dollar','usd','us dollars','u.s. dollars','united states dollar','canadian $','US $','united states $','c$','us$','euro']
units: ["$000","million","billion","thousand"]
```

Used to extract variables from the identified pdf documents. These variables are extracted in the pdf document. This is used in page_extraction.py script.