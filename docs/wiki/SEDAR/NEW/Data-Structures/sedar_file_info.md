Stores all PDF files and related info

:new: :sparkles: New Rows


:sparkles: Modified Rows

---- 
##### Proposed Schema

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| FILE_ID| str | Unique UUID generated for every file  | :sparkles: |
| ISSUERNO | str | Unique ID for company |   |
| GROUPNO  | str | Unique ID for Mutual Fund Group company |  |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| RECORD_DATE | DateTime | date for document submission|   |
| FINANCIAL_END_DATE | DateTime | date for financial end date | :new: :sparkles:  |
| PAGE_COUNT | int | total pages in pdf |   |
| FOLDER_YEAR | str| year, name of folder where file is present | :new: :sparkles:  |
| FOLDER_MONTH | str| month, name of folder where file is present | :new: :sparkles:  |
| FOLDER_DAY | str| day, name of folder where file is present | :new: :sparkles: |
| **OTHER FIELDS :arrow_down:** |  |   |   |
| CTL_INFO| str | Refer to the Control Block section above for content layout. | from sedar_data_dictionary.xlsx  |
| ACCESSION_NO| str |   |   |
| DOCUMENT_DESC | str | description of pdf |   |
| DOCUMENT_SUBTYPE| str | A numeric field that further defines the type of document | |
| DOCUMENT_TYPE | str | A numeric field relating a SEDAR system index with that of a document type | |  
| FILER_DESC | str| The description of the type of filer|  |
| FILER_TYPE | cell | cell |   |
| FILING_DATE | datetime| Date that the filing was originally made (yyyymmdd) (same as the Filing Date in the Control Block) |   |
| FILING_DESC | str | cell |   |
| FILING_TYPE | str | The description of the filing type |   |
| PROJECT_NO | str | A number identifying the project that the document is filed under (same as the number in the Control Block) |   |
| PUBLIC_DATE | DateTime | The date the document was processed by DDS (yyyymmdd) (same as the Acceptance Date in the Control Block) |   |
| SUBMISSION_NO | str | Each time a document is submitted to the SEDAR system, a submission is created. A submission is numbered for each project (same as the number in the Control Block) |   |
| THIRD_PARTY_FILER_INFO_NAME_E | str | Full name of 3rd Party Filer in English. The tag may be repeated within the <3rd PARTY FILER INFO> tag. |   |

##### Removed Fields

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| DATE_FOLDER | DateTime |  |  |
| RECORD_STATUS | str | cell |  thies field is not needed as control information can be used to remove these rows simply than changing this field |
| SEDAR_FILE_PATH | str | relative pdf file path for minio and data volumes |   |
| TYPE | str | company type | Should be in company info section   |
| ZIP_NAME | str | name of zip file |  not needed as it can be extracted frim zip name  |
| PDF_PATH | str | path of pdf relative to data and cache folder |  :sparkles: |
| XML_PATH | str | path of xml file relative to data and cache folder |  :sparkles: |
| ZIP_PATH | str | zip path from minio |   |
