Stores extracted tables from SLICE

:new: :sparkles: New Rows


:sparkles: Modified Rows

---- 
##### Proposed Schema

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| FILE_ID| str | Unique UUID generated for every file  | :sparkles: |
| ISSUERNO | str | Unique ID for company |   |
| GROUPNO  | str | Unique ID for Mutual Fund Group company |  |
| CREATED_ON | datetime | Date when data was added to ES |  |
| UNITS | str | Notational Unit for extracted table values  |  |
| CURRENCY | str | International code for currency  |  |
| FOLDER_YEAR | str| year, name of folder where file is present | :new: :sparkles:  |
| FOLDER_MONTH | str| month, name of folder where file is present | :new: :sparkles:  |
| FOLDER_DAY | str| day, name of folder where file is present | :new: :sparkles: |
| ORIGINAL_TABLE | JSON (list of Dictionaries) | Original extracted table by SLICE |  |
| EDITED_TABLE_1 |   JSON (list of Dictionaries) | Last edited table|  |
| EDITED_TABLE_2 |   JSON (list of Dictionaries) | Latest edited table |  |
| PAGE_NO | int | Page no of respective page type |  |
| PAGE_TYPE | str | BS/IS/CS/NOTES/SP | Value = BALANCE_SHEET, INCOME_STATEMENT, CASHFLOW_STATEMENT |

```
ORIGINAL_TABLE = List[Row]. # Each element of the list represent 0-base row of the table. 
Row => Dict[str,str] # Each key is a column and the value is the content of the cell. 
```