Stores page detection outputs related to detected page number 


:new: :sparkles: New Rows


:sparkles: Modified Rows

------

##### Proposed Schema

###### Benefit is that when we detect and add more pages to database it will be value manipulation of PAGE_INFO instead of adding a new column

| Field | DType | Description | Notes |
| ------ | ------ | ------ | ------ |
| FILE_ID| str | Unique UUID generated for every file  | :sparkles: |
| ISSUERNO | str | Unique ID for company |   |
| GROUPNO  | str | Unique ID for Mutual Fund Group company |  |
| FOLDER_YEAR | str| year, name of folder where file is present | :new: :sparkles:  |
| FOLDER_MONTH | str| month, name of folder where file is present | :new: :sparkles:  |
| FOLDER_DAY | str| day, name of folder where file is present | :new: :sparkles: |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| PAGE_INFO | Json | Detected Pages in PDF file | :new: :sparkles: Contains a Dict (JSON) as depicted below :arrow_down:  |
```
DETECTED_PAGES : JSON
key = Page type : str
value = Page Number : int starting from 1
```
```json
PAGE_INFO = {
"PAGE_COUNT": 20, 
"DETECTED_PAGES": {
  "BALANCE_SHEET": 2,
  "CASHFLOW_STATEMENT": 3, 
  "INCOME_STATEMENT": 6, 
  "SEGMENTATION_PAGE": 10, 
  "NOTES_PAGE": 20, 
  "OTHER": 17, 
}
}
```
