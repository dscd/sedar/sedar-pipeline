This table stores PDF text inside a page of PDF. Every row is a page inside PDF file

:new: :sparkles: New Rows


:sparkles: Modified Rows

---- 
##### Proposed Schema

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| FILE_ID| str | Unique UUID generated for every file  | :sparkles: |
| ISSUERNO | str | Unique ID for company |   |
| GROUPNO  | str | Unique ID for Mutual Fund Group company |  |
| FOLDER_YEAR | str| year, name of folder where file is present | :new: :sparkles:  |
| FOLDER_MONTH | str| month, name of folder where file is present | :new: :sparkles:  |
| FOLDER_DAY | str| day, name of folder where file is present | :new: :sparkles: |
| DOCUMENT_DESC | str | Quarterly report or Annual |   |
| ~~PAGE_NO~~|~~int~~| ~~page number of the PAGE_TEXT data~~|  |
|~~PAGE_TEXT~~ |~~str~~ |~~text for page number~~ |~~:sparkles: previously DOCUMENT_INFO~~|
| PAGES | json | list of strings with the content of all the pages. The index + 1 in the list defines the page number  | :sparkles: |

