Stores company names and their unique identifiers necessary for the API.

##### Proposed Schema

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| NAME_E| str | Name of company in english  | |
| NAME_F| str | Name of company in french |   |
| ID | str | Unique ID which is either ISSUERNO or GROUPNO |  |
| ISSUERNO| str  | Unique ID for company  |   |
| GROUPNO|  str |  Unique ID for Mutual Fund Group company |   |
| LASTUPDATE| DateTime | Last date this profile was updated before being included on the cover page of this filing (yyyymmdd) |  |
| CREATED_ON | DateTime | Date on which the record was created / updated |
| TYPE | str | Type of user / submission : OTHER_FILER, OTHER_ISSUER, MUTUAL_FUND_GROUP |