In Django, files which are uploaded or downloaded by the user are called Media or Media Files. When the user interacts with the webapp, a pdf downloads and saves in the media folder. This PDF is further displayed on the Company Statements Page.

Just as with static files, to serve media files we have do add some configurations in our settings.py file

## Media Files Configurations #

Media files depend upon two configurations:
- MEDIA_ROOT
- MEDIA_URL

None of these are set by default.

`MEDIA_URL = os.path.join(URL_POST_DOMAIN , 'webapp', 'media', '')`

##  MEDIA_ROOT Setting
It contains the absolute path to the file system where Media files will be uploaded. It accepts a string, not a list or tuple. 

```
MEDIA_ROOT = os.path.join(BASE_DIR, 'webapp', 'media')
MEDIAFILES_DIRS = [
    os.path.join(BASE_DIR, 'webapp', 'media')
]
```

## MEDIA_URL Setting
This setting works similar to STATIC_URL and is used to access media files. Just below the MEDIA_ROOT setting add the following code to the end of settings.py file.

`urlpatterns += static('/webapp/media/', document_root=settings.MEDIA_ROOT)`
