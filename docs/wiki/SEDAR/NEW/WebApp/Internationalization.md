
## Internationalization 

The goal of internationalization and localization is to allow a single web application to offer its content in languages and formats tailored to the audience.

Django has full support for translation of text, formatting of dates, times and numbers, and time zones.

## Step 1) Create/Add a Dashboard App in the webapp

The Dashboard app handles and maintains the session in the Django app. A language session is stored and and then passed to the Front UI which transaltes the text for the user. 

```
def HomeView(request):
    if request.session.get("LANGUAGE_SESSION_KEY") is None:
        request.session["LANGUAGE_SESSION_KEY"] = LANGUAGE_CODE

    DOMAIN_TO_USE = (
        f"https://{URL_BASE_DOMAIN}{URL_POST_DOMAIN}/"  # this is used url for rest_api calls
    )
    return render(request, "index.html", {"domain_to_use": DOMAIN_TO_USE})

def setLangView(request, language):
    if language:
        request.session["LANGUAGE_SESSION_KEY"] = language

    return JsonResponse({"lang": request.session["LANGUAGE_SESSION_KEY"]})

```

## Step 2) Declare dashboard app in the settings.py file


```INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    "whitenoise.runserver_nostatic", 
    'django.contrib.staticfiles',
    'rest_framework',
    'webapp.dashboard',
    'webapp.rest_api',]
```

## Step 3) Add language codes and their codes in the settings.py

```
LANGUAGE_CODE = "en"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = (
    ("en", "English"),
    ("fr", "French"),
)

TRANSLATE_DICT = TRANSLATE_DICT

```

## Step 4) Create Translate_dict.py file to store the english and french text used in the application.

`TRANSLATE_DICT = {
    "Details": {"en": "Details", "fr": "Des détails"},
    "Location": {"en": "Location", "fr": "Lieu"},
    "Statements": {"en": "Statements", "fr": "Déclarations"},
    "Select Company": {"en": "Select Company", "fr": "Sélectionnez l'entreprise"},
    "Select Date": {"en": "Select Date", "fr": "Sélectionner une date"},
    "Auto Page Change": {
        "en": "Auto Page Change",
        "fr": "Changement de page automatiquet",
    },`


## Step 5) Setting the text in JS and HTML files

**Usage: {{ "Text Input to show"| get_label_lang:request.session.LANGUAGE_SESSION_KEY}}**

- **Text Input to show** - Add the key to the text you would like to display, For instance, in order to display Select Company in either EN or FR, enter 'Select Company' in the span. Django uses the 'Select Compnay' as a key and returns the translated text depending upon the session running in the app.
- **get_label_lang:request.session.LANGUAGE_SESSION_KEY**  Returns the translated label for the text depensing on the language session.

