The webapp uses Django and there are some important files which store settings and required code. 
## webapp/settings.py
Settings.py stores all settings related to webapp. Some of the important settings here are 
- DEBUG mode (Is true when app is in development, false when in production) 
Extensive testing should be performed when switching to False.
- Static Root: is the path which will be used for storing static before running the app . Here static files from Django admin and Django rest framework will be saved.
- Static files dir: This is path which stores custom static files created by a developer. All the files are copied to static root when running the app with the help of `python manage.py collectstatic` command specified in the startapp.sh
- Media files dir: This is a path where media files such as pdf will be stored
- Database: This has default settings for sqlite database which is used to store session details of internationalization.
- URL_BASE_DOMAIN: This is kubeflow url 
- URL_POST_DOMAIN: This is the kubeflow post domain including the weabpp port
- FORCE_SCRIPT_NAME: Used to set aaw domain name for django. This is default django confriguration variable used to let django know what is the post domain for the webapp address. 
- Allowed_host: This setting will allow only the URL_BASE_DOMAIN to be used for running the webapp.

## webapp/urls.py
Here URL's are connected to Django app views. The settings include urls fot the dashboard app with static and media url. Individual settings can be found in app/url.py

## dashboard/views.py
- HomeView : This function is a Django view responsible to serve the index.html on the base webapp address. It sends domain_to_use value from the  settings.py used for REST API calls from the javascript.
- SetLangView: Refer international documentation here.

## dashboard/urls.py
Here Path for both homeView and langView are set. The homepage is set to `/` because this is the default home page required to served on the dashboard url.

 ## runapp.sh
this is a bash executable script which should be used to run the app. There are various process this app performs to run the app
- Loads .env variables
- initializes sqlite database for storing session details required for internationalization.
- Collects and copies required static files to static root directory set in settings.py
- Runs the app on a port with the help of django server or gunicorn server.