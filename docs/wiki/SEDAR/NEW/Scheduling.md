#### Frequency of running major components of the pipeline are as follows:
| Item | Frequency | Comments |
|------|-----------|----------|
| Receive new zip files | One per day | Only one new zip file is uploaded by data providers each day, if any. It is possible that some days no new files are uploaded in the data provider server side folder. |
| SFTP CronJob | Runs 6 AM daily | SFTP CronJob is set up to run everyday at 6 AM EST (10 AM UTC), it copies contents of the data provider server side folder, it copies new files and slightly dated files back into MinIO 'inbox' folder. Old zip files dating back to about 2 weeks usually reside in the folder along with the most recent one. |
| SEDAR pipeline | Run once in 2 weeks (biweekly) | SEDAR pipeline is currently run biweekly since clients have consented to that frequency. This frequency is not a hard requirement, can be modified to run more often. |
| Remove old SEDAR data | Run once very quarter | SEDAR retention policy agreement states that only past 3 years of data needs to be displayed on the SEDAR WebApp. Run the SEDAR pipeline with remove old data option to periodically remove data past 3 years old. This frequency is not a hard requirement, just a suggestion. It can be modified to run more often or less often (monthly, half yearly, yearly) |

