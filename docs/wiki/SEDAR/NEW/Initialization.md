The first step in the SEDAR pipeline is initialization of models, lookup tables and NLTK downloadable data.

The script used can be found as: src/modules/init_models.py

#### Steps followed by the script:

Look for a 'cache' folder in the local code repository-

* (if clause) If the 'cache' directory is **not** found,
  * create one.
  * List all the files available in MinIO 'models' folder. Download them all.
* (else clause) If 'cache' directory is found,
  * Compare the last update time for all models, lookup tables, NLTK data available in this local cache folder with the ones available in MinIO 'models' folder
    * If the date is the same, print message "Model <> in local is up-to-date"
    * If the date is not the same, download updated models, lookup tables and/or NLTK downloadable data.
    * This also downloads any new files not available in local 'cache' folder.

#### MinIO Models folder structure:

```plaintext
models 
  |- lookup_tables
             |- sedar_data_dictionary.xlsx
             |- sedar_lookup_table.xlsx
  |- page_detection
            |- IS_BS
                  |- finalized_model_rf.sav
                  |- is_bs_detection.sav
                  |- is_bs_detection_bow.sav
            |- Notes
                  |- notes-detection.sav
                  |- notes_detection_bow.sav
            |- SEG
                  |- segmentation_detection.sav
                  |- segmentation_detection_bow.sav
            |- nltk_data
                  |- corpora
                        |- \.......
                  |- tokenizers
                        |- \........
```