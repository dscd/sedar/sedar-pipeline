SEDAR data resides in ElasticSearch Database. The indices belonging to SEDAR start with 'sedar_'. Indices description is as follows

## sedar_*

##### NEW
- [sedar_prod_file_info ](SEDAR/NEW/Data-Structures/sedar_file_info )
- [sedar_prod_page_description](SEDAR/NEW/Data-Structures/sedar_page_description)
- [sedar_prod_extracted_tables](SEDAR/NEW/Data-Structures/sedar_extracted_tables)
- [sedar_prod_file_text](SEDAR/NEW/Data-Structures/sedar_file_text)
- [sedar_prod_company_names](SEDAR/NEW/Data-Structures/sedar_company_names)


Tables related to company information
- [sedar_prod_mutual_fund_group](SEDAR/OLD/Data-Structures/sedar_mutual_fund_group)
- [sedar_prod_mutual_fund_issuer](SEDAR/OLD/Data-Structures/sedar_mutual_fund_issuer)
- [sedar_prod_other_filer](SEDAR/OLD/Data-Structures/sedar_other_filer)
- [sedar_prod_other_issuer](SEDAR/OLD/Data-Structures/sedar_other_issuer)



## sedar_schema_*
- Schema tables which include field and Datatype.
- only used sedar_schema_page_detection for webapp for fetching detected pages.
- Notes: I think this table is not useful because datatype can be handled in pipeline and then python_elasticsearch_dsl returns datatype when querying data

## sedar_lookup_*
- This table stores lookup information because data is collected in binary digits which is converted to text using these lookup tables.


## New Schema (FileInfoExtractor) 
![fileInfo__1_](uploads/22513776eb7d00c91ac8b384296914d3/fileInfo__1_.jpg)

```python 
class FileInfoExtractor(FileID):
         pdfpath: str  
         xmlpath: str 
         
        FILE_INFO: FileInfo = Field(default_factory=FileInfo())
        def sync():
            self.FILE_INFO.__sync__(self, self.FILE_INFO)
            self.FILE_INFO.sync()
  
        def __init__(self, *args, **kawars):
            super().__init__(args,kawrs)
            self.sync()
          
        def compute_metadata(self) -> None: 
               “””
               Populates 
                     OtherIssuer, or 
                     MutualFundGroup, 
                     MutualFundIssuer, 
                     OtherFile
               “””
               self.sync()
               return None

        def compute_text() -> None: 
              “””
               Populates
                     FileText
              “””
              self.sync()
              Return None

         def compute_page() -> None: 
              “””
               Populates
                    PageDescription
              “””
               self.sync()
               Return None

         def compute_tables() -> None:
                “””
               Populates
                     extracted_tables
              “””
              self.sync()



````