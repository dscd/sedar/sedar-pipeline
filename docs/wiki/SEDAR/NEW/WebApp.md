Web App access link - [https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8990/](https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/8990/)

Web App is built using Django Framework. Documentation should be referred for the information about Django. The parts most useful to this dashboard are:
- [Part 1: Requests and responses](https://docs.djangoproject.com/en/4.0/intro/tutorial01/)
- [Part 3: Views and templates](https://docs.djangoproject.com/en/4.0/intro/tutorial03/)
- [Part 6: Static files](https://docs.djangoproject.com/en/4.0/intro/tutorial06/)
- Other Parts provide relevant content which should be used for advanced Django development. [Django Documentation](https://docs.djangoproject.com/en/4.0/)

## Django on AAW
Some settings are required for the Django deployment to work on AAW. These settings include url renaming for AAW's reverse proxy to work.
I have made a starter Django Project preconfigured for AAW which can be found at - [https://gitlab.com/dsd4/pssd/django-boilerplate-for-aaw](https://gitlab.com/dsd4/pssd/django-boilerplate-for-aaw)
The gitlab repo also contains documentation for the settings in the readme section.

## SEDAR WebApp
- [Folder Structure](Folder-Structure)
- [Django](SEDAR/NEW/WebApp/Django)
- [Static Files](SEDAR/NEW/WebApp/Static-Files)
- [Media Files](SEDAR/NEW/WebApp/Media-Files)
- [Internationalization](SEDAR/NEW/WebApp/Internationalization)
- [Templates](Templates)
- [Django Architecture](Architecture-of-Django-webapp)

## Running the SEDAR Webapp
Create a ```conda-sedar``` virtual environment for running this app with Python 3. 
Clone this repository, open your terminal/command prompt in the project root folder folder .

```bash
$conda env create -f environment.yml
$source activate conda-sedar
$cd src/ 
```

Once you have created environment. You have to go to create a .env file insite src/webapp directory

```bash
WEBAPP_DEBUG_MODE="True"
WEBAPP_PORT=8990
WEBAPP_SECRET_KEY= Generate a secret key 
WEBAPP_BASE_DOMAIN="kubeflow.aaw.cloud.statcan.ca"
MINIO_URL="http://minio-gateway.minio-gateway-standard-system:9000"
MINIO_ACCESS_KEY="access_key"
MINIO_SECRET_KEY="secret_key"
ES_HOST="http://daaas-es-http.shared-daaas-system:9200"
ES_USER="user"
ES_PASS="pass"
```
How to generate a secret key (Run this command in a console)
```bash
$python -c "from django.core.management.utils import get_random_secret_key; print(get_random_secret_key());" 
```

Finally after you change these settings you can run the app with typing this command in terminal
```bash
$ ./run_webapp.sh
```




