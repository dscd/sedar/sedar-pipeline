# Overview

The SFTP Cronjob retrieves a list of tar zip files from our external data provider. The data is placed into the MinIO location.

### Input:

MinIO path containing a list of of tar zip files. The files follow a name template as: `archive_yyyy-mm-dd.tar.gz` or `archive-yyyy-mm.tar.gz` .

### Output:

The pipeline doesn't output a result, instead it triages files to their correct MinIO output locations. It populates Elastic Search database. It places individual pdf's inside the folder. It places the raw tar files inside the folder.

### Data Flow (Pseudocode)

```plaintext
 
  0. Initialize cache/models folder and ctl_file_list with FILEID's from Control Information index.

for zip file in <inbox>:
  try:
     1. Try to match date archive file name with expected date format: archive_(yyyy)-(mm)-(dd)?.tar.gz
          1.a if file DOESN’T match regular expression, continue

     2. capture year and month from file name.
    
     3. check if zip file exists in MinIO historical data, using the S3 proxy class and year, month from filename 
        3.a (YES) Check if —-force flag was set as argument :
                  3.a.a (YES) Same as 3.b
                  3.a.b (NO) remove file from inbox, and continue
        3.b. (NO) Download file from inbox to “cache/historical/sedar/yyyy/mm/archive_yyyy-mm-dd.tar.gz” 

     4. check if local folder with name “cache/historical/sedar/yyyy/mm/archive_yyyy-mm-dd” exists?
         4.a (YES) continue to 5
         4.b (NO) untar unzip .tar.gz file into “cache/historical/sedar/yyyy/mm/archive_yyyy-mm-dd/…”

     5. For each *.ctl file in “cache/historical/sedar/yyyy/mm/archive_yyyy-mm-dd/”, generate file_id based on the information 
        inside the ctl files.
         5.1 if file_id in Control Information Index:
            5.1.1 (YES) Do no ingest the data
            5.1.2 (NO) Ingest the data into index.
         5.2 Consider all File Ids from Control Information Index and delete previous records in other Indices associated 
              with File Ids.
         5.3 Pass the list of File Ids from Control Information Index to the next step.

  except:
      Upload *.xml, *.pdf, *.txt and a *.message (with exception type, message and stack trace) files to MinIO error folder.
      Remove *.pdf, *.xml and *.txt  

     6. For each *.pdf, *.xml file pair in cache/historical/sedar/yyyy/mm/archive_yyyy-mm-dd/:
       try:
           6.1 Generate file_id and if file is not listed in the filter delete them, else process them
           6.2 Compute metadata (pass the xml file path)
           6.3 If file description is in extraction_document list, compute text extraction
           6.4 If file description is in sedar_document list: 
                6.4.1 Compute page detection 
                6.4.2 Run page extraction (pass the pdf path)
           6.5 Update ES records (override if exists: remove all data associated to this record and add new one) 
               Does a previous record for this document exists? 
                6.5.a (YES) remove previous record in indices that use file_id and add new one. Keep uuid from previous 
                             record and pass it to 6.6
                6.5.b (NO) add new record
           6.5.1 Update submission type indices that use ISSUERNO / GROUPNO as id
           6.6 Update MinIO pdf. Overwrite PDF if it exists on Minio.
           6.7 Remove *.pdf, *.xml and *.txt 
      except:
           Upload *.xml, *.pdf, *.txt and a *.message (with exception type, message and stack trace) files to 
           MinIO error folder.
           Remove *.pdf, *.xml and *.txt  

     7. move inbox file to historical data or error data. If --keep-inbox flag is set, keep a copy of the file in inbox. 

     8. remove cache files

```

# Few modifications

1. There is no need for logging user, the user is _sedar_ or _admin_ …… The pipeline runs autonomous, no user need to be declared.
2. Only one instance of ES or MinIO client should be created. This should be passed by reference to any method that needs it.