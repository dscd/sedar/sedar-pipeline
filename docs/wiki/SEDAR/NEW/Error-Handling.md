There are two try-except blocks within the sedar_pipeline.py script. The functionality is as follows:

1. try-except right after unzipping tar file:
   * Catches any error or exception occurred at any point between Step 1 - Step 5 of the data flow
2. try-except right after picking one .xml file from the unzipped tar files folder:
   * Catches any error or exception occurred at any point between Step 6.1 - Step 6.7 of the data flow 

#### Functionality within the except clause:

* Uses system exception information to create a text file with .message extension, named same as the .xml file currently being processed, that contains information as follows:
  * exception type
  * exception message
  * stack trace
* The current <>.xml, <>.pdf, <>.txt is uploaded to MinIO 'error' folder location along the <>.message file in a directory named archive-YYYY-MM-DD based on the tar folder the file belong to.
* These <>.xml, <>.pdf, <>.txt files are removed from the local cache unzipped archive-YYYY-MM-DD directory to move on to the next file for processing.

The idea is to come back to these errors at a later date and to avoid disruption of the pipeline run.