## Introduction
REST API is a microservice which will be used to GET,POST,DELETE data from the database to perform various tasks on the dashboard.

For this project, API's are created using the Django framework. 
Below is an example of how to create an API with to perform a GET request:

Install necessary libraries
```python
conda install -c anaconda django
conda install -c conda-forge djangorestframework
conda install -c conda-forge markdown
```
Create a webapp project
```python
$ django-admin startproject webapp
```
Make sure you’re in the same directory as manage.py and type this command to create an api directory
```python
$ python manage.py startapp api
```
Add API and rest_framework library to the main settings.py
```python
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",

    "rest_framework",
    "api",
]
```
Add api to the main urls.py
```python
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("api.urls")),
]

```

Create api's in view.py of the api folder
```python
from rest_framework.views import APIView
from rest_framework.response import Response

class nameView(APIView):
       """DocString"""
       def get(self, request, format=None):
               name = request.GET.get("company")
               data_format = request.GET.get("data_format")
               output = {"NAME": name, "FORMAT": data_format}

               return Response(output)
``` 

Then finally import and add the nameView api class to the urls.py in the api 
folder.
```python
from django.urls import path
from api.views import nameView

urlpatterns = [
    path("name/", nameView.as_view(), name="name-api"),
]

```
The api can now be called in a webbrowser using the url:
```python
https://kubeflow.aaw.cloud.statcan.ca/notebook/aaw-sedar/sedar-dev-2/proxy/{PORT}/api/name/?company=Apple&data_format=Select2
```

## Sedar Production Rest Api's
For this project there are 5 API required for the Dashboard:
company_names, company_details, file_dates, pdf_file, and extracted_tables..

### COMPANY NAMES
- This API returns list of all company names in the format of `Company Name (ISSUERNO)`
- GET: Returns the list of all unique company names in the specified format
- ARGUMENTS, filter_string, OPTIONAL: filter input string. If a string is provided the list of companies should be filtered if that string is found in the company name
- refer to **sample1**
Tables Used
* [sedar_mutual_fund_issuer](../../OLD/Data-Structures/sedar_mutual_fund_issuer)
* [sedar_other_filer](../../OLD/Data-Structures/sedar_other_filer)
* [sedar_other_issuer](../../OLD/Data-Structures/sedar_other_issuer)

# IF data_format is None
```python
{
  "items": [
        {
            "ISSUERNO": str,
            "NAME": str
        }
    ],
    "total_count": int,
    "scroll_id": str
}
``` 

# IF data_format == 'select2'
```python
{
    "items": [
        {
            "id": str,
            "text": str,
            "selected": bool
        }
    ],
    "total_count": int,
    "scroll_id": str
}

``` 

### COMPANY DETAILS
- This API accepts an Issuer Number or Exact Company Name and returns company related details and company address related details. Please refer old sedar dashboard for the required data. 
- GET, ARGUMENTS, issuerno, name NOT OPTIONAL: Filters based on issuer number to find company related details

Below is an example of the output from the api:
```python
{
    "Company Details": {
        "Company Name English": str,
        "Company Name French": str,
        "ISSUER NO": str,
        "Form Dates": str,
        "CUSIP": str,
        "Exchanges": str,
        "Form Type": str,
        "Year End Date": str,
        "Industry": str,
        "Report": str,
        "OTC Issuer Detail": str,
        "Size of Issuer": str,
        "Stock Symbol": str,
        "Type": str,
        "Financial Period Ended Date": str,
        "Financial Period Date Relates": str
    },
    "Contact Information": {
        "Contact Name": str,
        "Phone Area Code": str,
        "Phone Number": str,
        "Extension": str,
        "Fax Area Code": str,
        "Fax Number": str
    },
    "Record Details": {
        "Last Update": str,
        "Short Form Propectus Issuer": str,
        "Created On": str,
        "Record Date": str
    },
    "Other Information": {
        "Partner English": str,
        "Partner French": str,
        "Auditor English": str,
        "Auditor French": str,
        "Transfer Agent English": str,
        "Transfer Agent French": str,
        "Registration Application": str,
        "Application for Exemption": str,
        "National Policy Elections": str,
        "Offerings": str,
        "Security Type": str,
        "Proceeds": str,
        "Underwriter English": str,
        "Underwriter French": str,
        "Underwriter Counsel": str,
        "Underwriter Contact": str,
        "Promoter": str,
        "Prefiling": str,
        "Local Policy Statement": str
    },
    "Business Address": {
        "City": str,
        "Postal Code": str,
        "Country": str,
        "Phone Extension Number": str,
        "Fax Area Code": str,
        "Fax Number": str,
        "Phone Area Code": str,
        "Phone Number": str,
        "Province": str,
        "Street 1": str,
        "Street 2": str
    },
    "Mailing address": {
        "City": str,
        "Postal Code": str,
        "Country": str,
        "Phone Extension Number": str,
        "Fax Area Code": str,
        "Fax Number": str,
        "Phone Area Code": str,
        "Phone Number": str,
        "Province": str,
        "Street 1": str,
        "Street 2": str
    }
}
```

### FILE DATES
- This API always accepts data format and either exact company name or issuer number as input and returns available file dates for that company in the entire database. The data is always returned in the format of `Date - Document Type (annual or quarterly)`
- GET, ARGUMENTS, name, data format, issuer no NOT OPTIONAL: Filters based on the name and returns the data in the specified format
- refer to **sample1**

# sample1
```python
"""
id = integer
text = String (Which will be displayed in the dropdown of the specified format)
selected = Boolean (True or False, Value with true will be the default selected option in the dropdown)
"""
[
    {
        "id": str,
        "text": str,
        "selected": bool
    },
    {
        "id": str,
        "text": str,
        "selected": bool
    },
]

``` 
### PDF FILE
- This api accepts arguments company name and date or just File id. Based on the input, data is filtered and the respective pdf file minio link is fetched. The pdf is saved in a temporary folder inside static directory (inside web app folder) which is then used to show pdf file on dashboard. 
- GET: Returns the static link of pdf file based on name and date input
- GET, ARGUMENTS, name, date or just file_id: Filters based on the name and date and returns the pdf file link returns object , saves on webapp static and return link

Below is an example of a MinIO path
```python
{
    "Company PDF": str
}
```

### EXTRACTED TABLES
- This api accepts arguments company name and date or just File id. Based on the input arguments, extracted tables is queried.
- GET: Returns the JSON of extracted table of the company filtered based on name and date or just file_id input
- GET, ARGUMENTS, name, date, file_id NOT OPTIONAL: Filters based on the name and date or just file_id, and returns JSON of Extracted Table
- POST: This will be used for editing the tables which will return updated table json to be stored inside database
- DELETE: Deleting the oldest table will be handled by post api itself. 

Below is an example of the output from GET api:
```python
{
    "FILE_ID": str,
    "ISSUERNO": str,
    "GROUPNO": str,
    "BALANCE_SHEET": {
        "ORIGINAL_TABLE": List[dict],
        "EDITED_TABLE_1": List[dict],
        "EDITED_TABLE_2": List[dict],
    },
    "INCOME_STATEMENT": {
        "ORIGINAL_TABLE": List[dict],
        "EDITED_TABLE_1": List[dict],
        "EDITED_TABLE_2": List[dict],
    },
    "CASHFLOW_STATEMENT":{
        "ORIGINAL_TABLE": List[dict],
        "EDITED_TABLE_1": List[dict],
        "EDITED_TABLE_2": List[dict],
    },
}
```

Below is an example of the output from POST api:
```python
## If successful 
{'type': 'success', 'message': 'Table saved successfully'}

## If an error occurs
{'type': 'error', 'message': 'Error saving table'}
```
### Notes
- Arguments for some of REST services can be replaced/used along with FILEID to query the database.
- Pydantic models should be used to serialize and de-serialize data. 
- "name" as see above refers to the company name
