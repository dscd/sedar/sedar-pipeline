## Data Storage

![image](uploads/88147cc3c7906edff28f131afb0cfdaf/image.png)

The above diagram describes the entire process of file transfer and storage that takes place in the Sedar pipeline.

**Process Flow :**

1. CGI places the tar files on the SFTP server. We have developed a python script to pull these files from the SFTP server and place it in Minio Storage.
2. The Script to SFTP-cron job which executes every day to pull these files from SFTP server to Minio storage can be found in the following URL <https://gitlab.k8s.cloud.statcan.ca/data-science-division/aeas-aera/sedar/sftp-cronjob>
3. Minio Storage can be accessed through the following URL <https://minio-standard-tenant-1.covid.cloud.statcan.ca/minio/aaw-sedar/>

**Minio Storage:**

1. [Minio](https://www.minio.io/)<span dir=""> is a popular open-source object storage server compatible with the </span>[Amazon S3 cloud storage service](https://aws.amazon.com/free/storage/?sc_channel=PS&sc_campaign=acquisition_US&sc_publisher=google&sc_medium=ACQ-P%7CPS-GO%7CBrand%7CDesktop%7CSU%7CStorage%7CS3%7CUS%7CEN%7CText&sc_content=s3_e&sc_detail=amazon%20s3&sc_category=Storage&sc_segment=293617570044&sc_matchtype=e&sc_country=US&s_kwcid=AL!4422!3!293617570044!e!!g!!amazon%20s3&ef_id=EAIaIQobChMI0JiSkYys5gIVRZyzCh3Y8wcFEAAYASAAEgLqg_D_BwE:G:s)<span dir="">.</span> <span dir="">The service stores unstructured data such as photos, videos, log files, backups, and container/VM images, and can even provide a single object storage server that pools multiple drives spread across many servers</span>.
2. For Sedar project we create a bucket called **aaw-sedar.** There are multiple subfolders under this bucket. All the unstructed raw files that are placed by CGI(External client) will be pulled by the sftp-cron job and placed in the aaw-sedar bucket.
3. The incoming unzipped files are saved in **minio/standard-tenant-1/private/sedar/** folder.
4. Once we receive these files we extract these files. The processed files are saved in **minio/standard-tenant-1/shared/aaw-sedar/sedar/** folder.

**Storage statistics of Minio :**
| Time period | Storage space used |
|-------------|--------------------|
| Monthly | 5GB to 10GB |
| Daily | 500MB to 1GB |

The approximate time taken to transfer 5GB of data is 20mins

**Storage on AAW:**

All the processed file are also saved in **data-vol-1** on the **sedar-prod** server which is in **Sedar** name space in advanced analytics workspace.

**data-vol-2 contains:**

* 	sedar_models (All the machine learning models for page detection)
* 	sedar_temp (folder used to unzip tar files)
*         sedar_transfer_files (folder used to store newly processed pdf files)

**data-vol-3** is used for any additional storage activities or debugging purpose.

**Historical Data Storage** :

The data that is used before storing on Minio and Advanced analytics workspace is located in cloud.

The path to access the data is (portal.azure.com > fileshare> sedar-data (<https://sedar.file.core.windows.net/sedar-data>)