# Process Flow

1. Get ~~bag-of-words~~ TF-IDF vectorizer, selected_total_ratio, word_check_income, word_check_balance, word_check_flow flags for all pages in our sample.
2. Use selected_total_ratio score with rolling mean method to identify the best 8 pages in a group that might contain Balance sheet (BS), Income Statement(IS) and Cash Flows Statement(CF).
3. Use the Random Forest Classifier model to determine which page is Balance sheet or Income Statement or Cash Flows Statement in the group of 8 pages.
4. Identify rows and columns of the table in the IS, BS or CF page using SLICE technique
5. Extract financial variables to a digital format.

# [Page Detection](SEDAR/Algorithms/Page-Detection)

### Page range identification

1. ![Assumption](../../uploads/4ee3491a5f0b040739c07b5a2424e062/Assumption.png)

During Exploratory Data Analysis (EDA) phase with a labeled data set, it was observed that all 3 pages to be identified were in close proximity. It was determined that a range of 8 pages would be chosen based on selected_total_ratio before training and predicting CASHFLOW, INCOME STATEMENT and BALANCE SHEET page numbers using a classifier model.

Page range accuracy based on 148 companies - 98.6 %

### ML classification model - Input features

1. ~~Bag-of-words~~ TF-IDF vectorizer
2. selected_total_ratio
3. word_check_income
4. word_check_balance
5. work_check_flow

### ML classification model

Scikit-learn Random Forest Classifier

`RandomForestClassifier(n_estimators = 10, criterion = 'entropy', random_state = 42)`

### ML classification model accuracy

Training accuracy (70:30 split)= 99 %

Validation accuracy 
| Income Statement | Balance Sheet | Cashflow Statement |
|------------------|---------------|--------------------|
| 98.5 % | 98.1 % | 98.9 % |

Subject Matter validation accuracy
| Income Statement | Balance Sheet | Cashflow Statement |
|------------------|---------------|--------------------|
| 99.2 % | 99.2 % | 98.8 % |

# [Page Extraction](SEDAR/Algorithms/Page-Extraction)

### Overview

After the class of the page is determined, the bounding box (x<sub>min</sub>, y<sub>min</sub>, x<sub>max</sub>, y<sub>max</sub>) and the text for each token are extracted. This can be achieved by using [<span dir="">_pdftohtml_</span>](https://www.xpdfreader.com/pdftohtml-man.html) package. Once all the coordinates are identified and extracted, a black and white layout of the page is developed and the surface area for each token is reduced by 15%. This is done to create well-formed divisions between each token. This b/w layout is used to create a _H X W_ binary matrix where _H_ is height of the image and _W_ is width of the image. Upon aggregating on the horizontal and vertical axis, we are presented with a weighted total for each dimension. These totals when plotted, can also be seen as orthographic views (in form of histogram) for each axis of the given table.

Simple smoothing techniques like “moving average” are applied on the weighted totals and all local minima are identified by simple comparison of neighboring values for each axis. We use the [<span dir="">_find_peaks_</span>](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html) function in [_scipy_](https://docs.scipy.org/doc/scipy/getting_started.html)<span dir="">_, _</span>where a peak or local maximum is defined as any sample whose two direct neighbors have a smaller amplitude. On the flipside, a valley or local minima is defined as any sample whose two direct neighbors have a larger amplitude. For flat peaks (more than one sample of equal amplitude wide) the index of the middle sample is returned (rounded down in case the number of samples is even). For noisy signals, the peak/ valley locations can be off, since the noise might change the position of local maxima/ minima. In order to avoid this issue, we have smoothed the signals before searching for valleys.  

Once all minima for each axis are determined, the _x_ and _y_ intercepts are used to create a grid of subsections. This grid is equivalent to the dimension of the extracted table _(R X C)_ where _R_ is the number of _x_ intercepts of each valley on horizontal axis and _C_ is the number of _y_ intercepts of each valley on vertical axis. A custom membership algorithm was created where each token is assigned a position within the _R X C_ grid. This is determined by intersecting each token with each subsection and mapping it based on Intersection over Union score. <span dir=""> </span>

Post processing steps include:<span dir=""> </span>

1. Formatting text<span dir=""> </span>
2. <span dir="">Cleaning numerical values – remove $, include parenthesis  </span>
3. <span dir="">Cast typing numeric values </span>
4. <span dir="">Aligning dates</span>

### <span dir=""> </span>Accuracy
| Income Statement | Balance Sheet | Cashflow Statement |
|------------------|---------------|--------------------|
| 93 % | 94 % | 95.8 % |

