SEDAR data resides in ElasticSearch Database. The indices belonging to SEDAR start with 'sedar_'. Indices description is as follows

## sedar_*
Tables for pipeline data ingestion
##### OLD
- [sedar_file_info ](../OLD/Data-Structures/sedar_file_info )
- [sedar_new_page_detection](../OLD/Data-Structures/sedar_new_page_detection )
- [sedar_new_extracted_variables](../OLD/Data-Structures/sedar_new_extracted_variables )
- [sedar_file_info_text](../OLD/Data-Structures/sedar_file_info_text )
- [sedar_control_information](../OLD/Data-Structures/sedar_control_information )



## sedar_schema_*
- Schema tables which include field and Datatype.
- only used sedar_schema_page_detection for webapp for fetching detected pages.
- Notes: I think this table is not useful because datatype can be handled in pipeline and then python_elasticsearch_dsl returns datatype when querying data

## sedar_lookup_*
- This table stores lookup information because data is collected in binary digits which is converted to text using these lookup tables.


## All Sedar Indices
- [All Sedar Indices](../OLD/Data-Structures/All-Sedar-Indices)


## New Schema (FileInfoExtractor) 
![Untitled_drawing__1_](../../uploads/1b600ee86fda131721407d3b6b64e0f1/Untitled_drawing__1_.jpg)

```python 
class FileInfoExtractor(FileID):
         pdfpath: str  
         xmlpath: str 
         
        FILE_INFO: FileInfo = Field(default_factory=FileInfo())
        def sync():
            self.FILE_INFO.__sync__(self, self.FILE_INFO)
            self.FILE_INFO.sync()
  
        def __init__(self, *args, **kawars):
            super().__init__(args,kawrs)
            self.sync()
          
        def compute_metadata(self) -> None: 
               “””
               Populates 
                     OtherIssuer, or 
                     MutualFundGroup, 
                     MutualFundIssuer, 
                     OtherFile
               “””
               self.sync()
               return None

        def compute_text() -> None: 
              “””
               Populates
                     FileText
              “””
              self.sync()
              Return None

         def compute_page() -> None: 
              “””
               Populates
                    PageDescription
              “””
               self.sync()
               Return None

         def compute_tables() -> None:
                “””
               Populates
                     extracted_tables
              “””
              self.sync()



````