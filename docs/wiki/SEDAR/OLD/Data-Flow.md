## Data Flow through the sedar_information_extraction_pipeline Notebook

#### Settings

In this section, the settings files is loaded, which contains the ElasticSearch username(ES_USR) and password(ES_PSW). The username and password are used to access the ElasticSearch database, using the "ses.Espandas." The variable "running_user" takes in an ElasticSearch username and is used later to change the user accessing the database for logging purposes.

**Table 1: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable            | Datatype/File type |
| -------------------|:-------------:     | 
| running_user entry | string             | 
| settings.yaml      | yaml               |   
| timeout            | int                |    
| max_retries        | int                |    
| retry_on_timeout   | int                |     
| settings["ES_HOST"]| string             |    
| settings["ES_USR"] | string             |    
| settings["ES_PSW"] | string             |    

</td><td>

|Variable            | Datatype/File type |
| -------------------|:-------------:     | 
| running_user       | string             | 
| esp                | object             |
| settings           | object             |
| log                | Dataframe          |


</td></tr> </table> 

  
```python
settings = yaml.safe_load(open(r"settings.yaml"))

esp = ses.Espandas(
    hosts=settings["ES_HOST"],
    http_auth=(settings["ES_USR"], settings["ES_PSW"]),
    timeout=1200,
    max_retries=10,
    retry_on_timeout=True,
)

running_user="nikhil"
```

#### Get list of zipfiles to be processed
In this section, the input is "FILE_INFO" index/table from the ElasticSearch database and the location of the zip files. And the output is a dataframe of the location, name, and date processed of each zip file. The last date a file was processed is used to filter out the zip files that have not been processed.

**Table 2: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable                     | Datatype/File type |
| -------------------         |:-------------:     | 
| FILE_INFO table/index       | Dataframe          | 
| settings["minio_tar_files"] | string             |    

</td><td>

|Variable            | Datatype/File type |
| -------------------|:-------------:     | 
| zip_df             | Dataframe          | 
| log                | Dataframe          |


</td></tr> </table> 

```python
FILE_INFO_df = esdb.get_es_table("FILE_INFO")
current_max_date = FILE_INFO_df.DATE_FOLDER.max() # the last date a zip file was processed

zip_paths = glob2.glob(settings["minio_tar_files"] + "**/*.gz")
zip_df = pd.DataFrame([[z, Path(z).name] for z in zip_paths],columns = ["zip_path","zip_name"])
zip_df["file_date"] =  pd.to_datetime(zip_df.zip_name.str.replace("archive_","").str.replace(".tar.gz","").str.replace("archive-",""))
zip_df = zip_df[zip_df.file_date > current_max_date].reset_index(drop=True)
```

#### Run Meta Data Extraction Pipeline
The objective of this section is to across all the rows in the dataframe "zip_df" from the previous section and updates the "CONTROL_INFORMATION", "COMPANY", "FILE_INFO", "FILE_INFO_TEXT" and "PAGE_DETECTION" tables/indexes on the ElasticSearch database. It takes as input "zip_df" and the "running_user" variable. This is the stage where page detection occurs, which identifies the page of the balance sheet, income statement, segmentation, notes and others. 

**Table 3: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable                             | Datatype/File type |
| -------------------                 |:-------------:     | 
| zip_df                              | Dataframe          | 
| running_user                        | string               |   
| es_update_flag entry                | Boolean            |    
| execute_control_pipeline_flag entry | Boolean            |    

</td><td>

|Variable                   | Datatype/File type     |
| -------------------       |:-------------:         | 
| FILE_INFO index           | Dataframe              | 
| FILE_INFO_TEXT index      | Dataframe              |
| CONTROL_INFORMATION index | Dataframe              |
| PAGE_DETECTION index      | Dataframe              |
| COMPANY index             | Dataframe              |
| log                       | Dataframe              |




</td></tr> </table> 

```python
for idx, val in zip_df.iterrows():
    output = ex.run_meta_data_extraction_pipeline(tar_path = val.zip_path, 
                                                  user=running_user, 
                                                  es_update_flag=True,
                                               execute_control_pipeline_flag=False)
```

#### Execute Control File Pipeline
The objective of this section is to remove confidential files from the tables/indexes that will be used on the dashboard. These confidential files are highlighted in the "CONTROL_INFORMATION" index/table. In the "CONTROL_INFORMATION" index /table, there is a Column called "RECORD_STATUS." If "RECORD_STATUS" is "0 ", it means it has not been deleted. Vice-Versa if it is "1", it means it has been deleted.


 This section takes a Boolean as Input. If True, the function queries the "CONTROL_INFORMATION", "FILE_INFO", "FILE_INFO_TEXT" and "PAGE_DETECTION" tables/indexes with rows whose "RECORD_STATUS" is 0. It updates the "RECORD_STATUS" to 1 and "UPDATED_ON" to time of update and records an Error message if an issue occurs. In addition, the local file location of the document of that row is deleted and document is deleted on the "FILE_INFO", "FILE_INFO_TEXT" and "PAGE_DETECTION" tables/indexes.


**Table 4: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable                             | Datatype/File type |
| -------------------                 |:-------------:     | 
| execute_control_pipeline_flag entry | Boolean            | 

</td><td>

|Variable                          | Datatype/File type |
| -------------------              |:-------------:     | 
| RECORD_STATUS on FILE_INFO index | string             | 
| UPDATED_ON on FILE_INFO index    | string             |
| log                              | Dataframe          |


</td></tr> </table> 

```python
ex.execute_control_pipeline(  execute_control_pipeline_flag=True)
```
#### Execute Control Pipeline New
The objective of this section is to remove confidential files from the tables/indexes that will be used on the dashboard. When we receive a pdf document we initially look at the document type. If the document type is "ctl" then a dataframe is created using the information in the file. The dataframe is created using the construct_ctl_dataframe function.

![image](../../uploads/ea745e5e76ba2551d89dc8abef8c8d4f/image.png)

This function is used in the "run_meta_data_extraction_pipeline" to get the control files and variables in a dataframe. Once the dataframe is processed the output is stored as "Control Information".Once the output is generated then then we will execute the _execute_control_pipeline_ function which takes a boolean value as input. The variable passed in the function is "execute_control_pipeline_flag". This variable is intially set to False. If True then the function queries "CONTROL INFORMATION","FILE_INFO","FILE_INFO_TEXT" and "PAGE DETECTION" tables with RECORD_STATUS set to 0. This function merges each table with the control information on FILE_IDs. The merged dataframes are passed to the perform_control_operation function.

![image](../../uploads/a01ddb375b764b91ea4f1bb9f0a6ad5a/image.png)

This function takes update_ctl values as one of the inputs. The update_ctl flag takes a boolean values. If true then the files are deleted from the "FILE_INFO","FILE_INFO_TEXT" and "PAGE DETECTION" tables and the UPDATED_ON flag has the date and time on which these files are deleted on. Also the RECORD_STATUS is set to 1.
This way we are only having the control_information in the CONTROL_INFO table. This function saves the confidential information in one table and avoids the duplication of the files.

#### Execute Page Extraction Pipeline
This section is divided into two subsection. The first section takes the "page_detection" and "file_info" tables/indexes from the ElasticSearch database and "zip_df" dataframe from the "Get list of zipfiles to be processed" section. The output of this subsection is a dataframe, which a filtered subset of the "page_detection" table/index for zip files to be processed. 

**Table 5: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable              | Datatype/File type |
| -------------------  |:-------------:     | 
| PAGE_DETECTION index | Dataframe          | 
| FILE_INFO index      | Dataframe          |   
| zip_df               | Dataframe          |    
| running_user         | String             |    
| es_update_flag entry | Boolean            |    
| draw_img entry       | Boolean            |   
| include_metadata entry| Boolean            |    
 

</td><td>

|Variable                               | Datatype/File type |
| -------------------                   |:-------------:     | 
| SEDAR VARIABLE EXTRACTION index       | Dataframe          | 
| log                                   | Dataframe          |



</td></tr> </table> 

``` python
pg_detection_df = esdb.get_es_table("page_detection", typecast_data=True)
pg_detection_df = pg_detection_df[pg_detection_df.INCOME_STATEMENT != pg_detection_df.BALANCE_SHEET].reset_index(drop=True)
files_df = pd.concat([esdb.get_es_table("file_info",Q=Q("regexp", 
                                          ZIP_NAME__keyword=zip_name),
                                          fields=[], include_metadata=False) \
        for zip_name in zip_df.zip_name.unique()])
pg_detection_df = pg_detection_df[pg_detection_df.FILE_ID.isin(files_df.FILE_ID)].reset_index(drop=True)

len(pg_detection_df)
``` 
The second subsection takes the dataframe "pg_detection_df" from the first subsection as input. It iteratively extracts the Balance Sheet and Income Statement data from each row in the dataframe. It returns extracted data and updates the "extracted_statments" table/index on the ElasticSearch database, and logs any error that occurred during the extraction process.
```python 

def extract_statements( idx, val):
        
    try:
        balance_sheet_df = pe.get_token_coordinates(val.FILE_ID, val.BALANCE_SHEET+1, \
                             "BALANCE_SHEET",  val.SEDAR_FILE_PATH ,draw_img=False)
        balance_sheet_df[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO',
           'DOCUMENT_DESC', 'FILE_ID', 'ISSUERNO', 'PAGE_COUNT', 'PDF_PATH', 'RECORD_DATE',
           'RECORD_STATUS', 'SEDAR_FILE_PATH',  'TYPE']] =  val[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO',
           'DOCUMENT_DESC', 'FILE_ID', 'ISSUERNO', 'PAGE_COUNT', 'PDF_PATH', 'RECORD_DATE',
           'RECORD_STATUS', 'SEDAR_FILE_PATH',  'TYPE']].values
        balance_sheet_df["PAGE_NO"] = val.BALANCE_SHEET

 
        income_statement_df = pe.get_token_coordinates(val.FILE_ID, val.INCOME_STATEMENT+1, \
                             "INCOME_STATEMENT",  val.SEDAR_FILE_PATH ,draw_img=False)
        income_statement_df[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO',
           'DOCUMENT_DESC', 'FILE_ID', 'ISSUERNO', 'PAGE_COUNT', 'PDF_PATH', 'RECORD_DATE',
           'RECORD_STATUS', 'SEDAR_FILE_PATH',  'TYPE']] =  val[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO',
           'DOCUMENT_DESC', 'FILE_ID', 'ISSUERNO', 'PAGE_COUNT', 'PDF_PATH', 'RECORD_DATE',
           'RECORD_STATUS', 'SEDAR_FILE_PATH',  'TYPE']].values
        income_statement_df["PAGE_NO"] = val.INCOME_STATEMENT
        
        merged_df = pd.concat([balance_sheet_df, income_statement_df]).reset_index(drop=True)
        if len(merged_df) == 0:
            esp.logger("ERROR", "" , "SEDAR VARIABLE EXTRACTION", val.FILE_ID , 1, val.SEDAR_FILE_PATH, user=running_user)
        esdb.load_data_to_es(merged_df , "extracted_variables", mapping = [], es_update_flag = True,\
                                                 user=running_user, zip_name="")
        return "Done"
    except:
        esp.logger("ERROR", "" , "SEDAR VARIABLE EXTRACTION", val.FILE_ID , 1, val.SEDAR_FILE_PATH, user=running_user)
        return pd.DataFrame()

page_detection_output = Parallel(n_jobs=1)(delayed(extract_statements)(idx, val) for idx, val in pg_detection_df.iterrows())

```
#### Filter files to be transferred
The objective of this section is to move files that have been extracted to a temporary folder. It takes as input settings["extraction_documents"] (a list of the SEDAR financial statements used), settings["sedar_document_path"] (the directory for the SEDAR files), settings["sedar_temp_files_to_transfer"] (the temporary directory the files will be moved to), and "FILE_INFO" index/table queried as a dataframe from ElasticSearch. The settings["sedar_temp_files_to_transfer"] directory is cleaned to ensure files from other runs are not added to the new set of files.

**Table 6: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th><th>Output</th></tr>
<tr><td>

|Variable                                  | Datatype/File type |
| -------------------                      |:-------------:     | 
| FILE_INFO table/index                    | int                |    
| settings["extraction_documents"]         | list               |    
| settings["sedar_document_path"]          | list               |    
| settings["sedar_temp_files_to_transfer"] | string             |  
  

</td><td>

|Variable            | Datatype/File type |
| -------------------|:-------------:     | 
| log                | Dataframe          |



</td></tr> </table> 

```python
from tqdm import tqdm
from os import path

FILE_INFO_df = esdb.get_es_table("FILE_INFO")
move_files = FILE_INFO_df[(FILE_INFO_df.DATE_FOLDER > current_max_date) & 
             (FILE_INFO_df.DOCUMENT_DESC.isin(settings["extraction_documents"]))]
file_to_transfer = move_files[move_files.SEDAR_FILE_PATH.apply(lambda x: 
                path.exists(settings["sedar_document_path"] + x))]


fu.clean_temp_dir( temp_dir=settings["sedar_temp_files_to_transfer"])

for idx, val in tqdm(file_to_transfer.iterrows(),  total=file_to_transfer.shape[0]):
    fu.move_file(
        "all",
        settings["sedar_document_path"] + val.SEDAR_FILE_PATH,
        settings["sedar_temp_files_to_transfer"] + "sedar/"+ str(Path(val.SEDAR_FILE_PATH)),
        extraction_documents=["all"],
        sedar_document_path="" ,
    )
```
#### Optional
This is an optionally run section. The purpose of this section is to compare files in the "sedar_document_path" local directory and the minio storage. It takes the settings["minio_output_files"] (this is the minion storage directory), settings["sedar_document_path"] (the directory for the SEDAR files), settings["sedar_temp_files_to_transfer"] (the temporary directory the files will be moved to), and "ext" variable, which is a list that contains the file extension, as input. The files present in the "sedar_document_path" local directory and not in the minio storage will be transferred to the minio storage. 

**Table 7: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th></tr>
<tr><td>

|Variable                                  | Datatype/File type |
| -------------------                      |:-------------:     | 
| ext entry                                | int                |     
| settings["minio_output_files"]           | string             |    
| settings["sedar_document_path"]          | string             |    
| settings["sedar_temp_files_to_transfer"] | string             |    

</td></tr> </table> 

```python
import os
from tqdm import tqdm


def run_fast_scandir(dir, ext):    # dir: str, ext: list
    subfolders, files = [], []

    for f in os.scandir(dir):
        if f.is_dir():
            subfolders.append(f.path)
        if f.is_file():
            if "" not in ext:
                if os.path.splitext(f.name)[1].lower() in ext:
                    files.append(f.path)
            else:
                files.append(f.path)


    for dir in list(subfolders):
        sf, f = run_fast_scandir(dir, ext)
        subfolders.extend(sf)
        files.extend(f)
    return subfolders, files


minio_subfolders, minio_files = run_fast_scandir(settings["minio_output_files"], [".pdf"])
local_subfolders, local_files = run_fast_scandir(settings["sedar_document_path"], [".pdf"])

minio_files_df = pd.DataFrame([(f, "/".join(Path(f).parts[5:])) for f in minio_files], columns = ["path", "relative_path"])
local_files_df = pd.DataFrame([(f, "/".join(Path(f).parts[2:])) for f in local_files], columns = ["path", "relative_path"])
file_to_transfer =  local_files_df[~local_files_df.relative_path.isin(minio_files_df.relative_path)].reset_index(drop=True)

fu.clean_temp_dir( temp_dir=settings["sedar_temp_files_to_transfer"])


for idx, val in tqdm(file_to_transfer.iterrows(),  total=file_to_transfer.shape[0]):
    fu.move_file(
        "all",
        val.path,
        settings["sedar_temp_files_to_transfer"] + str(Path(val.relative_path)),
        extraction_documents=["all"],
        sedar_document_path="" ,
    )
```

#### Transfer newly processed files
In this section, a source directory and target S3-compatible host are taken as input into a Minio command line statement. The command recursively copies files from the source directory to the target.

**Table 8: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th></tr>
<tr><td>

|Variable            | Datatype/File type |
| -------------------|:-------------:     | 
| sedar transferfiles directory | string             | 
| minio shared aaw-sedat directory    | yaml               |   

</td></tr> </table> 

```python
!mc cp --recursive ../data-vol-2/sedar_transfer_files/sedar/ minio-minimal/shared/aaw-sedar/sedar/
```
#### Delete files from the temp folder
In this section, the location of temporary file directories(“sedar_temp_files_to_transfer” and “sedar_temp_directory”) and are used as input for a function to delete all files in them.

**Table 9: The table below highlights the inputs and outputs of this code section of the pipeline.**
<table>
<tr><th>Input </th></tr>
<tr><td>

|Variable                                 | Datatype/File type |
| -------------------                     |:-------------:     | 
| settings["sedar_temp_files_to_transfer"]| string             |    
| settings["sedar_temp_directory"]        | string             |    

</td></tr> </table> 

```python
fu.clean_temp_dir( temp_dir=settings["sedar_temp_files_to_transfer"])
fu.clean_temp_dir( temp_dir=settings["sedar_temp_directory"])
```