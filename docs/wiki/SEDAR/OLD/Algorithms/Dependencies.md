# pdftotext

SLICE uses pdftotext which is part of poppler-utils library. It is a tool that is called through the system command line from within the page_extraction.py script. It's main use is to convert PDF to plain text. 

SLICE uses pdftotext to extract plain text with it's word boundaries in XML format using option _-bbox-layout_. 

The various options available when using pdftotext can be seen here:  <https://manpages.debian.org/stretch/poppler-utils/pdftotext.1.en.html>

### 1.pdftotext API python wrapper 

An API wrapper for pdftotext can be installed using pip:

` pip install pdftotext`

However, this wrapper has limited functionality and only returns plain text in string format. Basic usage instructions for python package pdftotext can be found here: <https://pypi.org/project/pdftotext/>

Following parameters are available with pdftotext.PDF():

<table>
<tr>
<td>pdf_file</td>
<td>

<span dir="">A file opened for reading in binary mode.</span>
</td>
</tr>
<tr>
<td>password</td>
<td>Unlocks the document, if required. Either the owner password or the user password works.</td>
</tr>
<tr>
<td>raw</td>
<td>Default: False. If True, page text is output in the order it appears in the content stream.</td>
</tr>
<tr>
<td>physical</td>
<td>Default: False. If True, page text is output in the order it appears on the page, regardless of columns or other layout features</td>
</tr>
</table>

Code author's note: Usually, the most readable output is achieved by using the default mode, rather than raw or physical.

### 2.pdftotext API R wrapper

Another wrapper for poppler-util's pdftotext is implemented in R. This one implements only 1 function <span dir="">read_bbox_layout_xhtml()</span> which is used by SLICE.

It is available as a github repo under MIT License and can be found here: <https://github.com/balthasars/pdfparser>

This package returns word coordinates in XML tree parsed format.