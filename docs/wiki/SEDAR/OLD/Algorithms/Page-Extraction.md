# Dependencies

 1. pandas
 2. pdftotext
 3. pdftohtml
 4. geopandas
 5. lxml
 6. PIL
 7. shapely
 8. scipy
 9. wand
10. module - pandas_read_xml.py

# Functions

Page extraction pipeline accesses sedar_new_page_detection index to get identified balance sheet, income statement and cashflow statement page number in 0-indexed format.

Values from sedar_new_page_detection index copied to sedar_new_extracted_variables index for each PDF:

'ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 'ISSUERNO', 'PAGE_COUNT', 'RECORD_DATE', 'RECORD_STATUS', 'TYPE'

```plaintext
import utilities.page_extraction as pe

def extract_statements(idx, val):

    if val.BALANCE_SHEET > 0:
       
        balance_sheet_df = pe.extract_table(val.FILE_ID, int(val.BALANCE_SHEET)+1, "BALANCE_SHEET", settings["document_path"]+val.SEDAR_FILE_PATH, draw_img=False)
        balance_sheet_df[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 'ISSUERNO', 'PAGE_COUNT',
        'RECORD_DATE', 'RECORD_STATUS', 'TYPE']] = val[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 
                                                        'ISSUERNO', 'PAGE_COUNT', 'RECORD_DATE', 'RECORD_STATUS', 'TYPE']].values
        balance_sheet_df["PAGE_NO"] = int(val.BALANCE_SHEET)
    else:
        balance_sheet_df = pd.DataFrame()

    if val.INCOME_STATEMENT > 0:
        income_statement_df = pe.extract_table(val.FILE_ID, int(val.INCOME_STATEMENT)+1, "INCOME_STATEMENT", settings["document_path"]+val.SEDAR_FILE_PATH, draw_img=False)
        income_statement_df[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 'ISSUERNO', 'PAGE_COUNT',
        'RECORD_DATE', 'RECORD_STATUS', 'TYPE']] = val[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH',
                                                        'ISSUERNO', 'PAGE_COUNT', 'RECORD_DATE', 'RECORD_STATUS', 'TYPE']].values
        income_statement_df["PAGE_NO"] = int(val.INCOME_STATEMENT)
    else:
        income_statement_df = pd.DataFrame()

    if val.CASHFLOW_STATEMENT > 0:
        cashflow_df = pe.extract_table(val.FILE_ID, int(val.CASHFLOW_STATEMENT)+1, "CASHFLOW_STATEMENT", settings["document_path"]+val.SEDAR_FILE_PATH, draw_img=False)
        cashflow_df[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 'ISSUERNO', 'PAGE_COUNT',
        'RECORD_DATE', 'RECORD_STATUS', 'TYPE']] = val[['ACCESSION_NO', 'CREATED_ON', 'CTL_INFO', 'DOCUMENT_DESC', 'FILE_ID', 'SEDAR_FILE_PATH', 
                                                        'ISSUERNO', 'PAGE_COUNT', 'RECORD_DATE', 'RECORD_STATUS', 'TYPE']].values
        cashflow_df["PAGE_NO"] = int(val.CASHFLOW_STATEMENT)
    else:
        cashflow_df = pd.DataFrame()

    merged_df = pd.concat([balance_sheet_df, income_statement_df, cashflow_df]).reset_index(drop=True)
    if len(merged_df) == 0:
        print(f"Empty merged_df for row {idx}")
    else:
        merged_df['DATE_ADDED'] = pd.to_datetime("today")
        merged_df['IS_ORIGINAL'] = True
        esdb.load_data_to_es(merged_df, table_name="new_extracted_variables", mapping={}, es_update_flag = True, user=running_user, zip_name=val.FILE_ID)
    

    return merged_df

pg_detection_df = esdb.get_es_table("new_page_detection", typecast_data=True)
final_df = pd.DataFrame()
final_df = Parallel(n_jobs=1)(delayed(extract_statements)(idx, val) for idx, val in pg_detection_df.iterrows())
```

New values added to sedar_new_extracted_variables apart from extract_table() output:-

1. DATE_ADDED: To retain information about when the extarcted table was added to ES database
2. IS_ORIGINAL: For future functionality of editable tables. True for adding tables from pipeline run, False for manually edited tables stored in database.

Page Extraction contains three major functions that are described below **after** the main function that calls all three and outputs table in desired format:

## 1. Extract table

Main function called in pipeline to extract tables from identified income statement, balance sheet and cashflow statement pages.

### Input
| Variable | Type | Description |
|----------|------|-------------|
| file_id | string | SEDAR file ID, accessed from ES database (FILE_ID) |
| page | int | page number (1-indexed), accessed from ES database as INCOME_STATEMENT, BALANCE_SHEET or CASHFLOW_STATEMENT |
| page_type | string | can be either INCOME_STATEMENT, BALANCE_SHEET or CASHFLOW_STATEMENT |
| sedar_file_path | string | partial path to PDF location, accessed from ES database (SEDAR_FILE_PATH) |
| draw_img | bool | Always False in pipeline run to cut down runtime |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| output | Pandas Dataframe | Contains post-processed and cleaned extracted table in key 'EXTRACTED_VARIABLES' in records-style dictionary to match json format, units in 'units', currency in 'currency', one of 3 statement types in 'PAGE_TYPE' |

### Function call - default values

`extract_table(file_id, page, page_type, sedar_file_path, draw_img=False)`

## 2. Get token coordinates

First function to run in extraction process

### Utility

1. Finds text bounding boxes and coordinates using pdftotext and pdftohtml
2. Finds horizontal and vertical page divisions
3. Finds position of year in text using regular expressions

### Input
| Variable | Type | Description |
|----------|------|-------------|
| file_id | string | SEDAR file ID |
| page | int | page number (1-indexed) |
| page_type | string | can be either income statement, balance sheet or cashflow statement |
| sedar_file_path | string | partial path to PDF location, stored in ES database |
| draw_img | bool | True if debugging, output will contain images of page with page sections and histogram. Always False in pipeline run to cut down runtime. |
| entry | Pandas dataframe | Dataframe to assign to coordinate_table if function fails |
| sedar_images_path | string | path to data folder in notebook server |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| Coordinate_table | Pandas dataframe | columns with token index, full text, coordinates (x y min and max), page width, page height, date, units and currency on a page |
| horz | list | For debugging - Shapely.Geometry.LineString point coordinates for horizontal peaks found |
| verz | list | For debugging - Shapely.Geometry.LineString point coordinates for vertical peaks found |
| Vertical_distance_list | list | position of vertical page sections along horizontal axis |
| Horizontal_distance_list | list | position of horizontal page sections along vertical axis |
| img | wand.image | image of the sectioned page with histogram plot |
| original | wand.image | image of the sectioned page without histogram plot |

### Function call - default values

`get_token_coordinates(file_id, page, page_type, sedar_file_path, draw_img=True, entry=pd.DataFrame(), sedar_images_path=settings["images_path"])`

## 3. Convert page to table

Second function to run in extraction process, takes output variables of get_token_coordinates() as input.

### Utility

1. Finds areas of text bounding boxes overlapping with page sections
2. Assigns text to page sections, creates preliminary table without post-processing (contains all text on the page)
3. Create date table

### Input
| Variable | Type | Description |
|----------|------|-------------|
| coordinate_table | Pandas dataframe | columns with token index, full text, coordinates (x y min and max), page width, page height, date, units and currency on a page |
| vertical_distance_list | list | position of vertical page sections along horizontal axis |
| horizontal_distance_list | list | position of horizontal page sections along vertical axis |

### Output

Variable
| Variable | Type | Description |
|----------|------|-------------|
| coordinate_intersection | Geopandas dataframe | 4 vertices coordinates of text and area |
| extracted_table | Pandas dataframe | Dataframe with text in respective rows and columns, includes everthing on a page |
| date_table | Pandas dataframe | Sparse dataframe with -1 for (row, column) containing year |

### Function call

`convert_page_to_table(coordinate_table, vertical_distance_list, horizontal_distance_list)`

## 4. Post-process extracted table

Third function to run in extraction process. Takes output of convert_page_to_table() as input.

### Utility

1. Find start of table using date_table
2. Clean all numerical columns to have only float values
3. Combine rows containing continued sentence (check if line starts with lowercase alphabet)

### Input
| Variable | Type | Description |
|----------|------|-------------|
| extracted_table | Pandas dataframe | Dataframe with text in respective rows and columns, includes everthing on a page |
| date_table | Pandas dataframe | Sparse dataframe with -1 for (row, column) containing year |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| final_table | Pandas dataframe | Post-processed and cleaned extracted_table |

### Function call

`post_process_extracted_table(extracted_table, date_table)`