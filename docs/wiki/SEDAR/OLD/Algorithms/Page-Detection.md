# Dependencies

1. pandas
2. nltk
3. scipy

# Functions

Code snippet for page detection pipeline run

```
import utilities.page_detection as spd

file_info_text_df['page_no'] = file_info_text_df['PAGE_NO']
file_info_text_df["DOCUMENT_INFO"] = file_info_text_df["DOCUMENT_INFO"].apply(lambda lines: spd.parse_text(lines))
file_info_text_df[["text", "selected_total_ratio"]] = pd.DataFrame( file_info_text_df.DOCUMENT_INFO.to_list(), index=file_info_text_df.index )
file_info_text_df["PDF_PATH"] = ""
page_detection_df = file_info_text_df.groupby(['FILE_ID']).apply(lambda x: spd.detect_page(x))
```

## 1. Parse text

### Utility

Function to clean text layout and calculate ratio of number of lines ending with digits and total number of lines (selected_total_ratio). Returns cleaned text and selected_total_ratio of page as list.

### Input
| Variable | Type | Description |
|----------|------|-------------|
| lines | string | text on page; 'DOCUMENT_INFO" from sedar_file_info_text index in ES database  |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| \[lines, selected_total_ratio\] | list | cleaned text and selectec_total_ratio (ratio of number of lines ending with digits and total number of lines) of page |

### Function call

`parse_text(lines)`

## 2. Detect page

### Utility

Takes output from parse_text(0 and groups all pages belonging to the same PDF and run detect_df() on it. Converts output from detect_df() to desired format.

Detect_df creates feature vector from text and detects page using trained classifier model.

### Input
| Variable | Type | Description |
|----------|------|-------------|
| page_detection_df | Pandas Dataframe | sedar_file_info_text variables with new columns 'text' and 'selected_total_ratio' from parse_text() |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| page_detection_df | Pandas Dataframe | sedar_file_info_text variables with columns 'CTL_INFO', 'ACCESSION_NO', "FILE_ID", "ISSUERNO", "PAGE_COUNT", "PDF_PATH", "SEDAR_FILE_PATH", "RECORD_DATE", "TYPE", "DOCUMENT_DESC" and **new columns** "RECORD_STATUS", "CREATED_ON", "BALANCE_SHEET", "INCOME_STATEMENT", "CASHFLOW_STATEMENT", "SEGMENTATION_PAGE", "NOTES_PAGE", "OTHER"  |

### Function call

`detect_page(page_detection_df)`

## 3. Detect df

### Utility

1. Find page range using selected_total_ratio and word_check_flags


2. Creates feature vector from text using trained TF-IDF vectorizer
3. Detects page using trained Random Forest classifier model

### Input
| Variable | Type | Description |
|----------|------|-------------|
| table | Pandas Dataframe | sedar_file_info_text variables with columns 'CTL_INFO', 'ACCESSION_NO', "FILE_ID", "ISSUERNO", "PAGE_COUNT", "PDF_PATH", "SEDAR_FILE_PATH", "RECORD_DATE", "TYPE", "DOCUMENT_DESC" and new columns "text", "selected_total_ratio" |

### Output
| Variable | Type | Description |
|----------|------|-------------|
| detection_op | Dictionary | Model output in dictionary format with keys - "BALANCE_SHEET", "INCOME_STATEMENT", "CASHFLOW_STATEMENT", "SEGMENTATION_PAGE", "NOTES_PAGE", "OTHER" |

### Function call

detect_df(table)