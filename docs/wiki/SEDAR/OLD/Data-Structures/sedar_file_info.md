Stores all PDF files and related info

| Field | DType| Description| Notes |
| ------ | ------ | ------ |------ |
| ACCESSION_NO| str |   |   |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| CTL_INFO| str | Refer to the Control Block section above for content layout. | from sedar_data_dictionary.xlsx  |
| DATE_FOLDER | DateTime | | |
| DOCUMENT_DESC | str | description of pdf |   |
| DOCUMENT_SUBTYPE| str | A numeric field that further defines the type of document | |
| DOCUMENT_TYPE | str | A numeric field relating a SEDAR system index with that of a document type | |  
| FILER_DESC | str| The description of the type of filer|  |
| FILER_TYPE | cell | cell |   |
| FILE_ID | str | Unique file ID for pdf |   |
| FILING_DATE | str | Date that the filing was originally made (yyyymmdd) (same as the Filing Date in the Control Block) |   |
| FILING_DESC | str | cell |   |
| FILING_TYPE | str | The description of the filing type |   |
| ISSUERNO | str | Unique ID for company |   |
| PAGE_COUNT | int | total pages in pdf |   |
| PDF_PATH | str | path of pdf on data volume of notebook |   |
| PROJECT_NO | str | A number identifying the project that the document is filed under (same as the number in the Control Block) |   |
| PUBLIC_DATE | DateTime | The date the document was processed by DDS (yyyymmdd) (same as the Acceptance Date in the Control Block) |   |
| RECORD_DATE| DateTime | date for document submission|   |
| RECORD_STATUS | str | cell |   |
| SEDAR_FILE_PATH| str | relative pdf file path for minio and data volumes |   |
| SUBMISSION_NO | str | Each time a document is submitted to the SEDAR system, a submission is created. A submission is numbered for each project (same as the number in the Control Block) |   |
| THIRD_PARTY_FILER_INFO_NAME_E | str | Full name of 3rd Party Filer in English. The tag may be repeated within the <3rd PARTY FILER INFO> tag. |   |
| TYPE | str | company type |   |
| XML_PATH | str | path of xml file on data volume of notebook |   |
| ZIP_NAME | str | name of zip file |   |
| ZIP_PATH | str | zip path from minio |   |