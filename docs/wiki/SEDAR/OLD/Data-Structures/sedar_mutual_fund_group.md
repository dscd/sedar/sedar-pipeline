| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| APPSEXEMPTION | unknown | Related application to the commission’s flag. Indicates provinces in which the exemption application has been made. |  |
| APPSREGISTRATION | unknown | Related application to the commission’s flag. Indicates the province in which the registration application has been made. |  |
| CONTACT | str | contact person name |  |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| EXT | str | Phone Extension Number |  |
| FAXAREA | str | Fax Area Code |  |
| FAXNO | str | Fax Number |  |
| FPEDATE | DateTime | The mandatory financial Period Ended date (yyyymmdd). Not applicable for certain types of filings. |  |
| FPERELATES | str | The mandatory Financial Period Dates Relates to, paired with <FPEDATE>. Not applicable for certain types of filings. |  |
| GROUPNO | cell | Mutual Fund Group Number |  |
| LASTUPDATE | DateTime |  |  |
| MANAGER_E | str | Name of Mutual Fund Group Manager English |  |
| MANAGER_F | str | Name of Mutual Fund Group Manager French  |  |
| NAME_E | str | Mutual Fund Group Name English |  |
| NAME_F | str | Mutual Fund Group Name French |  |
| NPELECTIONS | str | National Policy Statement elections. |  |
| PHONEAREA | str | Phone Area Code |  |
| PHONENO | str | Phone Number |  |
| PREFILING | Unknown | An indicator that defines if a prefiling had been by Region |  |
| ~RECORD_DATE~| ~DateTime~ | ~date for document submission~|   |
| RECORD_STATUS | int |  |  |
| REGION | str | province of operation |  |
| ~TYPE~ | ~str~ | ~'MUTUAL_FUND_GROUP'~ |  |
