| Field | DType | Description | Notes |
| ------ | ------ | ------ | ------ |
| ACCESSION_NO| str |  A unique document index number assigned by the DDS system (same as the number in the Control Block) |   |
| CLIENT_FN| str |   |   |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| CTL_INFO| str | Refer to the Control Block section above for content layout. | from 
| CTL_PATH| str | path of ctl file. Apparently this file contains CLT info which is used somewhere |   |
| DATE_FOLDER| str |   |   |
| DOCUMENT_TYPE| str | A numeric field relating a SEDAR system index with that of a document type  |  This field is replaced with text from reference document [sedar_data_dictionary.xls/Submission Type] |
| FILER_TYPE| str |  code inside lookup table which is converted to text | [sedar_data_dictionary.xls/Sheet1] |
| FILE_ID | str | Unique file ID for pdf |   |
| FILING_DATE| DateTime | date of filing  |  |
| FILING_TYPE| str | code inside lookup table which is converted to text  | [sedar_data_dictionary.xls/Sheet1] |
| ISSUERNO | str | Unique ID |   |
| NAME_E| str |  Eng Name | |
| PROJECT_NO| str | A number identifying the project that the document is filed under (same as the number in the Control Block)  | |
| PUBLIC_DATE| str | The date the document was processed by DDS (yyyymmdd) (same as the Acceptance Date in the Control Block)  | |
| PUBLIC_FLAG| str | Private/Public  |   |
| RECORD_STATUS| int |  |  0/1 field |
| SUBMISSION_NO | str |   |   |
| UPDATED_ON| DateTime| dateof update when this document was changed  |   
| ZIP_NAME | str | name of zip file |   |
| ZIP_PATH | str | zip path from minio |   |

