List of all sedar_* indices in the database. 

Note: use `:heavy_check_mark:` to get the check mark inside cell.

| Indice Name | Used (yes/no) | code repo where used (pipeline/webapp/Other please mention) |Notes |
| ------ | ------ | ------ |------ |
| sedar_companies_nd_dec29_1 |  |  |  |
| sedar_company_elatic|  |  |  |
| sedar_control_information | :heavy_check_mark: | pipeline |  |
| sedar_experimental_exchanges_view |  |  |  |
| sedar_experimental_map_view |  |  |  |
| sedar_extracted_variable_information |  |  |  |
| sedar_extracted_variables| :heavy_check_mark: |  | Old table no longer used. replaced by sedar_new_Extracted_Variables |
| sedar_extracted_variables_partition |  |  |  |
| sedar_file_info | :heavy_check_mark: | pipeline, webapp |  |
| sedar_file_info_text | :heavy_check_mark: | pipeline |  |
| sedar_logger | :heavy_check_mark: |  pipeline, webapp |  |
| sedar_logger_updated |  |  |  |
| sedar_lookup_address |  |  |  |
| sedar_lookup_document_class_lookup |  |  |  |
| sedar_lookup_document_class_mapping |  |  |  |
| sedar_lookup_document_subtype |  |  |  |
| sedar_lookup_document_type |  |  |  |
| sedar_lookup_filer_type |  |  |  |
| sedar_lookup_filing_type |  |  |  |
| sedar_lookup_formal_correspondence |  |  |  |
| sedar_lookup_groupfund_basis |  |  |  |
| sedar_lookup_groupfund_regulator |  |  |  |
| sedar_lookup_issuer_type |  |  |  |
| sedar_lookup_issuerfund_formtype |  |  |  |
| sedar_lookup_issuerfund_fundtype|  |  |  |
| sedar_lookup_issuerfund_report |  |  |  |
| sedar_lookup_issuerfund_salescomp |  |  |  |
| sedar_lookup_otherissuer_basis |  |  |  |
| sedar_lookup_otherissuer_exchanges |  |  |  |
| sedar_lookup_otherissuer_formtype |  |  |  |
| sedar_lookup_otherissuer_industry |  |  |  |
| sedar_lookup_otherissuer_regulator |  |  |  |
| sedar_lookup_otherissuer_report |  |  |  |
| sedar_lookup_otherissuer_size |  |  |  |
| sedar_lookup_submission_type |  |  |  |
| sedar_lookup_table |  |  |  |
| sedar_mutual_fund_group | :heavy_check_mark: | pipeline |  |
| sedar_mutual_fund_issuer | :heavy_check_mark: | webapp |  |
| sedar_new_extracted_variables | :heavy_check_mark: | webapp,pipeline |  |
| sedar_new_fix_extracted |  |  |  |
| sedar_new_page_detection | :heavy_check_mark: | webapp, pipeline |  |
| sedar_other_filer | :heavy_check_mark: | pipeline |  |
| sedar_other_issuer | :heavy_check_mark: | webapp, pipeline |  |
| sedar_page_detection |  |  | Old table no longer used. replaced by sedar_new_page_detection |
| sedar_schema_file_info |  |  |  |
| sedar_schema_mutual_fund_group |  |  |  |
| sedar_schema_mutual_fund_issuer |  |  |  |
| sedar_schema_new_page_detection | :heavy_check_mark: | webapp, pipeline |  |
| sedar_schema_other_filer | :heavy_check_mark: | pipeline |  |
| sedar_schema_other_issuer | :heavy_check_mark: |  webapp, pipeline |  |
| sedar_schema_page_detection | :heavy_check_mark: | webapp | useless and can be removed with few modifications in webapp logic |
| sedar_user_logger | :heavy_check_mark: |  webapp |  |
| sedar_web_data_nd_dec23 |  |  |  |


