This table stores PDF text inside a page of PDF. Every row is a page inside PDF file

| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| ACCESSION_NO| str |   |   |
| CTL_INFO| str | Refer to the Control Block section above for content layout. | from sedar_data_dictionary.xlsx  |
| DOCUMENT_DESC | str | description of pdf |   |
| DOCUMENT_INFO | str | text for page number |  |
| FILE_ID | str | Unique file ID for pdf |   |
| ISSUERNO | str | Unique ID for company |   |
| PAGE_COUNT | int | total pages in pdf |   |
| PAGE_NO| int | page number of the document_info data |  |
| RECORD_DATE| DateTime | date for document submission|   |
| SEDAR_FILE_PATH| str | relative pdf file path for minio and data volumes |   |
| TYPE | str | company type |   |
