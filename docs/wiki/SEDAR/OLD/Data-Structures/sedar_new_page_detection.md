Stores page detection outputs related to detected page number 

| Field | DType | Description | Notes |
| ------ | ------ | ------ | ------ |
| ISSUERNO | str | Unique ID |   |
| FILE_ID | str | Unique file ID for pdf |   |
| DOCUMENT_DESC | str |Quarterly report or Annual|   |
| SEDAR_FILE_PATH| str | relative pdf file path for minio and data volumes |   |
| ACCESSION_NO| str |   |   |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| RECORD_DATE| DateTime | date for document submission|   |
| RECORD_STATUS|  |  |   |
| BALANCE_SHEET| int | page number inside PDF file(starts with 0 add 1 for actual page)|   |
| CASHFLOW_STATEMENT| int | page number inside PDF file(starts with 0 add 1 for actual page)|   |
| INCOME_STATEMENT| int | page number inside PDF file(starts with 0 add 1 for actual page)|   |
| SEGMENTATION_PAGE| int | page number inside PDF file(starts with 0 add 1 for actual page)|   |
| NOTES_PAGE| int | page number inside PDF file(starts with 0 add 1 for actual page)|   |
| OTHER| int | the page from 8 page range with the 6th highest numerical data page ratio  |   |
| PAGE_COUNT| int | Total pages in PDF file |   |
| PDF_PATH|  |  |   |
| CTL_INFO| str | Refer to the Control Block section above for content layout. | from sedar_data_dictionary.xlsx  |