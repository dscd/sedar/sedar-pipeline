| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| AUDITOR_E | str | Name of Mutual Fund Group Auditor English |  |
| AUDITOR_F | str|  |  |
| BUSINESS_ADDRESS_CITY | str|  |  |
| BUSINESS_ADDRESS_CODE | str|  |  |
| BUSINESS_ADDRESS_COUNTRY |str|  |  |
| BUSINESS_ADDRESS_EXT | str|  |  |
| BUSINESS_ADDRESS_FAXAREA | str|  |  |
| BUSINESS_ADDRESS_FAXNO | str|  |  |
| BUSINESS_ADDRESS_PHONEAREA | str|  |  |
| BUSINESS_ADDRESS_PHONENO | str|  |  |
| BUSINESS_ADDRESS_PROV | str|  |  |
| BUSINESS_ADDRESS_STREET1 | str|  |  |
| BUSINESS_ADDRESS_STREET2 | str|  |  |
| CREATED_ON | Date |  |  |
| CUSTODIAN_E | str| Name of Mutual Fund Group Custodian English |  |
| CUSTODIAN_F | str |  |  |
| DISTRIBUTOR_E | cestrll | Name of Mutual Fund Group Distributor English |  |
| DISTRIBUTOR_F | cestrll |  |  |
| FORMDATE | Date | Date of Formation |  |
| FORMTYPE | str| Manner of Formation |  |
| FUNDTYPE | str| Type of Mutual Fund |  |
| GROUPNO | str | SEDAR system Group Number |  |
| ISSUERNO | str| SEDAR System mutual fund issuer number |  |
| LASTUPDATE | Date | Date of Last Update to Mutual Fund Group Profile |  |
| LAW | str| Jurisdiction where formed |  |
| MAILING_ADDRESS_CITY | str|  |  |
| MAILING_ADDRESS_CODE | str|  |  |
| MAILING_ADDRESS_COUNTRY | str|  |  |
| MAILING_ADDRESS_EXT | str|  |  |
| MAILING_ADDRESS_FAXAREA | str|  |  |
| MAILING_ADDRESS_FAXNO | str|  |  |
| MAILING_ADDRESS_PHONEAREA | str|  |  |
| MAILING_ADDRESS_PHONENO | str|  |  |
| MAILING_ADDRESS_PROV | str|  |  |
| MAILING_ADDRESS_STREET1 | str|  |  |
| MAILING_ADDRESS_STREET2 | str|  |  |
| MANAGER_E | str | Name of Mutual Fund Group Manager English |  |
| MANAGER_F| str|  |  |
| NAME_E | str| Full name of previous profile in English |  |
| NAME_F | str|  |  |
| PREVIOUS_PROFILE_NAME_E | Union[str|List[str]] | Tag identifying the beginning and end of previous profile information |  |
| ~RECORD_DATE~ | ~Date~ |  |  |
| RECORD_STATUS | str|  |  |
| REPORT | str| Jurisdiction where reporting issuer |  |
| RRSP | str| RRSP Eligibility (Yes or No) |  |
| SALESCOMP | str| Sales Compensation Type |  |
| TRUSTEE_E | str| Name of Mutual Fund Group Trustee English |  |
| TRUSTEE_F | str| Name of Mutual Fund Group Trustee French |  |
| ~TYPE~ | ~str~ |  |  |
| YEDATE | str | Financial Year End (mmdd) |  |
