| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| ISSUERNO | str | Unique ID for company |   |
| NAME_E | str | Full name of Filer in English |  |
| NAME_F | str | Full name of Filer in French |  |
| CONTACT | str | Full name of Contact for Manager |  |
| PHONEAREA | str | Full name of Contact for Manager |  |
| PHONENO | str | Phone number for Contact |  |
| EXT | str | Extension for Contact |  |
| FAXAREA | str | Extension for Contact |  |
| FAXNO | str | Fax Number for Contact |  |
| LAW | str | Jurisdiction where formed |  |
| CUSIPCDSNO | str | Latest CUSIP number |  |
| EXCHANGES | str | Stock exchanges where securities are listed  |  |
| FORMTYPE | str | Manner of Formation |  |
| FORMDATE | DateTime | Date of Formation |   |
| YEDATE | DateTime | Financial Year End Date (mmdd) |  |
| INDUSTRY | str | The Industry Classification of the Issuer |  |
| REPORT | str | Jurisdictions where the issuer is a reporting issuer |  |
| OTCISSUERDETAIL | str | Jurisdictions where the issuer is an OTC issuer |  |
| PARTNER_E | str | Full name of general partner in English |  |
| PARTNER_F | str |  |  |
| AUDITOR_E | str | Full name of issuer's auditor in English |  |
| AUDITOR_F | str  |  |  |
| TRANAGENT_E | str | Full name of Transfer Agent and Registrar in English |  |
| TRANAGENT_F | str  |  |  |
| SIZE | str | The size of the issuer as measured by Total Assets |  |
| LASTUPDATE | str  | Last date this profile was updated before being included on the cover page of this filing (yyyymmdd) |  |
| TIMEUPDATE | str | Last time this profile was updated before being included in this filing (hhmmss) |  |
| SHORT_FORM_PROSPECTUS_ISSUER | str | Prompt Offering Prospectus issuer (Yes or No) |  |
| STOCKSYMBOL | str | Ticker Symbol |  |
| APPSREGISTRATION | str  | Related application to the commission’s flag. Indicates the province in which the registration application has been made. |  |
| APPSEXEMPTION | str | Related application to the commission’s flag. Indicates provinces in which the exemption application has been made. |  |
| NPELECTIONS | str | National Policy Statement elections. |  |
| OFFERINGS | str | Y/N field indicating if a National Policy Receipt is required |  |
| SECTYPE | str | Security Type being offered (e.g. Common, Subordinate) |  |
| PROCEEDS | str | Aggregate gross proceeds from offerings in Millions |  |
| UNDERWRITER_E | str | Full name of lead underwriter in English |  |
| UNDERWRITER_F | str |  |  |
| UWCOUNSEL | str | Full name of lead underwriter’s counsel |  |
| UWCONTACT | str | Full name of contact person for Underwriter’s Counsel |  |
| PROMOTER | str | Full name of lead promoter |  |
| PREFILING | str | An indicator that defines if a prefiling had been by Region. |  |
| LPSELECTIONS | str | Local policy statement elections (Yes or No) |  |
| FPEDATE | str | The mandatory financial Period Ended date (yyyymmdd). Not applicable for certain types of filings. |  |
| FPERELATES | str | The mandatory Financial Period Date Relates to, paired with <FPEDATE>. Not applicable for certain types of filings. |  |
| BUSINESS_ADDRESS_CITY | str |  |  |
| BUSINESS_ADDRESS_CODE | str |  |  |
| BUSINESS_ADDRESS_COUNTRY |str |  |  |
| BUSINESS_ADDRESS_EXT | str |  |  |
| BUSINESS_ADDRESS_FAXAREA | str |  |  |
| BUSINESS_ADDRESS_FAXNO | str |  |  |
| BUSINESS_ADDRESS_PHONEAREA | str |  |  |
| BUSINESS_ADDRESS_PHONENO | str |  |  |
| BUSINESS_ADDRESS_PROV | str |  |  |
| BUSINESS_ADDRESS_STREET1 | str |  |  |
| BUSINESS_ADDRESS_STREET2 | str |  |  |
| MAILING_ADDRESS_CITY | str |  |  |
| MAILING_ADDRESS_CODE | str |  |  |
| MAILING_ADDRESS_COUNTRY | str |  |  |
| MAILING_ADDRESS_EXT | str |  |  |
| MAILING_ADDRESS_FAXAREA | str |  |  |
| MAILING_ADDRESS_FAXNO | str |  |  |
| MAILING_ADDRESS_PHONEAREA | str |  |  |
| MAILING_ADDRESS_PHONENO | str |  |  |
| MAILING_ADDRESS_PROV | str |  |  |
| MAILING_ADDRESS_STREET1 | str |  |  |
| MAILING_ADDRESS_STREET2 | str |  |  |
| RECORD_STATUS | int |  |  |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| ~JURISDICTION~ | ~str~ |  |  |
| ~RECORD_DATE~ | ~DateTime~ | ~date for document submission~ |   |
| REGION | str |  |  |