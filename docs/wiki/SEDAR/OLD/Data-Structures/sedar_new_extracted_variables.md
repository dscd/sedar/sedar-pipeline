Stores extracted tables from SLICE

| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| ACCESSION_NO |  |  |  |
| CREATED_ON | DateTime | copied from page_detection |  |
| CTL_INFO |  |  |  |
| DATE_ADDED | DateTime | date when document added to ElasticSearch |  |
| DOCUMENT_DESC | str | Quarterly report or Annual |  |
| EXTRACTED_VARIABLES | JSON (list of Dictionaries) | extracted table by SLICE |  |
| FILE_ID | str | Unique file ID for pdf |  |
| ISSUERNO | str | Unique ID |  |
| IS_ORIGINAL | boolean | True if document added from pipeline else False | used for edit feature |
| PAGE_COUNT | int | Total pages in PDF file |  |
| PAGE_NO | int | Page no of respective page type |  |
| PAGE_TYPE | str | BS/IS/CS/NOTES/SP |  |
| RECORD_DATE | DateTime | date for document submission |  |
| RECORD_STATUS |  |  |  |
| SEDAR_FILE_PATH | str | relative pdf file path for minio and data volumes |  |
| TYPE | str | issuer type |
