Identify the beginning and the end of the Other Filer information within the Company Information tag

| Field | DType | Description | Notes |
|-------|-------|-------------|-------|
| ISSUERNO | str | Unique ID for company |   |
| NAME_E | str | Full name of Filer in English |  |
| NAME_F | str | Full name of Filer in French |  |
| CONTACT | str | Full name of Contact for Manager |  |
| PHONEAREA | str | Full name of Contact for Manager |  |
| PHONENO | str | Phone number for Contact |  |
| EXT | str | Extension for Contact |  |
| FAXAREA | str | Extension for Contact |  |
| FAXNO | str | Fax Number for Contact |  |
| LASTUPDATE | DateTime | Date and Time of Last Update to the Other Filer Profile Information |  |
| PREVIOUS_PROFILE_NAME_E | Union[str|List[str]]| Tag identifying the beginning and end of previous profile information |  |
| RECORD_STATUS | int |  |  |
| CREATED_ON| DateTime | date when document added to ElasticSearch |   |
| ~JURISDICTION~ | ~str~ |  |  |
| ~RECORD_DATE~ | ~DateTime~ | ~date for document submission~ |   |
| REGION | str |  |  |