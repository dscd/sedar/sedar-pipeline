## Gitlab repository:

<https://gitlab.k8s.cloud.statcan.ca/data-science-division/aeas-aera/sedar/sftp-cronjob>

## Basic Kubernetes CronJob guide:

<https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/>

## Slack notifications channel:
**How to Create the Slack App?**
* Follow this tutorial to create the [Slack App]( https://docs.celigo.com/hc/en-us/articles/7140655476507-How-to-create-an-app-and-retrieve-OAuth-token-in-Slack#A) and set the Bot user token.

**How to Retrieve Slack OAuth token?**
* Sign in to your [Slack ](https://slack.com/intl/en-in/)account.
* Navigate to [Slack API](https://api.slack.com/apps).
* Click Your apps in the top right corner and select the application.
* Navigate to **Features > OAuth & permissions > OAuth** Tokens for Your Workspace.
* Copy the required token (Bot user OAuth token).
![Slack_Image](uploads/6a74c70626c9e630cfeb3ac85cbfeaf4/Slack_Image.png)

**What are the Sedar Development Channels?**
- Development: https://app.slack.com/client/T0448FGTE8N/C0454RK8FG8 (Private)
- Production: https://app.slack.com/client/T0448FGTE8N/C044103PBKR (Private)
- Workspace: sedargroup.slack.com
- Email: Your @statcan.gc.ca account

## sftp-cron folder structure:

* chart
  * <span dir="">templates</span>
    * <span dir="">CronJob.yaml</span>
    * <span dir="">Script.yaml</span>
    * <span dir="">Secret.yaml</span>
    * <span dir="">SlackScript.yaml</span>
  * Chart.yaml
  * copy-script.sh
  * slack-notification.py
  * values.yaml
* Dockerfile
* Makefile
* create-secret.sh

## Dependencies:

1. Helm (to run helm chart)
2. YQ (to process yaml files)

## Helm chart:

Read more about helm charts here: <https://helm.sh/docs/topics/charts/>

* chart
  * <span dir="">templates</span>
    * <span dir="">CronJob.yaml</span>
    * <span dir="">Script.yaml</span>
    * <span dir="">Secret.yaml</span>
    * <span dir="">SlackScript.yaml</span>
  * Chart.yaml
  * values.yaml

Folders/files listed above are part of the helm chart. Any changes, if required, will only need to be done in _values.yaml_.

* _Script.yaml_ encodes the copy-script.sh in base 64 encoding
* _Secret.yaml_ fetches the sftp-key (SSH Key) from secrets
* _SlackScript.yaml_ encodes the slack-notification.py script in base 64 encoding
* _CronJob.yaml_ creates a container called "sftp-cron" which runs the copy-script and slack-notification script at the scheduled time everyday

## Initialization steps (required at initial setup, not after restarting notebook server):

1. Update Slack token for Slack notifications:

```plaintext
$ kubectl create secret generic slack-token --from-literal=token=XXXXXXXXXXX 
```

2. Generate SSH key and coordinate with data provider:

```plaintext
$ kubectl create secret generic <provider-ssh-key> --from-file=id_ed25519 
```

3. Add host information

```plaintext
$ kubectl create secret generic <provider-connection> \ 
                        --from-literal=host=<host ip> \
                        --from-literal=user=<sftp user> \ 
                        --from-literal=path=<folder to pull from> 
```

**NOTE: These commands do not need to be run unless there are changes from data provider server side or if slack notification bot needs to be modified.**

## Code usage:

#### 1. Dockerfile

Used image for sftp-pull from StatCan github:

(Direct link:)[ https://github.com/StatCan/aaw-contrib-containers/blob/master/sftp-pull-base/Dockerfile](https://github.com/StatCan/aaw-contrib-containers/blob/master/sftp-pull-base/Dockerfile)

Or, find dockerfile image in sftp-pull-base folder in following link:

<https://github.com/StatCan/aaw-contrib-containers>

This does not require regular updates but a need might arise to update docker image.

_CronJob.yaml_ uses the docker image, edit _values.yaml_ image option to reflect docker image update.

#### 2. Makefile

1. Install Helm (not required when notebook server restarts)

   ```plaintext
   $ make get_helm.sh
   ```
2. Install YQ (not required when notebook server restarts)

   ```plaintext
   $ make yq_linux_amd64
   ```
3. To manually run SFTP once:

   ```plaintext
   $ make manual-run
   ```
4. If changes are made to any part of the code, run following command to re-create cronjob with modifications:

   ```plaintext
   $ make clean
   $ make apply
   ```

![run_this_file](../../uploads/139173630e0921fb8b5681fbce82d1ce/run_this_file.JPG)

make apply creates _run-this-file.yaml_ that automatically runs the cronjob on it's scheduled time. This file is not uploaded on the gitlab repo. It is necessary to re-create _run-this-file.yaml_ if there are changes to any part of the code.

The script automates SEDAR live feed by connecting to data provider server and slack. The run-this-file.yaml file (created through `$ make apply` command) spins 3 processes -

1. Slack notification (runs [slack-notification.py](http://slack-notification.py) through chart/templates/SlackScript.yaml)
2. CronJob which decides when to run the script, gets paths, credentials, connects with vault, creates connection with data provider. (Found as chart/templates/CronJob.yaml)
3. Copy any files found in server side folder and move to MinIO location. (Runs [copy-script.sh](http://copy-script.sh) through chart/templates/Script.yaml)
4. Check folder contents on server side

   ```plaintext
   $ make live-shell
   ```

   ```plaintext
   /$ sftp -o "StrictHostKeyChecking=no" $${SFTP_USER}@$${SFTP_HOST}
   ```

![live_shell_step2](../../uploads/0e4bb3d327b66747d284447ae17d01fa/live_shell_step2.JPG)This command connects to the server.

![live_shell_step3](../../uploads/40140749ef71b9035dcaca41ae2c3fff/live_shell_step3.JPG)The only folder visible to us is 'sedi'. Change directory and list it's contents to manually check which files are available to us.

#### 3. chart/values.yaml

Change folder name in MinIO by changing `folder:` name in values.yaml

```plaintext
minio:
vaultFile: /vault/secrets/minio-gateway-standard
folder: inbox
```

#### 4. [create-secret.sh](http://create-secret.sh)

Only needs to be used once to generate ssh-key, does not overwrite existing ssh-key to avoid interruption.

#### 5. Scheduled time

The CronJob is currently scheduled to run at 6 AM daily. To change the scheduled time at which cronjob runs you can use [Crontab.guru ](http://Crontab.guru)as a guide. The scheduled time should be updated in _values.yaml._ The CronJob follows UTC time (EST + 4 hrs).

## Kubeflow Notebook server commands for error handling:

On a terminal, get pod names by running first command, then find any pods with their names starting with `sedar-live-feed` that have been running for too long without completion or have error status and run second command.

```plaintext
$ kubectl get pods
$ kubectl logs -f sedar-live-feed-<unique code found from previous step> sftp-cron
```

**NOTE:** MinIO URL changes should not create issues since the updated script gets MINIO_URL and credentials from vault/secrets.

## Pulling the data from CGI Pipeline

New tar files are copied from a data storage system hosted by CGI Group Inc. Sedar Project has visibility into 1 folder called 'sedi' and have limited permission for actions in the directory.

Each zip file is deleted from the directory by CGI within 10 - 14 days. If the SFTP CronJob wasn't able to copy files from the server location for a period greater than 10 days, check last date on the archive file and request CGI to copy missing files from a specific time period (to be provided by us) to 'sedi' location again.

Example: If SFTP CronJob stopped working on 1st October 2022 but it was only fixed by October 15 2022, check if all files dated archive_2022-10-01.tar.gz, archive_2022-10-02.tar.gz, archive_2022-10-03.tar.gz, … up to archive_2022-10-15.tar.gz are available in the 'sedi' folder to copy to our local storage location, if not contact CGI. 

Note: It is possible that a zip file was never created for a day (files received can be archive_2022-10-01.tar.gz, archive_2022-10-03.tar.gz, skipping 2nd October 2022) but better to check with CGI in case of pipeline interruption.

If missing data from a previous month is required, CGI will provide data for the entire month (Example: archive_2022-10.tar.gz) instead of daily files. We have to take care to not process already processed files after unzipping the tar file.

## Future development:

This CronJob script can be replaced by workflows. Workflows allow easy visualization of each part of the workflow chart and displays error logs. Workflows are not Protected-B yet, so not recommended to use as of now.

To check current development state: <https://kubeflow.aaw.cloud.statcan.ca/argo/workflows>