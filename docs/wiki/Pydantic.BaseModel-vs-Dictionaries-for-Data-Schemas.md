# Pydantic vs Dictionaries 

Dictionaries are a great data structure with multiple uses that comes integrated with python. It's one of its built-in types. We can probably get away with using a dict anytime in python and get the work done. There is one particular case when we should avoid the use of dictionaries: when we create Data Schemas, JSON formats, or complex dict structures. 

### Why dictionaries get short when defining new data schemas: 
1. Difficult to understand complex structures
2. Reduce typo errors by not having to type the "key" value. 
3. There is no self documentation. 
4. Difficult to debug and update. 

### What are pydantic models? 

The primary means of defining objects in pydantic is via models (models are simply classes which inherit from [BaseModel](https://pydantic-docs.helpmanual.io/usage/models/)).

You can think of models as similar to types in strictly typed languages, or as the requirements of a single endpoint in an API.

Untrusted data can be passed to a model, and after parsing and validation pydantic guarantees that the fields of the resultant model instance will conform to the field types defined on the model. 


Example: 
```
from pydantic import BaseModel
class Table(BaseModel):
    id: int
    name = 'Jane Doe'

table = Table(0)
table.id = 5
table.name = "Jane Smith"
table2 = Table(**{"id": 0, "name": "Jane Smith"})
```

### Some advantages of using pydantic BaseModel: 

1. Allow the usage of hierarchal data representation, and inheritance. 
2. remove the extensive usage of "key" duplication. You can access the the member of the class as attributes 
3. Defines a schema, self document the code. 
4. Easier to understand, debug, modify, and extends. 

### More about pydantic and defining Models
[Pydantic Reference](https://pydantic-docs.helpmanual.io/usage/models/)