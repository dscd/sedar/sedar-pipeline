# Environment Variables & Secrets (Authentication)

We leverage the use of pydantic and dotenv packages to supply our program with necessary secrets and credentials. (For a pydantic and dotenv tutorial follow [here](https://medium.com/codex/how-to-use-pydantic-to-read-environment-variables-and-secret-files-in-python-8a6b8c56381c))


The module config.env provides a pydantic GlobalConfig object that defines the necessary environment variables to run our program. If these variables are not set the program execution will gracefully abort and provide details about the missing variables. 


For example:
```python
class GlobalConfig(BaseSettings):
    """
    Necessary environment variables are needed for authentication.
    If these variables are not set, any file including this module will stop executing. 
    """
    MINIO_URL: str
    MINIO_ACCESS_KEY: str  
    MINIO_SECRET_KEY: str 
    ES_HOST: str 
    ES_USER: str 
    ES_PASS: str 
 
    """
    Provides the path to a ".env" containing a list of environments
    variables to be set in case that they don't exist yet. 
    """
    class Config:
        env_file: str = ConfigFile.environment.value
```



To proceed you should create a ".env" file under the *src* folder of this repository. NOTE! THIS FILE SHOULD NEVER BE INCLUDED IN THE REPOSITORY. The ".env" files have already been added to the ".gitignore" list, but extra care should be taken by the developers. 


The content of the ".env" file should be as follow (Note that are no spaces around the character "=" :


```bash
MINIO_URL=<url_to_minio>
MINIO_ACCESS_KEY=<access_key> 
MINIO_SECRET_KEY=<secret_key>
ES_HOST=<es_host>
ES_USER=<es_user>
ES_PASS=<es_pass>
```


#### MinIO Credentials 

To obtain your MinIO credentials, open a Terminal in your AAW notebook server and display the content of the following file: 
```/vault/secrets/minio-standard-tenant-1.json``` retrieve the necessary values and place them correctly inside your ".env" file. 


#### Elastic Search Credentials

Make sure to include the elastic search credentials inside the .env file as previously explained. 


# Create .env file at the src project folder 


The content of the ".env" file should be as follow (Note that are no spaces around the character "=" :

```bash
MINIO_URL=<url_to_minio>
MINIO_ACCESS_KEY=<access_key> 
MINIO_SECRET_KEY=<secret_key>
ES_HOST=<es_host>
ES_USER=<es_user>
ES_PASS=<es_pass>
```

#MinIO Credentials

To obtain your MinIO credentials, open a Terminal in your AAW notebook server and display the content of the following file: `/vault/secrets/minio-standard-tenant-1.json` retrieve the necessary values and place them correctly inside your ".env" file.

```bash
MINIO_URL=https://minio-standard-tenant-1.covid.cloud.statcan.ca/
MINIO_ACCESS_KEY=profile-aaw-sedar-xxxxxxxxxxxxxxxxxxxxx  
MINIO_SECRET_KEY=yyyyyyyyyyyyyyyyyyyy
ES_HOST=
ES_USER=
ES_PASS= 
```

# Elastic Search Credentials

Make sure to include the elastic search credentials inside the .env file as previously explained.

```bash
MINIO_URL=https://minio-standard-tenant-1.covid.cloud.statcan.ca/
MINIO_ACCESS_KEY=profile-aaw-sedar-xxxxxxxxxxxxxxxxxxxxx  
MINIO_SECRET_KEY=yyyyyyyyyyyyyyyyyyyy
ES_HOST=1xx.1x.1x.1
ES_USER=user
ES_PASS=password
```

# Accessing Credentials 

The module config.env uses pydantic and dotenv to obtain the secrets and credentials as environment variables. If the current environment doesn't contain the environment variables, 
the fallback option will be to read their values from the .env file. 

```python
from config.env import environ

environ().MINIO_URL
environ().MINIO_ACCESS_KEY

environ().ES_USER
environ().ES_PASS
```


# MinIO Client 


Using MinIO Objects 

```python
from config.api import DataSource, S3, S3Location
from modules.s3client import S3Client
client = S3Client.get_client()

## This is the old way to list minio location, we shouldn't use hardcoded strings. 
## Instead we should use the S3 proxy class in config.api module. 
for obj in client.ls_objs('aaw-sedar','historical_data/',recursive=True):
    print(obj.object_name)

## This is the correct way, using the S3 proxy class
for obj in client.ls_objs(S3.HISTORICAL_DATA.bucket,S3.HISTORICAL_DATA.prefix,recursive=True):
    print(obj.object_name)

## Listing all SEDAR historical data for year 1989, month = 3
loc = S3.get_historical_data_loc(DataSource.sedar, year=1980, month=3) 
client.ls_objs(loc.bucket,loc.prefix,recursive=True):
    print(obj.object_name)

## List all historical data from SEDAR under year 1990
loc = S3.get_historical_data_loc(DataSource.sedar, year=1990) 
client.ls_objs(loc.bucket,loc.prefix,recursive=True):
    print(obj.object_name)

### Preferred * Using S3Locations for input and output

loc = S3.HISTORICAL_DATA
#loc = S3.get_historical_data(DataSource.sedar, 2020)
#loc = S3.get_historical_data(DataSource.sedar, 2020, 12)
for obj in client.ls_locs(loc,recursive=True):
    print(obj)


```