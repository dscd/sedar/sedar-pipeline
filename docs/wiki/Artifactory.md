## Overview

After receiving from the Cloud Native team our SEDAR Artifactory Service Account credentials. We need to set the necessary CI/CD configuration and credentials to submit the SEDAR Docker image built by our CI/CD process. 

Under file ```.gitlab-ci.yml``` set the global variable value of ```{{ .variables.IMAGE_REPOSITORY }} to artifactory.cloud.statcan.ca/docker/sedar/sedar```. 

Under Gitlab Project ```Settings >> CI/CD >> Variables``` create two variables with the names: 
 - IMAGE_REGISTRY_USER | Value provided by Native Cloud Team (Service Account credentials)
 - IMAGE_REGISTRY_PASS | Value provided by Native Cloud Team (Service Account credentials)