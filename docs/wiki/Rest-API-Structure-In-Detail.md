**COMPANY DETAILS**

_Description_
- This API(GET) is to recover company details. It uses either issuerno or company name as arguments.

_Args_ 

IssuerNo (str)
- A unique Identifier for a company.
- Type - String 
- Length - fixed length of 8
- Example - 00042885

Name (str) - The name of the company to be searched or the default name of None.
 
- Lang (str) - if the parameter is EN then the response is translated to French else the response is sent in English. By default the response is in French. 

_Request_
- Get Request with the issuer number and language 
`(api/company-details/?issuerno={issuer_no}&lang=en)`
- Get Request with the company name and language 
`(api/company-details/?name={company_name}&lang=en)`

_Response_
- Returns the list containing the company details

_Sample Response_

```
{
    "Company Details": {
        "Company Name": (str),
        "Issuer No.": (str),
        "Form Dates": (str),
        "CUSIP": (str),
        "Exchanges": (str),
        "Form Type": (str),
        "Year End Date": (str),
        "Industry": (str),
        "Report": (str),
        "OTC Issuer Detail": (str),
        "Size of Issuer": (str),
        "Stock Symbol": (str),
        "Type": (str)
    },
    "Contact Information": {
        "Contact Name": (str),
        "Phone Area Code":(str),
        "Phone Number": (str),
        "Extension": (str),
        "Fax Area Code": (str),
        "Fax Number": (str),
    },
    "Record Details": {
        "Last Update": (str),
        "Short Form Propectus Issuer": (str),
        "Created On": (str),
        "Record Date": (str)
    },
    "Other Information": {
        "Partner": (str),
        "Auditor": (str),
        "Transfer Agent": (str),
        "Registration Application":(str),
        "Application for Exemption":(str),
        "National Policy Elections": (str),
        "Offerings": (str),
        "Security Type": (str),
        "Proceeds":(str),
        "Underwriter": (str),
        "Underwriter Counsel": (str),
        "Underwriter Contact":(str),
        "Promoter":(str),
        "Prefiling": (str),
        "Local Policy Statement":(str)
    },
    "Business Location": {
        "City": (str),
        "Postal Code": (str),
        "Country": (str),
        "Phone Extension Number": (str),
        "Fax Area Code": (str),
        "Fax Number": (str),
        "Phone Area Code": (str),
        "Phone Number": (str),
        "Province": (str),
        "Street 1":(str),
        "Street 2": (str)
    },
    "Mailing Location": {
        "City": (str),
        "Postal Code": (str),
        "Country":(str),
        "Phone Extension Number":(str),
        "Fax Area Code": (str),
        "Fax Number": (str),
        "Phone Area Code": (str),
        "Phone Number": (str),
        "Province":(str),
        "Street 1":(str),
        "Street 2": (str)
    }
}
```



**PDF File View**

_Description_
- This a GET API to recover the file path to the pdf file stored in the Azure Containers. 
- For the GET request, it accepts a combination of name and date or just file id to recover the link.

_Args_
- data_format (str): The format of the data to be returned. It could be select2 or select1.
- name (str): The name of the company to be searched or the default name of None.
- file_id (str): unique id of the document entry in ElasticSearch for that company

_Request_
- Get Request with the file_id `api/pdf-file/?file_id=a2dfe3c376ad8c8f5396001d4d673408f81ff637b78532cbbe0fa59bc443debb`

_Sample Response_

```
{
    "DETECTED_PAGES": {
        "BALANCE_SHEET": int,
        "CASHFLOW_STATEMENT": int,
        "INCOME_STATEMENT": int
    },
    "PDF_LINK": "<link to pdf>"
}
```