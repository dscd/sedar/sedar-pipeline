 #### Development Environment Status Check

 - [x] Cloud VM Configuration 
 - [x] Tech Stack 
 - [x] SDLC Connection
     - [x] Development
     - [x] Production 
 - [x] .bashrc Kubectl config & k8s alias 
     - [k8s .bashrc script](https://gitlab.k8s.cloud.statcan.ca/sedar/project/sedar_pipeline/-/blob/master/scripts/k8s_init.sh)
 - [x]  SSH Key for internal gitlab access 
     - [x] Sedar App repository clone 
     - [x] Cloudnative Sedar App Helm    
 - [x] Conda environment for Sedar 
 - [x] k9s
 - [ ] Role Matrix & Responsibilities Document

 #### Overview
 - [x] KT Meetings frequency
       Ad-Hoc / Recurrent every two-days. 
 - [x] Architecture & Components
     - [x] [Overview](SEDAR/NEW/Overview) 
     - [x] [Data Frequency](SEDAR/NEW/Scheduling)
     - [x] [MinIO Storage](MinIO/MinIO-Storage-Description)
        - [Dev](https://portal.azure.com/#@cloud.statcan.ca/asset/Microsoft_Azure_Storage/StorageAccount/subscriptions/1373e92a-8aeb-419e-93d5-c420de1c51cd/resourceGroups/sedar-dev-rg/providers/Microsoft.Storage/storageAccounts/stnbs0sedar00sa)
        - [Prod](https://portal.azure.com/#@cloud.statcan.ca/asset/Microsoft_Azure_Storage/StorageAccount/subscriptions/4e580731-fa3e-4957-954a-34d4b1da003e/resourceGroups/sedar-production-rg/providers/Microsoft.Storage/storageAccounts/stpbs0sedar00sa) 
     - [x] [Sedar Cronjob](https://gitlab.k8s.cloud.statcan.ca/cloudnative/k8s/charts/-/blob/master/stable/sedar/copy-script.sh)
     - [x] [Sedar Pipeline](https://gitlab.k8s.cloud.statcan.ca/sedar/project/sedar_pipeline/-/blob/master/src/sedar_pipeline.py)
     - [x] [Sedar Webapp](https://gitlab.k8s.cloud.statcan.ca/sedar/project/sedar_pipeline/-/blob/master/src/run_webapp.sh)

      

 ####  Relevant Links

 - [x] Documentation
    - [x] [Published Documentation](https://sedar.pages.cloud.statcan.ca/project/sedar_pipeline/)
    - [x] [Auto-generated API](https://sedar.pages.cloud.statcan.ca/project/sedar_pipeline/autoapi/index.html)
    - [x] [Wiki Documentation](home)
    - [x] [Architecture](Architecture)
    - [x] [ConOps](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=16494313)
    - [x] [SOS](https://gcdocs.gc.ca/statcan/llisapi.dll/app/nodes/22003224)
    - [x] [Administrative Items & Issue Tracking](Administrative-Cloud-Items)
 - [x] Git Repositories
    - [x] [SEDAR APP Pipeline](https://gitlab.k8s.cloud.statcan.ca/sedar/project/sedar_pipeline)
    - [x] [K8S Cloud Native SEDAR App](https://gitlab.k8s.cloud.statcan.ca/cloudnative/k8s/charts)
 - [x] [Octopus Deployment](https://octopus.cloud.statcan.ca/)
    - [x] Add Nicholas to Octopus SEDAR team 
    - [x] Update Helm config values 
       - [x] Update Image tags from sedar_pipeline CI/CD 
    - [x] Deploying in Dev 
    - [x] Deploying in Prod 
 - [x] Slack Channels
    - [x] [Development](https://sedargroup.slack.com/archives/C0454RK8FG8)
    - [x] [Production](https://sedargroup.slack.com/archives/C044103PBKR)
    - [x] Review SFTP Cronjob Slack Notification system
 - [x] K8S Development Access List
     - [x] [SEDAR Group Active Directory Owners](https://portal.azure.com/#view/Microsoft_AAD_IAM/GroupDetailsMenuBlade/~/Owners/groupId/4a18426d-ab08-4732-9aff-ac8978f55370)
     - [x] [SEDAR Group Active Directory Members](https://portal.azure.com/#view/Microsoft_AAD_IAM/GroupDetailsMenuBlade/~/Members/groupId/4a18426d-ab08-4732-9aff-ac8978f55370)
 
 - [x] [WebApp Access List](Azure/Adding-Web-App-User)
    - [x] Enterprise application: [sedar-dev](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Overview/objectId/4b080e12-75d4-4c6c-ba68-7f66226ca8a7/appId/760923e1-205c-46fa-81f7-6bf88ee170c5/preferredSingleSignOnMode~/null/servicePrincipalType/Application) 
       - [x] [Owners](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Owners/objectId/4b080e12-75d4-4c6c-ba68-7f66226ca8a7/appId/760923e1-205c-46fa-81f7-6bf88ee170c5/preferredSingleSignOnMode~/null/servicePrincipalType/Application)
       - [x] [Users](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Users/objectId/4b080e12-75d4-4c6c-ba68-7f66226ca8a7/appId/760923e1-205c-46fa-81f7-6bf88ee170c5/preferredSingleSignOnMode~/null/servicePrincipalType/Application)
    - [x] Enterprise Application: [sedar-prod](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Overview/objectId/c6ceac1a-424b-4088-b8d0-73a36bf7e9c5/appId/11fbead4-9b9c-4b77-82e7-198e8d830f67/preferredSingleSignOnMode~/null/servicePrincipalType/Application)
       - [x] [Owners](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Owners/objectId/c6ceac1a-424b-4088-b8d0-73a36bf7e9c5/appId/11fbead4-9b9c-4b77-82e7-198e8d830f67/preferredSingleSignOnMode~/null/servicePrincipalType/Application)
       - [x] [Users](https://portal.azure.com/#view/Microsoft_AAD_IAM/ManagedAppMenuBlade/~/Users/objectId/c6ceac1a-424b-4088-b8d0-73a36bf7e9c5/appId/11fbead4-9b9c-4b77-82e7-198e8d830f67/preferredSingleSignOnMode~/null/servicePrincipalType/Application)
 - [x] [Resource Group sedar-dev-reg Storage Account](https://portal.azure.com/#@cloud.statcan.ca/resource/subscriptions/1373e92a-8aeb-419e-93d5-c420de1c51cd/resourceGroups/sedar-dev-rg/overview)
 - [x] [Resource Group sedar-prod-reg Storage Account](https://portal.azure.com/#@cloud.statcan.ca/resource/subscriptions/4e580731-fa3e-4957-954a-34d4b1da003e/resourceGroups/sedar-production-rg/overview)
 - [x] WebApp 
     - [x] [Development](https://sedar-webapp.dev.cloud.statcan.ca/)
     - [x] [Production](https://sedar-webapp.prod.cloud.statcan.ca/) 

 - [ ] SEDAR+ Connectivity 
       Once confirmed, an encrypted email will be sent to the team. 

#### Troubleshooting 

- [x] Transfer Data
    - [x] Devto Prod / Prod to Dev
    - [x] [AzCopy for MinIO](Transferring-Data-between-SDLC-stages)
    - [x] [elasticdump for ES](Migrating-Elastic-Search-Data)
- [x] VSCode Debug 
   - [x] [Debug Script Initialization for storage port-forwarding](https://gitlab.k8s.cloud.statcan.ca/sedar/project/sedar_pipeline/-/blob/master/scripts/debug_env.sh)
   - [x] Debugging sedar_pipeline
   - [x] Debugging Sedar Webapp
   - [x] Updating Development and Production Image Tag (Octopus) 
- [x] [Pipeline Error Handling](SEDAR/NEW/Error-Handling)
- [x] [Helm Deployment Resource Allocation](https://kubecost.prod.cloud.statcan.ca/request-sizing?filters=namespace%3Asedar)
       - [Outgoing Cost](https://kubecost.prod.cloud.statcan.ca/details?name=sedar&type=namespace)


#### Feature Enhancement Exercise (Optional)
- Add Slack notifications to the data pipeline error handling 
   - [ ] Integrate slack-notification.py script into the sedar_pipeline repository

   - [ ] Take k8s Slack Secret and pass it to the sedar_pipeline deployment as env variables SLACK_CHANNEL and SLACK_TOKEN. 

   - [ ] Use the slack-notification.py code with the SLACK_CHANNEL and SLACK_TOKEN env variables inside the sedar_pipeline code to send notifications messages when an error occurs

   - [ ] Submit code to sedar_repository master branch 
   - [ ] Update image tags in Octopus deployment 