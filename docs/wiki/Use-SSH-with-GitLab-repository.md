# Overview

Full [documentation](https://docs.gitlab.com/ee/user/ssh.html)

#### Generate an SSH key pair

Open a terminal. Run `ssh-keygen -t` followed by the key type and an optional comment. This comment is included in the .pub file that’s created. You may want to use an email address for the comment.

For example, for ED25519:

`$ ssh-keygen -t ed25519 -C "<username@statcan.gc.ca>"`

Press Enter. Output similar to the following is displayed:

```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```

Accept the suggested filename and directory.

Specify a passphrase:

```
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

A confirmation is displayed, including information about where your files are stored.

A public and private key are generated. Add the public SSH key to your GitLab account and keep the private key secure.\`

#### Add your public key to your GitLab Instance

To use SSH with [https://gitlab.k8s.cloud.statcan.ca/](https://gitlab.k8s.cloud.statcan.ca/), copy your public key to your GitLab account:

Copy the contents of your public key file. For example, to copy an ED25519 key to the clipboard:

`cat ~/.ssh/id_ed25519.pub`

Sign in to [Gitlab](https://gitlab.k8s.cloud.statcan.ca/).

On the top bar, in the top right corner, select your avatar. Select Preferences. 
On the left sidebar, select SSH Keys. 
In the Key box, paste the contents of your public key. If you manually copied the key, make sure you copy the entire key, which starts with ssh-rsa, ssh-dss, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519, sk-ecdsa-sha2-nistp256@openssh.com, or sk-ssh-ed25519@openssh.com, and may end with a comment.

In the Title box, type a description, like Work Laptop or Home Workstation. Optional. Update Expiration date to modify the default expiration date. 

Select Add key


#### Clone the repository

```$ git clone git@gitlab-ssh.k8s.cloud.statcan.ca:sedar/project/sedar_pipeline.git ```