# Performance Statistics

Results are in terms of averages over multiple zip files.

### Control pipeline processing:
On average the process takes 1-2 seconds. This process only executes once for the entire zip file

### XML and PDF processing:
1. 150,777 xml files were processed for this analysis. Metadata extraction (compute_metadata and compute_text) was processed for each of these files.  
2. 9,574 (xml + pdf considered as a single file) files were processed for table text extraction (compute_metadata + compute_text + compute_page + compute_table).

 
Note: 
1. Processing a combination of a xml and pdf file for table text extraction takes <mark>6.32 secs</mark> if they are expected to be extracted as per sedar static variables else total time is : <mark> 0.5 secs </mark> per xml and file. Thus for 9,574 files (xml + pdf) the total processing time for table text extraction takes <mark> 16 hours </mark> (1,008 mins).

2. As an example a single tar file (archive-2021-06-29) contains 2954 files (a file is considered as a combination of pdf + xml). 354 files were processed for table text extraction while 2540 files were processed for metadata extraction.  
   * Total time to process this sample tar file :  <mark> 59.4 mins </mark> 
        - Step 0 : 2.2 secs
        - Step 1 - Step 5 : 20.8 secs
        - Step 6 : 6.32 secs for table text extraction on each file and
                   0.5 secs for metadata extraction on each file that does not require table 
                   text extraction. This indicates 37.8 mins for all files there processed for 
                   table text extraction and 21.2 mins for files that underwent metadata 
                   extraction.  
        - Step 7 - Step 8 : 1.9 secs 

| <div style="width:400px"> Description </div> | Time (secs) |
|-------------------------------- |------| 
| 0.  Initialize model and control file list | 2.2 |
| Iterate over tar files in inbox folder| |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1. Validate and extract filename | 0.001 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2. Extract date from filename	| 0.000004 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3. Get historical record of file | 0.019 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3.a. Check if file exist in historical else remove | 0.00002 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3.b. Copy file to local cache folder | 6.96 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 4. Extract tar file contents (PDF, XML and txt files) | 12.9 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5. Get CTL FILE_ID's for this file and add to control file list | 0.95 |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6*. <mark> Loop over all xml and pdf files in the extracted tar file </mark> | <mark> 6.32 </mark> | 
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.1. Generate FILE_ID an initialize FileInfoExtractor | 0.0006 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.2. Compute metadata | 0.003 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.3. Compute text | 0.002 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.4.1. Compute page | 0.37 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.4.2. Compute table | 5.4 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.5. Extract previous records with this FILE_ID | 0.12 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.5.a. Delete previous records with this FILE_ID from all indices | 0.05 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.5.b. Ingest new records with this FILE_ID to all indices | 0.16 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.6. Upload PDF to Minio | 0.21 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 6.7. Remove PDF, XML and txt files associated to this filename from local cache | 0.009 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 7. Save / Overwrite file to historical folder on Minio | 1.5 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 8. Remove ZIP file from inbox | 0.3 | |
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 9. Clear inbox | 0.1 | |