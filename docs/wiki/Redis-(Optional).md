
## Redis 
In the case you would like to enable the usage of Redis. Here is an working example and configuration. 
This is currently not enabled in our deployment, it's just here for future reference. 

```yaml 
apiVersion: v1
kind: Service
metadata:
  name: redis
  labels:
    app: node-redis
spec:
  ports:
  - name: redisk
    port: 6379
    targetPort: 6379
  type: ClusterIP
  selector:
    app: node-redis
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
spec:
  selector:
    matchLabels:
      app: node-redis
  replicas: 1
  template:
    metadata:
      labels:
        app: node-redis
    spec:
      containers:
      - name: redis
        image: redis:latest
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 6379
        # data volume where redis writes data
        volumeMounts:
        - name: data
          mountPath: /data
          readOnly: false
      volumes:
      - name: data
        persistentVolumeClaim:
          claimName: redis-data
---
# data volume
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: redis-data
  labels:
    app: node-redis
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
```