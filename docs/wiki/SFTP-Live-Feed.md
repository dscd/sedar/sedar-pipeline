## Gitlab repository:

<https://gitlab.k8s.cloud.statcan.ca/data-science-division/aeas-aera/sedar/sftp-cronjob>

Currently used code can be found in `sftp-cron-v2` folder in Kubeflow notebook server `sedar-prod`.

## Basic Kubernetes CronJob guide:

<https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/>

## sftp-cron folder structure:

* chart
  * <span dir="">templates</span>
    * <span dir="">CronJob.yaml</span>
    * <span dir="">Script.yaml</span>
    * <span dir="">Secret.yaml</span>
    * <span dir="">SlackScript.yaml</span>
  * Chart.yaml
  * [copy-script.sh](http://copy-script.sh)
  * [slack-notification.py](http://slack-notification.py)
  * values.yaml
* Dockerfile
* Makefile
* create-secret.sh

## Dependencies:

1. Helm (to run helm chart)
2. YQ (to process yaml files)

## Initialization steps (required at the very first setup, not after restarting notebook server):

1. Update Slack token for Slack notifications:

```plaintext
$ kubectl create secret generic slack-token --from-literal=token=XXXXXXXXXXX 
```

2. Generate SSH key and coordinate with data provider:

```plaintext
$ kubectl create secret generic <provider-ssh-key> --from-file=id_ed25519 
```

3. Add host information

```plaintext
$ kubectl create secret generic <provider-connection> \ 
                        --from-literal=host=<host ip> \
                        --from-literal=user=<sftp user> \ 
                        --from-literal=path=<folder to pull from> 
```

**NOTE: These commands do not need to be run unless there are changes from data provider server side or if slack notification bot needs to be modified.**

## Code usage:

#### 1. Dockerfile

Used image for sftp-pull from StatCan github:

(Direct link:)[ https://github.com/StatCan/aaw-contrib-containers/blob/master/sftp-pull-base/Dockerfile](https://github.com/StatCan/aaw-contrib-containers/blob/master/sftp-pull-base/Dockerfile)

Or, find dockerfile image in sftp-pull-base folder in following link:

<https://github.com/StatCan/aaw-contrib-containers>

This does not require regular updates but a need might arise to update docker image.

#### 2. Makefile

1. Install Helm (not required when notebook server restarts)

   ```plaintext
   $ make get_helm.sh
   ```
2. Install YQ (not required when notebook server restarts)

   ```plaintext
   $ make yq_linux_amd64
   ```
3. To manually run SFTP once:

   ```plaintext
   $ make manual-run
   ```
4. If changes are made to any part of the code, run following command to re-create cronjob with modifications:

   ```plaintext
   $ make clean
   $ make apply
   ```

The script automates SEDAR live feed by connecting to data provider server and slack. The run-this-file.yaml file (created through `$ make apply` command) spins 3 processes -

1. Slack notification (runs [slack-notification.py](http://slack-notification.py) through chart/templates/SlackScript.yaml)
2. CronJob which decides when to run the script, gets paths, credentials, connects with vault, creates connection with data provider. (Found as chart/templates/CronJob.yaml)
3. Copy any files found in server side folder and move to MinIO location. (Runs [copy-script.sh](http://copy-script.sh) through chart/templates/Script.yaml)

#### 4. chart/values.yaml

Change folder name in MinIO by changing `folder:` name in values.yaml

```plaintext
minio:
vaultFile: /vault/secrets/minio-gateway-standard
folder: sedar
```

#### 5. [create-secret.sh](http://create-secret.sh)

Only needs to be used once to generate ssh-key, does not overwrite existing ssh-key to avoid interruption.

## Kubeflow Notebook server commands for error handling:

On a terminal, get pod names by running first command, then find any pods with their names starting with `sedar-live-feed` that have been running for too long without completion or have error status and run second command.

```plaintext
$ kubectl get pods
$ kubectl logs -f sedar-live-feed-<unique code found from previous step> sftp-cron
```

**NOTE:** MinIO URL changes should not create issues since the updated script gets MINIO_URL and credentials from vault/secrets.

## Future development:

This CronJob script can be replaced by workflows. Workflows allow easy visualization of each part of the workflow chart and displays error logs. Workflows are not Protected-B yet, so not recommended to use as of now.

To check current development state: <https://kubeflow.aaw.cloud.statcan.ca/argo/workflows>