This is the detailed documentation of locations.json and sedar.json used for the entire extraction process in Sedar.


--------------------------------------
**Locations.json** file

```
{
    "INBOX"            : {"bucket":"aaw-sedar", "prefix":"inbox/"},
    "ERROR"            : {"bucket":"aaw-sedar", "prefix":"error/"},
    "CACHE"            : {"bucket":"", "prefix":"cache/"},
    "HISTORICAL_DATA"  : {"bucket":"aaw-sedar", "prefix":"historical/"},
    "DATA"             : {"bucket":"aaw-sedar", "prefix":"data/"},
    "DATABASE"         : {"bucket":"aaw-sedar", "prefix":"db/"},
    "MODELS"           : {"bucket":"aaw-sedar", "prefix":"models/"},
    "LOOKUP_TABLES"    : {"bucket":"aaw-sedar", "prefix":"models/lookup_tables/", "basename":"sedar_lookup_table.xlsx"},
    "TMP"              : {"bucket":"aaw-sedar", "prefix":"tmp/"},
    "NLTK_DATA"        : {"bucket":"aaw-sedar", "prefix":"models/page_detection/nltk_data/"},
    "IS_BS_BOW"	       : {"bucket":"aaw-sedar", "prefix":"models/page_detection/IS_BS/", "basename":"is_bs_detection_bow.sav"},
    "IS_BS_CLASSIFIER" : {"bucket":"aaw-sedar", "prefix":"models/page_detection/IS_BS/", "basename":"is_bs_detection.sav"},
    "RF_CLASSIFIER"	   : {"bucket":"aaw-sedar", "prefix":"models/page_detection/IS_BS/", "basename":"finalized_model_rf.sav"},
    "NOTES_BOW"        : {"bucket":"aaw-sedar", "prefix":"models/page_detection/Notes/", "basename":"notes_detection_bow.sav"},
    "NOTES_CLASSIFIER" : {"bucket":"aaw-sedar", "prefix":"models/page_detection/Notes/", "basename":"notes_detection.sav"},
    "SEG_BOW"	       : {"bucket":"aaw-sedar", "prefix":"models/page_detection/SEG/", "basename":"segmentation_detection_bow.sav"},
    "SEG_CLASSIFIER"   : {"bucket":"aaw-sedar", "prefix":"models/page_detection/SEG/", "basename":"segmentation_detection.sav"}
}
```

* The bucket is the Minio storage bucket where the Sedar files before processing(tar files) and also the processed files are saved. The Minio bucket for Sedar project is "aaw-sedar".
* There are multiple folders in the "aaw_sedar" bucket. Prefix is the actual folder inside the bucket where the files are stored.
* Minio storage bucket for Sedar can be accessed using the following URL "https://minio.aaw.cloud.statcan.ca/buckets/aaw-sedar/browse"
* **inbox** : The location where the new data comes to be moved.
* **Error** : All the instances are stored in this location where the pipeline failed to run.
* **cache** : This folder serves as a backup for unzipping the files.
* **historical** : This folder consists of all the zip and tar.gz files for all the previous year files.
* **lookup_tables** : This folder consists of "sedar_data_dictionary.xlsx" and "sedar_lookup_tables.xlsx".
* **nltk_path** : Path with the natural language tool kit. 
* **models** : the models are trained on Sedar training data and saved in the below mentioned paths.

```
SEG_CLASSIFIER: '../aaw-sedar/models/page_detection/SEG/segmentation_detection.sav'
SEG_BOW: '../aaw-sedar/models/page_detection/SEG/segmentation_detection_bow.sav'
NOTES_BOW : '../aaw-sedar/models/page_detection/Notes/notes_detection_bow.sav'
NOTES_CLASSIFIER: '../aaw-sedar/models/page_detection/Notes/notes_detection.sav'
IS_BS_BOW: '../aaw-sedar/models/page_detection/IS_BS/is_bs_detection_bow.sav'
IS_BS_CLASSIFIER: '../aaw-sedar/models/page_detection/IS_BS/is_bs_detection.sav'
RF_CLASSIFIER: '../aaw-sedar/models/page_detection/IS_BS/finalized_model_rf.sav'
```

| Model | Usage |
|-------|-------|
| seg_classify_model | Built to identify the segmentation page |
| seg_bag_of_word_model | Bag of words features to identify segmentation page |
| notes_bag_of_word_model | Bag of words model to identify notes page |
| notes_classify_model | Classification Model to identify notes page |
| bag_of_word_model | Bag of words model for identifying Balance sheet and Income statement. |
| classify_model | Random forest classifier to classify Balance sheet and Income statement. |
| finalized_model| Final Random forest classifier to classify Balance sheet and Income statement. |




---------------------------
**Sedar.json** file

```
extraction_documents: [
# SEDAR financial stements are used
"Audited annual financial statements - English",
"Audited annual financial statements - French",
"Interim financial statements/report - English",
"Interim financial statements/report - French",
"Interim financial statements/report (amended) - French",
"Interim financial statements/report (amended) - English",
"Audited annual financial statements (amended) - French",
"Audited annual financial statements (amended) - English",        
"Annual information form - English",
"Annual information form - French",
"Annual report - English", 
"Business acquisition report - English",
"Closing News Release (amended) - English",
"Closing News Release - English",
"Company's undertaking to provide material contracts on request",
"Crowdfunding offering document (MI 45-108) - English",
"Early warning report",
"Final annual information form - English",
"News Release - English",
"News release - English",
"Notice of change - Directors' circular - English",
"Notice of change or variation - English",
"Press release - English",
"Take-over bid circular - English",
"MD&A - English",
"MD&A - French",
"MD&A supplement - English",
"MD&A (amended) - English",
"MD&A - letter from foreign issuer",
"MD&A (amended) - French",
"MD&A of an operating entity",
"MD&A supplement - French",
"MD&A supplement (amended) - English",
"Report of exempt distribution excluding Schedule 1 of 45-106F1",
"Report of exempt distribution excluding Schedule 1 of 45-106F1 (amended)",
"Application for exemption from NI 81-101",
"Report of exempt distribution excluding Schedule 1 of Form 5",
"Report of exempt distribution excluding Schedule 1 of Form 5 (amended)"
]
```

* The Extracted document list is used to run file extraction pipeline in page_extraction.py. This notebook is used to used to extract information from unzipped tar files.
* We only need to identify and extract variables from the financial statements. In order to filter the financial statements we have created this dictionary with the types of financial statements included.
* To execute the file extraction part in the above mentioned notebook we developed a python script which is in sedar_utilities/file_extraction.py file.
* Once we filter the documents with just financial statements, we update the "file_info_text" table with the filtered info of all the pages in a pdf document.

```
# The below snippet is from the sedar_pipeline.py python script where the document types are filtered. 
## Step 6.2
## Compute metadata
file_info_ex.compute_metadata()

## Step 6.3

if file_info_ex.FILE_INFO.DOCUMENT_DESC in static_variables.extraction_documents:
	if file_info_ex.FILE_INFO.PAGE_COUNT != -1:
		file_info_ex.compute_text()
        else:
		for filename in glob2.glob(''.join([xml_file[:-8], "*"])):
			os.remove(filename)

output["FILE"]["FILE_INFO_TEXT"] = file_page_df

## Output of this code will result in the filtered data frame with financial documents. 

```

```
"sedar_documents" : [
"Audited annual financial statements - English",
"Interim financial statements/report - English",
"Interim financial statements/report (amended) - English",
"Audited annual financial statements (amended) - English"
]
```

* Information about the sedar_documents


```
months: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'] 
currencies: ['cad','cdn','canadian dollar','usd','us dollars','u.s. dollars','united states dollar','canadian $','US $','united states $','c$','us$','euro']
units: ["$000","million","billion","thousand"]
page_types: ["BALANCE_SHEET", "INCOME_STATEMENT", "CASHFLOW_STATEMENT"],
```

Used to extract variables from the identified pdf documents. These variables are extracted in the pdf document. This is are stored in "sedar.json" and used by "models.py" as a list.