# Deploying on Cloud Main SDLC Clusters

> TODO


## Kubernetes Ingress

**Important**: when you set `pathType` to `ImplementationSpecific` , it defaults to either `Exact` or `Prefix` depending on the ingress controller being used. In the case of Istio, this defaults to `Exact`. This means, for example, a configuration like that shown below will match all requests to `sedar-webapp.dev.cloud.statcan.ca/*`, whereas if the `pathType` was `ImplementationSpecific`, it would **only** match `sedar-webapp.dev.cloud.statcan.ca/` and nothing else.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: sedar
  namespace: sedar
spec:
  ingressClassName: ingress-istio-controller
  rules:
  - host: sedar-webapp.dev.cloud.statcan.ca
    http:
      paths:
      - backend:
          service:
            name: sedar
            port:
              number: 80
        path: /
        pathType: Prefix
```

Make sure that the ingress used matches the intended behaviour for your use case.
