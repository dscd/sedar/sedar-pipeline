#### Adding New User to the Web App. 

- Visit ```portal.azure.com```
- Enterprise Applications 
- Search Application by ```sedar-dev``` or ```sedar-prod```
- Click on ```User & Groups```
- Click on  ```+ Users/Group```
- Click on ```Users/Group None Selected ``` 
- Search for user email, click on email and ```Select```
- Click on Assign

New user can now navigate to the Sedar App and log into Azure with Statcan credentials. 
- [Development Sedar Dashboard](https://sedar-webapp.dev.cloud.statcan.ca)
- [Production Sedar Dashboard](https://sedar-webapp.prod.cloud.statcan.ca)



#### Add new [Member/Owner] to Kubernetes Dev resources
- Visit ```portal.azure.com```
- Search ```Azure Active Directory```
- Click on [Groups](https://portal.azure.com/#view/Microsoft_AAD_IAM/GroupsManagementMenuBlade/~/AllGroups)
- Search for ```SEDAR``` Group and click on it
- Click on ``` Members ``` or ``` Owners ``` to modify each list. 
- ```Add Member``` or ```Add Owner``` email. 

Note: Even if you are an Owner, you need to be a member to be able to access SDLC Kubernetes Cluster. 


#### Storage Account 
- Visit ```portal.azure.com```
- Search for Storage Account: ```stnbs0sedar00sa```
- Click on ```Containers```
- Navigate to desired files and folders. 

NOTE: DO NOT MODIFY FILES MANUALLY UNLESS YOU KNOW WHAT YOU ARE DOING. YOU ARE NOT SUPPOSE TO MANIPULATE THESE FILES FROM HERE. JUST FOR DEBUGGING PURPOSES. 

