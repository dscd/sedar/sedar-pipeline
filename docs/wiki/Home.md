# Documentation
- [Knowledge Transfer](Knowledge-Transfer) 
- Development Environment [Main Cloud]
  - [Setting up the development environment](Setting-up-the-development-environment)
  - [Connect to the SDLC Kubernetes Clusters](Connect-to-the-SDLC-Kubernetes-Clusters)
  - [WSL-Environment: Docker, Task, and Helm](/WSL-Environment:-Docker-+-Task-+-Helm)
  - [Use SSH with your GitLab repository](Use-SSH-with-GitLab-repository)
  
- Kubernetes Deployment
  - [Architecture](Architecture)
  - [OAuth](OAuth2-OpenID)
  - [Redis - Optional](Redis-(Optional))
  - [CI/CD Artifactory](Artifactory)
  - [Ingress](Kubernetes)
  - [Secrets](Secrets)
 
- Azure Management
  - [Adding Users & Storage Account](Azure/Adding-Web-App-User) 
  
- [SFTP Pull CronJob\*](SEDAR/OLD/SFTP-Live-Feed)
- SEDAR App
  - [Overview](SEDAR/NEW/Overview)
  - [Sedar OAuth Logo](Sedar-App-OAuth-Logo)
  - [Data Frequency](SEDAR/NEW/Scheduling)
  - [Folder Structure](Folder-Structure)
  - [Credentials and Config](Config-Module)
  - [Settings](Settings)
  - [Initialization](SEDAR/NEW/Initialization)
  - ETL Pipeline
    * [Data Flow](SEDAR/NEW/Data-Flow)
    * [Performance](Data-Flow-Performance)
  - [ML Algorithms](SEDAR/OLD/Algorithms)
  - [Error Handling](SEDAR/NEW/Error-Handling)
  - [Data Structures](SEDAR/NEW/Data-Structures)
- Data Storage
  * [MinIO](MinIO/MinIO-Storage-Description)
  * [S3 Locations](MinIO/S3-Location)
  * [S3Client](MinIO/S3Client)
- Dashboard
  - [REST API](SEDAR/NEW/Rest-Api)
  - [Web App](SEDAR/NEW/WebApp)

# Useful Links

- Extra Doc & References
  - [Administrative Items](Administrative-Cloud-Items)
  - [Migrating Elastic Search Data](Migrating-Elastic-Search-Data)
  - [Artifactory Upload Image](Artifactory-Upload-Image)
  - [Branches Workflow](Branches-Workflow)
  - [Conda Cheat Sheet](Conda-Cheat-Sheet)
  - [Pydantic vs Dict](Pydantic.BaseModel-vs-Dictionaries-for-Data-Schemas)
  - [Transfering Data](Transferring-Data-between-SDLC-stages)