## Create environment from an environment.yml file

```
$conda env create -f environment.yml 
```

## Activate environment 

```
$conda activate conda-sedar
```

## Deactivate environment 

```
$conda deactivate
```

## Export environment.yml file

```
$conda env export --no-builds | grep -v "^prefix: " > environment.yml
```