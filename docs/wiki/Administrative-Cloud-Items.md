#### Relevant Documents

- [SOS](https://gcdocs.gc.ca/statcan/llisapi.dll/app/nodes/22003224)
- [CONOPS](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=16494313)

#### Recollection of Items and Jira tickets for Future Reference. 

1- Authority to Operate and RMI Issue: 
   - [RMI Outline](https://confluenceb.statcan.ca/display/DScD/RMI+-+Risk+Management+Information)
   - [Jira 1220](https://jirab.statcan.ca/browse/CSAA-1220)

2- Project Request to Onboard to Cloud Main Environment
   - [Supported Workloads Cloud Native Platform](https://cloudnative.pages.cloud.statcan.ca/en/getting-started-debuter/supported-workloads/) 
      - [Onboarding Guide](https://cloudnative.pages.cloud.statcan.ca/en/getting-started-debuter/guides/onboarding/)
   - [Jira 14877](https://jirab.statcan.ca/browse/CLOUD-14877)
      - [Cloud Service Form](https://jirab.statcan.ca/secure/attachment/253434/Cloud%20Services%20Intake%20Form-SEDAR.docx)
      - [Finance Intake Form](https://jirab.statcan.ca/secure/attachment/253776/Cloud%20Finance%20Intake%20Form_v2.xlsx)
   - [Jira 15318](https://jirab.statcan.ca/browse/CLOUD-15318)
   - [Jira 15313](https://jirab.statcan.ca/browse/CLOUD-15313)
   - [Jira 15304](https://jirab.statcan.ca/browse/CLOUD-15304)

    [Octopus](https://octopus.cloud.statcan.ca/)
   
3- Architecture Review 
   - [Jira 15151](https://jirab.statcan.ca/browse/CLOUD-15151)
   
4- Resource Creation Support 

   - [Jira 15266](https://jirab.statcan.ca/browse/CLOUD-15266) 
   
5- Request for VMs Workstations
   
   - [Jira 15317](https://jirab.statcan.ca/browse/CLOUD-15317)
   - [Jira 15363](https://jirab.statcan.ca/browse/CLOUD-15363)

6- Artifactory Access & Credentials

   - [Jira 15388](https://jirab.statcan.ca/browse/CLOUD-15388) 

7- OAuth2 & App Registration
    
   Create Service Principal so we can register our Enterprise App.
   - [Jira 15389](https://jirab.statcan.ca/browse/CLOUD-15389)
   - [Jira 15562](https://jirab.statcan.ca/browse/CLOUD-15562)
   - [Jira 15611](https://jirab.statcan.ca/browse/CLOUD-15611)
   
8- MinIO Data Transfer from AAW to Main Cloud

   - [Jira 15390](https://jirab.statcan.ca/browse/CLOUD-15390)
   - [Jira 1705](https://jirab.statcan.ca/browse/CODAS-1705)

9- External SFTP Access To Data Provider
   - [Confluence](https://confluenceb.statcan.ca/display/ITODCP/Hybrid+Firewall+Connectivity+Workflow)
   - [Jira 15391](https://jirab.statcan.ca/browse/CLOUD-15391)
   - [Jira 786](https://jirab.statcan.ca/browse/ITCMO-786)
   - [ Jira 15923](https://jirab.statcan.ca/browse/CLOUD-15923)
   
      Documents Necessary for this [task](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objId=26380579&objAction=browse&viewType=1):
       - Workload Intake Form
       - CHM_RFC Form


10 - Application Vulnerability Assessment (AVA)
   - [Jira 261](https://jirab.statcan.ca/browse/CSDAVA-261)


11 - After ATO 
   - [Jira 16820](https://jirab.statcan.ca/browse/CLOUD-16820)


12 - CIO Approval (Production Env)
    - [Jira 17524](https://jirab.statcan.ca/browse/CLOUD-17524)
13 - OAuth2.0 for Production Env 
     - [Jira 17612](https://jirab.statcan.ca/browse/CLOUD-17612)

14 - SEDAR+ SFTP Testing Configurations.
Following steps from: [Hybrid Firewall Connectivity](https://confluenceb.statcan.ca/display/ITODCP/Hybrid+Firewall+Connectivity+Workflow)

- [CHM_RFC](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=31468560) 
- [WIF](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=31466717)


 Note: SRM #: 02969773

- [IT Updated WIF + Diagrams](https://gcdocs.gc.ca/statcan/llisapi.dll/link/31791957)
Jira Ticket by IT Team created after SRM scalation
- [ITCMO-1416](https://jirab.statcan.ca/browse/ITCMO-1416)
      
15 - Procuring new VM for ESD Team Member

![Create SR Before Jira](uploads/47a5c718a891f5829a5e79226ba69013/MicrosoftTeams-image__3_.png)

- [Cloud 18774](https://can01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fjirab.statcan.ca%2Fbrowse%2FCLOUD-18774&data=05%7C01%7CSimardeep.Singh%40statcan.gc.ca%7Ccbbc1bdd15c141f921f208db48265f7c%7C258f1f99ee3d42c7bfc57af1b2343e02%7C0%7C0%7C638183101873043532%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C3000%7C%7C%7C&sdata=TvP5M1Ujeo8lIgGDeKx1jXxuHar6BC5JxCwM3N1JAx0%3D&reserved=0)


16 - Increase Resource Quota for Production Environment 
- [Cloud 18835](https://jirab.statcan.ca/browse/CLOUD-18835)


