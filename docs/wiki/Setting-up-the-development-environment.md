## Overview 

For a more comprehensive document please refer to [Microsoft Official Documentation](https://docs.microsoft.com/en-us/windows/wsl/install).

For a third party tutorial look [here](https://www.c-sharpcorner.com/article/how-to-install-windows-subsystem-for-linux-wsl2-on-windows-11/). 

## Quick Start 

1. Open Windows Command Prompt with "Run As Administrator" option. 
2. Execute ```wsl --install```
   This command by default will install the Ubuntu distribution. This is our intended distribution, for other
   options please refer to the tutorial link above. 
3. Restart Computer to continue the WSL installation
   *Restart computer from the Start Menu, not the Azure Portal. Wait for about 5 minutes and reconnect using the RDP Client. 
    After restarting a Command Prompt will automatically open and start installing the Ubuntu distribution.
4. Unix username and Password 
   The Installation Command Prompt will ask for your UNIX username and password. These credentials will be used for getting access to our Ubuntu virtual machine.  
5. In the Unix prompt execute:
   - ```sudo apt-get update```
   - ```sudo apt-get dist-upgrade -y```
   - ```sudo apt-get install build-essential```
   - ```sudo apt-get install net-tools```

## Development tools 

For a comprehensive document refer [here](https://code.visualstudio.com/blogs/2019/09/03/wsl2)

1. Download & Install Visual Studio Code in the Windows Host Machine
   Official link to [VS Code](https://code.visualstudio.com/download)
   Follow the installation Setup process and answer with all the default values. 
2. Install Remote WSL extension in the Windows Host Machine. 
   Official link to [Remote WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
   Click install and follow the installation process. 
3. From the Windows Host Machine execute ```wsl code```. 
   The bottom left corner of the VS Code instance should say connected to WSL:Ubuntu. Now you are working in the Ubuntu environment. 

### VSCode Extensions 
From the VS Code Interface connected to the WSL Ubuntu server, select the Extensions tab and:

1. Install the following VS Code Extensions: 
   - YAML Language Support by Red Hat, with built-in Kubernetes 
   - Python by IntelliSense(Pylance), Linting, Debugging 

### Miniconda
From a WSL command line. 

1. ```wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh```
2. ```chmod +x Miniconda3-latest-Linux-x86_64.sh```
3. ```./Miniconda3-latest-Linux-x86_64.sh```
    Follow the installation process, select the following options: 
    - Accept License Agreement. Answer: yes
    - Install in default location. Answer: yes. 
    - Do you wish the installer to initialize Miniconda by running conda init? Answer: yes. 
4. Close and re-open another WSL terminal to initialize conda.  




