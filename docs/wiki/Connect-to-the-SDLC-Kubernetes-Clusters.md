## Overview

Connecting you WSL environment to the SDLC Kubernetes Clusters. These instructions are written assuming you are working in a WSL environment. If you are working from Windows directly, you will need to adjust accordingly

## Installing `kubectl` in WSL

Follow the [Kubernetes instructions](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) to install the `kubectl` cli tool.

## Download cluster configuration files

The cloud native team maintains cluster configuration files that you can download:

- **dev** [cluster](https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-dev-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false) `https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-dev-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false`



- **test** [cluster](https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-test-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false) `https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-test-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false`


- **prod** [cluster](https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-production-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false) `https://gitlab.k8s.cloud.statcan.ca/cloudnative/aks-engine/k8s-cancentral-01-production-shared/-/blob/master/kubeconfig.canadacentral.json?inline=false`




Download the appropriate file(s) to your local machine. Copy them to your WSL machine and add the downloaded files paths to the `KUBECONFIG` variable.

```bash
export KUBECONFIG=$HOME/dev.kubeconfig:$HOME/test.kubeconfig:$HOME/prod.kubeconfig
```

Once you have the `KUBECONFIG` environment variable pointing to each of the configuration files above, they can be merged into a single file using `kubectl` view:

```bash 
kubectl config view > $HOME/.kube/main_config
```

The above approach masks the cluster certificates required to properly connect. You can fix this by manually copying the values to the files where your `$HOME/.kube/main_config` contains `DATA+OMITTED`.

## Load configuration at login

While the connections are working for this session, as soon as you log out your configuration will be lost. To make it persist and be set at each login, add it to your `.bashrc`.

1. Open the `.bashrc` file using your preferred editor.
2. Add `export KUBECONFIG=$HOME/.kube/main_config` to the last line.
3. Save the file.


## More information

For more information, including learning resources about kubernetes, refer to the [cloud native documentation](https://cloudnative.pages.cloud.statcan.ca/en/getting-started-debuter/).


## Install K9S 

Inside a WSL terminal execute the following command line. Provide your account password when prompted for it. 

```bash
curl -s -L https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Linux_x86_64.tar.gz -o k9s && tar -xvf k9s && chmod 755 k9s && rm LICENSE README.md  && sudo mv k9s /usr/local/bin
```
