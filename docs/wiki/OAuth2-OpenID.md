# Overview
Kubernetes OAuth2-proxy deployment to add auth to any app. 
OAuth2 Proxy is a reverse proxy that sits in front of your application and handles the complexities of OpenID Connect / OAuth 2.0 for you; requests that make it to your application have already been authorized!

SEDAR implements the authentication as a standalone deployment. A single replica oauth2-proxy deployment, service, and ingress rule with upstream pointing to the SEDAR web app service. 

```
Ingress -> OauthSVC -> OAuthProxy Deployment -> Web App Service -> Web App Deployment. 
```

For a more comprehensive list of parameter and configuration refer to the official oauth2-proxy documentation: 

- [Command Line Options](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#command-line-options) 

- [Environment Variables](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#environment-variables)

- [Config File](https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview#config-file)

- [Endpoints](https://oauth2-proxy.github.io/oauth2-proxy/docs/features/endpoints)

# Quick Start

## Registering your app in Azure Active Directory 

Tutorial to register your app and restrict its access to a set of users or groups [here](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-restrict-your-app-to-a-set-of-users)

1. Register you App in Azure Active Directory.
2. Set your Redirect URI Authentication  
   Azure Active Directory >> App Registrations >> Your App >> Authentication >> Add a Platform >> Web >> Redirect URIs. 
   - e.g.: **```https://<fqdn>/oauth2/callback```** (See Endpoints for reference)
   - e.g.: **```http://localhost:<port>/oauth2/callback```**


Azure Active Directory >> App Registrations >> Your App >> Authentication >> Add a Platform >> Web >> Front-channel logout URL.
   - e.g.: **```https://<fqdn>/oauth2/sign_out```** (See Endpoints for reference)
   - e.g.: **```http://localhost:<port>/oauth2/sign_out```**
    
3. Gather OIDC client information values such as:
    - ```client-id``` : 

        Azure Active Directory >> App Registration >> Your App >> Application ID
    - ```client-secret```: 

       Azure Active Directory >> App Registration >> Your App >> Certificates & Secrets >> New Client Secret >> Copy Created Value
    - ```redirect-url```: 
    
        same url as Step 2 (e.g., ```https://<fqdn>/oauth/callback```)
    - ```tenant-id```: 

        Azure Active Directory >> App Registration >> Your App >> Directory tenant ID
4. Generate Cookie Secret: 
    - ```cookie-secret```: 
        
    ```python -c 'import os,base64; print(base64.b64encode(os.urandom(16)))'```


## Secrets 

Set OIDC client information in k8s file ```oauth2-proxy-secrets.yaml```

Note: all data is being entered in basic text form, not base64. For more "security" use the field ```data``` instead of ```stringData```.
```yaml
apiVersion: v1
kind: Secret 
metadata:
  name: oauth2-proxy
stringData:
  OAUTH2_PROXY_CLIENT_ID: <client-id>
  OAUTH2_PROXY_CLIENT_SECRET: <client-secret>
  OAUTH2_PROXY_REDIRECT_URL: <redirect-url>
  OAUTH2_PROXY_OIDC_ISSUER_URL: https://login.microsoftonline.com/<tenant-id>/v2.0
  OAUTH2_PROXY_COOKIE_SECRET: <cookie-secret>
```
### Ingress Rule

Set the correct FQDN (e.g., sedar-webapp.dev.cloud.statcan.ca) for the k8s Ingress rule to point to the Oauth service running on port 80 with target port 4180 of the Oauth proxy deployment. Configure Oauth2-proxy upstream to be the web app deployment service. 


### OAuth Configuration 

Command Line oauth2-proxy configuration
``` bash
          - --provider=oidc 
          - --email-domain=* 
          - --oidc-email-claim=oid
          - --upstream=http://sedar.sedar.svc.cluster.local:80
          - --custom-sign-in-logo=<url_to_logo>
          - --http-address=0.0.0.0:4180
          - --provider-display-name=OpenID
          - --pass-access-token=true
          - --set-xauthrequest=true
          - --cookie-samesite=lax
          - --reverse-proxy=true
          - --cookie-secure=false
          - --pass-host-header=false
          # - --session-store-type=redis  #See Redis Section (Optional)
          # - --redis-connection-url=redis://redis.sedar.svc.cluster.local:6379 #See Redis Section (Optional)
```

The **--oidc-email-claim=oid** argument is necessary due to the Statcan JWT payload structure. This argument sets the key value of the JWT token payload to retrieve the user's email. It's not mandatory for it to point to an email field (i.e, currently pointing to the key value "oid"). It's mandatory for this argument to point to an existing key value in the payload dictionary. The claim must exist in the payload. 

Due to the Istio controller in our kubernetes cluster and the standalone architecture selected. The flag **--pass-host-header=false** is necessary to avoid an "upstream/ connection termination" error due to the usage of two different DNS (i.e, sedar-webapp.dev.cloud.statcan.ca and upstream  sedar.sedar.svc.cluster.local). 

Although not completely necessary, it's possible to integrate Redis (i.e, an in-memory data structure store, used as a distributed, in-memory key–value database, cache and message broker, with optional durability.). To enable this capability we use the two argument flags **--session-store-type=redis** and ** --redis-connection-url=redis://redis.sedar.svc.cluster.local:6379**. See redis session for details regarding deployment. 





