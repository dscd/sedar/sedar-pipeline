# Overview

Configuring Azure Storage Explorer to easily transfer data between different SDLC stages (i.e., dev to prod and vice versa) 

# Step 1. Log in [portal.azure.com](https://portal.azure.com)

# Step 2. Install Azure Storage Explorer 
```
Home >>> <storage account> >>> Open In explorer >>> Download Azure Storage Explorer 
```

# Step 3. Link Storage Accounts

Development: stnbs0sedar00sa
Production:  stpbs0sedar00sa

After Azure Storage Explorer Installation, Click on the left bar icon with "Open Connect Dialog". 

```
Azure Storage Explorer >>> Open Connect Dialog >>> Select Resource "Storage account or Service" >>> Account name and key >>> Next >>> Enter Display Name, Account Name (e.g., stnbs0sedar00sa), and Account Key >>> Next
```

Account Key can be extracted from the portal.azure.com 
```
Home >>> Storage Accounts >>> <your store account >>> Access Key >> Key1 (Show) 
```

Repeat procedure for both account (i.e., dev and prod). 

# Step 4 Navigate, Copy, Sync 

You can use the explorer to move, add, remove data to both account. Make sure to access 

```
Linked Account >>> Blob Containers >>> sedar-bucket 
``` 