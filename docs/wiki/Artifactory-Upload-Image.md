#### Overview

It's sometime necessary to include images to the artifactory.cloud.statcan.ca instance relevant to your deployment. 
If the images are not available yet or it's not automatically downloading them. You can follow up these steps to include the necessary image to your artifactory group. 

#### Uploading Image to Artifactory

With docker installed in WSL cloud main. Make sure to pull your image to your local machine (i.e., <fqin> full qualified image name):

``` docker pull <fqin>```

The, re tag the image to use the correct repository:
``` docker tag <fqin> artifactory.cloud.statcan.ca/docker/<project_name>/<in>:<tag>```

Using your artifactory service account name and apiKey, log in into artifactory.cloud.statcan.ca 

```docker login -u <username> https://artifactory.cloud.statcan.ca```

It will prompt for a password, paste the apiKey for adding access. 

Finally, push the tagged image to the repository. 

```docker push artifactory.cloud.statcan.ca/docker/<project-name>/<in>:<tag>```