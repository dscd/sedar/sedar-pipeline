## Templates

Being a web framework, Django needs a convenient way to generate HTML dynamically. The most common approach relies on templates. A template contains the static parts of the desired HTML output as well as some special syntax describing how dynamic content will be inserted. 


- base.html -> .html file initializes the underlying scripts base UI for the application
- index.html -> .html file manages the navigation tab and initializes the scripts for company details, location and statements.
- company_details.html -> .html file rendered for Company Details Page
- company_location.html -> .html file to display Company Location Page
- company-statements.html -> .html file to display Company Statements Page
