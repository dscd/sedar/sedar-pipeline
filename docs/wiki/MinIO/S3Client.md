
Documentation around the S3 MinIO client used in the SEDAR project.

### S3Location

There are a few relevant S3 locations to our application. 
Each folder contains relevant data for different stages of our pipeline. 
```
S3Location  = namedtuple('S3Location', ['bucket', 'prefix', 'basename'], defaults=['','',''])
```

An S3Location is a named tuple with three relevant members: bucket, prefix, basename. 
We use an S3Location object to identify either a folder or object location. 

```
from config.api import S3Location

folder = S3Location(bucket='aaw-sedar', prefix='inbox/')
objec  = S3Location(bucket='aww-sedar', prefix='inbox/', basename='a.zip')
```


Inside the config.api module we have a few global variables defining some of our more relevant S3 locations. Although it's possible to specify multiple buckets, we are currently using only one (i.e., aaw-fc). 

Locations:
```
S3.HISTORICAL_DATA        = S3Location('aaw-sedar, 'historical_data/', '')
S3.DATA                   = S3Location('aaw-sedar', 'data/', '')
S3.DATABASE               = S3Location('aaw-sedar', 'db/', '')
S3.MODELS                 = S3Location('aaw-sedar', 'models/', '')
S3.TMP                    = S3Location('aaw-sedar', 'tmp/', '')
```

1. S3.HISTORICAL_DATA | Place where all the raw data is uploaded to. 
2. S3.DATA | Uncompressed files that are used in our live data app.
3. S3.DATABASE | Sqlite Database files 
4. S3.MODELS | Base folder for models and their configuration
5. S3.TMP | Temporary working spaces created while processing our data pipelines. 


The data inside of these locations are stored by following a certain pattern or guideline for easy query and organization. Please refer to [Storage Proposal](MinIO/Storage-Proposal) to review guidelines and patterns 



### S3Client

Our S3Client is an extension built around the Minio Client API 
(https://docs.min.io/docs/python-client-api-reference.html)
(https://github.com/minio/minio-py)

 Our implementation is in our git repository under src/modules/s3client.py

```python
from modules.s3client  import S3Client
""" 
Our static method get_client() searches for the following environment variables
   MINIO_URL, 
   MINIO_ACCESS_KEY, and 
   MINIO_SECRET_KEY. 
These can be obtained by looking at /vault/secrets/minio-standard-tenant1.json file in the AAW space

"""
client = S3Client.get_client()
```

The S3Client is a child class from minio.Minio and extends its functionality by implementing some functionalities. Mainly extending the usage of S3Locations objects. 


For a complete list of methods available from the parent class minio.Minio looks at:
(https://docs.min.io/docs/python-client-api-reference.html#select_object_content)

### Usage & Examples



#### mkdir 
Creates an empty "folder". 
Creates an empty file  "self._default_folder_file" in the specified folder path. 
``` python
def mkdir(self, bucket: str, folder:str) -> None:
```

Usage:
```python
from modules.s3client  import S3Client
from config.api import S3Location, ARCHIVE

client = S3Client.get_client()
client.mkdir(bucket='aaw-sedar', folder='folder/path/')

#this will add the obj folder/path/.empty to the bucket aaw-sedar 
```


#### list_object_ex 
Same API call than minio.list_objects with extra functionalities. It will not list the .empty files inside folders if default_folder_file is set to False
``` python
def ls_objs(self, bucket: str, prefix: str, recursive:bool  = False, 
                        only_files:bool = False, default_folder_file:bool=False):
```

Usage:
```python
from modules.s3client  import S3Client

client = S3Client.get_client()

for obj in client.ls_objs(bucket='aaw-sedar', prefix='folder/path/', recursive=True,    default_folder_file=False):
    print(obj.object_name)


```

#### rm 
Removes all the objects under the bucket and with the `folder` prefix. 
``` python
def rm(self, bucket:str, folder:str) -> None: 
```

Usage:
```python
from modules.s3client  import S3Client

client = S3Client.get_client()

client.rm(bucket='aaw-sedar', folder='inbox/')
```



#### mv_obj 
Moves an object from a bucket/path to another bucket path on the server-side. If no dst_bucket is defined, the src_bucket is used. The source object will be deleted. 
``` python
def mv_obj(self,src_bucket: str, src_obj   : str,dst_obj   : str, dst_bucket: str = None):
```

Usage:
```python
from modules.s3client  import S3Client

client = S3Client.get_client()

client.mv_obj(bucket='aaw-sedar', src_obj='inbox/a.dat', dst_bucket='error/a.dat')
```


#### cp_obj
Copies a object from a bucket/path to another bucket path in the server side. If 
no dst_bucket is defined, the src_bucket is used. 
``` python
def cp_obj(self,src_bucket: str, src_obj   : str,dst_obj   : str, dst_bucket: str = None):
```

Usage:
```python
from modules.s3client  import S3Client

client = S3Client.get_client()

client.cp_obj(bucket='aaw-fc', src_obj='inbox/a.dat', dst_bucket='error/a.dat')
```

