# Overview

To reduce the effects of the Data swamp we are proposing here a more concise and organization schema that improves the way we store SEDAR data. In here we define a schema of governance of data, allowing cleaning and sorting data more efficiently.

# SEDAR Database

The SEDAR database is a time-based database that contains monthly and daily tar.gz files containing financial information in pdf, text, and XML formats. We should leverage the fact that this database corresponds to a time-based series by organizing data based on date. As a general case, if the daily files have no name collisions, it's a good practice to divide data by year and month as "folders". e.g.,

```plaintext
2021-05-10.pdf 
*prefix/2021/05/<ISSUERNO/GROUPNO>/<UUID>.pdf 
```

# MinIO data organization

There are relevant top level MinIO locations to the application such as , , and . Each one fulfills a different role. All data is stored under the same S3 bucket and namespace using a system account. The users should never interact with these S3 locations directly.

```plaintext
/data   #Live data, SEDAR pdfs containing only the latest 3 years of data
/error  #Error Handling folder 
/historical #Historical Data containing tar.gz files
/inbox  #New data will be placed for the SFT pulls
/models         #Initialization data and lookup
```

# Name conventions

All folder paths are in lowercase, snake case. The variable `<datasource> = sedar`.

### 1. Historical Data

It will store tar.gz files grouped by year/month (one level higher than a day)

```plaintext
historical/<datasource>/<year>/<month>/archive_yyyy-mm(-dd)?.tar.gz
```

```plaintext
<year> format is YYYY, ie., 2020
<month> format is MM, i.e., 01, 02, 03, 04,..., 12
```

### 2. Live Data

We only store data under the live data to access SEDAR PDFs files for the last three years.

```plaintext
data/<datasource>/<year>/<month>/<ISSUERNO/GROUPNO>/ID.pdf 
```

```plaintext
<datasource> = sedar
<ISSUERNO/GROUPNO> = \d+
<year> format is YYYY
<month> format is MM 
```

### 3. Models

All Models relevant to our application will follow the same template

```plaintext
models/<model_name>/*

Example:
models/lookup_tables/
models/page_detection/
```

#### 4. Error Folder

The error folder will have the following format:

```plaintext
   error/<tar-file-name>/<filename>.xml
   error/<tar-file-name>/<filename>.doc
   error/<tar-file-name>/<filename>.txt
   error/<tar-file-name>/<filename>.message (this file is new, containing error message)

e.g./ 
    error/archive-2020-02-01/00000-00000-0000-Financialxxx.pdf 
```

Each error submission will contain several files with different extensions regarding the current submission plus a .message file with the error message from the exception captured.