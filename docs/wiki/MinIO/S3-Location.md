
We use a proxy class to generate S3 Locations systematically. Specifically, certain dimensions uniquely identify a file:

- Bucket 
- Prefix
- Basename

Between some (or all) of these dimensions, we can construct the correct S3 Location where a particular file should be located. The use of our proxy class is explained in more detail below.

# S3Location

We use a named tuple to represent S3Locations.

```python
S3Location  = namedtuple('S3Location', ['bucket', 'prefix', 'basename'], defaults=['','',''])
```

An S3Location is a named tuple with three relevant members: `bucket`, `prefix`, `basename`. 

We use an `S3Location` object to identify either a folder (prefix) or object location. 

**Examples**

```python
from config.API import S3Location

folder = S3Location(bucket='aaw-sedar', prefix='historical_data/')
objec  = S3Location(bucket='aww-sedar', prefix='historical_data/2021/10/', basename='archive-2021-10-05.tar.gz')
```





## Data Sources

Each piece of data that is used in the Sedar system is considered to be a data source. We distinguish between **time-based** data sources, which are received regularly, and **file-based** data sources, which do not change regularly (although their contents can change over time). The following table highlights some key differences between these two kinds of data sources.

| **time-based** | **file-based** |
| -------------- | -------------- |
| New data are received periodically (e.g. archive-2021-09.tar.gz) | New data or configurations are received on an ad hoc basis (e.g. SQLite.DB)  |
| We explicitly track these files over time (e.g. every month gets its dedicated S3 location) | We do not track these files over time; when a new file for an existing data source is received, we **overwrite** the contents of the previous one. For example, if there is an update to the `ocr_model.pkg, we overwrite the existing file with the contents of the new one. |
| Time-based files form the contents of the SEDAR database. | File-based files are auxiliary to the contents of the time-based data sources tables (i.e. they are used in the data processing steps, but they do not contain any records that could be separated by year-month-day).
| Time-based S3Locations are uniquely identified by the combination of `bucket`, `prefix`, and `basename` encoding the date as part of the prefix. | File-based S3Locations are uniquely identified by the combination of `bucket`, `prefix`, and `basename` (i.e. there are no date digits encoded inside the prefix nor base name).|
| Time-based Data sources contain the date as part of their path (i.e. year and month). The reason behind this is for easy time queries.  | File-based Data sources don’t have time series data and are represented by their filename. For example: `/<bucket>/<prefix>/<basename>`. |



# S3 Location Proxy Class API

To simplify the process of creating S3 paths, we should leverage the use of a proxy class. 
This is simply a class that we will always use in our code to generate the path to a particular S3 resource. This minimizes the usage of string concatenation that could be prone to errors and more difficult to update in case of any change. 

All parts of our code should only refer to the proxy class API to generate any MinIO S3 path. 
It's highly suggested to NOT generate a path ourselves by concatenating other strings. 


The *S3* class is found under module **config.api** provides all the necessary variables and static methods to avoid the manual creation of s3 paths while creating a proxy point for its creation. 


For example, the following S3 class attributes define the top-level S3 locations.
```
S3.HISTORICAL_DATA
S3.DATA
S3.MODELS 
...
```

Obtaining the historical data path for a file with data: 2020-05-10 could be obtained by using the following static method 

example:
```
from config.api import S3, DataSource

s3loc = S3.get_historical_data_loc(DataSource.sedar, year=2020, month=5, basename="archive_2020-05-10.tar.gz)
```
