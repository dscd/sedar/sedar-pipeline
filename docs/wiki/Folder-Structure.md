# Folder Structure 

SEDAR App project folder structure is currently following our datascience-cookiecutter template. The SEDAR repository holds two main components: SEDAR App ETL pipeline and the SEDAR web dashboard (i.e., under ```src/webapp```).

```
├── CONTRIBUTING.md             [Documentation for Contributing ]
├── LICENSE                     [License details] - TBD 
├── README.md                   [Quick Usage Documentation]
├── SECURITY.md                 [Security Details]
├── pipeline.environment.yml    [Conda environment for SEDAR App Pipeline]
├── webapp.environment.yml      [Conda environment for SEDAR App WebApp]
├── docs                        [Sphinx Documentation]
├── notebooks                   [Scripts and notebook files]
├── tests                       [Unit Tests]
├── data                        [Data]
└── src                         [Source Code]
    ├── sedar_pipeline.py       [CLI entry point]  
    ├── run_webapp.sh           [entry point for webapp]  
    ├── manage.py               [entry point for Django]  
    ├── modules                 [python modules]
    │   ├── __init__.py
    │   ├── control_pipeline.py	
    │ 	├── extract.py
    │	├── file_extractor.py
    │	├── init_models.py
    │	├── page_detection.py
    │	├── page_extraction.py
    │	├── pandas_read_xml.py
    │	├── s3client.py
    ├── config               [SEDAR pipeline config files]
    │    ├── api.py   
    │    ├── env.py
    │	 ├── locations.json
    │	 ├── model_serializers.py
    │	 ├── models.py
    │	 ├── sedar.json
    │	 ├── sedar.json
    │	 ├── web_models.py
    │	 ├── settings.yaml
    ├── webapp  [root webapp]
    │ 	├── media
    │ 	├── rest_api
    │ 	├── dashboard
    │   │   ├── __init__.py
    │   │   ├── admin.py
    │   │   ├── apps.py
    │   │   ├── models.py
    │   │   ├──	tests.py
    │   │   ├── urls.py
    │   │   ├── views.py
    │   ├── static 
    |   |   ├── admin
    |   |   ├── css
    |   |   ├── rest_framework
    |   |   ├── scripts
    |   │   │   ├── base.js
    |   │   │   ├── companyDetail.js
    |   │   │   ├── companyLocation.js
    |   │   │   ├── companyStatement.js
    |   │   │   ├── dropdown.js
    |   │   │   ├── editableTable.js
    |   │   │   ├── home.js
    |   │   │   ├── homeAPICall.js
    |   │   │   ├── language.js
    |   │   │   ├── pace.min.js
    |   │   │   ├── pdfobject.js
    |   │   │   ├── sb-admin-2.js
    |   │   │   ├── table2csv.js
    |   │   │   ├── toastme.js
    |   │   │   ├── toastme.min.js
    |   |   ├── vendor
    |   │   │   ├── awesome-markers
    |   │   │   ├── bootstrap-4.6.0
    |   │   │   ├── bootstrap.wcag2.1.AAA
    |   │   │   ├── fontawesome-free-5.12.1
    |   │   │   ├── jquery-3.4.1
    |   │   │   ├── popper-1.12.9
    │   ├── templates                
    │   │   ├── base.html
    │   │   ├── home.html
    │   │   ├──	index.html
    │   │   ├── components
    │   │       ├── company_details.html
    │   │       ├── company_location.html
    │   │       ├── company_statements.html	
    │   └── webapp           
    │       ├── __init__.py
    │       ├── asgi.py
    │	    ├── settings.py
    │	    ├── urls.py
    │	    ├── wsgi.py		
```



