### WSL Docker install 

A full comprehensive tutorial can be found [here](https://docs.docker.com/engine/install/ubuntu/)

In a WSL Ubuntu terminal:

#### Set up repository

1- Update the apt package index and install packages to allow apt to use a repository over HTTPS:
``` $ sudo apt-get update ```

2- Add Docker’s official GPG key:
``` 
    $ sudo apt-get  install \
                    ca-certificates \
                    curl \
                    gnupg \
                    lsb-release 
```

3- Use the following command to set up the repository:
```
 $ sudo mkdir -p /etc/apt/keyrings
 $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
``` 

4- 
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

#### Install Docker Engine

1- Update the apt package index, and install the latest version of Docker Engine, containerd, and Docker Compose, or go to the next step to install a specific version:
```
 $ sudo apt-get update
 $ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
``` 

2 - Make sure the engine is running
```
sudo /etc/init.d/docker start
```

3 - Verify that Docker Engine is installed correctly by running the hello-world image.
```
$ sudo docker run hello-world
```

4 - Run docker as non-root user
```
$ sudo usermod -aG docker $USER
$ newgrp docker
$ docker run hello-world
```

#### Install Task 

1- Install Task and update PATH environment variable
```
$ sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b ~/.local/bin
$ export PATH=$PATH:$HOME/.local/bin
```

#### Install Helm 

Full comprehensive [doc](https://helm.sh/docs/intro/install/)

1 - Install 
```
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

#### Install ElasticDump

**1) Install npm** -> Follow this [link ](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl)to install Node.js on Windows Subsystem for Linux (WSL2)

**2) Install Elasticdump** -> Since we're going to be using Elasticdump locally, we should first install it on our local computers. In this case, we can install it either per project or globally. We may use the following command to make the change globally:

`npm install elasticdump -g`