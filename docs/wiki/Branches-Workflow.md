## Branches

* **master** branch reflects our production-ready code or our latest stable published version. The goal of the branch is that it's always release-ready, meaning that we can deploy from it at any time.
* **dev** branch reflects the development branch, it's usually the most up to date branch with all the new developments and features. Users don't develop directly on the **dev** branch. The **dev** branch is used as a starting point for developing a new task or feature. We branch out of it another branch following the name template **@username/new_branch_name**. Once finished a Merge request to **dev** is done. Finally, when a release is needed **dev** is merged back to **master**

The dev branch is created only once and it's kept alive along the whole project.

```
$git checkout -b dev master
$git commit -m "Development Branch" 
$git push --set-upstream origin dev
```

* **@username/new_branch_name** this is a generic naming template to create new branches out of the **dev** branch. Its name identifies the user and the task being developed.

## Working on your own branch out of dev

Tag your branches off dev with your @username/new_feature_name Let's see an example using the username: @asolism

```
$git checkout -b @asolism/new_feature dev 
$git commit -m "Added a new feature" 
$git push --set-upstream origin @asolism/new_feature
```

## Requesting a Merge Request from your own developing branch

![Screen_Shot_2021-09-26_at_8.39.29_PM](uploads/1e725d9c98c3f62f91522873e476deb1/Screen_Shot_2021-09-26_at_8.39.29_PM.png)

![Screen_Shot_2021-09-26_at_8.39.36_PM](uploads/01dd73d6c048d35e8e2164b34c4093c1/Screen_Shot_2021-09-26_at_8.39.36_PM.png)

![Screen_Shot_2021-09-26_at_8.39.44_PM](uploads/f93abd0379d456c6898e05632e4cf37d/Screen_Shot_2021-09-26_at_8.39.44_PM.png)

## Reviewing a Merge Request from your own developing branch

![Screen_Shot_2021-09-26_at_8.39.51_PM](uploads/d87f704b8029d77e7224db2009155d71/Screen_Shot_2021-09-26_at_8.39.51_PM.png)