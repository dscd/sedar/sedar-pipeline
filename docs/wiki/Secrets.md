### Secrets

All deployment credentials are managed through Kubernetes secrets. Although we don't directly modify these files, it's a good reference to understand what are these credentials and how to obtain them. 

These values are configured through the ```values.yaml``` file from the Helm Chart. Accessible through Octupus. 

```yaml 
...
secrets:
  slack: 
    secret_name: slackbot-token
    token: <slack-app-token>
  sftp: 
    secret_name: sftp-connection
    user: <sftp-user>
    path: <sftp-path>
    host: <sftp-ip>
    id_ed25519: | 
      -----BEGIN OPENSSH PRIVATE KEY-----
      <private-key>
      -----END OPENSSH PRIVATE KEY-----
  oauth:  #see OAuth and App registration for details
    client_id: <client-id>
    client_secret: <client_secret>
    redirect_url: <redirect_url>
    issuer_url: https://login.microsoftonline.com/<tenant_id>/v2.0
    cookie_secret: <random_generated_string>
  webapp:
    secret_name: webapp 
    key: <random_generated_string>
  s3proxy:
    secret_name: s3proxy
    access_key: <s3_access_key>
    secret_key: <s3_secret_key>
    azure_account_name: stnbs0sedar00sa
    azure_key: <access key from Azure Storage Account above>
...
```




#### OAuth2 Proxy

All OIDC client information is stored under secret named `oauth2-proxy`

```
apiVersion: v1
kind: Secret 
metadata:
  name: oauth2-proxy-secrets
stringData:
  OAUTH2_PROXY_CLIENT_ID: <client-id>
  OAUTH2_PROXY_CLIENT_SECRET: <client-secret>
  OAUTH2_PROXY_REDIRECT_URL: <redirect-url>
  OAUTH2_PROXY_OIDC_ISSUER_URL: https://login.microsoftonline.com/<tenant-id>/v2.0
  OAUTH2_PROXY_COOKIE_SECRET: <cookie-secret>
```

#### SFTP

SFTP Credentials and connection information is stored in secret `sftp-connection`.

```
apiVersion: v1
kind: Secret 
metadata:
  name: sftp-connection
data:
  id_ed25519: | 
   -----BEGIN OPENSSH PRIVATE KEY-----
   .........
   -----END OPENSSH PRIVATE KEY-----
  user: <sftp-user>
  path: <sftp-path>
  host: <sftp-host>
```

#### WebApp

```
apiVersion: v1
kind: Secret
metadata:
  name: webapp
type: Opaque
data:
  key: <random_generated_string>
```

#### S3Proxy

```
apiVersion: v1
kind: Secret
metadata:
  name: s3proxy
type: Opaque
data:
  access_key: <s3_access_key>
  secret_key: <s3_secret_key>
  azure_account_name: stnbs0sedar00sa
  azure_key: <stnbs0sedar00sa access key from azure portal>
```


#### Slack Notifications


``` 
apiVersion: v1
kind: Secret
metadata:
  name: slackbot-token
type: Opaque
data:
  token: <slack bot token from https://api.slack.com/apps/>
```
