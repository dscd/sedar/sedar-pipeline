**Migrating the Elastic Search Data - Using Elasticdump**

Elasticdump is a fantastic tool for the job in situations where we want to handle data transfer between Elasticsearch (ES) indices in general. We can export saved data from one ES server, acting as the source and output, directly to another, functioning as the destination, by transferring an input to an output.

Additionally, it enables us to export a collection of datasets from an ES index or cluster (together with the mappings) to a file in JSON format or even gzipped.

**Installation:**

**1) Install npm** -> Follow this [link ](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl)to install Node.js on Windows Subsystem for Linux (WSL2)

**2) Install Elasticdump** -> Since we're going to be using Elasticdump locally, we should first install it on our local computers. In this case, we can install it either per project or globally. We may use the following command to make the change globally:

`npm install elasticdump -g`

**Usage of Elasticdump**

`elasticdump --input SOURCE --output DESTINATION [OPTIONS]`

We have an input source and an output destination, as shown by the command above. To specify additional arguments required for the command to run, use the options property.

In addition, Elasticdump operates by transferring an input to an output, where the input or output may either be a file or an Elastic URL, or vice versa.

As usual, the format for an Elasticsearch URL is shown below: `{protocol}://{host}:{port}/{index}`

Alternatively, an example file format is shown below: `/Users/demo/Desktop/sample_file.json`

The data in our sample index may then be transferred to a file using the elastic dump tool in the following manner:

```
elasticdump \
    --input=http://localhost:9200/sample_index \
    --output=/Users/demo/Desktop/sample_file.json \
    --type=data
```

We are using the elasticdump command with the necessary option flags defining the —input and —output sources, as we can see from the command above. Additionally, we are defining the type using the —type options flag. The same command may be used to execute our mappings or schema as well:

```
elasticdump \
    --input=http://localhost:9200/sample_index \
    --output=/Users/retina/Desktop/sample_mapping.json \
    --type=mapping
```

**Transferring the data from directly one Instance to local Instance**

You can set your current namespace in your config file to quickly access the pods. (Completely Optional) `kubectl config set-context --current --namespace= YOUR_PROJECT_NAMESPACE`

**Step 1)**

Port Forward the kubernetes elastic search http service to local port: 9200 `kubectl port-forward svc/elasticsearch-es-http 9200:9200`

Port Forward the kubernetes Kibana http service to local port: 5601 `kubectl port-forward svc/kibana-kb-http 5601:5601`

**Step 2)**

Retrieve the password for the Kibana and ES instance using this command:

`kubectl get secret "elasticsearch-es-elastic-user" -o go-template='{{.data.elastic | base64decode }}'`

**Step 3)**

Open the browser and launch the local instances and login using the credentials (Username: elastic, Password: From Step 2)

**Step 4)**

Migrate the mapping of my_index

Use this [link](https://silverhammermba.github.io/password/) to encode your password while accessing the data via HTTPS protocol

elasticdump \
\--input=https://username:ASCII_ENCODED_Password@daaas-system-elastic.aaw.cloud.statcan.ca/my_index \
\--output=[http://elastic:PASSWORD_FROM_SETP_2@localhost:9200/my_index](http://elastic:PASSWORD_FROM_SETP_2@localhost:9200/my_index) \
\--overwrite \
\--limit:500 \
\--type=mapping

Migrate the data of my_index elasticdump \
\--input=https://username:ASCII_ENCODED_Password@daaas-system-elastic.aaw.cloud.statcan.ca/my_index \
\--output=[http://elastic:PASSWORD_FROM_SETP_2@localhost:9200/my_index](http://elastic:PASSWORD_FROM_SETP_2@localhost:9200/my_index) \
\--overwrite \
\--limit:500 \
\--type=data

Here is an easy way to migrate all of your indices using a Script

```
#!/bin/bash
echo "[KILLING THE PROCESS ON LOCALHOST PORTS] 8081, 8082"

npx kill-port 8081
npx kill-port 8082

echo "[ALL PORTS KILLED] Start Port Forwarding Now!"

ES_FROM_PORT=8081
ENV_FROM=k8s-cancentral-01-development
ENV_FROM_PASSWORD=""

ES_TO_PORT=8082
ENV_TO=k8s-cancentral-01-production
ENV_TO_PASSWORD=""     

ES_INDICES=("sedar_prod_file_info" \
"sedar_prod_file_text" \
"sedar_prod_page_description" \
"sedar_prod_extracted_tables" \
"sedar_prod_other_issuer" \
"sedar_prod_other_filer" \
"sedar_prod_mutual_fund_group" \
"sedar_prod_mutual_fund_issuer" \
"sedar_prod_control_information" \
"sedar_prod_company_names")


expose_es_ports() {
    TARGET_ENV=$1
    EXPOSE_PORT=$2
    local -n ENV_PWD=$3
    kubectl config use-context $TARGET_ENV
    kubectl config set-context --current --namespace sedar
    kubectl port-forward svc/elasticsearch-es-http $EXPOSE_PORT:9200 &
    ENV_PWD=$(kubectl get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo)
    echo "[$TARGET_ENV Port Forwarded] running on port http://localhost:$EXPOSE_PORT"
    return 0
}


es_migration(){
    echo "[STARTING DATA TRANSFER]  $ENV_FROM($ES_FROM_PORT) ===> $ENV_TO($ES_TO_PORT)";
    for idx in ${ES_INDICES[@]}; 
    do 
    echo "[MAPPING TRANSFER STARTED FOR $idx] $ENV_FROM($ES_FROM_PORT) ===> $ENV_TO($ES_TO_PORT)";
    elasticdump \
    --input=http://elastic:$ENV_FROM_PASSWORD@localhost:$ES_FROM_PORT/$idx \
    --output=http://elastic:$ENV_TO_PASSWORD@localhost:$ES_TO_PORT/$idx \
    --type=mapping ; 
    echo "[MAPPING TRANSFER COMPLETE FOR $idx] $ENV_FROM($ES_FROM_PORT) ===> $ENV_TO($ES_TO_PORT)";
    echo "[DATA TRANSFER STARTED FOR $idx] $ENV_FROM($ES_FROM_PORT) ===> $ENV_TO($ES_TO_PORT)";
    elasticdump \
    --input=http://elastic:$ENV_FROM_PASSWORD@localhost:$ES_FROM_PORT/$idx \
    --output=http://elastic:$ENV_TO_PASSWORD@localhost:$ES_TO_PORT/$idx \
    --type=data;
    echo "[MAPPING TRANSFER COMPLETE FOR $idx] $ENV_FROM($ES_FROM_PORT) ===> $ENV_TO($ES_TO_PORT)";
    done
    return 0
}


expose_es_ports $ENV_FROM $ES_FROM_PORT ENV_FROM_PASSWORD
expose_es_ports $ENV_TO $ES_TO_PORT ENV_TO_PASSWORD
es_migration


```

**Reference this [link ](https://github.com/elasticsearch-dump/elasticsearch-dump) for further changes and documentation.**
