# SEDAR App &middot; [![Python 3.6](https://img.shields.io/badge/python-3.9-blue.svg)](https://www.python.org/downloads/release/python-390/) [![pipeline status](https://gitlab.com/dsd4/sedar/sedar_pipeline/badges/master/pipeline.svg)](https://gitlab.com/dsd4/sedar/sedar_pipeline/-/commits/master) 


## Overview

The SEDAR App takes its name after the  **S**ystem for **E**lectronic **D**ocument **A**nalysis and **R**etrieval file database. 

The SEDAR App automatically processes and extracts structured data from financial documents available in the SEDAR database. 

This repo contains two principal components of the SEDAR App:
 * **ETL Data Pipeline** 
     The SEDAR App pipeline inspects and process all valid SEDAR files inside the MinIO **\<inbox\>** folder placed by the SFTP CronJob. Files are extracted, transformed, and loaded to the MinIO and Elastic Search data storages.
     The SEDAR App pipeline is currently triggered manually (i.e., the frequency of execution depends on the clients, it could be montly or  after a SFTP pull action).

 * **Web Dashboard**
     A real-time user web interface that allows to query all previously proccessed by the SEDAR App pipeline. The displayed data is queried from the populated Elastic Search and MinIO storage instances. 

A component of the SEDAR App not part of this repository is the [**SEDAR SFTP CronJob**](https://gitlab.com/dsd4/sedar/SFTP-CronJob). 
For details and documentation about the STFP Cronjob refer [here](https://gitlab.com/dsd4/sedar/sedar_pipeline/-/wikis/SEDAR/OLD/SFTP-Live-Feed)

## Documentation

Our [documentation](https://dsd4.gitlab.io/sedar/sedar_pipeline/) is hosted in GitLab Pages. 

## Requirements

* Each component uses the same conda environment with  dependencies. 

    - **ETL Pipeline** (conda-sedar): environment.yml 
    - **Web Dashboard**(conda-sedar):  environment.yml 

## Running SEDAR App Pipeline

Create ```conda-sedar``` virtual environment for running this app with Python 3. 
Clone this repository, open your terminal/command prompt in the src folder and create an .env file

```bash
$conda env create -f environment.yml
$source activate conda-sedar
$cd src
$> .env
```

The .env file inside the ```src/``` folder and provide the MinIO and Elastic Search credentials
associated with running the ETL pipeline. For more details read [here](https://gitlab.com/dsd4/sedar/sedar_pipeline/-/wikis/Config-Module)


```bash
MINIO_URL=<url_to_minio>
MINIO_ACCESS_KEY=<access_key> 
MINIO_SECRET_KEY=<secret_key>
ES_HOST=<es_host>
ES_USER=<es_user>
ES_PASS=<es_pass>
```

Execute the SEDAR App pipeline 

```bash
$./sedar_pipeline.py
```


## SEDAR App CLI Arguments

The SEDAR App CLI have multiple optional arguments. The full list of arguments are:

| Flag | Case | Example |
|------|------|---------|
| -f,<br /> --force |  Zip file exist in historical and also in the inbox folder. If flag is set, then zip file in inbox is processed and replaces the one in historical else it is deleted from inbox |  ./sedar_pipeline.py -f |
| -k,<br /> --keep_inbox |  Keep the zip files in the inbox folder after processing them. Default value is False |  ./sedar_pipeline.py -c |
| -r,<br /> --clean_data |  Boolean flag to remove data from elastic search that is older than **y** number of years which can be mentioned with the -y / --years flag. When -y is not provided a default value of 3 years is set | ./sedar_pipeline.py -r |
| -y,<br /> --years |  Integer flag to provide the number of years from current date that the older records from elastic search should be removed. |  ./sedar_pipeline.py -r -y 3 |

<br/>

## Running the SEDAR Webapp
Create a ```conda-sedar``` virtual environment for running this app with Python 3. 
Clone this repository, open your terminal/command prompt in the project root folder folder .

```bash
$conda env create -f environment.yml
$source activate conda-sedar
$cd src/ 
```

Once you have created environment. You have to go to create a .env file insite src/webapp directory

```bash
WEBAPP_DEBUG_MODE="True"
WEBAPP_PORT=8990
WEBAPP_SECRET_KEY= Generate a secret key 
WEBAPP_BASE_DOMAIN="kubeflow.aaw.cloud.statcan.ca"
MINIO_URL="http://minio-gateway.minio-gateway-standard-system:9000"
MINIO_ACCESS_KEY="access_key"
MINIO_SECRET_KEY="secret_key"
ES_HOST="http://daaas-es-http.shared-daaas-system:9200"
ES_USER="user"
ES_PASS="pass"
```
How to generate a secret key (Run this command in a console)
```bash
$python -c "from django.core.management.utils import get_random_secret_key; print(get_random_secret_key());" 
```

Finally after you change these settings you can run the app with typing this command in terminal
```bash
$ ./run_webapp.sh
```

Copyright &copy; 2022, Data Science Division - Statistics Canada
